Attribute Writer Dropdown Widget
**********************************

The widget enables the user to write predefined values to an attribute. You should use this widget when you want to limit the values that can be written to an attribute in run mode. 

Widget setting 
===============

The user has the ability to:

- Add title to the submit button
- Add title to dropdown
- Add write values
- Show device name (boolean)
- Choose attribute display type (Label, Name or None)
- Set text color (string)
- Set background color (string)
- Set text size (number)
- Choose font type (Helvetica or Monospaced )
- Apply CSS to dropdown title
- Apply CSS to submit button
- Apply CSS to the widget container

\ |IMG1|\ 

Widget design 
===============

\ |IMG2|\ 

*Notice: The widget accepts only input of type string, number, boolean and DevEnum.*

Good to know about this widget:

- If attribute has different type than the supported types the **{attribute} is not supported** shows up.
- If the write value has wrong type the **Invalid type for value** message will show up on the option for that value and the option is disabled.
- For attribute of type boolean the write values has to be "true" or "false", other value than that is classified as invalid type.
- The label of a write value is not required. When the label is empty the write value will show up instead.
- Hovering over an option to see write value (In case the write value has label).

.. bottom of content

.. |IMG1| image:: _static/img/attribute_writer_setting.png
   :height: 760 px
   :width: 454 px

.. |IMG2| image:: _static/img/attribute_writer_output.png
   :height: 147 px
   :width: 422 px
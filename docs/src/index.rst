
****
Home
****

.. toctree::
  :maxdepth: 1
  :caption: Home
  :hidden:

.. include::    introduction.rst


.. toctree::
    :maxdepth: 1
    :caption: User guide 

    what_is_it
    install_howto
    howto_contribute
    architecture_overview
    usage_howto
    widgets
    deprecated
    configuration


For developers
================


Usage of the source repository
------------------------------

1. Clone the repository.

2. Run 

.. code-block:: bash

 npm install


3. Type 

.. code-block:: bash

 npm start


Minimum node version: 7.6 (introduced async/wait)

Verified working node version: 9.11.2 (currently used by the dockerfile)

Docker >= v18 and GNU Make must be installed.


.. toctree::
    :caption: Architectural view
    
    architecture_overview
    architecture
    taranta_suite_candc
    taranta_suite_module_view
    charts



.. toctree::
    :caption: How to extend Taranta

    writing_a_widget


.. toctree::
   :caption: External resources:

    TangoGQL <https://web-maxiv-tangogql.readthedocs.io/en/latest/index.html>
    Taranta authorization <https://webjive-auth.readthedocs.io/en/latest/>
    Taranta Dashboard <https://webjive-dashboards.readthedocs.io/en/latest/>

Redux state structure
---------------------

.. toctree::
    :caption: State structure

    redux_state

Authors
=======

Taranta was initially written by the KITS Group at MAX IV Laboratory. 
Since 2018 also SKA Teams are developing it.


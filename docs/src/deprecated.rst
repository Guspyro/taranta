Deprecated widgets
******************

There are some deprecated taranta widgets that should be replaced before version 1.4.0

In version 1.3.2 the deprecated widgets are:

- "COMMAND_EXECUTOR"
- "COMMAND_WRITER"


Example process of replacing the deprecated widgets:
====================================================
|IMG1|


.. |IMG1| image:: _static/img/deprecategif.gif
   :height: 270 px
   :width: 480 px


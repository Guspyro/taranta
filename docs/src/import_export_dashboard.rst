Import/Export Dashboard
***********************

Taranta is able to import/export existing dashboards into/from a file.
The feature is extremely useful when an operator uses different environments or Taranta installation, such as integration and production server, and she/he needs to move the dashboards between the installations.

Import a dashboard
==================

To import a dashboard into the repository, the user should be logged. The import button is available in the right pane of the Dashboard section, clicking the Dashboards tab (the second one). 
In the Current Dashboard section, there is an import button. On click the button a window explorer opens and it is possible to select the file to import. It is possible to import one file per time. 

\ |IMG1|\ 

If the import process succeeded, a new dashboard is created in the repository and automatically opened. If case of error during the import, an error message appears on the top menu, explaining the errors occurred. 

*Import button.*

JSON Schema
===========

This is the JSON schema of the dashboard exported from the MongoDB. 

.. code-block:: JSON
  
{
  "id": string,
  "name": string,
  "version": string,
  "user": string,
  "insertTime": datetime,
  "updateTime": datetime,
  "group": boolean,
  "groupWriteAccess": boolean,
  "lastUpdatedBy": string,
  "widget": [
    {
      "_id": string,
      "id": int,
      "x": int,
      "y": int,
      "canvas": int,
      "width": int,
      "height": int,
      "type": string,
      "inputs": {
        [...]
      },
      "order": int
    }
  ]
}

.. code-block:: JSON

``id`` is the primary key of the dashboard. Consider that during the import process, the id of the dashboard does not will be updated, but the procedure will create a new dashboard. 
``name`` the name of the dashboard. The procedure will create a new dashboard also if in the repository exist a dashboard with the same name. 
``version`` the Taranta version that the dashboard refers to. 
``user`` the username of the owner of the dashboard. During the import process, ``user`` will be updated using the current user who is importing the dashboard.
``insertTime`` and ``insertTime`` are the timestamp of the creation and the last update of the dashboard. During the import process, these data will be updated using the current timestamp. 
``group`` the name of the group that shared the dashboard. During the import process this value will be set on null. 
``groupWriteAccess`` explain if the group has the permission to write the dashboard. During the import process this value will be set on false.
``lastUpdatedBy`` is the username of the user who made the last edit. During the import process, this value will be set on null. 
``widget`` is an array containing all the widgets of the dashboard. 
``widget`` ``_id`` is the primary key of the database. Will be update during the import process
``widget`` ``id`` is the id of the instance of the widget
``widget`` ``x`` represents the horizontal position of the widget in the screen
``widget`` ``y`` represents the vertical position of the widget in the screen
``widget`` ``canvas`` represents the canvas
``widget`` ``width`` represents the width of the widget
``widget`` ``height`` represents the height of the widget
``widget`` ``type`` represents the type of the widget. The types accepted are defined in the following file: /dashboard/widgets/index.ts and can change with the version. 
``widget`` ``order`` represents the order of the widget
``widget`` ``inputs`` is an object containing all the inputs defined in the widget. The list of input changes per every widget.

Validation
==========

During the import process, the procedure validates the JSON file before import it into the database. It performs two types of validation:

- syntactic validation
- formal validation

**Syntactic validation** checks if the file uploaded is a valid JSON file. 
**Formal validation** checks if the file imported respect the JSON schema and also check if the mandatory fields are in place. Formal validation also checks if the widget imported are in the current Taranta version, and the input definition as well. 

In case of error, the exceptions raised during the validation process will be shown in the top bar menu, as the following screenshot. 

\ |IMG3|\ 

Export a dashboard
==================

To export a dashboard in a file, the user should be logged. The export button is available in the right pane of the Dashboard section, clicking the Dashboards tab (the second one). 
In the My Dashboards section, in the list of the available dashboard, there is a download button on every dashboard row. On click the button, the download procedure starts, using the browser preferences. 

\ |IMG2|\ 

*Export button*

File extension
==============
The file produced to the export procedure is a JSON file and uses the .wj extension. 

Version
=======
Import/export button is available on Taranta from version 1.0.1



.. bottom of content

.. |IMG1| image:: _static/img/import_button.png

.. |IMG2| image:: _static/img/export_button.png

.. |IMG3| image:: _static/img/import_error.png
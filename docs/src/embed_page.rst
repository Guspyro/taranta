Embed Page Widget
*********************

A widget that lets the user embed, embeddable webpages. This widget is completely independent of tange devices and is a stand-alone widget for porting externally developed web-tools to bring controls and monitoring on one platform.

Widget setting 
==============
The only input this widget requires from the user is the URL of a webpage that is accessible and allows itself to be embedded.

\ |IMG1|\ 

*The textbox requires the URL of an accessible webpage. By default 'http' is prepended to the URL before the webpage is loaded*


Widget design
==============
The URL provided by the user is embedded in an ``iframe`` tag and then rendered. The only pre-requisite for this embedding to work is that the web-page that the user supplies, allows being embedded.


\ |IMG2|\ 

*The URL provided in the settings bar is rendered and is fully interactive.*

.. bottom of content

.. |IMG1| image:: _static/img/embed_page_settings.png

.. |IMG2| image:: _static/img/embed_page_design.png
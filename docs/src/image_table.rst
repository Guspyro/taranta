Image table widget
******************

A widget that displays tango image as a table

\ |IMG1|\ 

Widget setting 
==============
When using the widget the user has a couple of options to configure it:

\ |IMG2|\ 

+-----------------------------------------------------------------------------------------------------------+
|Select the device and attribute to use, it accepts all types of tango images                               |
+-----------------------------------------------------------------------------------------------------------+
|Users can select the limit of rows and/or columns to display (pre-defined as 5)                            |
+-----------------------------------------------------------------------------------------------------------+
|Users can select the offset on rows and/or columns to display (pre-defined as 0)                           |
+-----------------------------------------------------------------------------------------------------------+
|User can select if wants the device name to appear on the widget or not (Show Device)                      |
+-----------------------------------------------------------------------------------------------------------+
|Users can select if wants the attribute name to appear on the widget or not (Show Attribute)               |
+-----------------------------------------------------------------------------------------------------------+
|Users can select if wants the index of X and Y to appear on the widget or not (Show Index)                 |
+-----------------------------------------------------------------------------------------------------------+
|Users can select if wants the labels of the index (X and Y) to appear on the widget or not (Show Labels)   |
+-----------------------------------------------------------------------------------------------------------+
|Users can select if wants a warning to be displayed in case the image size changes (Show Warning)          |
+-----------------------------------------------------------------------------------------------------------+
|Users can select the font size to be used on the widget (Font size px)                                     |
+-----------------------------------------------------------------------------------------------------------+

Widget in runMode
=================

\ |IMG3|\ 


.. bottom of content

.. |IMG1| image:: _static/img/image_table_widget.png
   :height: 174 px
   :width: 131 px

.. |IMG2| image:: _static/img/image_table_config.png
   :height: 579 px
   :width: 292 px

.. |IMG3| image:: _static/img/image_table_use.png
   :height: 242 px
   :width: 303 px


History of changes to Taranta (former Webjive)
**********************************************

The current version is |version|.

Latest changes on |version|:
    - fixed empty box widget bug
    - added possibility to run dashboards also with missing dashboard variables
    - added format in the Attribute Display widget

Previous versions
=====================

* version 2.4.0

    - Add loading spinner
    - Fix Timeline widget bug
    - Update history of changes

* version 2.3.9
    - Refactor remaining widgets
    - Add boolean display widget custom CSS and alignment
    - Refactor AttributeHeatMap widget
    - Refactor ImageDisplay widget
    - Refactor ImageTable widget
    - Refactor AttributeLogger widget
    - Add custom CSS to boolean display
    - Refactor Elasticsearch widget

Previous versions
=====================
* version 2.3.8
    - Refactor Device Status widget
    - Refactor AttributeScatter
    - Refactor Spectrum2D widget
    - Refactor SpectrumPlot widget
    - Refactor AttributeDial widget
    - Refactor AttributePlot widget
    - Fix timeline widget bug
    - Refactor Attribute Logger
    - Fix new/edit/delete dashboard bug
    - Fix all warnings on npm start
    - Refactor commandSwitch

* version 2.3.7
    - Refactor AttributeWriter Widget
    - Refactor Parametric widget
    - Refactor AttributeLed
    - 
    - Update docs & widget image

* version 2.3.6
    - Fix ESLint rule import/no-anonymous-default-export (discourages default exports of anonymous functions)

* version 2.3.5
    - Fix AttributeLedTest error
    - Remove CommandExecuter, CommandWriter and Multiple CommandExecuter
    - Refactor booleanDisplay widget

* version 2.3.1
    - Merged refactor widget into react 17
    - fix notifications bug

* version 2.3.0
    - Updated Elasticsearch widget to work with key auth on the STFC
    - Fix AttributeLedTest error

* version 2.2.4
    - Refactor CommandFileWidget
    - Add shortDescription to widget definition to allow tooltip on library
    - Add div protection to disable user interaction with widgets on library

* version 2.2.3
    - Refactor Spectrum Table widget

* version 2.2.2
    - Refactor Attribute Display widget
    - Refactor Tabular widget

* version 2.2.1
    - Dashboard refactoring
    - Websocket middleware subscription on dashboards
    - Get values from state on dashboards
    - Improve notifications control
    - Remove RunCanvas, handle new frame and recordAttribute function (set on redux state now)
    - Refactor attribute display widget to work with store
    - Improve test and coverage
    - Add tooltip to run button to see running since X time

* version 2.2.0

    - Updated Elasticsearch widget to work with key auth on the STFC

* version 2.1.0

    - Fixed copy-paste order bug
    - Fixed paramentric widget tangoDB name bug
    - Fixed tabular widget minor bug
    - Improved test for label and simple motor widget
    - Updated cotribution section documentation
    - Added Tabular widget

* version 2.0.1
    - Fixes variable not available on the input selector if deleted and created a new one
    - Fixes history undefined on the timeline widget

* version 2.0.0
    - Introduction of websocket as a middleware
    - Taranta redux state documentation
    - Taranta architecture docs update
    - Refactor of Devices tab with improvements on selective rendering

* version 1.3.12
    - Fixes not working Bar Chart plot style option

* version 1.3.11
    - Fix additional slashes in command widget

* version 1.3.10
    - add the option to display attribute quality to Attribute Display widget

* version 1.3.9
    - additional configuration options added to the Spectrum2D widget: 
      - scientific notation format for both axis,
      - logarithmic scale for both axis,
      - linear / bar / point /lienear+point indicators.

* version 1.3.8
    - fixed Attribute Dropdown Writer widget bug

* version 1.3.7
    - Added the possibility to customize the CSS from the Inspector panel to Attribute Display, Attribute Writer, Attribute dial, Boolean display, Device status, Attribute LED, Spectrum table, and Elasticsearch widgets.
    - Fixed Undo-Redo actions using the Box Widget
    - Fixed the status in the Jive part. In the previous version, the status near the device title didn't reflect the current status.
    - Enabled attribute writer to work with enums
    - Improved test coverage
    - Improved doc syntax in some documents

* version 1.3.6
    - Added image display widget
    - Added image table widget
    - Added history limit configuration for each widget
    - fixed box widget undo and redo actions
    - fixed attribute_heatmap issue

* version 1.3.5
    - Fixed bug related to auto-load variable when a dashboard is open using a link
    - Fixed Box widget bugs
    - Added string stype in the Spectrum table widget
    - Update architecture schemas in the docs

* version 1.3.4
    - Add elastic search widget

* version 1.3.3

    - Fix cookie login issues

* version 1.3.2

    - Renamed Command Array in Command
    - Added dropdown menu with argument parameters in the Command widget
    - Fixed bug related to Display output in the Command Widget
    - This version adds deprecated widgets "COMMAND_EXECUTOR" and "COMMAND_WRITER"
    - Fixed bug related to device name special characters Issue #36

* version 1.3.1

    - Fixed bug related to DevVarStringArray argument in Command Array Widget
    - Fixed bug related to getAllTangoClassDevices function

* version 1.3.0

- Renamed in Taranta
    - Added Box Widget

* version 1.2.2

* version 1.2.1

* version 1.2.0

* version 1.1.6:

    - Add MacroButton widget
    - Update Attribute Writer Dropdown widget to accept string spectrum as dropdown content.

* version 1.1.5:
    - Improve validation on Attribute Writer Widget
    - Improve UI of Attribute Writer widget and allow user to add custom css
    - Webjive is able to upload a dashboard even when a widget has been removed from the available ones

* version 1.1.4:

    - Fix the maximum height of canvas. user should be able to select, drop, delete, drag widget in inactive(white) area

* version 1.1.3:

    - Multiple swim lanes in timeline widget fixed issue with pause data update
    - Improve adding new device in all widgets using complex input(+) ex. Timeline, Attribute Plot, Spectrum2D etc

* version 1.1.2:

    - Notify user about logout on expiry login token
    - Added documentation related to old widgets
    - Draggable modal bug fixed. Now the users can drag all the modal dialogs
    - Improved notification area. Now the users can open the old notifications without closing the unread ones

* version 1.1.1: 

    - Fixed device selection issue on ``Device Status`` widget, also added documentation for widget


* version 1.1.0:

    - There is a mechanism for configuring a dashboard and specifying a Tango device class for the variable, as well a variable name and a default device instance for the variable
    - The device inspector allows specification of the variable name and selection of attributes of its instances
    - There is a new widget that can be used in a running dashboard to display the variable name and a dropdown menu to select an instance of the device class associated to the variable
    - All widgets are extended to cope with dashboard variables
    - Import / export dashboard variables along with dashboards
    - When importing a dashboard, Taranta checks that the device class and default device exist, and if not warns the user and provides whatever info is needed and possible for the user to amend the dashboard
    - Run time functionality for variable selector widget
    - Fix tabs navigation on taranta

* version 1.0.9:

* version 1.0.8:

    - Add timeline widget
    - Add devState to timeline widget
    - Fix dashboard load warnings
    - Fix notification load warnings
    - Fix input field concatenation warnings


* version 1.0.7:

    - Fix boolean command on devices
    - Add hover datatype display on devices
    - Fix command output display on devices
    - Add JSON command output display on devices
    - Fix output dashboard widget to unique widget display
    - Add command array widget max output display size
    - Add Modal output display to command array widget
    - Add JSON display to command array widget
    - Add timestamp build on version hover
    - Remove cooldown from command array widget
    - Add shared input control to command array widget
    - Add string type on attribute write
    - Add common input validation on attribute write
    - Add DevEnum attribute write capabilities
    - Add configuration variables to customize Webjive installation
    - Add the ability to upload a file as an argument of a DevString Command
    - Fix Led Widget to size properly the led at minimum when the MIN_WIDGET_SIZE value is 10

* version 1.0.6:

    - Added JSON display on dashboard attribute display widget
    - Added type of attribute on dashboards attribute selection
    - Added state persistency on navigation between dashboard and devices
    - Added draggable modals pop-ups
    - Added Multi-tenant charts to deploy Skampi 
    - Added Spectrum 2D widget
    - Added Embed page widget
    - Added Four channel scope widget
    - Refactor notification area 

* version 1.0.5:

    - Added new notification area
    - Fix void bug on switch widget
    - Added None display on attribute boolean display widget
    - Improved AttributeDisplay visualization
    - Improved SpectrumTable visualization
    - Refactor Documentation
    - Improve testing on widgets
    - Added Drag and drop on dashboard layer 

* version 1.0.4:

    - Added commandFile widget
    - Added k8s charts 
    - Fixed topBar display on smaller windows
    - Add Umbrella charts to Taranta
    - Updated react-boostrap
    - Improve testing on widgets

* WIP
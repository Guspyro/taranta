Label Widget
************

A widget that displays (mostly) static content.

Widget setting 
==============
The label widget is perhaps the most basic widgets in Taranta, but at the same time it's very flexible and rich in features.
In its simplest form, it just displays static text, but because of it's hyperlink feature it can also be a navigational tool between different dashboards or external resources. When automaticResize is set to 'Enabled', the label will allow the user to resize the label by dragging the bottom right corner of the widget. This is useful when the label contains a lot of text. Additionaly, it enables the creation of well structured and visually appealing dashboards.

Most of the inputs to the label are self-explanatory, such as ``Text`` (the widgets text content) or the text and background color. When applicable, the ``(in unit)`` size is a relative size, where 1 refers to the default font size. This means setting the text size to 1.5 would increase the size by 50%.
The ``Link to`` input can be used to render the label text as a hyperlink, which when clicked opens up the target in a new browser tab. When automaticResize is set to 'Enabled', the font size option will be disabled, as it will be user defined.

Finally, the advanced settings ``Custom css`` can be used to apply styling not covered by the other settings. As the placeholder explains, this css is applied to the ``div`` element containing the widget, and each css directive needs to be added in a separate line. Generally, the parser tries to be as forgiving as possible in terms of e.g. missing semicolor. It also prints each line that it failed to parse, and gives immediate feedback on lines it did manage to parse by applying the css to the widget directly.


\ |IMG1|\ 

*The settings of an label widget utilizing the advanced css feature.*

\ |IMG2|\ 

*The settings of an label widget when automaticResize is enabled*

Widget design
=============
The below picture shows a number of labels with different settings applied to them. The bottom right widget is using the custom css seen in the previous screenshot.


\ |IMG3|\ 

*Various Taranta labels, with the (invisible) cursor hovering over the Link label*

.. bottom of content

.. |IMG1| image:: _static/img/label_settings.png

.. |IMG2| image:: _static/img/label_settings_2.png
    
.. |IMG3| image:: _static/img/label_design.png


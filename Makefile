build-docs:
	docker run --rm -d -v $(PWD):/tmp -w /tmp/docs netresearch/sphinx-buildbox sh -c "make html"

search-string:
	grep -rnw '.' --exclude-dir=node_modules --exclude-dir=docs --exclude-dir=build -e '$(STR)'
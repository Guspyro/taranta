import React from "react";
import { render } from "react-dom";
import { Route, BrowserRouter, Switch } from "react-router-dom";
import { Provider } from "react-redux";
import configureStore from "./shared/state/store/configureStore";
import JiveApp from "./jive/App";
import DashboardApp from "./dashboard/App";
import "bootstrap/dist/css/bootstrap.min.css";

import config from "./config.json"
const store = configureStore();
render(
  <Provider store={store}>
    <BrowserRouter basename={config.basename}>
      <Switch>
        <Route path={"/:tangoDB/dashboard"} component={DashboardApp} />
        <Route path={"/"} component={JiveApp} />
      </Switch>
    </BrowserRouter>
  </Provider>,
  document.getElementById("root")
);

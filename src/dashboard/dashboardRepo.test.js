import {
    save,
    importDash,
    renameDashboard,
    shareDashboard,
    cloneDashboard,
    deleteDashboard,
    loadUserDashboards,
    getGroupDashboards,
    getGroupDashboardCount,
    load,
    exportDash,
    getTangoDB
} from './dashboardRepo';
import config from "../config.json"

const exampleWidget = {
    "_id": "609d1b9161a7cb00170d7027",
    "id": "1",
    "x": 27,
    "y": 5,
    "canvas": "0",
    "width": 18,
    "height": 3,
    "type": "ATTRIBUTE_DISPLAY",
    "inputs": {
        "attribute": {
            "device": "MyVar55",
            "attribute": "randomattr",
            "label": "RandomAttr"
        },
        "precision": 2,
        "showDevice": true,
        "showAttribute": "Label",
        "scientificNotation": false,
        "showEnumLabels": false,
        "textColor": "#000000",
        "backgroundColor": "#ffffff",
        "size": 1,
        "font": "Helvetica"
    },
    "order": 0,
    "valid": true
};

const exampleVariables = {
    "id": "940591kh5f5n",
    "name": "MyVar1",
    "class": "tarantaTestDevice",
    "device": "test/tarantatestdevice/1"
};

const jsonDashFileExample = {
    "id": "6092b53fd758430012abd91e",
    "name": "parametricWithVars",
    "version": "1.1.0",
    "user": "CREAM",
    "insertTime": "2021-05-05T15:09:51.032Z",
    "updateTime": "2021-05-11T13:52:12.113Z",
    "group": null,
    "groupWriteAccess": false,
    "lastUpdatedBy": "CREAM",
    "widget": [
        exampleWidget
    ],
    "variables": [
        exampleVariables
    ]
};

test('the save happy path', async () => {

    global.fetch = jest.fn(() =>
        Promise.resolve({
            ok: true,
            status: 200,
            json: () => Promise.resolve({ id: '123', created: true }),
        })
    );
    save('123', [exampleWidget], 'dash1', [exampleVariables]);
});

test('the importDash version 1.1.0 happy path', async () => {

    var blob = new Blob([JSON.stringify(jsonDashFileExample)], { type: "plain/text" });
    var file = new File([blob], "dashboard.wj", {
        type: "text/plain",
        lastModified: Date.now(),
      });

    global.fetch = jest.fn(() =>
      Promise.resolve({
          ok: true,
          status: 200,
          json: () => Promise.resolve({ id: '123', created: true }),
      })
    );

    await importDash(file);
});

test('the importDash version 1.0.4 happy path', async () => {

    const jsonDashFileExample2 = {
        "id": "6092b53fd758430012abd91e",
        "name": "parametricWithVars",
        "version": "1.0.4",
        "user": "CREAM",
        "insertTime": "2021-05-05T15:09:51.032Z",
        "updateTime": "2021-05-11T13:52:12.113Z",
        "group": null,
        "groupWriteAccess": false,
        "lastUpdatedBy": "CREAM",
        "widget": [
            exampleWidget
        ],
        "variables": [
            exampleVariables
        ]
    };

    var blob = new Blob([JSON.stringify(jsonDashFileExample2)], { type: "plain/text" });
    var file = new File([blob], "dashboard.wj", {
        type: "text/plain",
        lastModified: Date.now(),
      });

    global.fetch = jest.fn(() =>
      Promise.resolve({
          ok: true,
          status: 200,
          json: () => Promise.resolve({ id: '123', created: true }),
      })
    );

    await importDash(file);
});

test('the importDash empty file', async () => {

    var blob = new Blob([JSON.stringify({})], { type: "plain/text" });
    var file = new File([blob], "dashboard.wj", {
        type: "text/plain",
        lastModified: Date.now(),
      });

    global.fetch = jest.fn(() =>
      Promise.resolve({
          ok: true,
          status: 200,
          json: () => Promise.resolve({ }),
      })
    );

    await importDash(file);
});

test('the rename happy path', async () => {

    global.fetch = jest.fn(() =>
        Promise.resolve({
            ok: true,
            status: 200,
            json: () => Promise.resolve({ id: '123', created: true }),
        })
    );
    renameDashboard('123', 'dash1');
});

test('the shareDashboard happy path', async () => {

    global.fetch = jest.fn(() =>
        Promise.resolve({
            ok: true,
            status: 200,
            json: () => Promise.resolve({ id: '123', created: true }),
        })
    );
    shareDashboard('123', 'group1', true);
});

test('the cloneDashboard happy path', async () => {

    global.fetch = jest.fn(() =>
        Promise.resolve({
            ok: true,
            status: 200,
            json: () => Promise.resolve({ id: '123', created: true }),
        })
    );
    cloneDashboard('123');
});

test('the deleteDashboard happy path', async () => {

    global.fetch = jest.fn(() =>
        Promise.resolve({
            ok: true,
            status: 200,
            json: () => Promise.resolve({ id: '123', created: true }),
        })
    );
    deleteDashboard('123');
});

test('the loadUserDashboards happy path', async () => {

    global.fetch = jest.fn(() =>
        Promise.resolve({
            ok: true,
            status: 200,
            json: () => Promise.resolve({ id: '123', created: true }),
        })
    );
    loadUserDashboards();
});

test('the getGroupDashboards happy path', async () => {

    global.fetch = jest.fn(() =>
        Promise.resolve({
            ok: true,
            status: 200,
            json: () => Promise.resolve({ id: '123', created: true }),
        })
    );
    getGroupDashboards('Group1');
});

test('the getGroupDashboardCount happy path', async () => {

    global.fetch = jest.fn(() =>
        Promise.resolve({
            ok: true,
            status: 200,
            json: () => Promise.resolve({ id: '123', created: true }),
        })
    );
    getGroupDashboardCount();
});

test('the load happy path', async () => {

    const dashbExample = {
        "id": "609d2cbf61a7cb00170d702d",
        "name": "parametricWithVars",
        "user": "CREAM",
        "insertTime": "2021-05-13T13:42:23.545Z",
        "updateTime": "2021-05-13T15:12:43.705Z",
        "widgets": [
            {
                "_id": "609d2cbf61a7cb00170d702e",
                "id": "1",
                "x": 23,
                "y": 6,
                "canvas": "0",
                "width": 18,
                "height": 3,
                "type": "ATTRIBUTE_DISPLAY",
                "inputs": {
                    "attribute": {
                        "device": "MyVar55",
                        "attribute": "randomattr",
                        "label": "RandomAttr"
                    },
                    "precision": 2,
                    "showDevice": true,
                    "showAttribute": "Label",
                    "scientificNotation": false,
                    "showEnumLabels": false,
                    "textColor": "#000000",
                    "backgroundColor": "#ffffff",
                    "size": 1,
                    "font": "Helvetica"
                },
                "order": 0
            },
            {
                "_id": "609d41eb61a7cb00170d7033",
                "id": "2",
                "x": 23,
                "y": 9,
                "canvas": "0",
                "width": 18,
                "height": 3,
                "type": "ATTRIBUTE_DISPLAY",
                "inputs": {
                    "attribute": {
                        "device": "MyVar55",
                        "attribute": "randomattr",
                        "label": "RandomAttr"
                    },
                    "precision": 2,
                    "showDevice": true,
                    "showAttribute": "Label",
                    "scientificNotation": false,
                    "showEnumLabels": false,
                    "textColor": "#000000",
                    "backgroundColor": "#ffffff",
                    "size": 1,
                    "font": "Helvetica"
                },
            }
        ],
        "variables": [
            {
                "id": "940591kh5f5n",
                "name": "MyVar1",
                "class": "tarantaTestDevice",
                "device": "test/tarantatestdevice/1"
            },
            {
                "id": "kn87e6faf3h",
                "name": "MyVar2",
                "class": "tarantaTestDevice",
                "device": "test/tarantatestdevice/2"
            }
        ],
        "group": null,
        "groupWriteAccess": false,
        "lastUpdatedBy": "CREAM",
        "tangoDB": "testdb"
    };

    global.fetch = jest.fn(() =>
        Promise.resolve({
            ok: true,
            status: 200,
            json: () => Promise.resolve(dashbExample),
        })
    );
    load('123');
});


test('the exportDash happy path', async () => {

    const dashbExample = {
        "id": "609d2cbf61a7cb00170d702d",
        "name": "parametricWithVars",
        "user": "CREAM",
        "insertTime": "2021-05-13T13:42:23.545Z",
        "updateTime": "2021-05-13T15:12:43.705Z",
        "widgets": [
            {
                "_id": "609d2cbf61a7cb00170d702e",
                "id": "1",
                "x": 23,
                "y": 6,
                "canvas": "0",
                "width": 18,
                "height": 3,
                "type": "ATTRIBUTE_DISPLAY",
                "inputs": {
                    "attribute": {
                        "device": "MyVar55",
                        "attribute": "randomattr",
                        "label": "RandomAttr"
                    },
                    "precision": 2,
                    "showDevice": true,
                    "showAttribute": "Label",
                    "scientificNotation": false,
                    "showEnumLabels": false,
                    "textColor": "#000000",
                    "backgroundColor": "#ffffff",
                    "size": 1,
                    "font": "Helvetica"
                },
                "order": 0
            },
            {
                "_id": "609d41eb61a7cb00170d7033",
                "id": "2",
                "x": 23,
                "y": 9,
                "canvas": "0",
                "width": 18,
                "height": 3,
                "type": "ATTRIBUTE_DISPLAY",
                "inputs": {
                    "attribute": {
                        "device": "MyVar55",
                        "attribute": "randomattr",
                        "label": "RandomAttr"
                    },
                    "precision": 2,
                    "showDevice": true,
                    "showAttribute": "Label",
                    "scientificNotation": false,
                    "showEnumLabels": false,
                    "textColor": "#000000",
                    "backgroundColor": "#ffffff",
                    "size": 1,
                    "font": "Helvetica"
                },
                "order": 1
            }
        ],
        "variables": [
            {
                "id": "940591kh5f5n",
                "name": "MyVar1",
                "class": "tarantaTestDevice",
                "device": "test/tarantatestdevice/1"
            },
            {
                "id": "kn87e6faf3h",
                "name": "MyVar2",
                "class": "tarantaTestDevice",
                "device": "test/tarantatestdevice/2"
            }
        ],
        "group": null,
        "groupWriteAccess": false,
        "lastUpdatedBy": "CREAM",
        "tangoDB": "testdb"
    };

    global.fetch = jest.fn(() =>
        Promise.resolve({
            ok: true,
            status: 200,
            json: () => Promise.resolve(dashbExample),
        })
    );

    window.URL.createObjectURL = jest.fn()
    jest.mock('js-file-download', () => jest.fn(console.trace))
   
    exportDash('123');
});

test('the getTangoDB domain name', async () => {
    config.basename = 'tarantaname';
    //global.window = Object.create(window);
    const url = "http://localhost:3000/tarantaname/testdb/dashboard?id=60a35498fc21a20017125f8c";
    Object.defineProperty(window, 'location', {
        value: {
          href: url,
          pathname: '/tarantaname/testdb/dashboard'
        }
    });
    expect(getTangoDB()).toEqual('testdb');
});
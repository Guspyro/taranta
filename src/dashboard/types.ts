import React from "react";
import { NotificationLevel } from "../shared/notifications/notifications";

export interface BaseInputDefinition<T> {
  label?: string;
  repeat?: boolean;
  default?: T;
  required?: boolean;
  title?: string;
}

export interface Notification {
  level: NotificationLevel;
  sourceAction: string;
  msg: string;
}

export interface BooleanInputDefinition extends BaseInputDefinition<boolean> {
  type: "boolean";
}

export interface RadioInputDefinition extends BaseInputDefinition<boolean> {
  type: "radio";
}

export interface NumberInputDefinition extends BaseInputDefinition<number> {
  type: "number";
  nonNegative?: boolean;
  dependsOn?: string;
}

export interface StringInputDefinition extends BaseInputDefinition<string> {
  type: "string";
  renderAs?: "text" | "textarea";
  placeholder?: string;
  default?: string;
}
export interface StyleInputDefinition extends BaseInputDefinition<string> {
  type: "style";
}

export interface ComplexInputDefinition<T = any>
  extends BaseInputDefinition<null> {
  type: "complex";
  inputs: T;
}

interface SelectInputDefinitionOption<T> {
  name: string;
  label?: string;
  value: T; // ?
}

export interface SelectInputDefinition<T = string>
  extends BaseInputDefinition<string> {
  type: "select";
  options: SelectInputDefinitionOption<T>[];
}

export interface VariableInputDefinition<T = string>
  extends BaseInputDefinition<string> {
  type: "variable";
  options?: SelectInputDefinitionOption<T>[];
}

export interface DatePickerDefinition<T = string>
  extends BaseInputDefinition<string> {
  type: "datePicker";
  value?: Date | string;
  dateFormat?: string;
  timeFormat?: string;
  hideTime?: boolean;
  validate?: Function;
}

export interface AttributeInputDefinition
  extends BaseInputDefinition<{
    device: null;
    attribute: null;
    label: null;
  }> {
  type: "attribute";
  dataFormat?: "scalar" | "spectrum" | "image";
  dataType?: "numeric" | "enum" | "string";
  device?: string;
  attribute?: string;
  label?: string;
  invalidates?: string[];
}
export interface AttributeComplexInput {
  attribute: AttributeInputDefinition;
  yAxis: SelectInputDefinition<"left" | "right">;
  showAttribute: SelectInputDefinition;
  yAxisDisplay: SelectInputDefinition;
}
export interface ColorInputDefinition extends BaseInputDefinition<string> {
  type: "color";
}

export interface MultipleSelectionInputDefinition<T = string>
  extends BaseInputDefinition<null> {
  type: "multipleselection";
  value?: string;
  placeholder?: string;
  options?: SelectInputDefinitionOption<T>[];
}

export interface DeviceInputDefinition extends BaseInputDefinition<null> {
  type: "device";
  publish?: string;
}
export interface DeviceComplexInput {
  device: DeviceInputDefinition;
}
export interface CommandInputDefinition extends BaseInputDefinition<null> {
  type: "command";
  device?: string;
  command?: string;
  intype?: string;
  invalidates?: string[];
  parameter?: string;
  intypedesc?: string;
  outtypedesc?: string;
  outtype?: string;
  displevel?: string;
}

export interface CommandInputDefinitionMultiple
  extends BaseInputDefinition<null> {
  type: "command";
  device?: string;
  command?: Array<string>;
  intype?: string;
  invalidates?: string[];
  parameter?: string;
}

export interface CommandInputWithParameterDefinition
  extends BaseInputDefinition<null> {
  type: "command";
  device?: string;
  command?: string;
  intype?: string;
  invalidates?: string[];
  parameter?: string;
}

export type InputDefinition =
  | BooleanInputDefinition
  | RadioInputDefinition
  | NumberInputDefinition
  | StringInputDefinition
  | ComplexInputDefinition
  | AttributeInputDefinition
  | SelectInputDefinition
  | VariableInputDefinition
  | ColorInputDefinition
  | MultipleSelectionInputDefinition
  | DeviceInputDefinition
  | CommandInputDefinition
  | CommandInputDefinitionMultiple
  | StyleInputDefinition
  | DatePickerDefinition;

export interface InputDefinitionMapping {
  [name: string]: InputDefinition;
}

export interface Widget {
  _id?: string; //not really used, but automatically created by mongodb
  type: string;
  id: string;
  valid: number;
  canvas: string;
  x: number;
  y: number;
  width: number;
  height: number;
  inputs: InputMapping;
  innerWidgets?: Widget[];
  [percentage: string]: any;
  order: number;
}

//meta information about available group dashboards, with count and whether it has been loade for each group
export interface AvailableGroupDashboards {
  [group: string]: {
    count: number;
    loaded: boolean;
  };
}
export interface SharedDashboards {
  dashboards: Dashboard[];
  availableGroupDashboards: AvailableGroupDashboards;
}
export interface DashboardEditHistory {
  undoActions: Record<string, Widget>[];
  redoActions: Record<string, Widget>[];
  undoIndex: number;
  redoIndex: number;
  undoLength: number;
  redoLength: number;
}

export interface Dashboard {
  id: string;
  name: string;
  user: string;
  insertTime: Date | null;
  updateTime: Date | null;
  group: string | null;
  groupWriteAccess: boolean;
  lastUpdatedBy: string | null;
  selectedIds: string[];
  history: DashboardEditHistory;
  variables: Variable[];
}

export interface SelectedDashboard extends Dashboard {
  widgets: Record<string, Widget>;
  selectedIds: string[];
  history: DashboardEditHistory;
}

export interface Variable {
  _id?: string;
  name: string;
  class: string;
  device: string;
}

export interface InputMapping {
  [name: string]: any;
}

export interface WidgetDefinition<T extends InputDefinitionMapping> {
  type: string;
  name: string;
  shortDescription?: string;
  historyLimit?: number;
  defaultWidth: number;
  defaultHeight: number;
  inputs: T;
}

export interface WidgetBundle<T extends InputDefinitionMapping> {
  definition: WidgetDefinition<T>;
  component: React.ElementType;
}

export type IndexPath = Array<string | number>;

interface AttributeValue<ValueT> {
  value: ValueT;
  writeValue: ValueT;
  quality: string;
  timestamp: number;
}

export interface AttributeInput<ValueT = any> extends AttributeValue<ValueT> {
  device: string;
  attribute: string;
  label: string;
  history: Array<AttributeValue<ValueT>>;
  dataType: string;
  dataFormat: string;
  enumlabels: Array<string>;
  isNumeric: boolean;
  unit: string;
  write: (value: ValueT) => void;
  minAlarm?: number;
  maxAlarm?: number;
  minValue?: number;
  maxValue?: number;
}

export interface CommandInput<OutputT = any> {
  device: string;
  command: string;
  parameter?: string;
  dataType?: string;
  output: OutputT;
  execute: (argin?: any) => void;
}

export interface CommandInputMultiple<OutputT = any> {
  device: string;
  command: Array<string>;
  parameter?: string;
  dataType?: string;
  output: OutputT;
  execute: (argin?: any, command?: string) => void;
}

export interface DeviceInput {
  name: string;
  alias: string;
}
export interface StyleInput {
  [key: string]: any;
}

export interface CommandInputWithParameter<OutputT = any> {
  device: string;
  command: string;
  parameter?: string;
  dataType?: string;
  output: OutputT;
  execute: (value: OutputT) => void;
}

export type ComplexInput<T> = any;

export interface Canvas {
  id: string;
  name: string;
}

import React from "react";
import { AttributeInput } from "../../types";

import { configure, shallow } from "enzyme";
import Adapter from "@wojtekmaj/enzyme-adapter-react-17";
import AttributeLEDDisplay from "./AttributeLEDDisplay";

jest.mock("./AttributeLEDValues", () => () => (
  <div>Mock AttributeLEDDisplayValues</div>
));


interface Input {
  showAttributeValue: boolean;
  showAttribute: string;
  showDeviceName: boolean;
  compare: string;
  relation: string;
  attribute: AttributeInput;
  trueColor: string;
  falseColor: string;
  ledSize: number;
  textSize: number;
  customCss: string;
  alignTextCenter: boolean;
  alignValueRight: boolean;
}

configure({ adapter: new Adapter() });

describe("Attribute LED Display", () => {
  let myAttributeInput: AttributeInput;
  let myInput: Input;
  var writeArray: any = [];
  var date = new Date();
  var timestamp = date.getTime();

  it("hide the device name if showDeviceName is not set", () => {
    myAttributeInput = {
      device: "sys/tg_test/1",
      attribute: "short_scalar",
      label: "ShortScalar",
      history: [],
      dataType: "",
      dataFormat: "",
      isNumeric: true,
      unit: "",
      enumlabels: [],
      write: writeArray,
      value: 10,
      writeValue: "",
      timestamp: timestamp,
      quality: "VALID"
    };

    myInput = {
      showAttributeValue: true,
      showAttribute: "Name",
      showDeviceName: false,
      compare: "20",
      relation: "<",
      attribute: myAttributeInput,
      trueColor: "#ffffff",
      falseColor: "#000000",
      ledSize: 2,
      textSize: 2,
      alignTextCenter:false,
      alignValueRight:true,
      customCss: ""
    };

    const element = React.createElement(AttributeLEDDisplay.component, {
      mode: "run",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: myInput,
      id: 42
    });
    expect(shallow(element).html()).not.toContain("sys/tg_test/1");
  });

  it("show the device name if showDeviceName is set", () => {
    myAttributeInput = {
      device: "sys/tg_test/1",
      attribute: "short_scalar",
      label: "ShortScalar",
      history: [],
      dataType: "",
      dataFormat: "",
      isNumeric: true,
      unit: "",
      enumlabels: [],
      write: writeArray,
      value: 10,
      writeValue: "",
      timestamp: timestamp,
      quality: "VALID"
    };

    myInput = {
      showAttributeValue: true,
      showAttribute: "Name",
      showDeviceName: true,
      compare: "20",
      relation: "<",
      attribute: myAttributeInput,
      trueColor: "#ffffff",
      falseColor: "#000000",
      ledSize: 2,
      textSize: 2,
      alignTextCenter:false,
      alignValueRight:true,
      customCss: ""
    };

    const element = React.createElement(AttributeLEDDisplay.component, {
      mode: "run",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: myInput,
      id: 42
    });
    expect(shallow(element).html()).toContain("sys/tg_test/1");
  });

  it("hide the attribute name if showAttribute is not set", () => {
    myAttributeInput = {
      device: "sys/tg_test/1",
      attribute: "short_scalar",
      label: "ShortScalar",
      history: [],
      dataType: "",
      dataFormat: "",
      isNumeric: true,
      unit: "",
      enumlabels: [],
      write: writeArray,
      value: 10,
      writeValue: "",
      timestamp: timestamp,
      quality: "VALID"
    };

    myInput = {
      showAttributeValue: true,
      showAttribute: "",
      showDeviceName: false,
      compare: "20",
      relation: "<",
      attribute: myAttributeInput,
      trueColor: "#ffffff",
      falseColor: "#000000",
      ledSize: 2,
      textSize: 2,      
      alignTextCenter:false,
      alignValueRight:true,
      customCss: ""
    };

    const element = React.createElement(AttributeLEDDisplay.component, {
      mode: "run",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: myInput,
      id: 42
    });
    expect(shallow(element).html()).not.toContain("short_scalar");
  });

  it("show the attribute name if showAttribute is set", () => {
    myAttributeInput = {
      device: "sys/tg_test/1",
      attribute: "short_scalar",
      label: "ShortScalar",
      history: [],
      dataType: "",
      dataFormat: "",
      isNumeric: true,
      unit: "",
      enumlabels: [],
      write: writeArray,
      value: 10,
      writeValue: "",
      timestamp: timestamp,
      quality: "VALID"
    };

    myInput = {
      showAttributeValue: true,
      showAttribute: "Name",
      showDeviceName: true,
      compare: "20",
      relation: "<",
      attribute: myAttributeInput,
      trueColor: "#ffffff",
      falseColor: "#000000",
      ledSize: 2,
      textSize: 2,
      alignTextCenter:false,
      alignValueRight:true,
      customCss: ""
    };

    const element = React.createElement(AttributeLEDDisplay.component, {
      mode: "run",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: myInput,
      id: 42
    });
    expect(shallow(element).html()).toContain("short_scalar");
  });

  it("show the attribute lable if showAttribute is set", () => {
    myAttributeInput = {
      device: "sys/tg_test/1",
      attribute: "short_scalar",
      label: "ShortScalar",
      history: [],
      dataType: "",
      dataFormat: "",
      isNumeric: true,
      unit: "",
      enumlabels: [],
      write: writeArray,
      value: 10,
      writeValue: "",
      timestamp: timestamp,
      quality: "VALID"
    };

    myInput = {
      showAttributeValue: true,
      showAttribute: "Label",
      showDeviceName: true,
      compare: "20",
      relation: "<",
      attribute: myAttributeInput,
      trueColor: "#ffffff",
      falseColor: "#000000",
      ledSize: 2,
      textSize: 2,
      alignTextCenter:false,
      alignValueRight:true,
      customCss: ""
    };

    const element = React.createElement(AttributeLEDDisplay.component, {
      mode: "run",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: myInput,
      id: 42
    });
    expect(shallow(element).html()).toContain("ShortScalar");
  });

  it("hide the attribute value if showAttributeValue is not set", () => {
    myAttributeInput = {
      device: "sys/tg_test/1",
      attribute: "short_scalar",
      label: "ShortScalar",
      history: [],
      dataType: "",
      dataFormat: "",
      isNumeric: true,
      unit: "",
      enumlabels: [],
      write: writeArray,
      value: 1000000,
      writeValue: "",
      timestamp: timestamp,
      quality: "VALID"
    };

    myInput = {
      showAttributeValue: false,
      showAttribute: "",
      showDeviceName: false,
      compare: "20",
      relation: "<",
      attribute: myAttributeInput,
      trueColor: "#ffffff",
      falseColor: "#000000",
      ledSize: 2,
      textSize: 2,
      alignTextCenter:false,
      alignValueRight:true,
      customCss: ""
    };

    const element = React.createElement(AttributeLEDDisplay.component, {
      mode: "run",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: myInput,
      id: 42
    });
    expect(shallow(element).html()).not.toContain("1000000");
  });

  it("displays correct LED size", () => {
    myAttributeInput = {
      device: "sys/tg_test/1",
      attribute: "short_scalar",
      label: "ShortScalar",
      history: [],
      dataType: "",
      dataFormat: "",
      isNumeric: true,
      unit: "",
      enumlabels: [],
      write: writeArray,
      value: 10,
      writeValue: "",
      timestamp: timestamp,
      quality: "VALID"
    };

    myInput = {
      showAttributeValue: true,
      showAttribute: "Name",
      showDeviceName: true,
      compare: "20",
      relation: "<",
      attribute: myAttributeInput,
      trueColor: "#00ff00",
      falseColor: "#ff0000",
      ledSize: 10,
      textSize: 2,
      alignTextCenter:false,
      alignValueRight:true,
      customCss: ""
    };

    const element = React.createElement(AttributeLEDDisplay.component, {
      mode: "run",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: myInput,
      id: 42
    });
    expect(element.props.inputs.ledSize).toBe(10);
  });

  it("displays correct text size", () => {
    myAttributeInput = {
      device: "sys/tg_test/1",
      attribute: "short_scalar",
      label: "ShortScalar",
      history: [],
      dataType: "",
      dataFormat: "",
      isNumeric: true,
      unit: "",
      enumlabels: [],
      write: writeArray,
      value: 10,
      writeValue: "",
      timestamp: timestamp,
      quality: "VALID"
    };

    myInput = {
      showAttributeValue: true,
      showAttribute: "Name",
      showDeviceName: true,
      compare: "20",
      relation: "<",
      attribute: myAttributeInput,
      trueColor: "#00ff00",
      falseColor: "#ff0000",
      ledSize: 10,
      textSize: 2,
      alignTextCenter:false,
      alignValueRight:true,
      customCss: ""
    };

    const element = React.createElement(AttributeLEDDisplay.component, {
      mode: "run",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: myInput,
      id: 42
    });
    expect(shallow(element).children().first().get(0).props.style).toHaveProperty(
      'fontSize',
      '2em',
    );
  });

  it("align text on right", () => {
    myAttributeInput = {
      device: "sys/tg_test/1",
      attribute: "short_scalar",
      label: "ShortScalar",
      history: [],
      dataType: "",
      dataFormat: "",
      isNumeric: true,
      unit: "",
      enumlabels: [],
      write: writeArray,
      value: 10,
      writeValue: "",
      timestamp: timestamp,
      quality: "VALID"
    };

    myInput = {
      showAttributeValue: true,
      showAttribute: "Name",
      showDeviceName: true,
      compare: "20",
      relation: "<",
      attribute: myAttributeInput,
      trueColor: "#00ff00",
      falseColor: "#ff0000",
      ledSize: 10,
      textSize: 2,
      alignTextCenter:false,
      alignValueRight:true,
      customCss: ""
    };

    const element = React.createElement(AttributeLEDDisplay.component, {
      mode: "run",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: myInput,
      id: 42
    });
    expect(shallow(element).html()).toContain("float:right");
  });

  it("align text on left", () => {
    myAttributeInput = {
      device: "sys/tg_test/1",
      attribute: "short_scalar",
      label: "ShortScalar",
      history: [],
      dataType: "",
      dataFormat: "",
      isNumeric: true,
      unit: "",
      enumlabels: [],
      write: writeArray,
      value: 10,
      writeValue: "",
      timestamp: timestamp,
      quality: "VALID"
    };

    myInput = {
      showAttributeValue: true,
      showAttribute: "Name",
      showDeviceName: true,
      compare: "20",
      relation: "<",
      attribute: myAttributeInput,
      trueColor: "#00ff00",
      falseColor: "#ff0000",
      ledSize: 10,
      textSize: 2,
      alignTextCenter:false,
      alignValueRight:false,
      customCss: ""
    };

    const element = React.createElement(AttributeLEDDisplay.component, {
      mode: "run",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: myInput,
      id: 42
    });
    expect(shallow(element).html()).not.toContain("float:right;");
    expect(shallow(element).html()).not.toContain("text-align:center");
  });

  it("align text on right", () => {
    myAttributeInput = {
      device: "sys/tg_test/1",
      attribute: "short_scalar",
      label: "ShortScalar",
      history: [],
      dataType: "",
      dataFormat: "",
      isNumeric: true,
      unit: "",
      enumlabels: [],
      write: writeArray,
      value: 10,
      writeValue: "",
      timestamp: timestamp,
      quality: "VALID"
    };

    myInput = {
      showAttributeValue: true,
      showAttribute: "Name",
      showDeviceName: true,
      compare: "20",
      relation: "<",
      attribute: myAttributeInput,
      trueColor: "#00ff00",
      falseColor: "#ff0000",
      ledSize: 10,
      textSize: 2,
      alignTextCenter:true,
      alignValueRight:false,
      customCss: ""
    };

    const element = React.createElement(AttributeLEDDisplay.component, {
      mode: "run",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: myInput,
      id: 42
    });
    expect(shallow(element).html()).toContain("text-align:center");
  });
});

import React from "react";

import { AttributeInput } from "../../types";
import { act } from "react-dom/test-utils";

import { configure, shallow, mount } from "enzyme";
import Adapter from "@wojtekmaj/enzyme-adapter-react-17";
import AttributeWriter from "./AttributeWriter";
import { Provider } from "react-redux";
import configureStore from "../../../shared/state/store/configureStore";
import AttributeValues from "./AttributeValues";

type Input = {
  attribute: AttributeInput;
  showDevice: boolean;
  showAttribute: string;
  alignValueRight: boolean;
  textColor: string;
  backgroundColor: string;
  size: number;
  font: string;
  title: string;
  widgetCss: string;
};

configure({ adapter: new Adapter() });
const store = configureStore();

describe("AttributeWriter Display", () => {
  let myAttributeInput: AttributeInput;
  let myInput: Input;

  it("renders all true without crashing", () => {
    myAttributeInput = {
      device: "sys/tg_test/1",
      attribute: "boolean_scalar",
      dataType: "DevBoolean",
      dataFormat: "scalar",
      isNumeric: false,
      unit: "",
      enumlabels: [],
      write: () => {},
      value: true,
      writeValue: "",
      label: "",
      history: [],
      timestamp: 0,
      quality: "VALID",
    };

    myInput = {
      showDevice: true,
      attribute: myAttributeInput,
      size: 1,
      showAttribute: "name",
      alignValueRight: true,
      textColor: "red",
      backgroundColor: "blue",
      font: "mySelect",
      widgetCss: "",
      title: "",
    };

    const element = React.createElement(AttributeWriter.component, {
      mode: "run",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: myInput,
      id: 42,
    });
    let wrapper = mount(<Provider store={store}>{element}</Provider>);
    expect(wrapper.find("#AttributeWriter").exists()).toBe(true);
  });

  it("renders not showing device name", () => {
    myAttributeInput = {
      device: "sys/tg_test/1",
      attribute: "boolean_scalar",
      dataType: "DevBoolean",
      dataFormat: "scalar",
      isNumeric: false,
      unit: "",
      enumlabels: [],
      write: () => {},
      value: true,
      writeValue: "",
      label: "AttributeWriter",
      history: [],
      timestamp: 0,
      quality: "VALID",
    };

    myInput = {
      showDevice: false,
      attribute: myAttributeInput,
      size: 1,
      showAttribute: "Label",
      alignValueRight: true,
      textColor: "red",
      backgroundColor: "blue",
      font: "mySelect",
      widgetCss: "",
      title: "",
    };

    const element = React.createElement(AttributeWriter.component, {
      mode: "edit",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: myInput,
      id: 42,
    });
    let wrapper = mount(<Provider store={store}>{element}</Provider>);
    const componentWrapper = wrapper.find(AttributeValues);
    expect(componentWrapper.props().deviceName).toEqual("sys/tg_test/1");
  });

  it("renders showing device name", () => {
    myAttributeInput = {
      device: "sys/tg_test/1",
      attribute: "boolean_scalar",
      dataType: "DevBoolean",
      dataFormat: "scalar",
      isNumeric: false,
      unit: "",
      enumlabels: [],
      write: () => {},
      value: true,
      writeValue: "",
      label: "AttributeWriter",
      history: [],
      timestamp: 0,
      quality: "VALID",
    };

    myInput = {
      showDevice: true,
      attribute: myAttributeInput,
      size: 1,
      showAttribute: "Label",
      alignValueRight: true,
      textColor: "red",
      backgroundColor: "blue",
      font: "mySelect",
      widgetCss: "",
      title: "",
    };

    const element = React.createElement(AttributeWriter.component, {
      mode: "edit",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: myInput,
      id: 42,
    });
    let wrapper = mount(<Provider store={store}>{element}</Provider>);
    const componentWrapper = wrapper.find(AttributeValues);
    expect(componentWrapper.props().deviceName).toEqual("sys/tg_test/1");
  });

  it("render with DevEnum mode run with invalid value", () => {
    myAttributeInput = {
      device: "sys/tg_test/1",
      attribute: "DevEnum",
      dataType: "DevEnum",
      dataFormat: "scalar",
      isNumeric: false,
      unit: "",
      enumlabels: [],
      write: () => {},
      value: "",
      writeValue: "",
      label: "label",
      history: [],
      timestamp: 0,
      quality: "VALID",
    };

    myInput = {
      showDevice: false,
      attribute: myAttributeInput,
      size: 1,
      showAttribute: "name",
      alignValueRight: true,
      textColor: "red",
      backgroundColor: "blue",
      font: "mySelect",
      widgetCss: "",
      title: "",
    };

    const element = React.createElement(AttributeWriter.component, {
      mode: "run",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: myInput,
      id: 42,
    });

    let wrapper = mount(<Provider store={store}>{element}</Provider>);
    wrapper.setProps({
      input: "2a",
    });

    const componentWrapper = wrapper.find(AttributeValues);
    expect(componentWrapper.props().deviceName).toEqual("sys/tg_test/1");
  });

  it("render with unknow not implemented", () => {
    myAttributeInput = {
      device: "unknow",
      attribute: "unknow",
      dataType: "unknow",
      dataFormat: "scalar",
      isNumeric: false,
      unit: "",
      enumlabels: [],
      write: () => {},
      value: "",
      writeValue: "",
      label: "label",
      history: [],
      timestamp: 0,
      quality: "VALID",
    };

    myInput = {
      showDevice: false,
      showAttribute: "Name",
      alignValueRight: true,
      attribute: myAttributeInput,
      size: 1,
      textColor: "red",
      backgroundColor: "blue",
      font: "mySelect",
      widgetCss: "",
      title: "",
    };

    const element = React.createElement(AttributeWriter.component, {
      mode: "run",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: myInput,
      id: 42,
    });
    expect(shallow(element).html()).toContain("unknow not implemented");
  });

  it("simulate focus", () => {
    const myAttributeInput = {
      device: "sys/tg_test/1",
      attribute: "DevString",
      dataType: "DevString",
      dataFormat: "scalar",
      isNumeric: false,
      unit: "",
      enumlabels: [],
      write: () => {},
      value: "stringSend",
      writeValue: "stringSend",
      label: "",
      history: [],
      timestamp: 0,
      quality: "VALID",
    };

    const myInput = {
      showDevice: false,
      showAttribute: "Label",
      alignValueRight: true,
      attribute: myAttributeInput,
      size: 1,
      textColor: "red",
      backgroundColor: "blue",
      font: "mySelect",
      widgetCss: "",
      title: "",
    };

    const element = React.createElement(AttributeWriter.component, {
      mode: "run",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: myInput,
      id: 42,
    });
    const commandArrayWidget = mount(
      <Provider store={store}>{element}</Provider>
    );
    commandArrayWidget.setProps({
      inputs: {
        ...myInput,
        attribute: { ...myAttributeInput, value: "stringSend" },
      },
    });

    const input = commandArrayWidget.find("input");

    act(() => {
      input.simulate("change", { target: { value: "stringSend" } });
      commandArrayWidget.find("form").simulate("submit");
      input.simulate("focus");
      input.simulate("blur");
    });

    commandArrayWidget.update();

    const updatedInput = commandArrayWidget.find("input");

    expect(updatedInput.prop("value")).toEqual("stringSend");
  });

  it("submit on empty string", () => {
    myAttributeInput = {
      device: "sys/tg_test/1",
      attribute: "DevString",
      dataType: "DevString",
      dataFormat: "scalar",
      isNumeric: false,
      unit: "",
      enumlabels: [],
      write: () => {},
      value: "stringSend",
      writeValue: "stringSend",
      label: "",
      history: [],
      timestamp: 0,
      quality: "VALID",
    };

    myInput = {
      showDevice: false,
      showAttribute: "Label",
      alignValueRight: true,
      attribute: myAttributeInput,
      size: 1,
      textColor: "red",
      backgroundColor: "blue",
      font: "mySelect",
      widgetCss: "",
      title: "",
    };

    const element = React.createElement(AttributeWriter.component, {
      mode: "run",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: myInput,
      id: 42,
    });
    const commandArrayWidget = mount(
      <Provider store={store}>{element}</Provider>
    );
    commandArrayWidget.setProps({
      inputs: { ...myInput, attribute: { ...myInput.attribute, value: "" } },
    });
    expect(commandArrayWidget.html()).toContain("The input can't be empty");
    commandArrayWidget.find("form").simulate("submit");
  });

  it("submit DevDouble", () => {
    myAttributeInput = {
      device: "sys/tg_test/1",
      attribute: "DevDouble",
      dataType: "DevDouble",
      dataFormat: "scalar",
      isNumeric: true,
      unit: "",
      enumlabels: [],
      write: () => {},
      value: 1,
      writeValue: 1,
      label: "",
      history: [],
      timestamp: 0,
      quality: "VALID",
    };

    myInput = {
      showDevice: false,
      showAttribute: "Label",
      alignValueRight: true,
      attribute: myAttributeInput,
      size: 1,
      textColor: "red",
      backgroundColor: "blue",
      font: "mySelect",
      widgetCss: "",
      title: "",
    };

    const element = React.createElement(AttributeWriter.component, {
      mode: "run",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: myInput,
      id: 42,
    });
    const commandArrayWidget = mount(
      <Provider store={store}>{element}</Provider>
    );
    commandArrayWidget.setProps({
      inputs: { ...myInput, attribute: { ...myInput.attribute, value: 1 } },
    });
    expect(commandArrayWidget.html()).toContain("1.00");
    commandArrayWidget.find("form").simulate("submit");
  });

  it("submit DevBoolean", () => {
    myAttributeInput = {
      device: "sys/tg_test/1",
      attribute: "DevBoolean",
      dataType: "DevBoolean",
      dataFormat: "scalar",
      isNumeric: true,
      unit: "",
      enumlabels: [],
      write: () => {},
      value: 1,
      writeValue: 1,
      label: "",
      history: [],
      timestamp: 0,
      quality: "VALID",
    };

    myInput = {
      showDevice: false,
      showAttribute: "Label",
      alignValueRight: true,
      attribute: myAttributeInput,
      size: 1,
      textColor: "red",
      backgroundColor: "blue",
      font: "mySelect",
      widgetCss: "",
      title: "",
    };

    const element = React.createElement(AttributeWriter.component, {
      mode: "run",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: myInput,
      id: 42,
    });

    const commandArrayWidget = mount(
      <Provider store={store}>{element}</Provider>
    );

    commandArrayWidget.setProps({
      inputs: { ...myInput, attribute: { ...myInput.attribute, value: "1" } },
    });
    expect(commandArrayWidget.html()).toContain('placeholder="1"');
    commandArrayWidget.find("form").simulate("submit");
  });

  it("submit DevBoolean  with f", () => {
    myAttributeInput = {
      device: "sys/tg_test/1",
      attribute: "DevBoolean",
      dataType: "DevBoolean",
      dataFormat: "scalar",
      isNumeric: true,
      unit: "",
      enumlabels: [],
      write: () => {},
      value: 1,
      writeValue: 1,
      label: "",
      history: [],
      timestamp: 0,
      quality: "VALID",
    };

    myInput = {
      showDevice: false,
      showAttribute: "Label",
      alignValueRight: true,
      attribute: myAttributeInput,
      size: 1,
      textColor: "red",
      backgroundColor: "blue",
      font: "mySelect",
      widgetCss: "",
      title: "",
    };

    const element = React.createElement(AttributeWriter.component, {
      mode: "run",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: myInput,
      id: 42,
    });

    const commandArrayWidget = mount(
      <Provider store={store}>{element}</Provider>
    );

    let inputField = commandArrayWidget.find("input");
    inputField.simulate("change", { target: { value: "f" } });
    commandArrayWidget.update();
    commandArrayWidget.find("form").simulate("submit");
    expect(commandArrayWidget.html()).not.toContain('value="f"');
  });

  it("submit DevDouble not accepted input", () => {
    myAttributeInput = {
      device: "sys/tg_test/1",
      attribute: "DevDouble",
      dataType: "DevDouble",
      dataFormat: "scalar",
      isNumeric: true,
      unit: "",
      enumlabels: [],
      write: () => {},
      value: 1,
      writeValue: 1,
      label: "",
      history: [],
      timestamp: 0,
      quality: "VALID",
    };

    myInput = {
      showDevice: false,
      showAttribute: "Label",
      alignValueRight: true,
      attribute: myAttributeInput,
      size: 1,
      textColor: "red",
      backgroundColor: "blue",
      font: "mySelect",
      widgetCss: "",
      title: "",
    };

    const element = React.createElement(AttributeWriter.component, {
      mode: "run",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: myInput,
      id: 42,
    });
    const commandArrayWidget = mount(
      <Provider store={store}>{element}</Provider>
    );

    let inputField = commandArrayWidget.find("input");
    inputField.simulate("change", { target: { value: "1as" } });
    commandArrayWidget.update();
    commandArrayWidget.find("form").simulate("submit");
    expect(commandArrayWidget.html()).toContain('value="1as"');
  });
});

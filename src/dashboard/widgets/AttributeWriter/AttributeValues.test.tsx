import React from "react";
import { render } from "@testing-library/react";
import { useSelector as useSelectorMock } from "react-redux";
import AttributeValues from "./AttributeValues";
import {
  getAttributeLastQualityFromState as getAttributeLastQualityFromStateMock,
  getAttributeLastTimeStampFromState as getAttributeLastTimeStampFromStateMock,
  getAttributeLastValueFromState as getAttributeLastValueFromStateMock,
} from "../../../shared/utils/getLastValueHelper";

jest.mock("react-redux", () => ({
  useSelector: jest.fn(),
}));

jest.mock(
  "../../../shared/utils/AttributeInputType/AttributeInputType",
  () => (props) => (
    <div>
      AttributeInputType<div>value: {props.value}</div>
    </div>
  )
);

const getAttributeLastQualityFromState = getAttributeLastQualityFromStateMock as jest.Mock;
const getAttributeLastTimeStampFromState = getAttributeLastTimeStampFromStateMock as jest.Mock;
const getAttributeLastValueFromState = getAttributeLastValueFromStateMock as jest.Mock;

jest.mock("../../../shared/utils/getLastValueHelper", () => ({
  getAttributeLastValueFromState: jest.fn(),
  getAttributeLastTimeStampFromState: jest.fn(),
  getAttributeLastQualityFromState: jest.fn(),
}));

const useSelector = useSelectorMock as jest.Mock;

describe("AttributeValues", () => {
  beforeEach(() => {
    useSelector.mockImplementation((selectorFn: any) =>
      selectorFn({
        messages: {},
        ui: {
          mode: "run",
        },
      })
    );
    getAttributeLastValueFromState.mockReturnValue("-1");
    getAttributeLastTimeStampFromState.mockReturnValue(Date.now() / 1000);
    getAttributeLastQualityFromState.mockReturnValue("ATTR_INVALID");
  });

  afterEach(() => {
    jest.resetAllMocks();
  });

  it("render with DevBoolean mode edit", () => {
    const { container } = render(
      <AttributeValues
        attributeName="att"
        deviceName="sys/tg_test/1"
        mode="edit"
        alarms={[1, 2]}
        bounds={[3, 4]}
        type="DevBoolean"
        value={1}
        isValid={true}
        placeholder="placeholder"
        validating={false}
        onFocus={() => {}}
        onBlur={() => {}}
        onChange={() => {}}
        alignValueRight={true}
      />
    );
    expect(container.innerHTML).toContain("AttributeInputType");
  });

  it("render without device mode edit", () => {
    const { container } = render(
      <AttributeValues
        attributeName="att"
        deviceName=""
        mode="edit"
        alarms={[1, 2]}
        bounds={[3, 4]}
        type="DevBoolean"
        value={2}
        isValid={true}
        placeholder="placeholder"
        validating={false}
        onFocus={() => {}}
        onBlur={() => {}}
        onChange={() => {}}
        alignValueRight={true}
      />
    );
    expect(container.innerHTML).toContain("AttributeInputType");
  });

  it("render DevString mode run", () => {
    const { container } = render(
      <AttributeValues
        attributeName="DevString"
        deviceName="sys/tg_test/1"
        mode="run"
        alarms={[1, 2]}
        bounds={[3, 4]}
        type="DevString"
        value={"-1"}
        isValid={true}
        placeholder="placeholder"
        validating={false}
        onFocus={() => {}}
        onBlur={() => {}}
        onChange={() => {}}
        alignValueRight={true}
      />
    );
    expect(container.textContent).toContain("value: -1");
  });

  it("render DevEnum mode run", () => {
    getAttributeLastValueFromState.mockReturnValue("1as");
    const { container } = render(
      <AttributeValues
        attributeName="DevEnum"
        deviceName="sys/tg_test/1"
        mode="run"
        alarms={[1, 2]}
        bounds={[3, 4]}
        type="DevEnum"
        value={"1as"}
        isValid={true}
        placeholder="placeholder"
        validating={false}
        onFocus={() => {}}
        onBlur={() => {}}
        onChange={() => {}}
        alignValueRight={true}
      />
    );
    expect(container.textContent).toContain("value: 1as");
  });

  it("render DevBoolean", () => {
    getAttributeLastValueFromState.mockReturnValue(true);
    const { container } = render(
      <AttributeValues
        attributeName="DevBoolean"
        deviceName="sys/tg_test/1"
        mode="run"
        alarms={[1, 2]}
        bounds={[3, 4]}
        type="DevBoolean"
        value={1}
        isValid={true}
        placeholder="placeholder"
        validating={true}
        onFocus={() => {}}
        onBlur={() => {}}
        onChange={() => {}}
        alignValueRight={true}
      />
    );
    expect(container.textContent).toContain("AttributeInputTypevalue: 1");
  });
});

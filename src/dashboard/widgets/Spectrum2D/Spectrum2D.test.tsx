import React from "react";
import { AttributeInput, ComplexInputDefinition } from "../../types";

import { configure, shallow } from "enzyme";
import Adapter from "@wojtekmaj/enzyme-adapter-react-17";
import Spectrum2D, { Inputs } from "./Spectrum2D";
import { TypedInputs } from "../types";

configure({ adapter: new Adapter() });

jest.mock("./Spectrum2DValues", () => () => <div>Mock Spectrum2DValues</div>);

describe("Spectrum2D Tests", () => {
  let myInput: TypedInputs<Inputs>;
  var writeArray: any = [];
  var date = new Date();
  var timestamp = date.getTime();

  it("renders library mode true without crashing with logarithmic", () => {
    const myAttributeInputX = {
      device: "sys/tg_test/1",
      attribute: "time_range",
      label: "TimeRange",
      history: [],
      dataType: "",
      dataFormat: "",
      isNumeric: true,
      unit: "",
      enumlabels: [],
      write: writeArray,
      value: [1, 2, 3, 4, 5],
      writeValue: "",
      timestamp: timestamp,
      quality: "VALID",
    };
    const myAttributeInputY = [
      {
        attribute: {
          device: "sys/tg_test/1",
          attribute: "voltage",
          label: "Voltage",
          history: [],
          dataType: "",
          dataFormat: "",
          isNumeric: true,
          unit: "",
          enumlabels: [],
          write: writeArray,
          value: [0, 1, 2, 3, 4],
          writeValue: "",
          timestamp: timestamp,
          quality: "VALID",
        },
      },
    ];
    myInput = {
      attributeX: myAttributeInputX,
      attributes: myAttributeInputY,
      showTitle: true,
      showAttribute: "Name",
      xScientificNotation: false,
      yScientificNotation: false,
      xLogarithmicScale: true,
      yLogarithmicScale: true,
      plotStyling: "bar",
    };

    const element = React.createElement(Spectrum2D.component, {
      mode: "library",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: myInput,
      id: 1,
    });
    expect(shallow(element).html()).toContain("height:150px");
  });

  it("renders library mode true without crashing with exponent", () => {
    const myAttributeInputX = {
      device: "sys/tg_test/1",
      attribute: "time_range",
      label: "TimeRange",
      history: [],
      dataType: "",
      dataFormat: "",
      isNumeric: true,
      unit: "",
      enumlabels: [],
      write: writeArray,
      value: [1, 2, 3, 4, 5],
      writeValue: "",
      timestamp: timestamp,
      quality: "VALID",
    };
    const myAttributeInputY = [
      {
        attribute: {
          device: "sys/tg_test/1",
          attribute: "voltage",
          label: "Voltage",
          history: [],
          dataType: "",
          dataFormat: "",
          isNumeric: true,
          unit: "",
          enumlabels: [],
          write: writeArray,
          value: [0, 1, 2, 3, 4],
          writeValue: "",
          timestamp: timestamp,
          quality: "VALID",
        },
      },
    ];
    myInput = {
      attributeX: myAttributeInputX,
      attributes: myAttributeInputY,
      showTitle: true,
      showAttribute: "Name",
      xScientificNotation: true,
      yScientificNotation: true,
      xLogarithmicScale: false,
      yLogarithmicScale: false,
      plotStyling: "markers",
    };

    const element = React.createElement(Spectrum2D.component, {
      mode: "run",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: myInput,
      id: 2,
    });
    expect(shallow(element).html()).not.toContain("height:150px");
  });

  it("renders edit mode true without crashing with exponent", () => {
    const myAttributeInputX = {
      device: "sys/tg_test/1",
      attribute: "time_range",
      label: "TimeRange",
      history: [],
      dataType: "",
      dataFormat: "",
      isNumeric: true,
      unit: "",
      enumlabels: [],
      write: writeArray,
      value: [1, 2, 3, 4, 5],
      writeValue: "",
      timestamp: timestamp,
      quality: "VALID",
    };
    const myAttributeInputY = [
      {
        attribute: {
          device: "sys/tg_test/1",
          attribute: "voltage",
          label: "Voltage",
          history: [],
          dataType: "",
          dataFormat: "",
          isNumeric: true,
          unit: "",
          enumlabels: [],
          write: writeArray,
          value: [0, 1, 2, 3, 4],
          writeValue: "",
          timestamp: timestamp,
          quality: "VALID",
        },
      },
    ];
    myInput = {
      attributeX: myAttributeInputX,
      attributes: myAttributeInputY,
      showTitle: true,
      showAttribute: "Label",
      xScientificNotation: true,
      yScientificNotation: true,
      xLogarithmicScale: false,
      yLogarithmicScale: false,
      plotStyling: "markers",
    };

    const element = React.createElement(Spectrum2D.component, {
      mode: "edit",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: myInput,
      id: 2,
    });
    expect(shallow(element).html()).not.toContain("height:150px");
  });
});

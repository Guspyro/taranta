import React from 'react';
import { render } from '@testing-library/react';
import { useSelector as useSelectorMock } from 'react-redux';
import { getAttributeLastValueFromState as getAttributeLastValueFromStateMock } from '../../../shared/utils/getLastValueHelper';
import Spectrum2DValues from './Spectrum2DValues';

jest.mock('react-redux', () => ({
    useSelector: jest.fn(),
}));

jest.mock('../../../shared/utils/getLastValueHelper', () => ({
    getAttributeLastValueFromState: jest.fn(),
}));

const useSelector = useSelectorMock as jest.Mock;
const getAttributeLastValueFromState = getAttributeLastValueFromStateMock as jest.Mock;

describe('Spectrum2DValues', () => {
    beforeEach(() => {
        useSelector.mockImplementation((selectorFn: any) => selectorFn({
            messages: {},
        }));

        getAttributeLastValueFromState.mockReturnValue('1');
    });

    afterEach(() => {
        jest.resetAllMocks();
    });

    it('should render the Spectrum2DValues with type bar', () => {
        const { container } = render(
            <Spectrum2DValues
                attributeX={{
                    device: "deviceX",
                    attribute: "attributeX"
                }}
                attributes={[
                    {
                        attribute: {
                            device: "device1",
                            attribute: "attribute1"
                        }
                    },
                    {
                        attribute: {
                            device: "device2",
                            attribute: "attribute2"
                        }
                    }
                ]}
                plotStyling="bar"
                layout={{ margin: { t: 0, b: 0, l: 0, r: 0 } }}
                actualWidth={500}
                actualHeight={500}
            />,
        );
        // Assert that the plotly component was rendered
        // Since we cannot see the actual plot because it is rendered by a 3rd-party library, we test that the div where plotly should be rendered is present
        expect(container.innerHTML).toContain('myPlotlyDiv');
    });

    it('should render the Spectrum2DValues with type scatter', () => {
        const { container } = render(
            <Spectrum2DValues
                attributeX={{
                    device: "deviceX",
                    attribute: "attributeX"
                }}
                attributes={[
                    {
                        attribute: {
                            device: "device1",
                            attribute: "attribute1"
                        }
                    },
                    {
                        attribute: {
                            device: "device2",
                            attribute: "attribute2"
                        }
                    }
                ]}
                plotStyling="scatter"
                layout={{ margin: { t: 0, b: 0, l: 0, r: 0 } }}
                actualWidth={500}
                actualHeight={500}
            />,
        );
        // Assert that the plotly component was rendered
        // Since we cannot see the actual plot because it is rendered by a 3rd-party library, we test that the div where plotly should be rendered is present
        expect(container.innerHTML).toContain('myPlotlyDiv');
    });
});

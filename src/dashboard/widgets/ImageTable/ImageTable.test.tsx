import React from "react";
import { AttributeInput } from "../../types";
import { useSelector as useSelectorMock } from "react-redux";
import { configure, shallow } from "enzyme";
import Adapter from "@wojtekmaj/enzyme-adapter-react-17";
import ImageTable from "./ImageTable";

interface Input {
  showDevice: boolean;
  showAttribute: boolean;
  attribute: AttributeInput;
  showIndex: boolean;
  showLabel: boolean;
  fontSize: number;
  rowLimit: number;
  columnLimit: number;
  showWarning: boolean;
  rowOffset: number;
  columnOffset: number;
}

jest.mock('react-redux', () => ({
  useSelector: jest.fn(),
}));
const useSelector = useSelectorMock as jest.Mock;

configure({ adapter: new Adapter() });

describe("ImageTable", () => {
  let myAttributeInput: AttributeInput;
  let myInput: Input;
  var writeArray: any = [];
  var date = new Date();
  var timestamp = date.getTime();

  beforeEach(() => {
    useSelector.mockImplementation((selectorFn) =>
      selectorFn({
        messages: {
          'sys/tg_test/1': {
            attributes: {
              double_image: {
                values: [[[1,2,3],[4,5,6],[7,8,9]],[[10,11,12],[13,14,15],[16,17,18]]],
              }
            }
          }
        },
        ui: {
          mode: "run",
        },
      })
    );
  });


  it("renders without crashing in run mode", () => {
    myAttributeInput = {
      device: "sys/tg_test/1",
      attribute: "double_image",
      history: [],
      dataType: "",
      dataFormat: "",
      isNumeric: true,
      unit: "",
      write: writeArray,
      value: [],
      writeValue: "",
      timestamp: timestamp,
      label: "DOUBLE_IMAGE",
      quality: "VALID",
      enumlabels: ["DOUBLE_IMAGE"]
    };

    myInput = {
        showDevice: true,
        showAttribute: true,
        attribute: myAttributeInput,
        showIndex: false,
        showLabel: false,
        fontSize: 12,
        rowLimit: 4,
        columnLimit: 6,
        showWarning: false,
        rowOffset: 0,
        columnOffset: 0,
    };

    const element = React.createElement(ImageTable.component, {
      mode: "run",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: myInput,
      id: 42
    });
    
    expect(shallow(element).html()).toContain('double_image');
    expect((shallow(element).html().match(/<th/g) || []).length).toEqual(0);
    expect((shallow(element).html().match(/<td/g) || []).length).toEqual(9);
    expect((shallow(element).html().match(/<tr/g) || []).length).toEqual(3);
  });

  it("renders without crashing in edit mode", () => {
    myAttributeInput = {
      device: "sys/tg_test/1",
      attribute: "double_image",
      history: [],
      dataType: "",
      dataFormat: "",
      isNumeric: true,
      unit: "",
      write: writeArray,
      value: [],
      writeValue: "",
      timestamp: timestamp,
      label: "DOUBLE_IMAGE",
      quality: "VALID",
      enumlabels: ["DOUBLE_IMAGE"]
    };

    myInput = {
        showDevice: true,
        showAttribute: true,
        attribute: myAttributeInput,
        showIndex: true,
        showLabel: true,
        fontSize: 12,
        rowLimit: 4,
        columnLimit: 6,
        showWarning: false,
        rowOffset: 0,
        columnOffset: 0,
    };

    const element = React.createElement(ImageTable.component, {
      mode: "edit",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: myInput,
      id: 42
    });
    
    expect(shallow(element).html()).toContain('double_image');
    expect((shallow(element).html().match(/<th/g) || []).length).toEqual(11);
    expect((shallow(element).html().match(/<td/g) || []).length).toEqual(24);
    expect((shallow(element).html().match(/<tr/g) || []).length).toEqual(5);
  });

});

import React from "react";
import '../../../shared/tests/globalMocks';
import { configure, mount } from "enzyme";
import Adapter from "@wojtekmaj/enzyme-adapter-react-17";
import Box from "./Box";
import { Provider } from "react-redux";
import configureStore from "../../../shared/state/store/configureStore";
import { componentForWidget } from "..";

configure({ adapter: new Adapter() });

describe("AttributeWriter Display", () => {

  const store = configureStore();

  let myAttributeInput;

  it("render with DevBoolean mode run", () => {

    myAttributeInput = {
      title: "My Title",
      textColor: "black",
      backgroundColor: "green",
      borderColor: "red",
      borderWidth: 2,
      borderStyle: "dotted",
      textSize: 1,
      fontFamily: "",
      layout: "",
      padding: "",
      customCss: "",
    };

    const element = React.createElement(Box.component, {
      mode: "edit",
      // @ts-ignore
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: myAttributeInput,
    });

    //check output for edit mode
    const pageContent = mount(<Provider store={store}>{element}</Provider>);
    pageContent.setProps({
      username: "CREAM",
    });

    expect(pageContent.html()).toContain("My Title");
    expect(pageContent.html()).toContain("green");
    expect(pageContent.html()).toContain("red");
    expect(pageContent.html()).toContain("dotted");
    expect(pageContent.html()).not.toContain("2px dashed gray");

    const element1 = React.createElement(Box.component, {
      mode: "library",
      // @ts-ignore
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: myAttributeInput,
    });

    //check output for library mode
    const pageContent1 = mount(<Provider store={store}>{element1}</Provider>);
    expect(pageContent1.html()).toContain("2px dashed gray");

  });

  it("render with innerWidget ATTRIBUTE_DISPLAY", () => {

    myAttributeInput = {
      title: "My Title",
      textColor: "black",
      backgroundColor: "green",
      borderColor: "red",
      borderWidth: 2,
      borderStyle: "dotted",
      textSize: 1,
      fontFamily: "",
      layout: "",
      padding: "",
      customCss: "",
    };

    const widget1 = {
      _id: "1235",
      id: "10",
      x: 46.5,
      y: 4.9,
      canvas: "0",
      width: 16,
      height: 1.35,
      type: "ATTRIBUTE_DISPLAY",
      inputs: {
        attribute: {
          device: "sys/tg_test/1",
          attribute: "double_scalar",
          label: "double_scalar"
        },
        precision: 2,
        showDevice: true,
        showAttribute: "Label",
        scientificNotation: false,
        showEnumLabels: false,
        textColor: "#000000",
        backgroundColor: "#ffffff",
        size: 1,
        font: "Helvetica"
      },
      valid: 1,
      order: 3
    }

    const component = componentForWidget(widget1);
    const elementW = React.createElement(component, widget1);

    const element = React.createElement(Box.component, {
      mode: "edit",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: myAttributeInput,
      // @ts-ignore
      innerWidgets: [elementW]
    });

    const pageContent = mount(<Provider store={store}>{element}</Provider>);
    expect(pageContent.html()).toContain("double_scalar:");
    expect(pageContent.html()).toContain("AttributeDisplay");
    expect(pageContent.html()).toContain("sys/tg_test/1");

  });
})


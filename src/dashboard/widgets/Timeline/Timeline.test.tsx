import React from "react";
import Timeline from "./Timeline";

import { AttributeInput } from "../../types";
import { Inputs } from "./index";

import { configure, shallow, mount } from "enzyme";
import Adapter from "@wojtekmaj/enzyme-adapter-react-17";
import Plot from "./Plot";
import { TypedInputs } from "../types";

jest.mock('react-redux', () => ({
  useSelector: jest.fn(),
}));

configure({ adapter: new Adapter() });

describe("Testing Timeline", () => {
  var writeArray: any = [];
  var date = new Date();
  var timestamp = date.getTime();

  const complexInput = [
    {
      attribute: {
        device: "sys/tg_test/1",
        attribute: "short_scalar",
        label: "ShortScalar",
        history: [],
        dataType: "",
        dataFormat: "",
        isNumeric: true,
        unit: "",
        enumlabels: [],
        write: writeArray,
        value: [0, 1, 2, 3, 4],
        writeValue: "",
        timestamp: timestamp,
        quality: "VALID",
      },
      showAttribute: "Label",
      yAxis: "left",
      yAxisDisplay: "Label",
    },
  ];

  let myInput: TypedInputs<Inputs> = {
    timeWindow: 120,
    attributes: complexInput,
    overflow: true,
    groupAttributes: true,
  };

  it("render widget with normal scenario", async () => {
    const element = React.createElement(Timeline, {
      mode: "edit",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: myInput,
      id: 1,
    });

    //check output for edit mode
    const shallowElement = shallow(element);
    expect(shallowElement.find(".timeline-wrapper").length).toEqual(1);
  });


  let plotParams = {
    height: 120,
    width: 120,
    staticMode: true,
    timeWindow: 120,
  };

  let trace = {
    x: [1, 2, 4],
    y: [2, 4, 5],
    fullName: "Timeline",
    axisLocation: "left",
    yAxisDisplay: "yAxis",
    position: 10,
  };

  let plotProps = {
    traces: [trace],
    params: plotParams,
  };

  it("Testing Plot", () => {
    let shallowElement = mount(
      <Plot
        traces={plotProps.traces}
        params={plotProps.params}
        dataFlow={true}
        eventView={true}
        mode={"run"}
      />
    );
    const elemNoWhiteSpace = shallowElement.html().replace(/\s/g, "");
    expect(elemNoWhiteSpace).toContain("width:100%");
  });

  it("render widget with showAttribute name empty attribute", async () => {
    const complexInput = [
      {
        attribute: {
          device: "",
          attribute: "",
          label: "",
          history: [],
          dataType: "",
          dataFormat: "",
          isNumeric: true,
          unit: "",
          enumlabels: [],
          write: writeArray,
          value: [0, 1, 2, 3, 4],
          writeValue: "",
          timestamp: timestamp,
          quality: "VALID",
        },
        showAttribute: "Label",
        yAxis: "left",
        yAxisDisplay: "Label",
      },
    ];

    let myInput: TypedInputs<Inputs> = {
      timeWindow: 120,
      attributes: complexInput,
      overflow: true,
      groupAttributes: true,
    };

    const element = React.createElement(Timeline, {
      mode: "edit",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: myInput,
      id: 3,
    });

    const shallowElement = shallow(element);
    expect(shallowElement.find(".timeline-wrapper").length).toEqual(1);
  });

  it("render widget with showAttribute Label empty attribute", async () => {
    const complexInput = [
      {
        attribute: {
          device: "",
          attribute: "",
          label: "",
          history: [],
          dataType: "",
          dataFormat: "",
          isNumeric: true,
          unit: "",
          enumlabels: [],
          write: writeArray,
          value: [0, 1, 2, 3, 4],
          writeValue: "",
          timestamp: timestamp,
          quality: "VALID",
        },
        showAttribute: "",
        yAxis: "left",
        yAxisDisplay: "Label",
      },
    ];

    let myInput: TypedInputs<Inputs> = {
      timeWindow: 120,
      attributes: complexInput,
      overflow: true,
      groupAttributes: true,
    };

    const element = React.createElement(Timeline, {
      mode: "edit",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: myInput,
      id: 4,
    });

    const shallowElement = shallow(element);
    expect(shallowElement.find(".timeline-wrapper").length).toEqual(1);
  });

  it("render widget with showAttribute Label", async () => {
    const complexInput = [
      {
        attribute: {
          device: "sys/tg_test/1",
          attribute: "short_scalar",
          label: "ShortScalar",
          history: [],
          dataType: "",
          dataFormat: "",
          isNumeric: true,
          unit: "",
          enumlabels: [],
          write: writeArray,
          value: [0, 1, 2, 3, 4],
          writeValue: "",
          timestamp: timestamp,
          quality: "VALID",
        },
        showAttribute: "Attribute",
        yAxis: "left",
        yAxisDisplay: "Label",
      },
    ];

    let myInput: TypedInputs<Inputs> = {
      timeWindow: 20,
      attributes: complexInput,
      overflow: true,
      groupAttributes: true,
    };

    const element = React.createElement(Timeline, {
      mode: "edit",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: myInput,
      id: 5,
    });

    const mountElement = mount(element);
    mountElement
      .find(".options")
      .find("button")
      .at(0)
      .simulate("click");
    expect(mountElement.html()).toContain("Continue data update");
    mountElement
      .find(".options")
      .find("button")
      .at(0)
      .simulate("click");
    expect(mountElement.html()).toContain("Pause data update");
    mountElement
      .find(".options")
      .find("button")
      .at(1)
      .simulate("click");
    expect(mountElement.html()).not.toContain("disabled");
  });
});

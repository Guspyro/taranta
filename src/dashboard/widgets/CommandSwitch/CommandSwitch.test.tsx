import React from "react";
import { render, fireEvent } from "@testing-library/react";
import { useSelector as useSelectorMock, useDispatch } from "react-redux";
import Enzyme from "enzyme";
import Adapter from "@wojtekmaj/enzyme-adapter-react-17";
import { getAttributeLastValueFromState as getAttributeLastValueFromStateMock } from "../../../shared/utils/getLastValueHelper";
import { CommandInput } from "../../types";
import CommandSwitch from "./CommandSwitch";
import { AttributeInput } from "../../types";

jest.mock("react-redux", () => ({
  useSelector: jest.fn(),
  useDispatch: jest.fn(),
}));
jest.mock("../../../dashboard/dashboardRepo", () => ({
  getTangoDB: jest.fn(),
}));

const getAttributeLastValueFromState = getAttributeLastValueFromStateMock as jest.Mock;

jest.mock("../../../shared/utils/getLastValueHelper", () => ({
  getAttributeLastValueFromState: jest.fn(),
}));

const useSelector = useSelectorMock as jest.Mock;
Enzyme.configure({ adapter: new Adapter() });

describe("switch-test", () => {
  const dispatch = jest.fn();
  let myCommandInput: CommandInput;
  let myAttributeInput: AttributeInput;
  var writeArray: any = [];
  var date = new Date();
  var timestamp = date.getTime();
  myAttributeInput = {
    device: "sys/tg_test/1",
    attribute: "State",
    label: "ShortScalar",
    history: [],
    dataType: "",
    dataFormat: "",
    isNumeric: true,
    unit: "",
    enumlabels: [],
    write: writeArray,
    value: "Off",
    writeValue: "",
    quality: "VALID",
    timestamp: timestamp,
  };

  beforeEach(() => {
    useSelector.mockImplementation((selectorFn: any) =>
      selectorFn({
        messages: {
          "sys/tg_test/1": {
            attributes: myAttributeInput,
          },
        },
        ui: {
          mode: "run",
        },
      })
    );
    getAttributeLastValueFromState.mockReturnValue("");
    (useDispatch as jest.Mock).mockReturnValue(dispatch);
  });

  afterEach(() => {
    jest.resetAllMocks();
  });
  it("render switch with normal scenario", () => {
    myCommandInput = {
      device: "sys/tg_test/1",
      command: "configureArray",
      output: "",
      execute: () => null,
    };
    const inputs = {
      title: "Master Switch",
      onCommand: myCommandInput,
      offCommand: myCommandInput,
      commandOn: "",
      commandOff: "",
      attribute: myAttributeInput,
      showDevice: true,
      showCommand: true,
      onStateArgs: "",
      offStateArgs: "",
      onStateImageUrl: "",
      offStateImageUrl: "",
      onStateCss: "background-color:green",
      offStateCss: "background-color:red",
      imageCss: "",
      displayOutput: true,
      alignSendButtonRight: true,
      timeDisplayOutput: 1,
      textColor: "black",
      backgroundColor: "white",
      size: 1.7,
      font: "Helvetica",
      inputWidgetCss: "background-color: green"
    };
    const { container } = render(
      <CommandSwitch.component
        mode="edit"
        t0={1}
        actualWidth={100}
        actualHeight={100}
        inputs={inputs}
        id={42}
      />
    );

    expect(container.innerHTML).toContain("sys/tg_test/1/configureArray");
    expect(container.innerHTML).toContain("Master Switch");
  });

  it("test autoclose popup", () => {
    jest.useFakeTimers();

    myCommandInput = {
      device: "sys/tg_test/1",
      command: "configureArray",
      output: "",
      execute: () => null,
    };
    const inputs = {
      title: "Master Switch",
      onCommand: myCommandInput,
      offCommand: myCommandInput,
      commandOn: "",
      commandOff: "",
      attribute: myAttributeInput,
      showDevice: true,
      showCommand: true,
      onStateArgs: "",
      offStateArgs: "",
      onStateImageUrl: "",
      offStateImageUrl: "",
      onStateCss: "background-color:green",
      offStateCss: "background-color:red",
      imageCss: "",
      displayOutput: true,
      alignSendButtonRight: true,
      timeDisplayOutput: 1,
      textColor: "black",
      backgroundColor: "white",
      size: 1.7,
      font: "Helvetica",
      inputWidgetCss: "background-color: green"
    };
    const { container } = render(
      <CommandSwitch.component
        mode="edit"
        t0={1}
        actualWidth={100}
        actualHeight={100}
        inputs={inputs}
        id={42}
      />
    );
    let checkbox = container.querySelectorAll(
      "input[type='checkbox']"
    )[0] as HTMLInputElement;
    fireEvent.click(checkbox);
    expect(checkbox.checked).toBe(true);
  });

  it("render switch with different input types", () => {
    myCommandInput = {
      device: "sys/tg_test/1",
      command: "DevString",
      output: "",
      execute: () => null,
    };
    let inputs = {
      title: "Master Switch",
      onCommand: myCommandInput,
      offCommand: myCommandInput,
      commandOn: "",
      commandOff: "",
      attribute: myAttributeInput,
      showDevice: true,
      showCommand: true,
      onStateArgs: "",
      offStateArgs: "",
      onStateImageUrl: "",
      offStateImageUrl: "",
      onStateCss: "background-color:green",
      offStateCss: "background-color:red",
      imageCss: "",
      displayOutput: true,
      alignSendButtonRight: true,
      timeDisplayOutput: 1,
      textColor: "black",
      backgroundColor: "white",
      size: 1.7,
      font: "Helvetica",
      inputWidgetCss: "background-color: green"
    };
    const element = render(
      <CommandSwitch.component
        mode="run"
        t0={1}
        actualWidth={100}
        actualHeight={100}
        inputs={inputs}
        id={42}
      />
    );

    let checkbox = element.container.querySelectorAll(
      "input[type='checkbox']"
    )[0] as HTMLInputElement;
    fireEvent.click(checkbox);
    expect(checkbox.checked).toBe(true);
    expect(dispatch).toHaveBeenCalled();
    inputs.onStateArgs = "true";
    inputs.offStateArgs = "true";
    element.rerender(
      <CommandSwitch.component
        mode="run"
        t0={1}
        actualWidth={100}
        actualHeight={100}
        inputs={inputs}
        id={42}
      />
    );
    fireEvent.click(checkbox);
    expect(checkbox.checked).toBe(false);

    inputs.onStateArgs = "123";
    inputs.offStateArgs = "123";
    element.rerender(
      <CommandSwitch.component
        mode="run"
        t0={1}
        actualWidth={100}
        actualHeight={100}
        inputs={inputs}
        id={42}
      />
    );
    fireEvent.click(checkbox);
    expect(checkbox.checked).toBe(true);

    inputs.onStateArgs = "11.11";
    inputs.offStateArgs = "11.11";
    element.rerender(
      <CommandSwitch.component
        mode="edit"
        t0={1}
        actualWidth={100}
        actualHeight={100}
        inputs={inputs}
        id={42}
      />
    );
    fireEvent.click(checkbox);
    expect(checkbox.checked).toBe(false);

    inputs.onCommand["acceptedType"] = "DevVarStringArray";
    inputs.offCommand["acceptedType"] = "DevVarStringArray";
    inputs.onStateArgs = "[1,2,3]";
    inputs.offStateArgs = "[1,2,3]";
    element.rerender(
      <CommandSwitch.component
        mode="edit"
        t0={1}
        actualWidth={100}
        actualHeight={100}
        inputs={inputs}
        id={42}
      />
    );
    fireEvent.click(checkbox);
    expect(checkbox.checked).toBe(true);

    inputs.onCommand["acceptedType"] = "DevString";
    inputs.offCommand["acceptedType"] = "DevString";
    inputs.onStateArgs = "[1,2,3]";
    inputs.offStateArgs = "[1,2,3]";
    element.rerender(
      <CommandSwitch.component
        mode="edit"
        t0={1}
        actualWidth={100}
        actualHeight={100}
        inputs={inputs}
        id={42}
      />
    );
    fireEvent.click(checkbox);
    expect(checkbox.checked).toBe(false);
  });

  it("render switch with custom image", () => {
    myCommandInput = {
      device: "sys/tg_test/1",
      command: "DevString",
      output: "",
      execute: () => null,
    };
    const inputs = {
      title: "Master Switch",
      onCommand: myCommandInput,
      offCommand: myCommandInput,
      commandOn: "",
      commandOff: "",
      attribute: myAttributeInput,
      showDevice: true,
      showCommand: true,
      onStateArgs: "",
      offStateArgs: "",
      onStateImageUrl: "https://picsum.photos/200/300",
      offStateImageUrl:
        "https://www.dumpaday.com/wp-content/uploads/2020/06/00-100-750x280.jpg",
      onStateCss: "background-color:green",
      offStateCss: "background-color:red",
      imageCss: "border-radius:50%",
      displayOutput: true,
      alignSendButtonRight: true,
      timeDisplayOutput: 1,
      textColor: "black",
      backgroundColor: "white",
      size: 1.7,
      font: "Helvetica",
      inputWidgetCss: "background-color: green"
    };
    const element = render(
      <CommandSwitch.component
        mode="run"
        t0={1}
        actualWidth={100}
        actualHeight={100}
        inputs={inputs}
        id={42}
      />
    );

    inputs.displayOutput = false;
    inputs.onCommand["output"] = "1";
    inputs.offCommand["output"] = "1";
    element.rerender(
      <CommandSwitch.component
        mode="run"
        t0={1}
        actualWidth={100}
        actualHeight={100}
        inputs={inputs}
        id={42}
      />
    );

    let checkbox = element.container.querySelectorAll(
      "input[type='checkbox']"
    )[0] as HTMLInputElement;
    fireEvent.click(checkbox);
    expect(checkbox.checked).toBe(true);
    fireEvent.click(checkbox);

    expect(element.container.innerHTML).toContain("sys/tg_test/1/DevString");
    expect(element.container.innerHTML).toContain("border-radius: 50%");
  });

  it("check click event of switch", () => {
    myCommandInput = {
      device: "sys/tg_test/1",
      command: "DevString",
      output: "",
      execute: () => null,
    };
    const inputs = {
      title: "Master Switch",
      onCommand: myCommandInput,
      offCommand: myCommandInput,
      commandOn: "",
      commandOff: "",
      attribute: myAttributeInput,
      showDevice: true,
      showCommand: true,
      onStateArgs: "",
      offStateArgs: "",
      onStateImageUrl: "https://picsum.photos/200/300",
      offStateImageUrl:
        "https://www.dumpaday.com/wp-content/uploads/2020/06/00-100-750x280.jpg",
      onStateCss: "background-color:green",
      offStateCss: "background-color:red",
      imageCss: "border-radius:50%",
      displayOutput: true,
      alignSendButtonRight: true,
      timeDisplayOutput: 1,
      textColor: "black",
      backgroundColor: "white",
      size: 1.7,
      font: "Helvetica",
      inputWidgetCss: "background-color: green"
    };
    const element = render(
      <CommandSwitch.component
        mode="run"
        t0={1}
        actualWidth={100}
        actualHeight={100}
        inputs={inputs}
        id={42}
      />
    );

    //Trigger the click event on checkbox & confirm if image changes on switch
    let checkbox = element.container.querySelectorAll(
      "input[type='checkbox']"
    )[0] as HTMLInputElement;
    fireEvent.click(checkbox);
    const elemNoWhiteSpace = element.container.innerHTML.replace(/\s/g, "");
    if (checkbox.checked) {
      expect(elemNoWhiteSpace).toContain("https://picsum.photos/200/300");
    } else {
      expect(elemNoWhiteSpace).toContain("background-color:red");
      expect(elemNoWhiteSpace).toContain(
        "https://www.dumpaday.com/wp-content/uploads/2020/06/00-100-750x280.jpg"
      );
    }

    expect(elemNoWhiteSpace).toContain("sys/tg_test/1/DevString");
    expect(elemNoWhiteSpace).toContain("border-radius:50%");
  });

  it("test status check for On/Off command execution", () => {
    myCommandInput = {
      device: "sys/tg_test/1",
      command: "DevString",
      output: "",
      execute: () => null,
    };
    let inputs = {
      title: "Master Switch",
      onCommand: myCommandInput,
      offCommand: myCommandInput,
      commandOn: "On",
      commandOff: "Off",
      attribute: myAttributeInput,
      showDevice: true,
      showCommand: true,
      onStateArgs: "",
      offStateArgs: "",
      onStateImageUrl: "",
      offStateImageUrl: "",
      onStateCss: "background-color:green",
      offStateCss: "background-color:red",
      imageCss: "border-radius:50%",
      displayOutput: true,
      alignSendButtonRight: true,
      timeDisplayOutput: 1,
      textColor: "black",
      backgroundColor: "white",
      size: 1.7,
      font: "Helvetica",
      inputWidgetCss: "background-color: green"
    };
    const element = render(
      <CommandSwitch.component
        mode="run"
        t0={1}
        actualWidth={100}
        actualHeight={100}
        inputs={inputs}
        id={42}
      />
    );

    let newAttributeInput: AttributeInput = {
      device: "sys/tg_test/1",
      attribute: "State",
      label: "ShortScalar",
      history: [],
      dataType: "",
      dataFormat: "",
      isNumeric: true,
      unit: "",
      enumlabels: [],
      write: writeArray,
      value: "On",
      writeValue: "",
      quality: "VALID",
      timestamp: timestamp,
    };

    inputs = {
      title: "Master Switch",
      onCommand: myCommandInput,
      offCommand: myCommandInput,
      commandOn: "On",
      commandOff: "Off",
      attribute: newAttributeInput,
      showDevice: true,
      showCommand: true,
      onStateArgs: "",
      offStateArgs: "",
      onStateImageUrl: "",
      offStateImageUrl: "",
      onStateCss: "background-color:green",
      offStateCss: "background-color:red",
      imageCss: "border-radius:50%",
      displayOutput: true,
      alignSendButtonRight: true,
      timeDisplayOutput: 1,
      textColor: "black",
      backgroundColor: "white",
      size: 1.7,
      font: "Helvetica",
      inputWidgetCss: "background-color: green"
    };
    getAttributeLastValueFromState.mockReturnValue("On");
    element.rerender(
      <CommandSwitch.component
        mode="run"
        t0={1}
        actualWidth={100}
        actualHeight={100}
        inputs={inputs}
        id={42}
      />
    );

    let checkbox = element.container.querySelectorAll(
      "input[type='checkbox']"
    )[0] as HTMLInputElement;
    expect(checkbox.checked).toBe(true);

    const incorrectAttributeInput: AttributeInput = {
      device: "sys/tg_test/1",
      attribute: "State",
      label: "ShortScalar",
      history: [],
      dataType: "",
      dataFormat: "",
      isNumeric: true,
      unit: "",
      enumlabels: [],
      write: writeArray,
      value: "Error",
      writeValue: "",
      quality: "VALID",
      timestamp: timestamp,
    };
    inputs = {
      title: "Master Switch",
      onCommand: myCommandInput,
      offCommand: myCommandInput,
      commandOn: "On",
      commandOff: "Off",
      attribute: incorrectAttributeInput,
      showDevice: true,
      showCommand: true,
      onStateArgs: "",
      offStateArgs: "",
      onStateImageUrl: "",
      offStateImageUrl: "",
      onStateCss: "background-color:green",
      offStateCss: "background-color:red",
      imageCss: "border-radius:50%",
      displayOutput: true,
      alignSendButtonRight: true,
      timeDisplayOutput: 1,
      textColor: "black",
      backgroundColor: "white",
      size: 1.7,
      font: "Helvetica",
      inputWidgetCss: "background-color: green"
    };
    getAttributeLastValueFromState.mockReturnValue("Error");
    element.rerender(
      <CommandSwitch.component
        mode="run"
        t0={1}
        actualWidth={100}
        actualHeight={100}
        inputs={inputs}
        id={42}
      />
    );
    expect(element.container.innerHTML).toContain(
      "The specified command On/Off status not correct"
    );
  });
});

import React, { CSSProperties } from "react";
import { render } from "@testing-library/react";
import { useSelector as useSelectorMock } from "react-redux";
import SpectrumValues from "./SpectrumValues";
import {
  getAttributeLastValueFromState as getAttributeLastValueFromStateMock,
} from "../../../shared/utils/getLastValueHelper";

jest.mock("react-redux", () => ({
  useSelector: jest.fn(),
}));

const getAttributeLastValueFromState = getAttributeLastValueFromStateMock as jest.Mock;

jest.mock("../../../shared/utils/getLastValueHelper", () => ({
  getAttributeLastValueFromState: jest.fn()
}));

const useSelector = useSelectorMock as jest.Mock;

describe("SpectrumValues", () => {


    beforeEach(() => {
      useSelector.mockImplementation((selectorFn: any) =>
        selectorFn({
          messages: {
            "sys/tg_test/1": {
              attributes: {
                boolean_scalar: {
                  values: [true, true, true],
                  quality: ["ATTR_VALID", "ATTR_VALID", "ATTR_VALID"],
                  timestamp: [
                    1689077614.017916,
                    1689077614.061947,
                    1689077674.123604,
                  ],
                },
              },
            },
          },
          ui: {
            mode: "run",
          },
        })
      );
      getAttributeLastValueFromState.mockReturnValue(true);
    });

    afterEach(() => {
        jest.resetAllMocks();
      });

      it("should render the div containing  plotly", () => {
        const { container } = render(
          <SpectrumValues
            deviceName = "sys/tg_test/1" 
            attributeName = "double_spectrum_ro" 
            mode =  "edit" 
            showTitle = {true}
            title = "sys/tg_test/1/double_spectrum_ro" 
            titlefont = {{size: 12}}
            font = {{family: 'Helvetica, Arial, sans-serif'}}
            margin = {{l: 30, r: 15, t: 35, b: 20}}
            autosize =  {true} 
            inelastic = {true} 
            config = {{staticPlot: true}} 
            responsive = {true} 
            style = {{width: 599, height: 407}}
          />
        );
    
        expect(container.innerHTML).toContain('plotlySpectrumDiv');
      });

      it("checks width and height", () => {
        const { container } = render(
          <SpectrumValues
            deviceName = "sys/tg_test/1" 
            attributeName = "double_spectrum_ro" 
            mode =  "edit" 
            showTitle = {true}
            title = "sys/tg_test/1/double_spectrum_ro" 
            titlefont = {{size: 12}}
            font = {{family: 'Helvetica, Arial, sans-serif'}}
            margin = {{l: 30, r: 15, t: 35, b: 20}}
            autosize =  {true} 
            inelastic = {true} 
            config = {{staticPlot: true}} 
            responsive = {true} 
            style = {{width: 599, height: 407}}
          />
        );
    
        expect(container.innerHTML).toContain('width: 599px');
        expect(container.innerHTML).toContain('height: 407px');
      });
});

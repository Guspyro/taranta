import React from "react";
import { AttributeInput } from "../../types";

import { configure, shallow } from "enzyme";
import Adapter from "@wojtekmaj/enzyme-adapter-react-17";
import Spectrum from "./Spectrum";
import SpectrumValues from "./SpectrumValues";

interface Input {
  attribute: AttributeInput;
  showTitle: boolean;
  inelastic: boolean;
  showAttribute: string;
}

configure({ adapter: new Adapter() });

jest.mock("./SpectrumValues", () => () => (
  <div>Mock SpectrumValues</div>
));

describe("Spectrum Tests", () => {
  let myAttributeInput: AttributeInput;
  let myInput: Input;
  var writeArray: any = [];
  var date = new Date();
  var timestamp = date.getTime();

  beforeEach(() => {
    myAttributeInput = {
      device: "sys/tg_test/1",
      attribute: "time_range",
      dataType: "DevBoolean",
      dataFormat: "spectrum",
      isNumeric: true,
      unit: "",
      enumlabels: [],
      write: writeArray,
      value: [1, 2, 3, 4, 5],
      writeValue: "",
      label: "TimeRange",
      history: [],
      timestamp: timestamp,
      quality: ""
    };
    myInput = {
      attribute: myAttributeInput,
      showTitle: true,
      inelastic: true,
      showAttribute: "Name",
    };
  })



  it("check if SpectrumValues component is invoked ", () => {
    const element = React.createElement(Spectrum.component, {
      mode: "library",
      t0: 1,
      id: 42,
      actualWidth: 100,
      actualHeight: 100,
      inputs: myInput,
    });

    const shallowElement = shallow(element);
    expect(shallowElement.find(SpectrumValues).length).toBe(1);
  });

  

});


import React from 'react';
import { render } from '@testing-library/react';
import { useSelector as useSelectorMock } from 'react-redux';
import LogValues from './LogValues';
import {
    getAttributeLastTimeStampFromState as getAttributeLastTimeStampFromStateMock,
    getAttributeLastValueFromState as getAttributeLastValueFromStateMock
} from '../../../../shared/utils/getLastValueHelper';

jest.mock('react-redux', () => ({
    useSelector: jest.fn(),
}));

const getAttributeLastTimeStampFromState = getAttributeLastTimeStampFromStateMock as jest.Mock;
const getAttributeLastValueFromState = getAttributeLastValueFromStateMock as jest.Mock;

jest.mock('../../../../shared/utils/getLastValueHelper', () => ({
    getAttributeLastValueFromState: jest.fn(),
    getAttributeLastTimeStampFromState: jest.fn(),
}));

const useSelector = useSelectorMock as jest.Mock;

describe('LogValues', () => {
    beforeEach(() => {
        useSelector.mockImplementation((selectorFn) => selectorFn({
            messages: {},
        }));
        getAttributeLastValueFromState.mockReturnValue('1');
        getAttributeLastTimeStampFromState.mockReturnValue(Date.now() / 1000);
    });

    afterEach(() => {
        jest.resetAllMocks();
    });

    it('should render the log message and timestamp when showTime is true', () => {
        const { container } = render(
            <LogValues
                attributeName="attr"
                deviceName="device"
                showAttribute="Name"
                showDevice={true}
                label="Label"
                linesDisplayed={10}
                showTime={true}
                showLastValue={true}
                mode="run"
                OuterDivCSS=""
                LastValueCSS=""
                TableCSS=""
            />,
        );
        expect(container.textContent).toContain('Log Message');
        expect(container.textContent).toContain('device');
        expect(container.textContent).toContain('attr');
    });

    it('should render only the log message when showTime is false', () => {
        const { container } = render(
            <LogValues
                attributeName="attr"
                deviceName="device"
                showAttribute="Name"
                showDevice={false}
                label="Label"
                linesDisplayed={10}
                showLastValue={true}
                showTime={false}
                mode="run"
                OuterDivCSS=""
                LastValueCSS=""
                TableCSS=""
            />,
        );
        expect(container.textContent).toContain('Log Message');
        expect(container.textContent).not.toContain('device');
        expect(container.textContent).toContain('attr');
    });
});

import React from "react";
import '../../../shared/tests/globalMocks';
import { AttributeInput } from "../../types";

import { configure, shallow, mount } from "enzyme";
import Adapter from "@wojtekmaj/enzyme-adapter-react-17";
import { AttributeLogger } from "./AttributeLogger";
import { Provider } from "react-redux";
import configureStore from "../../../shared/state/store/configureStore";

configure({ adapter: new Adapter() });
const store = configureStore();

describe("AttributeLogger", () => {
  //React.useLayoutEffect = React.useEffect;
  const date = new Date();
  const timestamp = date.getTime();
  const writeArray: any = [];

  const myAttributeInput: AttributeInput = {
    device: "sys/tg_test/1",
    attribute: "short_scalar",
    history: [],
    dataType: "",
    dataFormat: "",
    isNumeric: true,
    unit: "",
    enumlabels: [],
    write: writeArray,
    value: "",
    writeValue: "",
    timestamp: timestamp,
    quality: "VALID",
    label: "label"
  };


  it("renders without crashing", () => {
    const element = React.createElement(
      Provider,
      {
        store: store
      },
      React.createElement(AttributeLogger, {
        mode: "run",
        t0: 1,
        actualWidth: 100,
        actualHeight: 100,
        id: 42,
        inputs: {
          showDevice: true,
          showAttribute: 'attr',
          linesDisplayed: 30,
          showTime: false,
          showLastValue: true,
          attribute: myAttributeInput,
          OuterDivCSS: "",
          LastValueCSS: "",
          TableCSS: ""
        }
      })
    );
    expect(mount(element).html()).toContain("Recent  on sys/tg_test/1");
  });

  it("in edit mode the log is not displayed", () => {
    const element = React.createElement(
      Provider,
      {
        store: store
      },
      React.createElement(AttributeLogger, {
        mode: "edit",
        t0: 1,
        actualWidth: 100,
        actualHeight: 100,
        id: 42,
        inputs: {
          showDevice: true,
          showAttribute: 'attr',
          linesDisplayed: 30,
          showTime: false,
          showLastValue: true,
          attribute: myAttributeInput,
          OuterDivCSS: "",
          LastValueCSS: "",
          TableCSS: ""
        }
      })
    );
    expect(mount(element).html()).toEqual(
      expect.not.stringContaining("AttributeLog")
    );
  });

  it("The list updates when a new value comes in", () => {
    const attributeLogger = React.createElement(AttributeLogger, {
      mode: "run",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      id: 42,
      inputs: {
        showDevice: true,
        showAttribute: 'bleee',
        linesDisplayed: 30,
        showTime: false,
        showLastValue: true,
        attribute: myAttributeInput,
        OuterDivCSS: "",
        LastValueCSS: "",
        TableCSS: ""
      }
    });

    //check the status update when the message is different
    const provider = mount(
      <Provider store={store}>{attributeLogger}</Provider>
    );
    expect(provider.html()).toContain("Recent  on sys/tg_test/1");
    const prevProps = {
      inputs: { attribute: { value: "Old Message", timestamp: 11111 } }
    };

    //check the list population
    const shallowLogger = shallow(attributeLogger);
    const instance = shallowLogger.instance();

    expect(instance.componentDidUpdate)

    if (instance.componentDidUpdate) {
      instance.componentDidUpdate(prevProps, {});
      expect("valueLog" in instance.state);
    }
  });
});

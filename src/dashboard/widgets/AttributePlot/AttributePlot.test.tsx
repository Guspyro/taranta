import React from "react";
import { Inputs } from "./index";
import { useSelector as useSelectorMock } from "react-redux";
import { render } from "@testing-library/react";
import { configure } from "enzyme";
import Adapter from "@wojtekmaj/enzyme-adapter-react-17";
import { TypedInputs } from "../types";
import AttributePlot, { TracesFromAttributeInputs } from "./AttributePlot";
import { dataAndRange } from "./Plot";

jest.mock('react-redux', () => ({
  useSelector: jest.fn(),
}));
const useSelector = useSelectorMock as jest.Mock;

configure({ adapter: new Adapter() });

describe("Testing Attribute Plot", () => {
  var writeArray: any = [];
  var date = new Date();
  var timestamp = date.getTime();

  beforeEach(() => {
    useSelector.mockImplementation((selectorFn) =>
      selectorFn({
        messages: {
          'sys/tg_test/1': {
            attributes: {
              long_scalar: {
                values: [
                  228,
                  228,
                  91
                ],
                quality: [
                  'ATTR_VALID',
                  'ATTR_VALID',
                  'ATTR_VALID'
                ],
                timestamp: [
                  1690542017.171402,
                  1690542017.127803,
                  1690542041.218554
                ]
              },
              short_scalar: {
                values: [
                  170,
                  170,
                  90
                ],
                quality: [
                  'ATTR_VALID',
                  'ATTR_VALID',
                  'ATTR_VALID'
                ],
                timestamp: [
                  1690542017.181211,
                  1690542017.128467,
                  1690542041.218627
                ]
              }
            }
          }
        },
        ui: {
          mode: "run",
        },
      })
    );
  });

  const complexInput = [
    {
      attribute: {
        device: "sys/tg_test/1",
        attribute: "short_scalar",
        label: "ShortScalar",
        history: [],
        dataType: "",
        dataFormat: "",
        isNumeric: true,
        unit: "",
        enumlabels: [],
        write: writeArray,
        value: [0, 1, 2, 3, 4],
        writeValue: "",
        timestamp: timestamp,
        quality: "VALID",
      },
      lineColor: "green",
      showAttribute: "Label",
      yAxis: "left",
      yAxisDisplay: "Label",
    },
  ];

  let myInput: TypedInputs<Inputs> = {
    timeWindow: 120,
    showZeroLine: true,
    logarithmic: false,
    textColor: "",
    backgroundColor: "cyan",
    attributes: complexInput,
  };

  it("render widget with normal scenario edit mode", async () => {
    const { container } = render(<AttributePlot
      mode={"edit"}
      t0={1}
      actualWidth={100}
      actualHeight={100}
      inputs={myInput}
      id={1} />
    )

    expect(container.innerHTML).toContain('width: 100%')
    expect(container.innerHTML).toContain('height: 100%')
  });

  it("render widget with normal scenario library mode", async () => {
    const { container } = render(<AttributePlot
      mode={"library"}
      t0={1}
      actualWidth={100}
      actualHeight={100}
      inputs={myInput}
      id={1} />
    )
    expect(container.innerHTML).toContain('width: 100%')
    expect(container.innerHTML).toContain('height: 100%')
  });

  it("render widget with normal scenario run mode", async () => {
    const { container } = render(<AttributePlot
      mode={"run"}
      t0={1}
      actualWidth={100}
      actualHeight={100}
      inputs={myInput}
      id={1} />
    )
    expect(container.innerHTML).toContain('width: 100%')
    expect(container.innerHTML).toContain('height: 100%')
  });

  it("test TracesFromAttributeInputs", async () => {
    const complexInput = [
      {
        "attribute": {
          "device": "sys/tg_test/1",
          "attribute": "long_scalar",
          "label": "long_scalar",
          "history": [],
          "dataType": "DevLong",
          "enumlabels": [],
          "dataFormat": "scalar",
          "isNumeric": true,
          "unit": "",
          write: writeArray,
          value: [0, 1, 2, 3, 4],
          writeValue: "",
          timestamp: timestamp,
          quality: "VALID",
        },
        "showAttribute": "Label",
        "yAxis": "left",
        "lineColor": "#935c5c"
      },
      {
        "attribute": {
          "device": "sys/tg_test/1",
          "attribute": "short_scalar",
          "label": "short_scalar",
          "history": [],
          "dataType": "DevShort",
          "enumlabels": [],
          "dataFormat": "scalar",
          "isNumeric": true,
          "unit": "",
          write: writeArray,
          value: [0, 1, 2, 3, 4],
          writeValue: "",
          timestamp: timestamp,
          quality: "VALID",
        },
        "showAttribute": "Label",
        "yAxis": "left",
        "lineColor": "#20b15f"
      }
    ]
    const res = TracesFromAttributeInputs(complexInput, 1690541115.955);

    expect(res[0].fullName).toEqual('sys/tg_test/1/long_scalar')
    expect(res[0].y).toEqual([228,228,91])

    expect(res[1].fullName).toEqual('sys/tg_test/1/short_scalar')
    expect(res[1].y).toEqual([170, 170, 90])
  });

  let plotParams = {
    height: 120,
    width: 120,
    staticMode: true,
    timeWindow: 120,
    textColor: "black",
    backgroundColor: "white"
  };

  let trace = {
    x: [1, 2, 4],
    y: [2, 4, 5],
    fullName: "AttributePlot",
    axisLocation: "left",
    yAxisDisplay: "yAxis",
    position: 10,
    lineColor: "black"
  };

  it("Testing Plot", () => {
    const res = dataAndRange([trace], plotParams, "TIME_WINDOW")
    expect(res.data[0].x).toEqual(trace.x);
    expect(res.data[0].y).toEqual(trace.y);
    expect(res.data[0].yaxis).toEqual("y1");
  });

  it("Testing Plot right side yaxis", () => {

    let trace = {
      x: [],
      y: [2, 4, 5],
      fullName: "AttributePlot",
      axisLocation: "right",
      yAxisDisplay: "yAxis",
      position: 10,
      lineColor: "black"
    };

    const res = dataAndRange([trace], plotParams, "");
    expect(res.data[0].y).toEqual(trace.y);
    expect(res.data[0].yaxis).toEqual("y2");
  });
});

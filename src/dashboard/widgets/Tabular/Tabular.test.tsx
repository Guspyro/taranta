import React from "react";
import { configure, shallow } from "enzyme";
import Adapter from "@wojtekmaj/enzyme-adapter-react-17";
import Tabular from "./Tabular";

configure({ adapter: new Adapter() });

function input(
  device = "sys/tg_test/1",
  customAttribute: any = "",
  showDefaultAttribute = true,
  borderedTable = false,
  compactTable = false,
  css = "",
  font = 1,
  fontFamily = "Courier new"
) {
  customAttribute = customAttribute || "CustomState";
  return {
    devices: [
      {
        device: {
          name: device,
          alias: "",
        },
      },
    ],
    showDefaultAttribute: showDefaultAttribute,
    precision: 2,
    scientificNotation: false,
    showEnumLabels: false,
    customAttributes: [
      {
        attribute: {
          device: "",
          attribute: "state",
          label: "State",
          value: "On",
          writeValue: 0,
          timestamp: 1668079933.435179,
          dataFormat: "scalar",
          dataType: "DevDouble",
          enumlabels: [],
          history: [],
          isNumeric: true,
          unit: "",
          write: () => {},
          quality: "VALID",
        },
      },
    ],
    widgetCss: css,
    compactTable: compactTable,
    borderedTable: borderedTable,
    textColor: "#000000",
    backgroundColor: "#ffffff",
    size: font,
    font: fontFamily,
  };
}

describe("AttributeDisplayTests", () => {
  it("renders without crashing", () => {
    let element = React.createElement(Tabular.component, {
      mode: "run",
      t0: 1,
      id: 42,
      actualWidth: 100,
      actualHeight: 100,
      inputs: input(),
    });
    expect(shallow(element).html()).toContain("TabularView");
  });

  it("renders devices", () => {
    const device = "unit/test/device";

    let element = React.createElement(Tabular.component, {
      mode: "run",
      t0: 1,
      id: 42,
      actualWidth: 100,
      actualHeight: 100,
      inputs: input(device),
    });
    expect(shallow(element).html()).toContain(device);
  });

  it("renders attributes", () => {
    const device = "dserver/databaseds/2";
    const customAttributes = {
      device: "dserver/databaseds/2",
      attribute: "status",
      label: "Status",
      history: [],
      isNumeric: false,
    };

    let element = React.createElement(Tabular.component, {
      mode: "run",
      t0: 1,
      id: 42,
      actualWidth: 100,
      actualHeight: 100,
      inputs: input(device, customAttributes),
    });

    expect(shallow(element).html()).toContain("state");
  });

  it("test table style", () => {
    const device = "unit/test/device";
    const customAttributes = "UnitTestAttribute";
    const borderedTable = true;
    const compactTable = true;

    let element = React.createElement(Tabular.component, {
      mode: "run",
      t0: 1,
      id: 42,
      actualWidth: 100,
      actualHeight: 100,
      inputs: input(device, customAttributes, false),
    });
    expect(shallow(element).html()).not.toContain("table-bordered");
    expect(shallow(element).html()).not.toContain("table-sm");

    element = React.createElement(Tabular.component, {
      mode: "run",
      t0: 1,
      id: 42,
      actualWidth: 100,
      actualHeight: 100,
      inputs: input(
        device,
        customAttributes,
        false,
        borderedTable,
        compactTable
      ),
    });
    expect(shallow(element).html()).toContain("table-bordered");
    expect(shallow(element).html()).toContain("table-sm");
  });

  it("test css and font", () => {
    const device = "unit/test/device";
    const customAttributes = "UnitTestAttribute";
    const css = "background-color:red;";
    const font = 0.7;
    const family = "Helvetica";

    let element = React.createElement(Tabular.component, {
      mode: "run",
      t0: 1,
      id: 42,
      actualWidth: 100,
      actualHeight: 100,
      inputs: input(device, customAttributes, false, true, true),
    });
    expect(
      shallow(element)
        .find("#TabularView")
        .prop("style")
    ).not.toHaveProperty("backgroundColor", "red");
    expect(
      shallow(element)
        .find("#TabularView")
        .prop("style")
    ).toHaveProperty("fontSize", "1em");
    expect(
      shallow(element)
        .find("#TabularView")
        .prop("style")
    ).toHaveProperty("fontFamily", "Courier new");

    element = React.createElement(Tabular.component, {
      mode: "run",
      t0: 1,
      id: 42,
      actualWidth: 100,
      actualHeight: 100,
      inputs: input(
        device,
        customAttributes,
        false,
        true,
        true,
        css,
        font,
        family
      ),
    });

    expect(
      shallow(element)
        .find("#TabularView")
        .prop("style")
    ).toHaveProperty("backgroundColor", "red");
    expect(
      shallow(element)
        .find("#TabularView")
        .prop("style")
    ).toHaveProperty("fontSize", "0.7em");
    expect(
      shallow(element)
        .find("#TabularView")
        .prop("style")
    ).toHaveProperty("fontFamily", family);
  });
});

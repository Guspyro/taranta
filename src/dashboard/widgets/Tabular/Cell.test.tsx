import React from "react";
import { render } from "@testing-library/react";
import { useSelector as useSelectorMock } from "react-redux";
import Cell from "./Cell";
import {
  getAttributeLastTimeStampFromState as getAttributeLastTimeStampFromStateMock,
  getAttributeLastValueFromState as getAttributeLastValueFromStateMock,
} from "../../../shared/utils/getLastValueHelper";
import { AttributeInput } from "../../types";

jest.mock("../../../shared/utils/getLastValueHelper", () => ({
  getAttributeLastValueFromState: jest.fn(),
  getAttributeLastTimeStampFromState: jest.fn(),
}));

jest.mock("react-redux", () => ({
  useSelector: jest.fn(),
}));

// cast as jest.Mock
const getAttributeLastValueFromState = getAttributeLastValueFromStateMock as jest.Mock;
const getAttributeLastTimeStampFromState = getAttributeLastTimeStampFromStateMock as jest.Mock;

const useSelector = useSelectorMock as jest.Mock;

jest.mock("../../../shared/utils/getLastValueHelper", () => ({
  getAttributeLastValueFromState: jest.fn(),
  getAttributeLastTimeStampFromState: jest.fn(),
}));

let attribute_obj: AttributeInput = {
  device: "sys/tg_test/1",
  attribute: "state",
  label: "State",
  history: [],
  dataType: "",
  enumlabels: [],
  dataFormat: "scalar",
  isNumeric: false,
  unit: "",
  minAlarm: 0,
  maxAlarm: 0,
  write: () => null,
  value: "",
  writeValue: "",
  quality: "",
  timestamp: new Date().getTime(),
};

describe("AttributeValues", () => {
  beforeEach(() => {
    useSelector.mockImplementation((selectorFn) =>
      selectorFn({
        messages: {},
        ui: {
          mode: "run",
        },
      })
    );

    getAttributeLastValueFromState.mockReturnValue("RUNNING");

    getAttributeLastTimeStampFromState.mockReturnValue(Date.now() / 1000);
  });

  afterEach(() => {
    jest.resetAllMocks();
  });

  it("should render the cell with value RUNNING", () => {
    const { container } = render(
      <table>
        <tbody>
          <tr>
            <Cell
              attribute_obj={attribute_obj}
              showEnumLabels={false}
              precision={2}
              scientificNotation={false}
            />
          </tr>
        </tbody>
      </table>
    );
    expect(container.innerHTML).toContain("RUNNING");
  });

  it("should render the cell with number integer value", () => {
    getAttributeLastValueFromState.mockReturnValue(123);

    getAttributeLastTimeStampFromState.mockReturnValue(1683279159.668973);
    const { container } = render(
      <table>
        <tbody>
          <tr>
            <Cell
              attribute_obj={attribute_obj}
              showEnumLabels={false}
              precision={2}
              scientificNotation={false}
            />
          </tr>
        </tbody>
      </table>
    );
    expect(container.innerHTML).toContain("123");
  });

  it("should render the cell with number float value", () => {
    getAttributeLastValueFromState.mockReturnValue(123.12245457);

    getAttributeLastTimeStampFromState.mockReturnValue(1683279159.668973);
    const { container } = render(
      <table>
        <tbody>
          <tr>
            <Cell
              attribute_obj={attribute_obj}
              showEnumLabels={false}
              precision={2}
              scientificNotation={false}
            />
          </tr>
        </tbody>
      </table>
    );
    expect(container.innerHTML).toContain("123.12");
  });

  it("should render the cell with JSON value", () => {
    getAttributeLastValueFromState.mockReturnValue(
      '{ "routes": [ { "src": { "channel": 90 }\
    , "dst": { "port": 16 } }\
    , { "src": { "channel": 344 },\
    "dst": { "port": 27 } } ] }'
    );

    getAttributeLastTimeStampFromState.mockReturnValue(Date.now() / 1000);
    const { container } = render(
      <table>
        <tbody>
          <tr>
            <Cell
              attribute_obj={attribute_obj}
              showEnumLabels={false}
              precision={2}
              scientificNotation={false}
            />
          </tr>
        </tbody>
      </table>
    );
    expect(container.innerHTML).toContain("routes");
  });

  it("should render the cell with showEnumLabels", () => {
    attribute_obj.enumlabels = ["ON", "OFF"];

    getAttributeLastValueFromState.mockReturnValue(1);

    getAttributeLastTimeStampFromState.mockReturnValue(Date.now() / 1000);
    const { container } = render(
      <table>
        <tbody>
          <tr>
            <Cell
              attribute_obj={attribute_obj}
              showEnumLabels={true}
              precision={2}
              scientificNotation={false}
            />
          </tr>
        </tbody>
      </table>
    );
    expect(container.innerHTML).toContain("OFF");
  });

  it("should render the cell with showEnumLabels value out of range", () => {
    attribute_obj.enumlabels = ["ON", "OFF"];

    getAttributeLastValueFromState.mockReturnValue("2");

    getAttributeLastTimeStampFromState.mockReturnValue(Date.now() / 1000);
    const { container } = render(
      <table>
        <tbody>
          <tr>
            <Cell
              attribute_obj={attribute_obj}
              showEnumLabels={true}
              precision={2}
              scientificNotation={false}
            />
          </tr>
        </tbody>
      </table>
    );
    expect(container.innerHTML).toContain("2");
  });

  it("should render the cell with scientificNotation", () => {
    getAttributeLastValueFromState.mockReturnValue(10000);

    getAttributeLastTimeStampFromState.mockReturnValue(Date.now() / 1000);
    const { container } = render(
      <table>
        <tbody>
          <tr>
            <Cell
              attribute_obj={attribute_obj}
              showEnumLabels={false}
              precision={2}
              scientificNotation={true}
            />
          </tr>
        </tbody>
      </table>
    );
    expect(container.innerHTML).toContain("1.00e+4");
  });

  it("should render the cell with DevState value", () => {
    attribute_obj.dataType = "DevState";

    getAttributeLastValueFromState.mockReturnValue("ON");

    getAttributeLastTimeStampFromState.mockReturnValue(Date.now() / 1000);
    const { container } = render(
      <table>
        <tbody>
          <tr>
            <Cell
              attribute_obj={attribute_obj}
              showEnumLabels={false}
              precision={2}
              scientificNotation={false}
            />
          </tr>
        </tbody>
      </table>
    );
    expect(container.innerHTML).toContain("stateTango ON");
  });

  it("should render the cell with undefined value", () => {
    getAttributeLastValueFromState.mockReturnValue(undefined);

    getAttributeLastTimeStampFromState.mockReturnValue(Date.now() / 1000);
    const { container } = render(
      <table>
        <tbody>
          <tr>
            <Cell
              attribute_obj={attribute_obj}
              showEnumLabels={false}
              precision={2}
              scientificNotation={false}
            />
          </tr>
        </tbody>
      </table>
    );
    expect(container.innerHTML).toContain(">...<");
  });
});

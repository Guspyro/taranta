import React from "react";
import { AttributeInput } from "../../types";
import { configure, shallow } from "enzyme";
import Adapter from "@wojtekmaj/enzyme-adapter-react-17";
import AttributeScatterExport from "./AttributeScatter";

const PlotlyComponent = () => <div>PlotlyComponent</div>;
jest.mock("./AttributeScatterValues", () => PlotlyComponent);

configure({ adapter: new Adapter() });

describe("Attribute Scatter Tests", () => {
  let independentInput: AttributeInput<any>;
  let dependentInput: AttributeInput<any>;
  let myInput: any;
  var date = new Date();
  var timestamp = date.getTime();
  var writeArray: any = [];

  interface JsonObject {
    timestamp: number;
    value: number;
    writeValue: string;
    quality: string;
  }

  function generateHistoryObject(size: number): JsonObject[] {
    const jsonArray: JsonObject[] = [];
    for (let i = 0; i < size; i++) {
      const historyObject: JsonObject = {
        timestamp: Math.floor(Math.random() * 100),
        value: Math.floor(Math.random() * 10),
        writeValue: "",
        quality: "",
      };
      jsonArray.push(historyObject);
    }

    return jsonArray;
  }

  beforeEach(() => {
    dependentInput = {
      device: "sys/tg_test/1",
      attribute: "time_range",
      label: "TimeRange",
      isNumeric: true,
      value: -1,
      history: [
        {
          timestamp: timestamp, //This is used as currX
          value: -2,
          writeValue: "",
          quality: "",
        },
        {
          timestamp: timestamp, //This is used as nextX
          value: -3,
          writeValue: "",
          quality: "",
        },
      ],
      dataType: "",
      dataFormat: "",
      unit: "",
      enumlabels: [],
      write: writeArray,
      writeValue: "",
      quality: "",
      timestamp: timestamp, //This is used as Input
    };
    independentInput = {
      device: "sys/tg_test/2",
      attribute: "time_range",
      label: "TimeRange",
      isNumeric: true,
      value: 1,
      history: [
        {
          timestamp: timestamp,
          value: 2,
          writeValue: "",
          quality: "",
        },
      ],
      dataType: "",
      dataFormat: "",
      unit: "",
      enumlabels: [],
      write: writeArray,
      writeValue: "",
      quality: "",
      timestamp: timestamp,
    };

    myInput = {
      dependent: dependentInput,
      independent: independentInput,
      showAttribute: "Label",
    };
  });

  it("does not throw exception when mode is 'run' and history is '1000' elements", () => {
    dependentInput.history = generateHistoryObject(1000);
    independentInput.history = generateHistoryObject(1000);
    dependentInput.timestamp += 200;
    independentInput.timestamp += 100;

    myInput = {
      dependent: dependentInput,
      independent: independentInput,
      showAttribute: "Label",
    };

    const { component: AttributeScatterComponent } = AttributeScatterExport;

    const shallowElement = shallow(
      <AttributeScatterComponent
        mode="run"
        actualWidth={100}
        actualHeight={100}
        inputs={myInput}
        t0={1}
        id={123}
      />
    );

    expect(() => shallowElement.setProps({ inputs: myInput })).not.toThrow(
      "xs.length != ys.length"
    );
    expect(() => shallowElement.setProps({ inputs: myInput })).not.toThrow(
      "xs.length == 0"
    );
  });

  it("does not throw exception when mode is 'edit'", () => {
    const { component: AttributeScatterComponent } = AttributeScatterExport;

    const shallowElement = shallow(
      <AttributeScatterComponent
        mode="run"
        actualWidth={100}
        actualHeight={100}
        inputs={myInput}
        t0={1}
        id={123}
      />
    );

    expect(() => shallowElement.setProps({ inputs: myInput })).not.toThrow(
      "xs.length != ys.length"
    );
    expect(() => shallowElement.setProps({ inputs: myInput })).not.toThrow(
      "xs.length == 0"
    );
  });

  it("returns a PlotlyComponent when mode showAttribute is 'Name' and (input < currX || input >= nextX)", () => {
    dependentInput.history[0].timestamp += 100;
    dependentInput.history[1].timestamp -= 100;

    myInput = {
      dependent: dependentInput,
      independent: independentInput,
      showAttribute: "Name",
    };

    const { component: AttributeScatterComponent } = AttributeScatterExport;

    const shallowElement = shallow(
      <AttributeScatterComponent
        mode="run"
        actualWidth={100}
        actualHeight={100}
        inputs={myInput}
        t0={1}
        id={123}
      />
    );

    expect(shallowElement.setProps({ inputs: myInput }).name()).toBe(
      "PlotlyComponent"
    );
  });

  it("returns a PlotlyComponent when mode showAttribute is 'Name' and deltaX is not '0'", () => {
    dependentInput.history[0].timestamp -= 100;
    dependentInput.history[1].timestamp += 200;

    myInput = {
      dependent: dependentInput,
      independent: independentInput,
      showAttribute: "Name",
    };

    const { component: AttributeScatterComponent } = AttributeScatterExport;

    const shallowElement = shallow(
      <AttributeScatterComponent
        mode="run"
        actualWidth={100}
        actualHeight={100}
        inputs={myInput}
        t0={1}
        id={123}
      />
    );

    expect(shallowElement.setProps({ inputs: myInput }).name()).toBe(
      "PlotlyComponent"
    );
  });
});

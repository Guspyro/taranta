import React from "react";
import { configure, shallow } from "enzyme";
import Adapter from "@wojtekmaj/enzyme-adapter-react-17";
import Label from "./Label";

configure({ adapter: new Adapter() });

describe("Label", () => {
  it("renders attributes correctly", () => {
    let element = React.createElement(Label.component, {
      mode: "run",
      t0: 1,
      id: 42,
      actualWidth: 25,
      actualHeight: 25,
      inputs: {
        linkTo: "http://myLink.com",
        backgroundColor: "#ffffff",
        textColor: "#000000",
        size: 1,
        automaticResize: "Disabled",
        font: "Helvetica",
        borderWidth: 0,
        borderColor: "#000000",
        customCss: "",
        text: "My Link",
      },
    });
    expect(shallow(element).html()).toContain("http://myLink.com");
  });

  it("renders attributes correctly when automaticResize is enabled", () => {
    let element = React.createElement(Label.component, {
      mode: "run",
      t0: 1,
      id: 42,
      actualWidth: 25,
      actualHeight: 25,
      inputs: {
        linkTo: "http://myLink.com",
        backgroundColor: "#ffffff",
        textColor: "#000000",
        size: 1,
        automaticResize: "Enabled",
        font: "Helvetica",
        borderWidth: 0,
        borderColor: "#000000",
        customCss: "",
        text: "My Link",
      },
    });
    expect(shallow(element).html()).toContain('<svg viewBox="0 0 56 18">');
  });

  it("renders label with empty text when text is 'empty' and mode is 'edit'", () => {
    let element = React.createElement(Label.component, {
      mode: "edit",
      t0: 1,
      id: 42,
      actualWidth: 25,
      actualHeight: 25,
      inputs: {
        text: "",
        linkTo: "",
        backgroundColor: "",
        textColor: "",
        size: 1,
        automaticResize: "Disabled",
        font: "",
        borderWidth: 200,
        borderColor: "",
        customCss: "",
      },
    });
    expect(shallow(element).html()).not.toContain('<svg viewBox="0 0 56 18">');
    expect(shallow(element).html()).toContain(
      'div style=\"background-color:;color:;word-break:break-word;border:200em solid;height:25px;width:25px;overflow:hidden\"><div style=\"font-size:1em;color:;fill:;font-family:;text-align:left;vertical-align:top;line-height:1;align-items:flex-start;justify-content:flex-start\"></div></div>'
    );
  });

  it("renders label when mode is 'library'", () => {
    let element = React.createElement(Label.component, {
      mode: "library",
      t0: 1,
      id: 42,
      actualWidth: 25,
      actualHeight: 25,
      inputs: {
        text: "",
        linkTo: "",
        backgroundColor: "",
        textColor: "",
        size: 1,
        automaticResize: "Disabled",
        font: "",
        borderWidth: 200,
        borderColor: "",
        customCss: "",
      },
    });
    expect(shallow(element).html()).not.toContain('<svg viewBox="0 0 56 18">');
    expect(shallow(element).html()).toContain(
      '<div style=\"background-color:;color:;word-break:break-word;border:200em solid;height:25px;width:100%;overflow:hidden\"><div style=\"font-size:1em;color:;fill:;font-family:;text-align:left;vertical-align:top;line-height:1;align-items:flex-start;justify-content:flex-start\"></div></div>'
    );
  });
});

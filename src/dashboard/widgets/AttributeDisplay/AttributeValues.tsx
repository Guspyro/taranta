import React from "react";
import { IRootState } from "../../../shared/state/reducers/rootReducer";
import { useSelector } from "react-redux";
import {
  getAttributeLastQualityFromState,
  getAttributeLastTimeStampFromState,
  getAttributeLastValueFromState,
} from "../../../shared/utils/getLastValueHelper";
import { JSONTree, parseJSONObject } from "../../../shared/utils/JSONTree";
import "../Tabular/Tabular.css";
import numeral from "numeral";
import { sprintf } from "sprintf-js";

interface Props {
  attributeName: string;
  deviceName: string;
  dataType: string;
  mode: string;
  showAttrQuality: boolean;
  precision: number;
  format: string;
  scientificNotation: boolean;
  showEnumLabels: boolean;
  enumlabels: string[];
  unit: string | undefined;
  minAlarm?: number | undefined;
  maxAlarm?: number | undefined;
  alignValueRight: boolean;
}

const renderQuality = (mode: string, quality: string) => {
  if (mode !== "run") return null;

  const sub =
    {
      ATTR_VALID: "valid",
      ATTR_INVALID: "invalid",
      ATTR_CHANGING: "changing",
      ATTR_ALARM: "alarm",
      ATTR_WARNING: "warning",
    }[quality] || "invalid";

  return (
    <span
      style={{ marginLeft: "5px" }}
      className={`QualityIndicator ${sub}`}
      title={quality}
    >
      {sub.toUpperCase()}
    </span>
  );
};

const AttributeValues: React.FC<Props> = ({
  attributeName,
  deviceName,
  dataType,
  mode,
  showAttrQuality,
  precision,
  format,
  scientificNotation,
  showEnumLabels,
  enumlabels,
  unit,
  minAlarm,
  maxAlarm,
  alignValueRight,
}) => {
  const unitSuffix = unit ? ` ${unit} ` : "";
  const value = useSelector((state: IRootState) => {
    return getAttributeLastValueFromState(
      state.messages,
      deviceName,
      attributeName
    );
  });

  const timestamp = useSelector((state: IRootState) => {
    return getAttributeLastTimeStampFromState(
      state.messages,
      deviceName,
      attributeName
    )?.toString();
  });

  const quality = useSelector((state: IRootState) => {
    return getAttributeLastQualityFromState(
      state.messages,
      deviceName,
      attributeName
    )?.toString();
  });

  let backgroundColor = "";
  if (maxAlarm && minAlarm && value !== undefined) {
    if (value >= maxAlarm || value <= minAlarm) backgroundColor = "#ffaaaa";
  }

  const cssObject = {
    backgroundColor: backgroundColor,
    marginLeft: alignValueRight ? "auto" : "",
    height: "fit-content",
  };
  let valueDisplay: string | number | JSX.Element | null = "...";

  if (value !== undefined) {
    const parsedValue = parseJSONObject(value);
    if (parsedValue !== null) {
      valueDisplay = (
        <div className="json-tree">
          <JSONTree data={parsedValue} />
        </div>
      );
    } else {
      if (typeof value === "number") {
        if (showEnumLabels && enumlabels?.length > 0)
          valueDisplay = enumlabels?.[value] || value;
        else if (format !== "") {
          if (format.startsWith("%"))
            try {
              valueDisplay = sprintf(format, value);
            } catch (e) {
              valueDisplay = "Format not valid";
            }
          else 
            valueDisplay = numeral(value).format(format);
        } else if (scientificNotation)
          valueDisplay = value.toExponential(precision);
        else
          valueDisplay = Number.isInteger(value)
            ? value
            : Number(value).toFixed(precision);
      } else valueDisplay = String(value);
    }
  }

  return (
    <div
      title={new Date(timestamp * 1000).toUTCString()}
      style={cssObject}
      className={dataType === "DevState" ? "stateTango " + value : ""}
    >
      {valueDisplay !== undefined ? valueDisplay : "..."}
      {showAttrQuality ? renderQuality(mode, quality) : null} {unitSuffix}
    </div>
  );
};

export default AttributeValues;

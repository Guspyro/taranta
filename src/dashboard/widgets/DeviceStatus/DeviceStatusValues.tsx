import React, { CSSProperties } from "react";
import { useSelector } from "react-redux";
import {
  getAttributeLastTimeStampFromState,
  getAttributeLastValueFromState,
} from "../../../shared/utils/getLastValueHelper";
import { IRootState } from "../../../shared/state/reducers/rootReducer";
import { STATE_COLORS, brighten } from "../../colorUtils";
import { parseCss } from "../../components/Inspector/StyleSelector";

interface Props {
  device: string;
  showDeviceName: boolean;
  showStateString: boolean;
  showStateLED: boolean;
  LEDSize: number;
  textColor: string;
  backgroundColor: string;
  textSize: number;
}

const DeviceStatusValues: React.FC<Props> = ({
  device,
  showDeviceName,
  showStateString,
  showStateLED,
  LEDSize,
  textColor,
  backgroundColor,
  textSize,
}) => {
  let stateValue = useSelector((state: IRootState) => {
    return getAttributeLastValueFromState(
      state.messages,
      device,
      'state'
    );
  });

  const timestamp = useSelector((state: IRootState) => {
    return getAttributeLastTimeStampFromState(
      state.messages,
      device,
      'state'
    )?.toString();
  });

  return (
    <div title={new Date(timestamp * 1000).toUTCString()}>
      <Text
        device={device}
        textSize={textSize}
        textColor={textColor}
        backgroundColor={backgroundColor}
        showDeviceName={showDeviceName}
        showStateString={showStateString}
        state={stateValue}
        showStateLED={showStateLED}
      />
      {<LED LEDSize={LEDSize} state={stateValue} showStateLED={showStateLED} />}
    </div>
  );
};

const Text = (props: {
  widgetCss?: any;
  device?: any;
  textSize?: any;
  textColor?: any;
  backgroundColor?: any;
  showDeviceName?: any;
  showStateString?: any;
  state?: any;
  showStateLED?: any;
}) => {
  const {
    device,
    textSize,
    textColor,
    backgroundColor,
    showDeviceName,
    showStateString,
    state,
    showStateLED,
  } = props;
  const deviceName = showDeviceName ? device || "Device name" : "";
  const stateString = showStateString ? state || "STATE" : "";
  const widgetCss = parseCss(props.widgetCss).data;
  delete widgetCss["border"];

  const style: CSSProperties = {
    fontSize: textSize + "em",
    color: textColor,
    backgroundColor: backgroundColor,
    ...widgetCss,
  };
  if (showStateLED) {
    style["marginRight"] = "0.5em";
  }
  return (
    <span style={style}>
      {deviceName} {stateString}
    </span>
  );
};

const LED = (props: {
  size?: any;
  LEDSize?: any;
  state?: any;
  showStateLED?: any;
}) => {
  const { LEDSize, state, showStateLED } = props;
  if (!showStateLED) {
    return null;
  }
  const stateValue = state;
  const color: string = STATE_COLORS[stateValue || "UNKNOWN"];
  const emSize = 1 * LEDSize + "em";
  let baseColor = color;
  let highLightColor = brighten(color, 30);
  let borderColor = color;

  switch (color) {
    case "green":
      baseColor = brighten(baseColor, 15);
      highLightColor = brighten(highLightColor, 15);
      borderColor = brighten(borderColor, 15);
      break;
    case "white":
      borderColor = "#aaa";
      baseColor = "#f8f8f8";
      break;
    case "lightblue":
      baseColor = brighten(baseColor, -10);
      highLightColor = brighten(highLightColor, -10);
      break;
    case "yellow":
      baseColor = brighten(baseColor, -5);
      highLightColor = brighten(highLightColor, -5);
      borderColor = brighten(borderColor, -5);
      break;
    case "red":
      borderColor = brighten(borderColor, -5);
      break;
    case "beige":
      baseColor = brighten(baseColor, -15);
      highLightColor = brighten(highLightColor, -15);
      borderColor = brighten(borderColor, -15);
      break;
    case "darkgreen":
      borderColor = brighten(borderColor, 5);
      break;
    case "magenta":
      borderColor = brighten(borderColor, -5);
      break;
    case "grey":
      borderColor = brighten(borderColor, 5);
      break;
    default:
      break;
  }
  const background =
    "radial-gradient(circle, " +
    highLightColor +
    " 0%, " +
    baseColor +
    " 100%)";
  const border = props.size / 50 + "em solid " + borderColor;
  return (
    <div style={{ display: "inline-block", verticalAlign: "text-bottom" }}>
      <div
        style={{
          width: emSize,
          height: emSize,
          background,
          borderRadius: "50%",
          border,
        }}
        title={stateValue}
      ></div>
    </div>
  );
};

export default DeviceStatusValues;

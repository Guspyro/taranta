import React from "react";
import { AttributeInput, DeviceInput, CommandInput } from "../../types";
import { configure, shallow } from "enzyme";
import Adapter from "@wojtekmaj/enzyme-adapter-react-17";
import SimpleMotor from "./SimpleMotor";
import { Inputs } from "./SimpleMotor";
import { TypedInputs } from "../types";

configure({ adapter: new Adapter() });

describe("SimpleMotor", () => {
  let doubleScalarAttribute: AttributeInput,
    ulongScalarAttribute: AttributeInput;
  let myInput: TypedInputs<Inputs>;

  it("renders run mode true without issues", () => {
    doubleScalarAttribute = {
      device: "sys/tg_test/1",
      attribute: "double_scalar",
      label: "DoubleScalar",
      history: [],
      dataType: "",
      dataFormat: "",
      isNumeric: true,
      unit: "",
      enumlabels: [],
      write: () => { },
      value: [1, 2, 3, 4, 5],
      timestamp: 0,
      writeValue: "",
      quality: ""
    }

    ulongScalarAttribute = {
      device: "sys/tg_test/1",
      attribute: "ulong_scalar",
      label: "UlongScalar",
      history: [],
      dataType: "",
      dataFormat: "",
      isNumeric: true,
      unit: "",
      enumlabels: [],
      write: () => { },
      value: [6, 7, 8, 9, 10],
      timestamp: 0,
      writeValue: "",
      quality: ""
    }

    myInput = {
      device: {
        name: "sys/tg_test/1",
        alias: "sys/tg_test/1"
      },
      state: {
        device: "sys/tg_test/1",
        command: "State",
        output: "On",
        execute: () =>{}
      },
      status: {
        device: "sys/tg_test/1",
        command: "Status",
        output: "It is On",
        execute: () =>{}
      },
      doubleScalar: doubleScalarAttribute,
      ulongScalar: ulongScalarAttribute,      
    };

    const element = React.createElement(SimpleMotor.component, {
      mode: "run",
      t0: 1,
      inputs: myInput,
      actualWidth: 100,
      actualHeight: 100,
      id: 1,
    });
    let ulongScalar = ulongScalarAttribute.value.map((value: { toString: () => any; }) => value.toString()).join("");
    let doubleScalar = doubleScalarAttribute.value.map((value: { toString: () => any; }) => value.toString()).join("");
    expect(shallow(element).html()).toContain(`ULong Scalar: ${ulongScalar}`);
    expect(shallow(element).html()).toContain(`Double Scalar: ${doubleScalar}`);

  });

});
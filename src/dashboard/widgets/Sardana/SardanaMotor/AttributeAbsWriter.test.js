import React from "react";
import { configure, shallow, mount } from "enzyme";
import { render } from "@testing-library/react";

import Adapter from "@wojtekmaj/enzyme-adapter-react-17";
import { AttributeAbsWriter } from "./AttributeAbsWriter";

configure({ adapter: new Adapter() });

describe("AttributeAbsWritter", () => {

    it("renders run mode true without issues", () => {
        const props = {
            writeValue: 10,
            state: "ON",
            onSetPosition: () => { },
            onStop: () => { },
            mode: 'run',
            t0: 1,
            actualWidth: 100,
            actualHeight: 100,
            id: 1,
        }

        const element = React.createElement(AttributeAbsWriter, props);
        const shallowElement = shallow(element);

        expect(shallowElement.html()).toContain('Move');
        shallowElement.find("button.movement").simulate('click');
        shallowElement.find(".change-type").simulate('click');
    });

    it("renders run mode in moving state", () => {
        const props = {
            writeValue: 10,
            state: "MOVING",
            onSetPosition: () => { },
            onStop: () => { },
            mode: 'run',
            t0: 1,
            actualWidth: 100,
            actualHeight: 100,
            id: 1,
        }
        const element = React.createElement(AttributeAbsWriter, props);
        const shallowElement = shallow(element);

        expect(shallowElement.html()).toContain('Stop');
        shallowElement.find("button.movement").simulate('click');
        shallowElement.find(".change-type").simulate('click');
    });

    it("renders run mode and input change event works", () => {
        const props = {
            writeValue: 10,
            state: "ON",
            onSetPosition: () => { },
            onStop: () => { },
            mode: 'run',
            t0: 1,
            actualWidth: 100,
            actualHeight: 100,
            id: 1,
        }
        const element = React.createElement(AttributeAbsWriter, props);
        const shallowElement = shallow(element);

        const input = shallowElement.find('input');

        input.simulate('change', { target: { value: '10' } });
    });
});
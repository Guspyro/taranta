import React from "react";
import { configure, shallow } from "enzyme";
import Adapter from "@wojtekmaj/enzyme-adapter-react-17";
import { AttributeStepWriter } from "./AttributeStepWriter";

configure({ adapter: new Adapter() });

describe("AttributeStepWriter", () => {
    it("renders run mode in moving state", () => {
        const props = {
            writeValue: 10,
            state: 'ON',
            onStop: () => { },
            mode: 'run',
            onSetPosition: () => { },
            t0: 1,
            actualWidth: 100,
            actualHeight: 100,
            id: 1,
        }
        const element = React.createElement(AttributeStepWriter, props);
        const shallowElement = shallow(element);

        expect(shallowElement.find("div.attributeStepWriter").exists()).toBe(true);
        shallowElement.find("button.btn").at(0).simulate('click');
        shallowElement.find("button.btn").at(1).simulate('click');
    });
});
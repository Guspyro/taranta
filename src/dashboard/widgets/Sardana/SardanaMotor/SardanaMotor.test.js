import React from "react";
import { configure, mount } from "enzyme";
import Adapter from "@wojtekmaj/enzyme-adapter-react-17";
import configureStore from "redux-mock-store";
import SardanaMotor from "./SardanaMotor";
import { Provider } from "react-redux";

configure({ adapter: new Adapter() });
const mockStore = configureStore([]);

describe("SardanaMotor", () => {
  let myInput;
  const precisionAttribute = { type: "number", nonNegative: false };
  const limitsAttribute = { value: [1, 2] };
  const powerAttribute = { write: () => { }, };

  const positionAttribute = {
    device: "tg_test",
    attribute: "position",
    label: "LongScalar",
    history: [],
    dataType: "",
    dataFormat: "",
    isNumeric: true,
    unit: "",
    enumlabels: [],
    write: () => { },
    value: [1, 2, 3, 4, 5],
    timestamp: 0,
    writeValue: "",
    quality: ""
  }

  it("renders run mode true without issues", () => {

    const store = mockStore({
      user: {
        username: 'user1'
      },
      deviceDetail: {
        activeDataFormat: 'SCALAR',
        disabledDisplevels: ['1'],
      },
      messages: {
        tg_test: {
          attributes: {
            power: {
              values: [54],
              timestamp: [null]
            },
            state: {
              values: ["OK"],
              timestamp: [null]
            },
            position: {
              values: [51],
              timestamp: [null]
            }
          }
        }
      }
    });

    myInput = {
      device: {
        name: "tg_test",
        alias: "test"
      },
      precision: precisionAttribute,
      position: positionAttribute,
      limits: limitsAttribute,
      power: powerAttribute,
    };

    const element = React.createElement(SardanaMotor.component, {
      mode: "run",
      t0: 1,
      inputs: myInput,
      actualWidth: 100,
      actualHeight: 100,
      id: 1,
    });
    let wrapper = mount(<Provider store={store}>{element}</Provider>);
    expect(wrapper.find("#SardanaMotor").exists()).toBe(true);
    expect(wrapper.text()).toContain("OK");
    
  });

  it("renders run mode without power value", () => {
    const store = mockStore({
      user: {
        username: 'user1'
      },
      deviceDetail: {
        activeDataFormat: 'SCALAR',
        disabledDisplevels: ['1'],
      },
      messages: {
        tg_test: {
          attributes: {
            power: {
              values: [null],
              timestamp: [null]
            },
            state: {
              values: ["OK"],
              timestamp: [null]
            },
            position: {
              values: [51],
              timestamp: [null]
            }
          }
        }
      }
    });

    myInput = {
      device: {
        name: "tg_test",
        alias: "test"
      },
      precision: precisionAttribute,
      position: positionAttribute,
      limits: limitsAttribute,
      power: powerAttribute,
    };
    
    const element = React.createElement(SardanaMotor.component, {
      mode: "run",
      t0: 1,
      inputs: myInput,
      actualWidth: 100,
      actualHeight: 100,
      id: 1,
    });
    let wrapper = mount(<Provider store={store}>{element}</Provider>);
    expect(wrapper.find("#SardanaMotor").exists()).toBe(true);
    expect(wrapper.text()).toContain("Power is off");
    wrapper.find("button.btn-stopped").at(0).simulate('click');
  });


  it("renders run mode without power value", () => {
    const positionAttributeNulled = {
      device: "tg_test",
      attribute: "position",
      label: "LongScalar",
      history: [],
      dataType: "",
      dataFormat: "",
      isNumeric: true,
      unit: "",
      enumlabels: [],
      write: () => { },
      value: [null],
      timestamp: 0,
      writeValue: "",
      quality: ""
    }

    const store = mockStore({
      user: {
        username: 'user1'
      },
      deviceDetail: {
        activeDataFormat: 'SCALAR',
        disabledDisplevels: ['1'],
      },
      messages: {
        tg_test: {
          attributes: {
            power: {
              values: [null],
              timestamp: [null]
            },
            state: {
              values: ["OK"],
              timestamp: [null]
            },
            position: {
              values: [51],
              timestamp: [null]
            }
          }
        }
      }
    });

    myInput = {
      device: {
        name: "tg_test",
        alias: "test"
      },
      precision: precisionAttribute,
      position: positionAttributeNulled,
      limits: limitsAttribute,
      power: powerAttribute,
    };
    
    const element = React.createElement(SardanaMotor.component, {
      mode: "run",
      t0: 1,
      inputs: myInput,
      actualWidth: 100,
      actualHeight: 100,
      id: 1,
    });
    let wrapper = mount(<Provider store={store}>{element}</Provider>);
    expect(wrapper.isEmptyRender())
  });
});
import React from "react";
import { configure, shallow } from "enzyme";
import Adapter from "@wojtekmaj/enzyme-adapter-react-17";
import { MultiDialWriter } from "./MultiDialWriter";

configure({ adapter: new Adapter() });

describe("MultiDialWriter", () => {
    it("renders run mode in moving state", () => {
        const props = {
            value: 10,
            precision: 1,
            maxMagnitude: 2,
            mode: 'run',
            state: 'ON',
            onSetPosition: () => { },
            t0: 1,
            actualWidth: 100,
            actualHeight: 100,
            id: 1,
        }
        const element = React.createElement(MultiDialWriter, props);
        const shallowElement = shallow(element);

        expect(shallowElement.html()).toContain('Set position');
        shallowElement.find("button.btn-sign").simulate('click');
        shallowElement.find("button.btn-copy").at(0).simulate('click');
        shallowElement.find("button.btn-up").at(0).simulate('click');
        shallowElement.find("button.btn-down").at(0).simulate('click');
    });

    it("renders run mode in moving state", () => {
        const precision = 2;
        const maxMagnitude = 2;

        const props = {
            value: 10.3,
            precision,
            maxMagnitude,
            mode: 'run',
            state: 'MOVING',
            onSetPosition: () => { },
            t0: 1,
            actualWidth: 100,
            actualHeight: 100,
            id: 1,
        }
        const element = React.createElement(MultiDialWriter, props);
        const shallowElement = shallow(element);

        expect(shallowElement.html()).toContain('Set position');
        shallowElement.find("button.btn-sign").simulate('click');
        shallowElement.find("button.btn-copy").at(1).simulate('click');

        for (let i = 0; i < precision + maxMagnitude; i++) {
            shallowElement.find("button.btn-up").at(i).simulate('click');
            shallowElement.find("button.btn-down").at(i).simulate('click');
        }
    });
});
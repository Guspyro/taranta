import React from "react";
import { DeviceInput } from "../../../types";
import { fireEvent, render, waitFor } from "@testing-library/react";
import Enzyme from "enzyme";
import TangoAPI from "../../../../shared/api/tangoAPI";
import { useSelector as useSelectorMock, useDispatch } from "react-redux";
import Adapter from "@wojtekmaj/enzyme-adapter-react-17";
import { getAttributeLastValueFromState as getAttributeLastValueFromStateMock } from "../../../../shared/utils/getLastValueHelper";
import MacroButton from "./MacroButton";

interface Inputs {
  door: DeviceInput;
  macroserver: DeviceInput;
  pool: DeviceInput;
  alignButtonRight: boolean;
  outerDivCss: string;
}

const getAttributeLastValueFromState = getAttributeLastValueFromStateMock as jest.Mock;
const executeCommand = TangoAPI.executeCommand as jest.Mock;
const fetchAttributes = TangoAPI.fetchAttributesValues as jest.Mock;

jest.mock("react-redux", () => ({
  ...jest.requireActual("react-redux"),
  useSelector: jest.fn(),
  useDispatch: jest.fn(),
}));

jest.mock("../../../../shared/utils/getLastValueHelper", () => ({
  getAttributeLastValueFromState: jest.fn(),
}));

jest.mock("../../../../shared/api/tangoAPI", () => ({
  executeCommand: jest.fn(),
  fetchAttributesValues: jest.fn(),
}))

const useSelector = useSelectorMock as jest.Mock;
Enzyme.configure({ adapter: new Adapter() });

const { component: MacroButtonComponent } = MacroButton;


describe("AttributeWriterDropdown", () => {
  const initialDoorInput: DeviceInput = {
    alias: "",
    name: "door/test/1"
  };

  const initialMacroServerInput: DeviceInput = {
    alias: "",
    name: "macroserver/test/1"
  };

  const initialPoolDeviceInput: DeviceInput = {
    alias: "",
    name: "pool/test/1"
  };

  const initialInputs: Inputs = {
    door: initialDoorInput,
    macroserver: initialMacroServerInput,
    pool: initialPoolDeviceInput,
    alignButtonRight: false,
    outerDivCss: "",
  };

  beforeEach(() => {
    const dispatch = jest.fn();

    useSelector.mockImplementation((selectorFn: any) =>
      selectorFn({
        messages: {
          "macroserver/test/1": {
            attributes: {
              macrolist: {
                values: [["macro1", "macro2"]],
              }
            }
          },
          "motor/test/1": {
            attributes: {
              motorlist: {
                values: [["mot01", "mot02"]],
              }
            }
          },
          "door/test/1": {
            attributes: {
              output: {
                values: ["output1", "outpu2"],
              },
              state: {
                values: ["RUNNING", "RUNNING"],
              }
            }
          }
        },
        ui: {
          mode: "run",
        },
      })
    );
    getAttributeLastValueFromState.mockImplementation((state, name, field) => {
      if (field === 'motorlist')
        return [JSON.stringify({name: "mot01"})]
      if (field === 'macrolist')
        return ['macro1', 'macro2']
      return ["RUNNING"]
    });

    executeCommand.mockReturnValue(
      ({ 
        output: [
          JSON.stringify({ 
            description: "description  Mock  test", 
            parameters: [{ value: "mot01", name: "mot01", type: "Moveable" }] 
          })
        ]
      })
    );

    fetchAttributes.mockReturnValue([{ value: ["error"] }]);
    (useDispatch as jest.Mock).mockReturnValue(dispatch);
  });

  afterEach(() => {
    jest.resetAllMocks();
  });

  it("renders an empty widget in edit/run mode without crashing", () => {
    const testInput = initialInputs;
    const props = {
      t0: 1,
      actualWidth: 32,
      actualHeight: 12,
      inputs: testInput,
    };

    const { container: elementInEdit } = render(
      <MacroButtonComponent
        mode="edit"
        id={42}
        {...props}
      />
    );

    expect(elementInEdit.innerHTML).toContain("door/test/1");
    expect(elementInEdit.innerHTML).toContain("Door output will be displayed here");

    const { container: elementInRun } = render(
      <MacroButtonComponent
        mode="run"
        id={42}
        {...props}
      />
    );
    expect(elementInRun.innerHTML).toContain("Door output will be displayed here");
    expect(elementInRun.innerHTML).toContain("Run");
    expect(elementInRun.innerHTML).toContain("Select Macro");
    const items = elementInEdit.getElementsByClassName("macro-list-dropdown")
    expect(items).toHaveLength(1)
    expect(elementInRun.innerHTML).toContain("Door output will be displayed here");
  }); //renders an empty widget in edit/run mode without crashing

  it('it displays selects with good options', async () => {
    const props = {
      t0: 1,
      actualWidth: 32,
      actualHeight: 12,
      inputs: initialInputs,
    };

    const element = render(
      <MacroButtonComponent
        mode="run"
        id={42}
        {...props}
      />
    );

    let { container: elementInRun } = element;

    expect(elementInRun.innerHTML).toContain("Run");
    expect(elementInRun.innerHTML).toContain("Select Macro");

    const selectElement = elementInRun.querySelectorAll('#macroListSelect')[0] as HTMLInputElement;

    await waitFor(() => {
      fireEvent.change(selectElement, { target: { value: 'macro1' } });
    })

    expect(await (selectElement).value).toBe('macro1');

    const selectMovalbleElement = elementInRun.querySelectorAll('#movableMacroSelect')[0] as HTMLInputElement;

    await waitFor(() => {
      fireEvent.change(selectMovalbleElement, { target: { value: 'mot01' } });
    })

    expect(await (selectMovalbleElement).value).toBe('mot01');

    const runButton = elementInRun.querySelectorAll('#executeButton')[0] as HTMLInputElement;

    await waitFor(() => {
      fireEvent.click(runButton);
    })
  })
});

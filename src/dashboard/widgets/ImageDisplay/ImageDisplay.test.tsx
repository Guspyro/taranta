import React from "react";
import { AttributeInput } from "../../types";

import { configure, shallow } from "enzyme";
import Adapter from "@wojtekmaj/enzyme-adapter-react-17";
import ImageDisplay from "./ImageDisplay";
import { useSelector as useSelectorMock } from "react-redux";
import { render } from "@testing-library/react";

jest.mock("react-redux", ()=> ({
  useSelector: jest.fn()
}))
const useSelector = useSelectorMock as jest.Mock;
configure({ adapter: new Adapter() });

interface Input {
  showAttribute: string;
  attribute: AttributeInput;
  scale: string;
  showDevice: boolean;
  textColor: string;
  backgroundColor: string;
  size: number;
  font: string;
  widgetCss: any;
}
const imgSample =
  "iVBORw0KGgoAAAANSUhEUgAAAPsAAAD7CAAAAACJGb16AAAC40lEQVR4nO3b227jMAxFUbLo//8y+6Ab5Sh902GR7jUdFM1gEB+TlBM5NQMAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA4H/x4icM9fMn39Jn89PJLouvzO7n7GXpdT0/km/PGP2rJL2s7n7MPlWEF2X3+ec08B5ekV6T3VvZ3w68mYXLw0uyt+C/ZQ83feUV2d1n+hY/Cx/JXR1ekL1HTyO/m8nV4e9n90f6HD58fg+3MO3MS+ru7e+6zu3aGdCv9dezu/uIf2r6dn3rwcXpb2cfybeRf2r93tILwwvmPRf+lL0Nu77st7O7u+X4/TrXzevbDC6NL6l77vu98P01zSj7J9XdfXzfF7zs0e/Cib+/1rW23xe8ZS1zfRY+aq2z0fZpwRvmMqdveLuc3VNMzwteeygtcyu3cLWT9Pxj7FPTb4PeH5FR7V3ksZ/dEEWD3kn3Kn0r/FjmyvYqtXvUthW+ZqdqkWe3tARWBrea7CZ+7frOV9Hzyu+FHVRlr696Wc//hegl2XvwqD4B8uxhY0cyLGrjK7OHWXuPOsKHWUTdCRBl77Fz2VPhi07A7ew9ko8f8tZEWCt8/ynUp+Bq9sh7czaDr8LH7Pv1f3TxJT3f36u1fvet8KPvK7pe0vNj0J93ncJm4c30Y383ezwG/XG/LXr6feyvHlF2v+fnoLdpz/GjjbzN+B91P27ebkjL3D7wacEz6Uonqru/LnPj3yIteB9W93F78c3N9VjpR/zLx5Ndr/u2zPnjE0UxrnMlhRdc3+eIvxY+8sjLr/CCee/9PpK/XuHTgnf9aDJJ3UfyU+HHda6nV1K8pk3Jn+lT0+v3MnSv54/JIl/nFIeSad6/v/3AaKSRlxxJJtsrPn+OOtZ1Tk+2Z3UON7OrDiNT3iM4fbKwLnnBXuXrY2V7tf/596QAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD+gh9FZWLIxuSZwgAAAABJRU5ErkJggg==";

let myAttributeInput: AttributeInput;
let myInput: Input;
var writeArray: any = [];
var date = new Date();
var timestamp = date.getTime();

describe("ImageDisplayTests", () => {
  beforeEach(() => {
    useSelector.mockImplementation((selectorFn) =>
      selectorFn({
        messages: {
          'sys/tg_test/1': {
            attributes: {
              long_scalar: {
                quality: [
                  "ATTR_VALID"
                ],
                timestamp: [
                    1675353614.375299
                ],
                values: [[[1,2,3],[1,2,3],[1,2,3]]]
              }
            }
          }
        },
        ui: {
          mode: "run",
        },
      })
    );
  });
  it("renders all false without crashing", () => {
    myAttributeInput = {
      device: "sys/tg_test/1",
      attribute: "double_image_ro",
      label: "double_image_ro",
      history: [],
      dataType: "image",
      dataFormat: "",
      isNumeric: false,
      unit: "",
      enumlabels: [],
      write: writeArray,
      value: [
        [0, 0],
        [0, 0],
      ],
      writeValue: "",
      timestamp: timestamp,
      quality: "VALID"
    };

    myInput = {
      showAttribute: "",
      attribute: myAttributeInput,
      scale: "",
      showDevice: false,
      textColor: "",
      backgroundColor: "",
      size: 1,
      font: "",
      widgetCss: []
    };
    window.HTMLCanvasElement.prototype.getContext = () => {
      return null;
    };
    const element = React.createElement(ImageDisplay.component, {
      mode: "run",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: myInput,
      id: 42
    });
    expect(shallow(element).html()).toContain("ImageDisplay");
    expect(shallow(element).html()).toContain('No data found for sys/tg_test/1/double_image_ro');
  });

  it("renders all true without crashing", () => {
    myAttributeInput = {
      device: "sys/tg_test/1",
      attribute: "",
      label: "",
      history: [],
      dataType: "",
      dataFormat: "",
      isNumeric: true,
      unit: "",
      enumlabels: [],
      write: writeArray,
      value: undefined,
      writeValue: "",
      timestamp: timestamp,
      quality: "VALID"
    };

    myInput = {
      showAttribute: "Name",
      attribute: myAttributeInput,
      scale: "1",
      showDevice: true,
      textColor: "black",
      backgroundColor: "white",
      size: 1,
      font: "Helvetica",
      widgetCss: []
    };
    window.HTMLCanvasElement.prototype.getContext = (cxt) => {
      return cxt;
    };

    const element = React.createElement(ImageDisplay.component, {
      mode: "edit",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: myInput,
      id: 42
    });

    expect(shallow(element).html()).toContain(
      "Failed at fetching the device attribute"
    );
    expect(shallow(element).html()).toContain(imgSample);
  });

  it("renders in edit mode before device and attribute are set", () => {
    myAttributeInput = {
      device: "sys/tg_test/1",
      attribute: "long_scalar",
      label: "long_scalar",
      history: [],
      dataType: "",
      dataFormat: "",
      isNumeric: true,
      unit: "",
      enumlabels: [],
      write: writeArray,
      value: [],
      writeValue: "",
      timestamp: timestamp,
      quality: "VALID"
    };

    myInput = {
      showAttribute: "Name",
      attribute: myAttributeInput,
      scale: "1",
      showDevice: true,
      textColor: "black",
      backgroundColor: "white",
      size: 1,
      font: "Helvetica",
      widgetCss: []
    };
    window.HTMLCanvasElement.prototype.getContext = (cxt) => {
      return cxt;
    };
    const element = React.createElement(ImageDisplay.component, {
      mode: "run",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: myInput,
      id: 42
    });

    expect(shallow(element).html()).toContain("Error found");
  });

  it("renders in edit mode for empty device", () => {
    myAttributeInput = {
      device: "",
      attribute: "long_scalar",
      label: "long_scalar",
      history: [],
      dataType: "",
      dataFormat: "",
      isNumeric: true,
      unit: "",
      enumlabels: [],
      write: writeArray,
      value: [],
      writeValue: "",
      timestamp: timestamp,
      quality: "VALID"
    };

    myInput = {
      showAttribute: "Name",
      attribute: myAttributeInput,
      scale: "1",
      showDevice: true,
      textColor: "black",
      backgroundColor: "white",
      size: 1,
      font: "Helvetica",
      widgetCss: []
    };
    window.HTMLCanvasElement.prototype.getContext = (cxt) => {
      return cxt;
    };
    const element = React.createElement(ImageDisplay.component, {
      mode: "run",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: myInput,
      id: 42
    });

    expect(shallow(element).html()).toContain("device");
  });

  it("does not display the device name if showDevice is not set", () => {
    myAttributeInput = {
      device: "sys/tg_test/1",
      attribute: "double_image_ro",
      label: "double_image_ro",
      history: [],
      dataType: "",
      dataFormat: "",
      isNumeric: true,
      unit: "",
      enumlabels: [],
      write: writeArray,
      value: [
        [0, 0],
        [0, 0],
      ],
      writeValue: "",
      timestamp: timestamp,
      quality: "VALID"
    };

    myInput = {
      showAttribute: "Name",
      attribute: myAttributeInput,
      scale: "1",
      showDevice: false,
      textColor: "black",
      backgroundColor: "white",
      size: 1,
      font: "Helvetica",
      widgetCss: []
    };
    window.HTMLCanvasElement.prototype.getContext = (cxt) => {
      return cxt;
    };

    const element = React.createElement(ImageDisplay.component, {
      mode: "edit",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: myInput,
      id: 42
    });
    expect(shallow(element).html()).not.toContain("sys/tg_test/1");
  });

  it("display the device attribute if showAttribute is set", () => {
    myAttributeInput = {
      device: "sys/tg_test/1",
      attribute: "double_image_ro",
      label: "double_image_ro",
      history: [],
      dataType: "",
      dataFormat: "",
      isNumeric: true,
      unit: "",
      enumlabels: [],
      write: writeArray,
      value: [
        [0, 0],
        [0, 0],
      ],
      writeValue: "",
      timestamp: timestamp,
      quality: "VALID"
    };

    myInput = {
      showAttribute: "Label",
      attribute: myAttributeInput,
      scale: "1",
      showDevice: false,
      textColor: "black",
      backgroundColor: "white",
      size: 1,
      font: "Helvetica",
      widgetCss: []
    };

    window.HTMLCanvasElement.prototype.getContext = (cxt) => {
      return cxt;
    };
    const element = React.createElement(ImageDisplay.component, {
      mode: "edit",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: myInput,
      id: 42
    });
    expect(shallow(element).html()).toContain("double_image_ro");
  });


  it("Empty context object", () => {
    myAttributeInput = {
      device: "sys/tg_test/1",
      attribute: "long_scalar",
      label: "long_scalar1",
      history: [],
      dataType: "",
      dataFormat: "",
      isNumeric: true,
      unit: "",
      enumlabels: [],
      write: writeArray,
      value: [
        [0, 0],
        [0, 0],
      ],
      writeValue: "",
      timestamp: timestamp,
      quality: "VALID"
    };

    myInput = {
      showAttribute: "Label",
      attribute: myAttributeInput,
      scale: "1",
      showDevice: false,
      textColor: "black",
      backgroundColor: "white",
      size: 1,
      font: "Helvetica",
      widgetCss: []
    };

    window.HTMLCanvasElement.prototype.getContext = (cxt) => {
      return null;
    };
    const element = React.createElement(ImageDisplay.component, {
      mode: "run",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: myInput,
      id: 42
    });
    expect(shallow(element).html()).toContain("Context not found");
  });
});

describe('test Generate Image', () => {
  const mockCreateImageData = jest.fn();
  const mockPutImageData = jest.fn();

  beforeEach(() => {
    useSelector.mockImplementation((selectorFn) =>
      selectorFn({
        messages: {
          'sys/tg_test/1': {
            attributes: {
              long_scalar: {
                quality: [
                  "ATTR_VALID"
                ],
                timestamp: [
                    1675353614.375299
                ],
                values: [[[1,2,3],[1,2,3],[1,2,3]]]
              }
            }
          }
        },
        ui: {
          mode: "run",
        },
      })
    );

    const contx = {
      createImageData: mockCreateImageData,
      putImageData: mockPutImageData
    }
    const contxx = jest.fn();
    (contxx as jest.Mock).mockReturnValue(contx);

    const canv = {
      getContext: contxx,
      toDataURL: jest.fn()
    }
    document.createElement = jest.fn().mockReturnValueOnce(canv);
  });

  afterEach(() => {
    jest.resetAllMocks();
  });

  it("test Generate Image good path", () => {
    myAttributeInput = {
      device: "sys/tg_test/1",
      attribute: "long_scalar",
      label: "long_scalar",
      history: [],
      dataType: "",
      dataFormat: "",
      isNumeric: true,
      unit: "",
      enumlabels: [],
      write: writeArray,
      value: [],
      writeValue: "",
      timestamp: timestamp,
      quality: "VALID"
    };

    myInput = {
      showAttribute: "Name",
      attribute: myAttributeInput,
      scale: "1",
      showDevice: true,
      textColor: "black",
      backgroundColor: "white",
      size: 1,
      font: "Helvetica",
      widgetCss: []
    };
    window.HTMLCanvasElement.prototype.getContext = (cxt) => {
      return cxt;
    };

    const element = React.createElement(ImageDisplay.component, {
      mode: "run",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: myInput,
      id: 42
    });

    const shallowE = shallow(element).html();
    expect(mockCreateImageData).toHaveBeenCalledTimes(1);
  });
});

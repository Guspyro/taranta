import React from "react";
import { DeviceInput, AttributeInput } from "../../types";

import { configure } from "enzyme";
import Adapter from "@wojtekmaj/enzyme-adapter-react-17";
import AttributeDial from "./AttributeDial";
import { useSelector as useSelectorMock } from "react-redux";
import { render } from "@testing-library/react";

jest.mock('react-redux', () => ({
  useSelector: jest.fn(),
}));
const useSelector = useSelectorMock as jest.Mock;

configure({ adapter: new Adapter() });

describe("DeviceStatus", () => {
  beforeEach(() => {
    useSelector.mockImplementation((selectorFn) =>
      selectorFn({
        messages: {
          'sys/tg_test/1': {
            attributes: {
              long_scalar: {
                values: [
                  228,
                  228,
                  91
                ],
                quality: [
                  'ATTR_VALID',
                  'ATTR_VALID',
                  'ATTR_VALID'
                ],
                timestamp: [
                  1690542017.171402,
                  1690542017.127803,
                  1690542041.218554
                ]
              },
              short_scalar: {
                values: [
                  170,
                  170,
                  90
                ],
                quality: [
                  'ATTR_VALID',
                  'ATTR_VALID',
                  'ATTR_VALID'
                ],
                timestamp: [
                  1690542017.181211,
                  1690542017.128467,
                  1690542041.218627
                ]
              }
            }
          }
        },
        ui: {
          mode: "run",
        },
      })
    );
  });

  let myDeviceInput: DeviceInput;
  myDeviceInput = {
    alias: "",
    name: "sys/tg_test/1"
  }
  let myAttributeInput: AttributeInput;
  myAttributeInput = {
    device: "sys/tg_test/1",
    attribute: "double_scalar",
    label: "double_scalar",
    value: 8.5,
    writeValue: 0,
    timestamp: 1668079933.435179,
    dataFormat: "scalar",
    dataType: "DevDouble",
    enumlabels: [],
    history: [],
    isNumeric: true,
    unit: "",
    write: () => {},
    quality: "VALID"
  }
  it("AttributeDial: Check for custom CSS ", () => {

    const input = {
      widgetCss: "background-color: cyan\nborder: 2px dashed red;\nfont-size: 22px;",
      attribute: myAttributeInput,
      label: "attribute",
      min: -250,
      max: 250,
      showWriteValue: true
    }
    const element = render(<AttributeDial.component
      mode="run"
      t0={1}
      actualWidth={500}
      actualHeight={140}
      id={42}
      inputs={{
        widgetCss: "background-color: cyan\nborder: 2px dashed red;\nfont-size: 22px;",
        attribute: myAttributeInput,
        label: "attribute",
        min: -250,
        max: 250,
        showWriteValue: true,
      }}
    />);

    expect(element.container.innerHTML).toContain("background-color: cyan");
    expect(element.container.innerHTML).toContain("font-size: 22px");
    expect(element.container.innerHTML).toContain("double_scalar");
    //default color is black
    expect(element.container.innerHTML).toContain('fill="black"');
  });

  it("renders with parameters correctly read", () => {
    myAttributeInput.value = undefined;

    const element = render(<AttributeDial.component
      mode={"library"}
      t0={1}
      actualWidth={500}
      actualHeight={140}
      id={42}
      inputs={{
        widgetCss: "background-color: cyan\nborder: 2px dashed red;\nfont-size: 22px;\ncolor: red;",
        attribute: myAttributeInput,
        label: "aattribute",
        min: -250,
        max: 250,
        showWriteValue: false
      }}
    />);

    expect(element.container.innerHTML).toContain("width: 60px");
    expect(element.container.innerHTML).toContain("aattribute");
    expect(element.container.innerHTML).toContain('fill="red"');
  });

})
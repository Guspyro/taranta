import React from "react";
import { configure, shallow, mount } from "enzyme";
import { Provider } from "react-redux";
import Adapter from "@wojtekmaj/enzyme-adapter-react-17";
import configureStore from "../../../shared/state/store/configureStore";
import { ParametricWidget } from "./Parametric";
import { Dashboard, DashboardEditHistory } from "../../types";

configure({ adapter: new Adapter() });
const store = configureStore();

describe("Parametric Widget", () => {
  let changeDevice = jest.fn();
  const selectedDashboard: any = {
    selectedId: null,
    selectedIds: ["1"],
    widgets: {
      "1": {
        id: "1",
        x: 19,
        y: 8,
        canvas: "0",
        width: 10,
        height: 2,
        type: "ATTRIBUTE_DISPLAY",
        inputs: {
          attribute: {
            device: "sys/tg_test/1",
            attribute: "state",
            label: "State",
          },
          precision: 2,
          showDevice: false,
          showAttribute: "Label",
          scientificNotation: false,
          showEnumLabels: false,
        },
        valid: 1,
        order: 0,
      },
    },
    id: "604b5fac8755940011efeea1",
    name: "mydashboard",
    user: "user1",
    group: null,
    groupWriteAccess: false,
    lastUpdatedBy: "user1",
    insertTime: new Date("2021-04-11T15:27:03.803Z"),
    variables: [],
  };
  const widgets = [
    {
      _id: "607e8e9c99ba3e0017cf9080",
      id: "1",
      x: 16,
      y: 5,
      canvas: "0",
      width: 14,
      height: 3,
      type: "ATTRIBUTE_DISPLAY",
      inputs: {
        attribute: {
          device: "CSPVar1",
          attribute: "state",
          label: "State",
        },
        precision: 2,
        showDevice: false,
        showAttribute: "Label",
        scientificNotation: false,
        showEnumLabels: false,
        textColor: "#000000",
        backgroundColor: "#ffffff",
        size: 1,
        font: "Helvetica",
      },
      order: 0,
      valid: 1,
    },
    {
      _id: "607e946599ba3e0017cf90a0",
      id: "2",
      x: 16,
      y: 10,
      canvas: "0",
      width: 20,
      height: 3,
      type: "COMMAND_ARRAY",
      inputs: {
        command: {
          device: "myvar",
          command: "DevString",
          acceptedType: "DevString",
        },
        showDevice: true,
        showCommand: true,
        requireConfirmation: true,
        displayOutput: true,
        OutputMaxSize: 20,
        timeDisplayOutput: 3000,
        textColor: "#000000",
        backgroundColor: "#ffffff",
        size: 1,
        font: "Helvetica",
      },
      order: 1,
      valid: 1,
    },
  ];
  let myHistory: DashboardEditHistory;
  myHistory = {
    undoActions: [{ "1": widgets[0] }],
    redoActions: [{ "1": widgets[0] }],
    undoIndex: 1,
    redoIndex: 2,
    undoLength: 3,
    redoLength: 4,
  };
  const dashboards: Dashboard[] = [
    {
      id: "604b5fac8755940011efeea1",
      name: "Untitled dashboard",
      user: "user1",
      groupWriteAccess: true,
      selectedIds: ["aaa"],
      history: myHistory,
      insertTime: new Date("2021-03-12T12:33:48.981Z"),
      updateTime: new Date("2021-04-20T08:12:53.689Z"),
      group: null,
      lastUpdatedBy: "user1",
      variables: [
        {
          _id: "0698hln1j359a",
          name: "myvar",
          class: "tg_test",
          device: "sys/tg_test/1",
        },
      ],
    },
    {
      id: "6073154703c08b0018111066",
      name: "Untitled dashboard",
      user: "user1",
      groupWriteAccess: true,
      selectedIds: ["aaa"],
      history: myHistory,
      insertTime: new Date("2021-04-11T15:27:03.803Z"),
      updateTime: new Date("2021-04-20T08:44:21.129Z"),
      group: "aGroup",
      lastUpdatedBy: "user1",
      variables: [
        {
          _id: "78l4d190kh2g",
          name: "cspvar1",
          class: "tg_test",
          device: "sys/tg_test/1",
        },
        {
          _id: "i5am90g1il0e",
          name: "myvar",
          class: "tg_test",
          device: "sys/tg_test/1",
        },
      ],
    },
  ];
  const tangoClasses = [
    { name: "DataBase", devices: [{ name: "sys/database/2" }] },
    {
      name: "DServer",
      devices: [
        { name: "dserver/DataBaseds/2" },
        { name: "dserver/Starter/f5d56d5f249f" },
        { name: "dserver/TangoAccessControl/1" },
        { name: "dserver/TangoTest/test" },
        { name: "dserver/tarantaTestDevice/test" },
      ],
    },
    { name: "Starter", devices: [{ name: "tango/admin/f5d56d5f249f" }] },
    { name: "TangoAccessControl", devices: [{ name: "sys/access_control/1" }] },
    { name: "TangoTest", devices: [{ name: "sys/tg_test/1" }] },
    {
      name: "tarantaTestDevice",
      devices: [{ name: "test/tarantatestdevice/1" }],
    },
  ];

  it("renders without crashing", () => {
    const saveDashboard = jest.fn();
    const changeSubscription = jest.fn();
    const element = React.createElement(ParametricWidget.WrappedComponent, {
      mode: "run",
      // id: 22,
      inputs: {
        name: {
          type: "string",
          label: "Name:",
          default: "Name",
          placeholder: "Name of variable",
          required: true,
        },
        variable: {
          type: "variable",
          label: "Variable:",
        },
      },
      selectedDashboard: selectedDashboard,
      dashboards: dashboards,
      tangoDB: "testdb",
      saveDashboard: saveDashboard,
      changeSubscription : changeSubscription,
      updateState: () => {},
    });

    const content = mount(<Provider store={store}>{element}</Provider>);

    content.setProps({
      username: "CREAM",
      selectedDashboard: selectedDashboard,
      dashboards: dashboards,
      changeDevice: changeDevice(),
      fetchClasses: () => {
        return tangoClasses;
      },
    });

    expect(content.find(".parametric-dropdown").length).toEqual(1);
    expect(content.find(".parametric-dropdown").html()).toContain(
      "No device found"
    );
    expect(content.find(".parametric-dropdown").html()).not.toContain(
      "sys/tg_test/1"
    );
    content.find(".parametric-dropdown").simulate("change");
  });

  it("renders without name variable", () => {
    const saveDashboard = jest.fn();
    const changeSubscription = jest.fn();
    const elementHtml = React.createElement(ParametricWidget.WrappedComponent, {
      mode: "run",
      // id: 22,
      inputs: {
        name: {
          type: "string",
          label: "Name:",
          default: "Name",
          placeholder: "Name of variable",
          required: true,
        },
        variable: {
          type: "variable",
          label: "Variable:",
        },
      },
      selectedDashboard: selectedDashboard,
      dashboards: dashboards,
      tangoDB: "testdb",
      saveDashboard: saveDashboard,
      changeSubscription: changeSubscription,
      updateState: () => { },
    });

    const shallowElement = shallow(elementHtml);
    shallowElement.setState({ tangoClasses: tangoClasses });
    expect(elementHtml.props.dashboards[0].variables[0].name).toEqual("myvar");
    expect(elementHtml.props.dashboards[1].variables[0].name).toEqual("cspvar1");
    //After 1.2.2 only selected classes will be fetched
    expect(shallowElement.html()).toContain("No device found");

    shallowElement
      .find(".parametric-dropdown")
      .simulate("change", {
        target: { value: "dserver/Starter/f5d56d5f249f" },
      });
    expect(saveDashboard).toHaveBeenCalledTimes(1);
    expect(changeSubscription).toHaveBeenCalledTimes(1);
    expect(
      shallowElement.find(".parametric-dropdown").getElement().props.value
    ).toEqual("dserver/Starter/f5d56d5f249f");
  });
});

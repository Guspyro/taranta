import React from "react";

import { configure, shallow } from "enzyme";
import Adapter from "@wojtekmaj/enzyme-adapter-react-17";
import EmbedPage from "./EmbedPage";

interface Input {
    url: string;
    overflow: boolean;
  }

  configure({ adapter: new Adapter() });

  describe("EmbedPageTests", () => {
    let myInput: Input;

    it("renders library mode true without crashing", () => {

      myInput = {
        url: "",
        overflow: false
      };

      const element = React.createElement(EmbedPage.component, {
        mode: "library",
        t0: 1,
        actualWidth: 100,
        actualHeight: 100,
        inputs: myInput,
        id: 42
      });
      expect(shallow(element).html()).toContain("EMBED A PAGE HERE");
    });

    it("renders edit mode true without crashing", () => {

        myInput = {
          url: "abc.xyz",
          overflow: false
        };

        const element = React.createElement(EmbedPage.component, {
          mode: "edit",
          t0: 1,
          actualWidth: 100,
          actualHeight: 100,
          inputs: myInput,
          id: 42
        });
        expect(shallow(element).html()).toContain("http://abc.xyz");
      });

      it("renders run mode true without crashing", () => {

        myInput = {
          url: "abc.xyz",
          overflow: false
        };

        const element = React.createElement(EmbedPage.component, {
          mode: "run",
          t0: 1,
          actualWidth: 100,
          actualHeight: 100,
          inputs: myInput,
          id: 42
        });
        expect(shallow(element).html()).toContain("http://abc.xyz");
      });
  });

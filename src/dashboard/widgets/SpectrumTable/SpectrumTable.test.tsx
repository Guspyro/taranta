import React from "react";
import { AttributeInput } from "../../types";
import { configure, mount } from "enzyme";
import Adapter from "@wojtekmaj/enzyme-adapter-react-17";
import SpectrumTable from "./SpectrumTable";
import { Provider } from "react-redux";
import configureStore from "../../../shared/state/store/configureStore";
import SpectrumValues from "./SpectrumValues";

jest.mock("./SpectrumValues", () => () => <div>Mock SpectrumValues</div>);

configure({ adapter: new Adapter() });
const store = configureStore();

interface Input {
  showDevice: boolean;
  showAttribute: string;
  showSpecificIndexValue: string;
  precision: number;
  attribute: AttributeInput;
  showIndex: boolean;
  showLabel: boolean;
  fontSize: number;
  layout: "horizontal" | "vertical";
  customCss: string;
}

describe("SpectrumValuesTests", () => {
  let myAttributeInput: AttributeInput;
  let myInput: Input;
  var writeArray: any = [];
  var date = new Date();
  var timestamp = date.getTime();

  it("renders without crashing", () => {
    myAttributeInput = {
      device: "sys/tg_test/1",
      attribute: "double_spectrum",
      label: "Double Spectrum",
      history: [],
      dataType: "",
      dataFormat: "",
      isNumeric: true,
      unit: "",
      enumlabels: [],
      write: writeArray,
      value: [54],
      writeValue: "",
      timestamp: timestamp,
      quality: "valid",
    };

    myInput = {
      showDevice: true,
      showAttribute: "Name",
      attribute: myAttributeInput,
      showIndex: true,
      showLabel: true,
      fontSize: 12,
      layout: "horizontal",
      showSpecificIndexValue: "0",
      precision: 2,
      customCss: "",
    };
    let element = React.createElement(SpectrumTable.component, {
      id: 1,
      mode: "run",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: myInput,
    });

    let wrapper = mount(<Provider store={store}>{element}</Provider>);
    expect(wrapper.find("#SpectrumTable").exists()).toBe(true);
  });

  it("renders all true without crashing", () => {
    myAttributeInput = {
      device: "sys/tg_test/1",
      attribute: "double_spectrum",
      label: "Double Spectrum",
      history: [],
      dataType: "",
      dataFormat: "",
      isNumeric: true,
      unit: "",
      enumlabels: [],
      write: writeArray,
      value: [],
      writeValue: "",
      timestamp: timestamp,
      quality: "",
    };

    myInput = {
      showDevice: false,
      showAttribute: "Name",
      attribute: myAttributeInput,
      showIndex: true,
      showLabel: true,
      fontSize: 12,
      layout: "horizontal",
      showSpecificIndexValue: "0",
      precision: 2,
      customCss: "",
    };

    let element = React.createElement(SpectrumTable.component, {
      id: 1,
      mode: "run",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      
      inputs: myInput,
    });
    let wrapper = mount(<Provider store={store}>{element}</Provider>);
    expect(wrapper.find("#SpectrumTable").exists()).toBe(true);
  });

  it("renders when when showAttribute is Label and label is not empty", () => {
    myAttributeInput = {
      device: "sys/tg_test/1",
      attribute: "double_spectrum",
      label: "Attribute Label",
      history: [],
      dataType: "",
      dataFormat: "",
      isNumeric: true,
      unit: "",
      enumlabels: [],
      write: writeArray,
      value: [],
      writeValue: "",
      timestamp: timestamp,
      quality: "",
    };

    myInput = {
      showDevice: true,
      showAttribute: "Label",
      attribute: myAttributeInput,
      showIndex: true,
      showLabel: true,
      fontSize: 12,
      layout: "horizontal",
      showSpecificIndexValue: "0",
      precision: 2,
      customCss: "",
    };

    let element = React.createElement(SpectrumTable.component, {
      id: 1,
      mode: "run",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      
      inputs: myInput,
    });
    let wrapper = mount(<Provider store={store}>{element}</Provider>);
    const componentWrapper = wrapper.find(SpectrumValues);
    expect(componentWrapper.props().attribute["label"]).toEqual(
      "Attribute Label"
    );
  });

  it("renders when showAttribute is Label and label is empty", () => {
    myAttributeInput = {
      device: "sys/tg_test/1",
      attribute: "double_spectrum",
      label: "",
      history: [],
      dataType: "",
      dataFormat: "",
      isNumeric: true,
      unit: "",
      enumlabels: [],
      write: writeArray,
      value: [],
      writeValue: "",
      timestamp: timestamp,
      quality: "",
    };

    myInput = {
      showDevice: true,
      showAttribute: "Label",
      attribute: myAttributeInput,
      showIndex: true,
      showLabel: true,
      fontSize: 12,
      layout: "horizontal",
      showSpecificIndexValue: "0",
      precision: 2,
      customCss: "",
    };

    let element = React.createElement(SpectrumTable.component, {
      id: 1,
      mode: "run",
      t0: 0,
      actualWidth: 100,
      actualHeight: 100,
      
      inputs: myInput,
    });
    let wrapper = mount(<Provider store={store}>{element}</Provider>);
    const componentWrapper = wrapper.find(SpectrumValues);
    expect(componentWrapper.prop("attributeName")).toEqual("double_spectrum");
  });

  it("renders when showAttribute is Name and attribute.attribute is null", () => {
    myAttributeInput = {
      device: "sys/tg_test/1",
      
      attribute: "",
      label: "",
      history: [],
      dataType: "",
      dataFormat: "",
      isNumeric: true,
      unit: "",
      enumlabels: [],
      write: writeArray,
      value: [],
      writeValue: "",
      timestamp: timestamp,
      quality: "",
    };

    myInput = {
      showDevice: true,
      showAttribute: "Name",
      attribute: myAttributeInput,
      showIndex: true,
      showLabel: true,
      fontSize: 12,
      layout: "horizontal",
      showSpecificIndexValue: "0",
      precision: 2,
      customCss: "",
    };

    let element = React.createElement(SpectrumTable.component, {
      id: 1,
      mode: "run",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,      
      inputs: myInput,
    });
    let wrapper = mount(<Provider store={store}>{element}</Provider>);
    expect(wrapper.find("#SpectrumTable").exists()).toBe(true);
  });
});

import React from "react";
import { render } from '@testing-library/react';
import { useSelector } from 'react-redux';
import SpectrumValues from './SpectrumValues';
import {
    getAttributeLastQualityFromState,
    getAttributeLastTimeStampFromState,
    getAttributeLastValueFromState
} from '../../../shared/utils/getLastValueHelper';

jest.mock('react-redux', () => ({
    useSelector: jest.fn(),
}));

jest.mock('../../../shared/utils/getLastValueHelper', () => ({
    getAttributeLastValueFromState: jest.fn(),
    getAttributeLastTimeStampFromState: jest.fn(),
    getAttributeLastQualityFromState: jest.fn(),
}));

describe('SpectrumValues', () => {
    beforeEach(() => {
        useSelector.mockImplementation((selectorFn) => selectorFn({
            messages: {},
            ui: {
                mode: 'run',
            },
        }));
        getAttributeLastValueFromState.mockReturnValue([1, 2, 3]);
        getAttributeLastTimeStampFromState.mockReturnValue(Date.now() / 1000);
        getAttributeLastQualityFromState.mockReturnValue('ATTR_INVALID');
    });

    afterEach(() => {
        jest.resetAllMocks();
    });

    it('should render the attribute value', () => {
        const timestamp = new Date().getTime();
        const { container } = render(
            <SpectrumValues
                device="sys/tg_test/1"
                attribute="double_spectrum"
                label="Double Spectrum"
                history={[]}
                dataType=""
                dataFormat=""
                isNumeric={true}
                mode="run"
                enumlabels={[]}
                write={[]}
                value={[12, 23, 34, 45, 56]}
                writeValue=""
                timestamp={timestamp}
            />
        );
        expect(container.textContent).toContain("123");
    });

    it('should handle a null value', () => {
        const timestamp = new Date().getTime();
        const { container } = render(
            <SpectrumValues
                device="sys/tg_test/1"
                attribute="double_spectrum"
                label="Double Spectrum"
                history={[]}
                dataType=""
                dataFormat=""
                isNumeric={true}
                mode="run"
                enumlabels={[]}
                write={[]}
                value={null}
                writeValue=""
                timestamp={timestamp}
            />
        );
        expect(container.textContent).toContain("123");
    });

    it('should render the attribute value in library mode', () => {
        const timestamp = new Date().getTime();
        const { container } = render(
            <SpectrumValues
                device="sys/tg_test/1"
                attribute="double_spectrum"
                label="Double Spectrum"
                history={[]}
                dataType=""
                dataFormat=""
                isNumeric={true}
                mode="library"
                enumlabels={[]}
                write={[]}
                value={[10, 25, 38, 135, 9856]}
                writeValue=""
                timestamp={timestamp}
            />
        );
        expect(container.textContent).toContain("12345");
    });

    it('should render the attribute value in edit mode', () => {
        const timestamp = new Date().getTime();
        const { container } = render(
            <SpectrumValues
                device="sys/tg_test/1"
                attribute="double_spectrum"
                label="Double Spectrum"
                history={[]}
                dataType=""
                dataFormat=""
                isNumeric={true}
                mode="edit"
                enumlabels={[]}
                write={[]}
                value={[10, 25, 38, 135, 9856]}
                writeValue=""
                timestamp={timestamp}
            />
        );
        expect(container.textContent).toContain("12345");
    });

    it('renders all elements horizontally when mode is set to run and layout is set to horizontal', () => {
        const timestamp = new Date().getTime();
        const { container } = render(
            <SpectrumValues
                device="sys/tg_test/1"
                attribute="double_spectrum"
                label="Double Spectrum"
                history={[]}
                dataType=""
                dataFormat=""
                isNumeric={true}
                mode="run"
                enumlabels={[]}
                write={[]}
                value={[10, 25, 38, 135, 9856]}
                writeValue=""
                timestamp={timestamp}
                layout="horizontal"
            />
        );
        expect((container.innerHTML.match(/<th/g) || []).length).toEqual(3);
        expect((container.innerHTML.match(/<tr/g) || []).length).toEqual(1);
    });

    it('renders all elements verticaly when mode is set to edit layout is set to vertical', () => {
        const timestamp = new Date().getTime();
        const { container } = render(
            <SpectrumValues
                device="sys/tg_test/1"
                attribute="double_spectrum"
                label="Double Spectrum"
                history={[]}
                dataType=""
                dataFormat=""
                isNumeric={true}
                mode="edit"
                enumlabels={[]}
                write={[]}
                value={[10, 25, 38, 135, 9856]}
                writeValue=""
                timestamp={timestamp}
                layout="vertical"
            />
        );
        expect((container.innerHTML.match(/<tr/g) || []).length).toEqual(5);
    });

    it('renders all elements verticaly when mode is set to edit layout is set to vertical', () => {
        const timestamp = new Date().getTime();
        const { container } = render(
            <SpectrumValues
                device="sys/tg_test/1"
                attribute="double_spectrum"
                label="Double Spectrum"
                history={[]}
                dataType=""
                dataFormat=""
                isNumeric={true}
                mode="edit"
                enumlabels={[]}
                write={[]}
                value={[10, 25, 38, 135, 9856]}
                writeValue=""
                timestamp={timestamp}
                showIndex={true}
                fontSize={12}
                showLabel={true}
            />
        );
        expect(container.innerHTML).toContain("Index");
        expect(container.innerHTML).toContain("Value");
    });

    it('should show a specific index value', () => {
        const timestamp = new Date().getTime();
        const { container } = render(
            <SpectrumValues
                device="sys/tg_test/1"
                attribute="double_spectrum"
                label="Double Spectrum"
                history={[]}
                dataType=""
                dataFormat=""
                isNumeric={true}
                mode="edit"
                enumlabels={[]}
                write={[]}
                value={[10, 25, 38, 135, 9856]}
                writeValue=""
                timestamp={timestamp}
                showIndex={true}
                showSpecificIndexValue="3"
            />
        );
        expect(container.innerHTML).toContain("4");
    });

    it('renders indices when showIndex is set but showLabel is not set', () => {
        const timestamp = new Date().getTime();
        const { container } = render(
            <SpectrumValues
                device="sys/tg_test/1"
                attribute="double_spectrum"
                label="Double Spectrum"
                history={[]}
                dataType=""
                dataFormat=""
                isNumeric={true}
                mode="edit"
                enumlabels={[]}
                write={[]}
                value={[10, 25, 38, 135, 9856]}
                writeValue=""
                timestamp={timestamp}
                showIndex={true}
            />
        );
        expect(container.innerHTML).toContain("0");
    });

    it('renders indices when showIndex is set and there is only one null value', () => {
        const timestamp = new Date().getTime();
        getAttributeLastValueFromState.mockReturnValue([null]);
        const { container } = render(
            <SpectrumValues
                device="sys/tg_test/1"
                attribute="double_spectrum"
                label="Double Spectrum"
                history={[]}
                dataType=""
                dataFormat=""
                isNumeric={true}
                mode="run"
                enumlabels={[]}
                write={[]}
                value={[null]}
                writeValue=""
                timestamp={timestamp}
                showIndex={true}
                showSpecificIndexValue="0"
                showLabel={true}
            />
        );
        expect(container.innerHTML).toContain("<div>No data</div>");
    });

    it('renders when showSpecificIndexValue is set and value exists when layout is horizontal', () => {
        const timestamp = new Date().getTime();
        getAttributeLastValueFromState.mockReturnValue([111, 222, 333]);
        const { container } = render(
            <SpectrumValues
                device="sys/tg_test/1"
                attribute="double_spectrum"
                label="Double Spectrum"
                history={[]}
                dataType=""
                dataFormat=""
                isNumeric={true}
                mode="run"
                enumlabels={[]}
                write={[]}
                value={null}
                writeValue=""
                timestamp={timestamp}
                showIndex={true}
                showLabel={true}
                showSpecificIndexValue="1"
                layout="horizontal"
            />
        );
        expect(container.innerHTML).toContain("222");
    });

    it('renders when showSpecificIndexValue is set and value exists when layout is vertical', () => {
        const timestamp = new Date().getTime();
        getAttributeLastValueFromState.mockReturnValue([111, 222, 333]);
        const { container } = render(
            <SpectrumValues
                device="sys/tg_test/1"
                attribute="double_spectrum"
                label="Double Spectrum"
                history={[]}
                dataType=""
                dataFormat=""
                isNumeric={true}
                mode="run"
                enumlabels={[]}
                write={[]}
                value={null}
                writeValue=""
                timestamp={timestamp}
                showIndex={true}
                showLabel={true}
                showSpecificIndexValue="1"
            />
        );
        expect(container.innerHTML).toContain("222");
    });

});
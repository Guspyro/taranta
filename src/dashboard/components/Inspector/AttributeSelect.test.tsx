import React from "react";
import '../../../shared/tests/globalMocks';
import { configure, shallow, mount } from "enzyme";
import Adapter from "@wojtekmaj/enzyme-adapter-react-17";
import configureStore from "../../../shared/state/store/configureStore";
import { Provider } from "react-redux";
import AttributeSelect from "./AttributeSelect";

configure({ adapter: new Adapter() });

const store = configureStore();
const tangoDB = "testdb"
const device = "sys/tg_test/1";
const attributeList = {
    attributeList: [
      {
        name: 'state',
        label: 'State',
        dataformat: 'SCALAR',
        datatype: 'DevState'
      },
      {
        name: 'status',
        label: 'Status',
        dataformat: 'SCALAR',
        datatype: 'DevString'
      }
    ]
  };
const input = {
    "nonEditable": false,
    "tangoDB": tangoDB,
    "device": device,
    "hideDeviceSuggester": false,
    "attribute": "double_scalar",
    "label": "double_scalar",
    "dataFormat": "scalar",
    "variables": [
        {
            "_id": "6440f8ed9e355700182c7a0b",
            "name": "DBVar",
            "class": "TangoTest",
            "device": "sys/tg_test/1"
        }
    ],
    "attributesList": attributeList
};


describe("Test AttributeSelect", () => {
    
    it("run without crash", () => {
        const element = React.createElement(AttributeSelect, {
            tangoDB: tangoDB,
            device: device,
            attribute: input.attribute,
            label: input.label,
            onSelect: () => {},
            nonEditable: input.nonEditable,
            variables: [],
            hideDeviceSuggester: input.hideDeviceSuggester,
        })

        const content = mount(<Provider store={store}>{element}</Provider>);
        expect(content.html()).toContain(device)
    });

    it("run without device", () => {
        const element = React.createElement(AttributeSelect, {
            tangoDB: tangoDB,
            device: device,
            attribute: input.attribute,
            label: input.label,
            onSelect: () => {},
            nonEditable: input.nonEditable,
            variables: [],
            hideDeviceSuggester: true,
        })

        const content = mount(<Provider store={store}>{element}</Provider>);
        expect(content.html()).not.toContain(device)
    });

    it("test changing device", () => {
        const deviceToUpdate = 'sys/database/1';
        const element = React.createElement(AttributeSelect, {
            tangoDB: tangoDB,
            device: device,
            attribute: input.attribute,
            label: input.label,
            onSelect: () => {},
            nonEditable: input.nonEditable,
            variables: [],
            hideDeviceSuggester: false,
        })

        const content = mount(<Provider store={store}>{element}</Provider>);

        content.find('.form-control').first().simulate('change', { target: { value: deviceToUpdate } });
        expect(content.html()).not.toContain(device)
        expect(content.html()).toContain(deviceToUpdate)

    });

    it("test more device", () => {
        const deviceToUpdate = 'sys/database/1';
        const element = React.createElement(AttributeSelect, {
            tangoDB: tangoDB,
            device: device + ',' + deviceToUpdate,
            attribute: input.attribute,
            label: input.label,
            onSelect: () => {},
            nonEditable: input.nonEditable,
            variables: [],
            hideDeviceSuggester: false,
        })

        const content = mount(<Provider store={store}>{element}</Provider>);

        expect(content.html()).toContain(device)
        expect(content.html()).toContain(deviceToUpdate)

    });

    it("test changing attribute", () => {
        const attributeToUpdate = 'State';
        const element = React.createElement(AttributeSelect, {
            tangoDB: tangoDB,
            device: device,
            attribute: input.attribute,
            label: input.label,
            onSelect: () => {},
            nonEditable: input.nonEditable,
            variables: [],
            hideDeviceSuggester: false,
        })

        const content = mount(<Provider store={store}>{element}</Provider>);

        expect(content.html()).toContain(input.attribute)
        content.find('.form-control').at(1).simulate('change', { target: { value: attributeToUpdate } });
        expect(content.html()).not.toContain(input.attribute);
        expect(content.html()).toContain(attributeToUpdate);

    }); 


})


import React from "react";
import { configure, mount } from "enzyme";
import Adapter from "@wojtekmaj/enzyme-adapter-react-17";
import '../../../shared/tests/globalMocks';
import { Provider } from "react-redux";
import configureStore from "../../../shared/state/store/configureStore";
import InputList from './InputList'


configure({ adapter: new Adapter() });

const store = configureStore();
const tangoDB = "testdb"
const inputDefinitions = JSON.parse('{ "name": { "type": "string", "label": "Name:", "default": "Name", "placeholder": "Name of variable", "required": true }, "variable": { "type": "variable", "label": "Variable:" } }');
const inputs = JSON.parse('{ "name": "Name", "variable": "asd" }')
const widgets = JSON.parse('[ { "_id": "608c07e22708ac001305a307", "id": "2", "x": 25, "y": 3, "canvas": "0", "width": 30, "height": 7, "type": "PARAMETRIC_WIDGET", "inputs": { "name": "Name", "variable": "testVariable" }, "order": 0, "valid": true } ]')
const widgetType = "PARAMETRIC_WIDGET";
const nonEditable = false;
let variables = [];
const inputDateDefinition: any = {
    elasticsearchUrl: "http://localhost:9200",
    overflow: true,
    searchText: "",
    fromDate: new Date(),
    toDate: new Date(new Date().getTime() + 24 * 60 * 60 * 1000)
}

describe("Test input list", () => {
    
    it("run without crash without variable using PARAMETRIC_WIDGET", () => {
        const element = React.createElement(InputList, {
            tangoDB: tangoDB,
            inputDefinitions: inputDefinitions,
            inputs: inputs,
            widgets: widgets,
            onChange: () => { },
            onAdd: () => { },
            onDelete: () => { },
            widgetType: widgetType,
            basePath: undefined,
            nonEditable: nonEditable,
            variables: variables,
        })

        const content = mount(<Provider store={store}>{element}</Provider>);
        expect(content.html()).toContain("No variables defined")
    })

    it("check if render with variables", () => {

        let variableName = "testVariable"
        variables = JSON.parse('[ { "id": "h0ajdfaa8fb9", "name": "' + variableName + '", "class": "DataBase", "device": "sys/database/2" } ]')

        const element = React.createElement(InputList, {
            tangoDB: tangoDB,
            inputDefinitions: inputDefinitions,
            inputs: inputs,
            widgets: widgets,
            onChange: () => { },
            onAdd: () => { },
            onDelete: () => { },
            widgetType: widgetType,
            basePath: undefined,
            nonEditable: nonEditable,
            variables: variables,
        })

        const content = mount(<Provider store={store}>{element}</Provider>);

        expect(content.html()).not.toContain("No variables defined")
        expect(content.find({ value: variableName }).length).toBe(1)
    })

    it("test input type date", () => {
        const element = React.createElement(InputList, {
            tangoDB: tangoDB,
            inputDefinitions: inputDateDefinition,
            inputs: inputs,
            widgets: widgets,
            onChange: () => {},
            onAdd: () => {},
            onDelete: () => {},
            widgetType: widgetType,
            basePath: undefined,
            nonEditable: nonEditable,
            variables: variables,
        })

        const shallowElement = mount(<Provider store={store}>{element}</Provider>);

        expect(shallowElement.html()).not.toContain("From date");
        expect(shallowElement.html()).not.toContain("To date");
    })

    it("run without crash for complex input > command array", () => {
        const element = React.createElement(InputList, {
            tangoDB: tangoDB,
            inputDefinitions: {
                "name": {
                    "type": "string",
                    "label": "Name"
                },
                "value": {
                    "type": "string",
                    "label": "Value"
                },
                "isDefault": {
                    "type": "radio",
                    "label": "Make default"
                }
            },
            inputs: {
                "name": "One",
                "value": "String1",
                "isDefault": true
            },
            widgets: [],
            onChange: () => { },
            onAdd: () => { },
            onDelete: () => { },
            widgetType: "COMMAND",
            basePath: undefined,
            nonEditable: nonEditable,
            variables: [],
        })

        const content = mount(<Provider store={store}>{element}</Provider>);
        expect(content.html()).toContain("radio");
        content.find('input[type="radio"]').first().simulate('click');
    })

    it("run without crash for complex input > attribute", () => {
        const tabularProps: any = {
            tangoDB: tangoDB,
            inputDefinitions: {
                "devices": {
                    "type": "complex",
                    "label": "Devices",
                    "repeat": true,
                    "inputs": {
                        "device": {
                            "type": "device",
                            "label": "",
                            "publish": "$device"
                        }
                    }
                },
                "showDefaultAttribute": {
                    "type": "boolean",
                    "label": "Show default attributes",
                    "default": false
                },
                "customAttributes": {
                    "type": "complex",
                    "label": "Custom Attributes",
                    "repeat": true,
                    "inputs": {
                        "attribute": {
                            "label": "Attribute",
                            "type": "attribute",
                            "required": true
                        }
                    }
                },
            },
            inputs: {
                "devices": [
                    {
                        "device": 'sys/tg_test/1'
                    }
                ],
                "showDefaultAttribute": false,
                "customAttributes": [
                    {
                        "attribute": {
                            "device": "sys/tg_test/1",
                            "attribute": "state",
                            "label": "State"
                        }
                    },
                    {
                        "attribute": {
                            "device": "sys/tg_test/1",
                            "attribute": "double_scalar",
                            "label": "double_scalar"
                        }
                    }
                ]
            },
        }

        const element = React.createElement(InputList, {
            ...tabularProps,
            widgets: [],
            onChange: () => { },
            onAdd: () => { },
            onDelete: () => { },
            widgetType: "TABULAR_VIEW",
            basePath: undefined,
            nonEditable: nonEditable,
            variables: [],
        })

        const content = mount(<Provider store={store}>{element}</Provider>);
        expect(content.html()).toContain("double_scalar")
        expect(content.html()).toContain("sys/tg_test/1")
    });
})

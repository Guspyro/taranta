import React from 'react'
import '../../../shared/tests/globalMocks';
import { configure, shallow, mount } from "enzyme";
import Adapter from "@wojtekmaj/enzyme-adapter-react-17";
import DashboardLibrary from "./DashboardLibrary";
import "@testing-library/jest-dom";
import { Provider } from "react-redux";
import configureStore from "../../../shared/state/store/configureStore";
import {
    SelectedDashboard,
    Dashboard,
    DashboardEditHistory,
  } from "../../types";
  
configure({ adapter: new Adapter() });

const tangoDB = "testdb";
const widgets = [
  {
    _id: "607e8e9c99ba3e0017cf9080",
    id: "1",
    x: 16,
    y: 5,
    canvas: "0",
    width: 14,
    height: 3,
    type: "ATTRIBUTE_DISPLAY",
    inputs: {
      attribute: {
        device: "CSPVar1",
        attribute: "state",
        label: "State",
      },
      precision: 2,
      showDevice: false,
      showAttribute: "Label",
      scientificNotation: false,
      showEnumLabels: false,
      textColor: "#000000",
      backgroundColor: "#ffffff",
      size: 1,
      font: "Helvetica",
    },
    order: 0,
    valid: 1,
  },
  {
    _id: "607e946599ba3e0017cf90a0",
    id: "2",
    x: 16,
    y: 10,
    canvas: "0",
    width: 20,
    height: 3,
    type: "COMMAND_ARRAY",
    inputs: {
      command: {
        device: "myvar",
        command: "DevString",
        acceptedType: "DevString",
      },
      showDevice: true,
      showCommand: true,
      requireConfirmation: true,
      displayOutput: true,
      OutputMaxSize: 20,
      timeDisplayOutput: 3000,
      textColor: "#000000",
      backgroundColor: "#ffffff",
      size: 1,
      font: "Helvetica",
    },
    order: 1,
    valid: 1,
  },
];

let myHistory: DashboardEditHistory;
myHistory = {
  undoActions: [{ "1": widgets[0] }],
  redoActions: [{ "1": widgets[0] }],
  undoIndex: 1,
  redoIndex: 2,
  undoLength: 3,
  redoLength: 4,
};
const myDashboards: Dashboard[] = [
  {
    id: "604b5fac8755940011efeea1",
    name: "Untitled dashboard",
    user: "user1",
    groupWriteAccess: true,
    selectedIds: ["aaa"],
    history: myHistory,
    insertTime: new Date("2021-03-12T12:33:48.981Z"),
    updateTime: new Date("2021-04-20T08:12:53.689Z"),
    group: null,
    lastUpdatedBy: "user1",
    variables: [
      {
        _id: "0698hln1j359a",
        name: "myvar",
        class: "tg_test",
        device: "sys/tg_test/1",
      },
    ],
  },
  {
    id: "6073154703c08b0018111066",
    name: "Untitled dashboard",
    user: "user1",
    groupWriteAccess: true,
    selectedIds: ["aaa"],
    history: myHistory,
    insertTime: new Date("2021-04-11T15:27:03.803Z"),
    updateTime: new Date("2021-04-20T08:44:21.129Z"),
    group: "aGroup",
    lastUpdatedBy: "user1",
    variables: [
      {
        _id: "78l4d190kh2g",
        name: "cspvar1",
        class: "tg_test",
        device: "sys/tg_test/1",
      },
      {
        _id: "i5am90g1il0e",
        name: "myvar",
        class: "tg_test",
        device: "sys/tg_test/1",
      },
    ],
  },
];
const selectedDashboard: any = {
  selectedId: null,
  selectedIds: ["1"],
  widgets: {
    "1": {
      id: "1",
      x: 19,
      y: 8,
      canvas: "0",
      width: 10,
      height: 2,
      type: "ATTRIBUTE_DISPLAY",
      inputs: {
        attribute: {
          device: "sys/tg_test/1",
          attribute: "state",
          label: "State",
        },
        precision: 2,
        showDevice: false,
        showAttribute: "Label",
        scientificNotation: false,
        showEnumLabels: false,
      },
      valid: true,
      order: 0,
    },
  },
  id: "604b5fac8755940011efeea1",
  name: "mydashboard",
  user: "user1",
  group: null,
  groupWriteAccess: false,
  lastUpdatedBy: "user1",
  insertTime: new Date("2021-04-11T15:27:03.803Z"),
  variables: [],
};

describe("Import dashboard from file", () => {

  const store = configureStore();

  it("button renders correctly", () => {
    const elementHtml = React.createElement(DashboardLibrary.WrappedComponent, {
      tangoDB: tangoDB,
      render: true,
      dashboards: myDashboards,
      isLoggedIn: true,
      selectedDashboard: selectedDashboard,
      deviceList: [],
      onDeleteDashboard: () => {},
      loadDashboard: () => {},
      exportDashboard: () => {},
      saveDashboard: () => {},
      onUploadFile: () => {},
      onFilesChange: () => {},
      onFilesError: () => {},
    });

    const pageContent = mount(
      <Provider store={store}>{elementHtml}</Provider>
    ).html();
    expect(pageContent).toContain("dashboard-menu-button");
    expect(pageContent).toContain("Add Dashboard");
    expect(pageContent).toContain("dashboard-settings-title");

    expect(pageContent).toContain("My Dashboards");
    expect(pageContent).toContain("Shared Dashboards");
    expect(pageContent).toContain("Create a new dashboard");
    expect(pageContent).toContain("Import an existing dashboard from a file");
  });

  it("hide button for not logged user", () => {
    const elementHtml = React.createElement(DashboardLibrary.WrappedComponent, {
      tangoDB: tangoDB,
      render: true,
      dashboards: myDashboards,
      isLoggedIn: false,
      selectedDashboard: selectedDashboard,
      deviceList: [],
      onDeleteDashboard: () => {},
      loadDashboard: () => {},
      exportDashboard: () => {},
      saveDashboard: () => {},
      onUploadFile: () => {},
      onFilesChange: () => {},
      onFilesError: () => {},
    });

    expect(
      mount(<Provider store={store}>{elementHtml}</Provider>).html()
    ).not.toContain("dashboard-menu-button");
    expect(
      mount(<Provider store={store}>{elementHtml}</Provider>).html()
    ).toContain("You have to be logged in to view and manage your dashboards.");
  });

  it("Test config modal", () => {
    const elementHtml = React.createElement(DashboardLibrary.WrappedComponent, {
      isLoggedIn: true,
      render: true,
      tangoDB: "testdb",
      dashboards: myDashboards,
      selectedDashboard: selectedDashboard,
      deviceList: [],
      onDeleteDashboard: () => {},
      loadDashboard: () => {},
      exportDashboard: () => {},
      saveDashboard: () => {},
      onUploadFile: () => {},
      onFilesChange: () => {},
      onFilesError: () => {},
    });

    const dashComponent = mount(
      <Provider store={store}>{elementHtml}</Provider>
    );
    const mountedComponent = mount(elementHtml);

    expect(dashComponent.html()).toContain("Configure");
    expect(dashComponent.html()).toContain("fa-cog");

    mountedComponent.find("#dashConfig").at(0).simulate("click");
    expect(mountedComponent.state()["configDashboardModalId"]).not.toBe("");
  });

  it("Test config modal fns", () => {
    let dashboardVariables = [
      { id: "11", name: "CSPVar1", class: "tg_test", device: "sys/tg_test/1" },
      {
        id: "12",
        name: "TMCVar2",
        class: "tarantatestdevice",
        device: "dserver/databaseds/2",
      },
    ];
    let saveDashboard = jest.fn();

    const elementHtml = React.createElement(DashboardLibrary.WrappedComponent, {
      tangoDB: "testdb",
      isLoggedIn: true,
      render: true,
      dashboards: myDashboards,
      selectedDashboard: selectedDashboard,
      deviceList: [],
      saveDashboard: saveDashboard,
      onDeleteDashboard: () => {},
      loadDashboard: () => {},
      exportDashboard: () => {},
      onUploadFile: () => {},
      onFilesChange: () => {},
      onFilesError: () => {},
    });

    const shallowElement = shallow(elementHtml);
    shallowElement.setState({
      configDashboardModalId: "6045fdf77a53fe001155cf8b",
      dashboardVariables: dashboardVariables,
      tangoClass: ["tg_test", "tarantatestdevice", "tarantatestdevice2"],
      tangoDevices: [
        "sys/tg_test/1",
        "dserver/tangotest/test",
        "dserver/databaseds/2",
      ],
      originalDashboardVariables: dashboardVariables,
    });

    const dashComponent = mount(
      <Provider store={store}>{elementHtml}</Provider>
    );
    const mountedComponent = mount(elementHtml);

    mountedComponent.find("#dashConfig").at(1).simulate("click");
    expect(mountedComponent.state()["configDashboardModalId"]).not.toBe("");

    //create new variable
    mountedComponent
      .find(".config-wrapper")
      .find(".add-variable-form")
      .simulate("click");
    mountedComponent
      .find(".config-wrapper")
      .find("#add-new-var-name")
      .simulate("change", { target: { value: "CSP5" } });
    mountedComponent
      .find(".config-wrapper")
      .find("#add-new-var-class")
      .simulate("change", { target: { value: "tg_test" } });
    mountedComponent
      .find(".config-wrapper")
      .find("#add-new-var-device")
      .simulate("change", { target: { value: "sys/tg_test/1" } });

    mountedComponent
      .find(".config-wrapper")
      .find("#btn-add-variable")
      .simulate("click");
    mountedComponent
      .find("#config-search-variable")
      .simulate("change", { target: { value: "CSPVar1" } });

    mountedComponent
      .find(".config-wrapper")
      .find(".btn-delete")
      .first()
      .simulate("click");

    expect(mountedComponent.state()["dashboardVariables"].length).toEqual(2);
    expect(mountedComponent.find(".config-wrapper").html()).toContain(
      "Are you sure to delete"
    );
    mountedComponent
      .find(".config-wrapper")
      .find(".confirm-delete")
      .simulate("click");
    expect(mountedComponent.state()["dashboardVariables"].length).toEqual(1);
  });
});

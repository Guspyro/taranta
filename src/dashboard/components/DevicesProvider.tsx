import React, { Component } from "react";
import alphanumSort from "alphanum-sort";
import { connect } from "react-redux";
import { getDeviceNames } from "../../shared/state/selectors/deviceList";
import { fetchDeviceNames } from "../../shared/state/actions/tango";

interface Props {
  tangoDB: string;
  variables: any;
  devices: string[];
  children: React.ReactNode;
  loadDeviceNames: (tangoDB: string) => void;
}

interface State {
  fetching: boolean;
  error: boolean;
  devices: string[];
  onlyDevices: string[];
  tangoDB: string;
}

const initialState: Readonly<State> = {
  fetching: true,
  error: false,
  devices: [],
  onlyDevices: [],
  tangoDB: ""
};

const deviceContext = React.createContext<State>({ ...initialState });

export class DeviceProvider extends Component<Props, State> {
  public constructor(props: Props) {
    super(props);
    this.state = { ...initialState, tangoDB: props.tangoDB };
    this.props.loadDeviceNames(props.tangoDB);
  }

  public componentDidUpdate(prevProps) {
    if (prevProps?.devices !== this.props?.devices || prevProps?.variables !== this.props?.variables) {
      const error = this.props.devices.length === 0;
      const devices = [...this.props.variables, ...alphanumSort(this.props.devices)];
      this.setState({ devices: devices, error, onlyDevices: devices, fetching: false });
    }
  }
  public render() {
    if (this.state.fetching) {
      return <>Loading Devices</>;
    }
    return (
      <deviceContext.Provider value={this.state}>
        {this.props.children}
      </deviceContext.Provider>
    );
  }
}


function mapStateToProps(state) {
  return {
    devices: getDeviceNames(state)
  };
}

function mapDispatchToProps(dispatch) {
  return {
    loadDeviceNames: (tangoDB: string) => dispatch(fetchDeviceNames(tangoDB))
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DeviceProvider);


export const DeviceConsumer = deviceContext.Consumer;
import fileDownload from 'js-file-download';
import {resolveWidgetCompatibility,validateJson} from "../shared/state/reducers/selectedDashboard/lib";
import config from "../config.json"
import { devicePresent, variablePresent } from '../shared/utils/DashboardVariables';
import { WIDGET_MISSING_DEVICE, WIDGET_WARNING } from '../shared/state/actions/actionTypes';

const headers = {
  "Content-Type": "application/json; charset=utf-8"
};

/**
 * [backwards compatibility] In case we loaded a dashboard with widgets that don't have 
 * order set on them, we assign them a incrementing integer order starting with zero here
 * Assuming the dashboard is edited in some way, this order will get saved to the database as well
 */
function ensureProperOrdering(widgets){
  let highest = widgets.reduce((max, current) => Math.max(max, current.order || -1), -1);
  widgets.forEach(widget => {
    if (!widget.hasOwnProperty("order")){
      highest++;
      widget.order = highest;
    }
  });
}

async function save(id, widgets, name, variables = []) {

  const res = await fetch("/dashboards/", {
    method: "POST",
    headers,
    credentials: "include",
    body: JSON.stringify({
      id,
      widgets: widgets,
      name,
      variables,
      tangoDB: getTangoDB()
    })
  });
  if (!res.ok) {
    throw await res.json();
  }
  return res.ok ? res.json() : null;
}

async function load(id) {
  const res = await fetch("/dashboards/" + id, {
    method: "GET",
    credentials: "include",
    headers
  });
  const dashboard = await res.json();
  ensureProperOrdering(dashboard.widgets);
  return dashboard;
}

async function getGroupDashboardCount() {
  const res = await fetch(
    "/dashboards/group/dashboardsCount?excludeCurrentUser=true&tangoDB=" +
      getTangoDB(),
    {
      method: "GET",
      credentials: "include",
      headers
    }
  );
  return res.json();
}

async function getGroupDashboards(groupName) {
  const res = await fetch(
    "/dashboards/group/dashboards?excludeCurrentUser=true&group=" +
      groupName +
      "&tangoDB=" +
      getTangoDB(),
    {
      method: "GET",
      credentials: "include",
      headers
    }
  );
  return res.json();
}

async function loadUserDashboards() {
  const res = await fetch(
    "/dashboards/user/dashboards?tangoDB=" + getTangoDB(),
    {
      method: "GET",
      credentials: "include",
      headers
    }
  );
  return res.json();
}

async function deleteDashboard(dashboardId) {
  const res = await fetch("/dashboards/" + dashboardId, {
    method: "DELETE",
    credentials: "include",
    headers
  });
  return res.json();
}

async function cloneDashboard(dashboardId) {
  const res = await fetch("/dashboards/" + dashboardId + "/clone", {
    method: "POST",
    credentials: "include",
    headers
  });
  return res.json();
}

async function shareDashboard(dashboardId, group, groupWriteAccess) {
  const res = await fetch("/dashboards/" + dashboardId + "/share", {
    method: "POST",
    credentials: "include",
    headers,
    body: JSON.stringify({ group, groupWriteAccess })
  });
  return res.json();
}

async function renameDashboard(id, newName) {
  const res = await fetch("/dashboards/" + id + "/rename", {
    method: "POST",
    credentials: "include",
    headers,
    body: JSON.stringify({ newName })
  });
  return res.json();
}

function getTangoDB() {
  try {
    if(config.basename !== "")
      return window.location.pathname.split("/")[2];
    else
      return window.location.pathname.split("/")[1];
  } catch (e) {
    return "";
  }
}

async function exportDash(id) {

  const res = await fetch("/dashboards/" + id, {
    method: "GET",
    credentials: "include",
    headers
  });

  const dashboard = await res.json();
  ensureProperOrdering(dashboard.widgets);

  try
  {
    const version = `${process.env.REACT_APP_VERSION}`;
    const {
      widgets,
      name,
      user,
      insertTime,
      updateTime,
      group,
      groupWriteAccess,
      lastUpdatedBy,
      variables
    } = dashboard;

    const { widgets: newWidgets } = resolveWidgetCompatibility(
      widgets
    );
    
    //remove the _id in widgets, in order to reimport a widget with the same primary key into MongoDB
    newWidgets.forEach(widget => (
      delete(widget["_id"])
    ))


    let canvas = {
      id: id,
      name: name,
      version: version,
      user: user,
      insertTime: insertTime,
      updateTime: updateTime,
      group: group,
      groupWriteAccess: groupWriteAccess,
      lastUpdatedBy: lastUpdatedBy,
      widget: newWidgets,
      variables
    }

    var output = JSON.stringify(canvas, null, 2);
    //sanitize the file name in order to prevent incompability across different OS
    var sanitize = require("sanitize-filename");
    const nameSanitized = sanitize(name);
    fileDownload(output, nameSanitized+'.wj');
    return dashboard;

  } catch (exception) {
    //const errorMsg = {level: NotificationLevel.ERROR, message: "Error exporting dashboard: "+exception}
    //feedBackService.setData(errorMsg);
    console.log(exception);
    return exception;
  }
}

function readFileAsync(file) {
  return new Promise((resolve, reject) => {
    let reader = new FileReader();

    reader.onload = () => {
      resolve(reader.result);
    };

    reader.onerror = reject;

    reader.readAsText(file);
  })
}

async function importDash(content, deviceList = []) {

  let contentBuffer = await readFileAsync(content);
  const {validationResult, importError, missingBundleIds} = validateJson(contentBuffer);

  if(validationResult) {
    try {
      var obj = JSON.parse(contentBuffer);
      var version = obj.version.split(".");
      version = parseInt(version[0]+version[1]+version[2]);
      if(version < 105) {
        //const infoMsg = {level: NotificationLevel.WARNING, message: "Dashboard version was "+version+" widget order was fixed"}
        //feedBackService.setData(infoMsg);
        var orderArray = [];
        obj.widget.forEach(widget => {
          orderArray.push(widget.order);
        });
        var reversed = orderArray.reverse();
        obj.widget.forEach((widget,index) => {
          widget.order = reversed[index];
        });
      }
      if (missingBundleIds.length > 0) {
        obj.widget = obj.widget.filter(widget => missingBundleIds.indexOf(Number(widget.id)) === -1)
      }

      checkWidgetValidity(obj.widget, deviceList, obj.variables);

      let resultSave = await save('',obj.widget,obj.name,obj.variables);
      resultSave.name = obj.name;
      resultSave.warning = importError;

      return resultSave;
    } catch (e) {
      //const errorMsg = {level: NotificationLevel.ERROR, message: "Error parsing Json: "+e}
      //feedBackService.setData(errorMsg);
      console.log(e);
      return e;
    }
  } else {
    return importError;
  }
}

function checkWidgetValidity(widgets, deviceList, variables) {
  widgets.forEach(wid => {
    if (wid.type === "BOX") {
      checkWidgetValidity(wid?.innerWidgets, deviceList, variables);
    } else {
        switch(true) {
          default:
            const checkVar = variablePresent(wid, variables);
            if (!checkVar.found) {
              wid.valid = WIDGET_WARNING;
              break;
            }

            const checkDevice = devicePresent(wid, deviceList);
            if (!checkDevice.found) {
              wid.valid = WIDGET_MISSING_DEVICE;
              break;
            }
        }
    }
  })
}

export { 
  renameDashboard, 
  shareDashboard, 
  getTangoDB, 
  cloneDashboard, 
  deleteDashboard, 
  loadUserDashboards,
  getGroupDashboards,
  getGroupDashboardCount,
  load,
  exportDash,
  importDash,
  save
 };

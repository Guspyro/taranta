import React from "react";
import '../../tests/globalMocks';
import Adapter from "@wojtekmaj/enzyme-adapter-react-17";
import { configure, mount } from "enzyme";
import { Provider } from "react-redux";
import { MemoryRouter, Route, Switch } from "react-router";

import configureStore from "../../../shared/state/store/configureStore";
import { NavContainer, Navbar } from "./Navbar";

configure({ adapter: new Adapter() });

let loadStateUrls = jest.fn();
let saveStateUrls = jest.fn();
let onLoadTangoDBName = jest.fn();

describe("Test Navbar", () => {
  const store = configureStore();
  
  it("renders without crashing", () => {
    let sampleStateUrl = {
      devices: "/testdb/devices",
      dashboard: "/testdb/dashboard",
      deviceUrl: "http://localhost:22484/testdb/devices/sys/tg_test",
      dashboardUrl: "http://localhost:22484/testdb/dashboard",
    };

    const element = React.createElement(Navbar.WrappedComponent, {
      stateUrls: sampleStateUrl,
      onLoadStateUrls: loadStateUrls,
      onSaveStateUrls: saveStateUrls,
      onLoadTangoDBName: onLoadTangoDBName
    })

    const content = mount(<Provider store={store}>{element}</Provider>);

    content.setProps({
      username: "CREAM",
      stateUrls: sampleStateUrl,
      onLoadStateUrls: jest.fn(),
      onSaveStateUrls: jest.fn(),
      onLoadTangoDBName: jest.fn()
    });

    expect(content.find(".navigation").length).toEqual(1);
  });

  it("test Navbar with Memory Router for Navbar & NavContainer", async () => {
    const wrapper = mount(
      <MemoryRouter initialEntries={["/testdb/devices"]}>
        <Provider store={store}>
          <Switch>
            <Route path="/testdb/devices" render={() => <Navbar />} />
          </Switch>
        </Provider>
      </MemoryRouter>
    );

    expect(wrapper.find(Navbar)).toHaveLength(1);

    let params = {
      section: "dashboard",
      tangoDB: "testdb",
    };
    let stateUrls = {
      deviceUrl: "",
      dashboardUrl: "",
    };
    const wrapper1 = mount(
      <MemoryRouter initialEntries={["/testdb/devices"]}>
        <Provider store={store}>
          <Switch>
            <Route
              path="/testdb/devices"
              render={() => (
                <NavContainer
                  params={params}
                  saveStateUrls={saveStateUrls}
                  stateUrls={stateUrls}
                />
              )}
            />
          </Switch>
        </Provider>
      </MemoryRouter>
    );

    expect(wrapper1.find(NavContainer)).toHaveLength(1);
    const NavHtml = wrapper1.find(NavContainer).html();
    expect(NavHtml).toContain("non-active");
    expect(NavHtml).toContain("active");
    expect(NavHtml).toContain("pagelinks");
    expect(NavHtml).toContain("tabbed-menu");
    expect(NavHtml).toContain("Devices");
    expect(NavHtml).toContain("Dashboards");

    wrapper1
      .find(NavContainer)
      .find(".tabbed-menu.active")
      .simulate("click");
    expect(saveStateUrls).toHaveBeenCalledTimes(1);

    wrapper1
      .find(NavContainer)
      .find(".non-active")
      .simulate("click");
    expect(saveStateUrls).toHaveBeenCalledTimes(2);
  });
});

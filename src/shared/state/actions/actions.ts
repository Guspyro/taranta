import { Action } from "redux";
import {
  UNDO,
  REDO,
  ADD_WIDGET,
  MOVE_WIDGETS,
  SELECT_WIDGETS,
  DELETE_WIDGET,
  DELETE_INPUT,
  ADD_INPUT,
  SET_INPUT,
  RESIZE_WIDGET,
  SELECT_CANVAS,
  TOGGLE_MODE,
  DASHBOARD_LOADED,
  DASHBOARDS_LOADED,
  DASHBOARD_RENAMED,
  DASHBOARD_DELETED,
  DASHBOARD_CLONED,
  DASHBOARD_SAVED,
  SHOW_NOTIFICATION,
  HIDE_NOTIFICATION,
  REORDER_WIDGETS,
  SAVE_DASHBOARD,
  RENAME_DASHBOARD,
  CLONE_DASHBOARD,
  DUPLICATE_WIDGET,
  SHARE_DASHBOARD,
  DASHBOARD_SHARED,
  DASHBOARD_EDITED,
  WIDGET_CLIPBOARD_PASTE,
  WIDGET_CLIPBOARD_COPY,
  TOGGLE_INSPECTOR_COLLAPSED,
  TOGGLE_LIBRARY_COLLAPSED,
  MOUSE_DOWN_ON_WIDGET,
  DASHBOARD_EXPORTED,
  DASHBOARD_IMPORTED,
  ADD_INNER_WIDGET,
  REORDER_INNER_WIDGET,
  DROP_INNER_WIDGET,
  UPDATE_WIDGET
} from "./actionTypes";

import { IndexPath, Widget, Dashboard, Notification, Variable } from "../../../dashboard/types";
import { SelectedDashboardState } from "../reducers/selectedDashboard";

export interface UndoAction extends Action {
  type: typeof UNDO;
}

export interface RedoAction extends Action {
  type: typeof REDO;
}

export interface DuplicateWidgetAction extends Action {
  type: typeof DUPLICATE_WIDGET;
}

export interface AddWidgetAction extends Action {
  type: typeof ADD_WIDGET;
  x: number;
  y: number;
  widgetType: string;
  canvas: string;
}

export interface AddInnerWidgetAction extends Action {
  type: typeof ADD_INNER_WIDGET;
  x: number;
  y: number;
  widget: Widget;
  parentID: string;
  innerWidgetCheck: boolean;
}

export interface ReorderInnerWidgetAction extends Action {
  type: typeof REORDER_INNER_WIDGET;
  x: number;
  y: number;
  parentWidget: Widget;
  innerWidget: Widget;
}

export interface DropInnerWidgetAction extends Action {
  type: typeof DROP_INNER_WIDGET;
  x: number;
  y: number;
  innerWidget: Widget;
}

export interface MoveWidgetsAction extends Action {
  type: typeof MOVE_WIDGETS;
  dx: number;
  dy: number;
  ids: string[];
}

export interface ResizeWidgetAction extends Action {
  type: typeof RESIZE_WIDGET;
  mx: number;
  my: number;
  dx: number;
  dy: number;
  id: string;
}

export interface SelectWidgetsAction extends Action {
  type: typeof SELECT_WIDGETS;
  ids: string[];
}

export interface DeleteWidgetAction extends Action {
  type: typeof DELETE_WIDGET;
}

export interface SetInputAction extends Action {
  type: typeof SET_INPUT;
  path: IndexPath;
  value: any;
}

export interface AddInputAction extends Action {
  type: typeof ADD_INPUT;
  path: IndexPath;
}

export interface DeleteInputAction extends Action {
  type: typeof DELETE_INPUT;
  path: IndexPath;
}

export interface SelectCanvasAction extends Action {
  type: typeof SELECT_CANVAS;
  id: string;
}

export interface ToggleModeAction extends Action {
  type: typeof TOGGLE_MODE;
}

export interface PreloadDashboardAction extends Action {
  type: typeof DASHBOARD_LOADED;
  id: string;
  widgets: Widget[];
  name: string;
  user: string;
}

export interface DashboardsLoadedAction extends Action {
  type: typeof DASHBOARDS_LOADED;
  dashboards: Dashboard[];
}

export interface RenameDashboardAction extends Action {
  type: typeof RENAME_DASHBOARD;
  id: string;
  name: string;
}

export interface DashboardRenamedAction extends Action {
  type: typeof DASHBOARD_RENAMED;
  id: string;
  name: string;
}

export interface DashboardDeletedAction extends Action {
  type: typeof DASHBOARD_DELETED;
  id: string;
}

export interface CloneDashboardAction extends Action {
  type: typeof CLONE_DASHBOARD;
  id: string;
  newUser: string;
}
export interface DashboardClonedAction extends Action {
  type: typeof DASHBOARD_CLONED;
  id: string;
}
export interface ShareDashboardAction extends Action{
  type: typeof SHARE_DASHBOARD;
  id: string;
  group: string;
}

export interface DashboardSharedAction extends Action{
  type: typeof DASHBOARD_SHARED;
  id: string;
  group: string;
  groupWriteAccess: boolean;
}

export interface DashboardLoadedAction extends Action {
  type: typeof DASHBOARD_LOADED;
  dashboard: Dashboard;
  widgets: Widget[];
  deviceList: string[];
}
export interface DashboardExportedAction extends Action {
  type: typeof DASHBOARD_EXPORTED;
  dashboard: Dashboard;
  widgets: Widget[];
}
export interface DashboardImportedAction extends Action {
  type: typeof DASHBOARD_IMPORTED;
  dashboard: Dashboard;
  widgets: Widget[];
}
export interface ReorderWidgetsAction extends Action{
  type: typeof REORDER_WIDGETS;
  widgets: Widget[];
}
export interface UpdateWidgetAction extends Action{
  type: typeof UPDATE_WIDGET;
  updatedWidget: Widget;
}
export interface SaveDashboardAction extends Action {
  type: typeof SAVE_DASHBOARD;
  id: string;
  name: string;
  widgets: Widget[];
  variables: Variable[];
}

export interface DashboardSavedAction extends Action {
  type: typeof DASHBOARD_SAVED;
  id: string;
  created: boolean;
  name: string;
  variables: Variable[];
}

export interface ShowNotificationAction extends Action {
  type: typeof SHOW_NOTIFICATION;
  notification: Notification;
}

export interface HideNotificationAction extends Action {
  type: typeof HIDE_NOTIFICATION;
  notification: Notification;
}

export interface dashboardEditedAction extends Action {
  type: typeof DASHBOARD_EDITED;
  dashboard: SelectedDashboardState

}

export interface WidgetClipboardCopyAction extends Action {
  type: typeof WIDGET_CLIPBOARD_COPY;
  widgets: Widget[];
}

export interface WidgetClipboardPasteAction extends Action {
  type: typeof WIDGET_CLIPBOARD_PASTE;
}

export interface ToggleInspectorCollapseAction extends Action {
  type: typeof TOGGLE_INSPECTOR_COLLAPSED;
}
export interface ToggleLibraryCollapseAction extends Action {
  type: typeof TOGGLE_LIBRARY_COLLAPSED;
}
export interface MouseDownOnWidgetAction extends Action {
  type: typeof MOUSE_DOWN_ON_WIDGET;
  isDown: boolean;
}

export type DashboardAction =
  | UndoAction
  | RedoAction
  | DuplicateWidgetAction
  | AddWidgetAction
  | MoveWidgetsAction
  | ResizeWidgetAction
  | SelectWidgetsAction
  | DeleteWidgetAction
  | SetInputAction
  | AddInputAction
  | DeleteInputAction
  | SelectCanvasAction
  | ToggleModeAction
  | DashboardLoadedAction
  | DashboardExportedAction
  | DashboardImportedAction
  | DashboardsLoadedAction
  | RenameDashboardAction
  | DashboardRenamedAction
  | DashboardDeletedAction
  | DashboardClonedAction
  | ShareDashboardAction
  | DashboardSharedAction
  | DashboardSavedAction
  | ShowNotificationAction
  | HideNotificationAction
  | CloneDashboardAction
  | DashboardClonedAction
  | dashboardEditedAction
  | WidgetClipboardCopyAction
  | WidgetClipboardPasteAction
  | ToggleInspectorCollapseAction
  | ToggleLibraryCollapseAction
  | MouseDownOnWidgetAction;

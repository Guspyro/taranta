import reducer from "./selectedDashboard";
import {
  selectWidgets,
  dashboardRenamed
} from "../actions/actionCreators";

const basicState = {
  widgets: {},
  selectedIds: [],
  id: "",
  name: "",
  user: "",
  group: "",
  lastUpdatedBy: "",
  insertTime: null,
  updateTime: null,
  groupWriteAccess: true,
  history: {
    undoActions: [],
    redoActions: [],
    undoIndex: 0,
    redoIndex: 0,
    undoLength: 0,
    redoLength: 0,
  },
  variables: [
    {
      _id: "0698hln1j359a",
      name: "myvar",
      class: "tg_test",
      device: "sys/tg_test/1",
    },
  ],
};

const savedState = {
  ...basicState,
  id: "5cb49ad146f4c024ece1cea5",
  insertTime: new Date("1990"),
  updateTime: new Date("2000"),
};

test("SELECT_WIDGETS", () => {
  const ids = ["2", "3", "5"];
  const action = selectWidgets(ids);

  const state = reducer(basicState, action);
  expect(state.selectedIds).toEqual(ids);
});

test("DASHBOARD_RENAMED", () => {
  const newName = "new name";
  const action = dashboardRenamed(savedState.id, newName);

  const state = reducer(savedState, action);
  expect(state.name).toEqual(newName);
});

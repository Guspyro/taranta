import { validateJson, duplicateWidgets, updateDependentInputs, validate } from "./lib";
import config from "../../../../config.json";
import { attributePlot } from "../../sagas/stateTestExamples";
import { WIDGET_MISSING_DEVICE, WIDGET_VALID, WIDGET_WARNING } from "../../actions/actionTypes";

const notValidJson = "{this is a not valid json}";
const validDashboard =
  '{ "id": "5ef9fbab6c54d30017ba960e", "name": "download", "version": "1.0.1", "user": "user1", "insertTime": "2020-06-29T14:33:15.553Z", "updateTime": "2020-06-29T14:40:28.991Z", "group": null, "groupWriteAccess": false, "lastUpdatedBy": "user1", "widget": [ { "id": "1", "x": 27, "y": 8, "canvas": "0", "width": 10, "height": 2, "type": "LABEL", "inputs": { "text": "", "textColor": "#000000", "backgroundColor": "#ffffff", "size": 1, "borderWidth": 0, "borderColor": "#000000", "font": "Helvetica", "linkTo": "", "customCss": "" }, "order": 0 }, {  "id": "2", "x": 29, "y": 16, "canvas": "0", "width": 10, "height": 2, "type": "ATTRIBUTE_DISPLAY", "inputs": { "attribute": { "device": null, "attribute": null }, "precision": 2, "showDevice": false, "showAttribute": "Label", "scientificNotation": false, "showEnumLabels": false, "textColor": "#000000", "backgroundColor": "#ffffff", "size": 1, "font": "Helvetica" }, "order": 1 } ] }';
const NotValidRequiredFields_name =
  '{ "id": "5efb1ba26c54d30017ba9615", "name": "", "version": "1.0.1", "user": "user1", "insertTime": "2020-06-30T11:01:54.233Z", "updateTime": "2020-06-30T11:01:54.233Z", "group": null, "groupWriteAccess": false, "lastUpdatedBy": "user1", "widget": [] }';
const NotValidRequiredField_user =
  '{ "id": "5efb1ba26c54d30017ba9615", "name": "NotValidFields", "version": "1.0.1", "user": "", "insertTime": "2020-06-30T11:01:54.233Z", "updateTime": "2020-06-30T11:01:54.233Z", "group": null, "groupWriteAccess": false, "lastUpdatedBy": "user1", "widget": [] }';
const NotValidRequiredField_updateTime =
  '{ "id": "5efb1ba26c54d30017ba9615", "name": "NotValidFields", "version": "1.0.1", "user": "user1", "insertTime": "2020-06-30T11:01:54.233Z", "updateTime": "", "group": null, "groupWriteAccess": false, "lastUpdatedBy": "user1", "widget": [] }';
const NotValidRequiredField_widget =
  '{ "id": "5efb1ba26c54d30017ba9615", "name": "NotValidFields", "version": "1.0.1", "user": "user1", "insertTime": "2020-06-30T11:01:54.233Z", "updateTime": "2020-06-30T11:01:54.233Z", "group": null, "groupWriteAccess": false, "lastUpdatedBy": "user1"}';
const NotValidRequiredField_notAllowedElement =
  '{ "id": "5efb1ba26c54d30017ba9615", "name": "NotValidFields", "userId": "1", "version": "1.0.1", "user": "user1", "insertTime": "2020-06-30T11:01:54.233Z", "updateTime": "2020-06-30T11:01:54.233Z", "group": null, "groupWriteAccess": false, "lastUpdatedBy": "user1", "widget": [] }';
const validDashboard_wrongWidget =
  '{ "id": "5ef9fbab6c54d30017ba960e", "name": "download", "version": "1.0.1", "user": "user1", "insertTime": "2020-06-29T14:33:15.553Z", "updateTime": "2020-06-29T14:40:28.991Z", "group": null, "groupWriteAccess": false, "lastUpdatedBy": "user1", "widget": [ { "id": "1", "x": 27, "y": 8, "canvas": "0", "width": 10, "height": 2, "type": "WRONGWIDGET", "inputs": { "text": "", "textColor": "#000000", "backgroundColor": "#ffffff", "size": 1, "borderWidth": 0, "borderColor": "#000000", "font": "Helvetica", "linkTo": "", "customCss": "" }, "order": 0 }, {  "id": "2", "x": 29, "y": 16, "canvas": "0", "width": 10, "height": 2, "type": "ATTRIBUTE_DISPLAY", "inputs": { "attribute": { "device": null, "attribute": null }, "precision": 2, "showDevice": false, "showAttribute": "Label", "scientificNotation": false, "showEnumLabels": false, "textColor": "#000000", "backgroundColor": "#ffffff", "size": 1, "font": "Helvetica" }, "order": 1 } ] }';
let valideDashboardAllWidget =
  '{ "id": "5efc43476c54d30017ba9616", "name": "CompleteDashboard", "version": "1.0.1", "user": "user1", "insertTime": "2020-07-01T08:03:19.515Z", "updateTime": "2020-07-01T08:07:25.582Z", "group": null, "groupWriteAccess": false, "lastUpdatedBy": "user1", "widget": [ {  "id": "1", "x": 4, "y": 1, "canvas": "0", "width": 10, "height": 2, "type": "LABEL", "inputs": { "text": "test label", "textColor": "#000000", "backgroundColor": "#ffffff", "size": 1, "borderWidth": 0, "borderColor": "#000000", "font": "Helvetica", "linkTo": "", "customCss": "" }, "order": 0 }, {  "id": "2", "x": 3, "y": 5, "canvas": "0", "width": 10, "height": 2, "type": "ATTRIBUTE_DISPLAY", "inputs": { "attribute": { "device": "sys/tg_test/1", "attribute": "ampli", "label": "ampli" }, "precision": 2, "showDevice": false, "showAttribute": "Label", "scientificNotation": false, "showEnumLabels": false, "textColor": "#000000", "backgroundColor": "#ffffff", "size": 1, "font": "Helvetica" }, "order": 1 }, {  "id": "3", "x": 2, "y": 10, "canvas": "0", "width": 15, "height": 2, "type": "ATTRIBUTE_WRITER", "inputs": { "attribute": { "device": "sys/tg_test/1", "attribute": "throw_exception", "label": "throw_exception" }, "showDevice": true, "showAttribute": "Label", "textColor": "#000000", "backgroundColor": "#ffffff", "size": 1, "font": "Helvetica" }, "order": 2 }, { "id": "4", "x": 1, "y": 16, "canvas": "0", "width": 30, "height": 20, "type": "ATTRIBUTE_PLOT", "inputs": { "timeWindow": 120, "showZeroLine": true, "logarithmic": false, "attributes": [ { "attribute": { "device": "sys/tg_test/1", "attribute": "double_scalar", "label": "double_scalar" }, "showAttribute": "Label", "yAxis": "left" } ] }, "order": 3 }, { "id": "5", "x": 22, "y": 1, "canvas": "0", "width": 24, "height": 12, "type": "ATTRIBUTE_SCATTER", "inputs": { "showAttribute": "Label", "independent": { "device": "sys/tg_test/1", "attribute": "double_scalar", "label": "double_scalar" }, "dependent": { "device": "sys/tg_test/1", "attribute": "ampli", "label": "ampli" } }, "order": 4 }, { "id": "6", "x": 52, "y": 2, "canvas": "0", "width": 30, "height": 20, "type": "SPECTRUM", "inputs": { "attribute": { "device": "sys/tg_test/1", "attribute": "double_spectrum_ro", "label": "double_spectrum_ro" }, "showAttribute": "Label", "showTitle": true, "inelastic": true }, "order": 5 }, { "id": "8", "x": 37, "y": 31, "canvas": "0", "width": 10, "height": 10, "type": "ATTRIBUTE_DIAL", "inputs": { "attribute": { "device": "sys/tg_test/1", "attribute": "short_scalar", "label": "short_scalar" }, "min": 0, "max": 10, "label": "attribute", "showWriteValue": false }, "order": 7 }, { "id": "9", "x": 50, "y": 25, "canvas": "0", "width": 10, "height": 2, "type": "BOOLEAN_DISPLAY", "inputs": { "attribute": { "device": "sys/tg_test/1", "attribute": "boolean_scalar", "label": "boolean_scalar" }, "showAttribute": "Label", "showDevice": false }, "order": 8 }, { "id": "10", "x": 50, "y": 32, "canvas": "0", "width": 24, "height": 2, "type": "DEVICE_STATUS", "inputs": { "device": "sys/tg_test/1", "state": { "device": null, "attribute": null }, "showDeviceName": true, "showStateString": true, "showStateLED": true, "LEDSize": 1, "textSize": 1 }, "order": 9 }, { "id": "11", "x": 53, "y": 37, "canvas": "0", "width": 2, "height": 2, "type": "LED_DISPLAY", "inputs": { "attribute": { "device": "sys/tg_test/1", "attribute": "double_scalar", "label": "double_scalar" }, "relation": ">", "compare": 0, "trueColor": "#3ac73a", "falseColor": "#ff0000", "ledSize": 1, "textSize": 1, "showAttributeValue": false, "showDeviceName": false, "showAttribute": "Label" }, "order": 10 }, { "id": "12", "x": 4, "y": 42, "canvas": "0", "width": 24, "height": 2, "type": "SARDANA_MOTOR", "inputs": { "device": "sys/tg_test/1", "precision": 3, "stop": { "device": null, "command": null }, "limits": { "device": null, "attribute": null }, "position": { "device": null, "attribute": null }, "power": { "device": null, "attribute": null }, "state": { "device": null, "attribute": null } }, "order": 11 }, { "id": "13", "x": 35, "y": 43, "canvas": "0", "width": 50, "height": 20, "type": "ATTRIBUTE_LOGGER", "inputs": { "attribute": { "device": "sys/tg_test/1", "attribute": "ampli", "label": "ampli" }, "linesDisplayed": 50, "showDevice": false, "showAttribute": "Label", "logIfChanged": false }, "order": 12 }, { "id": "14", "x": 2, "y": 47, "canvas": "0", "width": 21, "height": 17, "type": "ATTRIBUTEHEATMAP", "inputs": { "attribute": { "device": "sys/tg_test/1", "attribute": "boolean_image", "label": "boolean_image" }, "showTitle": true, "showAttribute": "Label", "fixedScale": true, "maxValue": 30, "minValue": 1 }, "order": 13 }, { "id": "15", "x": 6, "y": 38, "canvas": "0", "width": 10, "height": 3, "type": "SPECTRUM_TABLE", "inputs": { "attribute": { "device": "sys/tg_test/1", "attribute": "double_spectrum", "label": "double_spectrum" }, "layout": "horizontal", "showDevice": false, "showAttribute": "Label", "showIndex": false, "showLabel": false, "fontSize": 16 }, "order": 14 }, { "id": "16", "x": 3, "y": 65, "canvas": "0", "width": 15, "height": 20, "type": "MULTIPLE_COMMANDS_EXECUTOR", "inputs": { "command": { "device": "sys/tg_test/1", "command": "DevVoid" }, "title": "title", "requireConfirmation": true, "displayOutput": true, "cooldown": 0, "textColor": "#000", "backgroundColor": "#ffffff", "size": 1, "font": "Helvetica" }, "order": 15 } ] }';

  let duplicateWidgetState = {
  widgets: JSON.parse(
    '{ "1": { "_id": "61c07a50aad3740019b2945d", "id": "1", "x": 19, "y": 0, "canvas": "0", "width": 15, "height": 8, "type": "BOX", "inputs": { "title": "", "bigWidget": 10, "smallWidget": 1, "textColor": "#000000", "backgroundColor": "#ffffff", "borderColor": "", "borderWidth": 0, "borderStyle": "none", "textSize": 1, "fontFamily": "Helvetica", "layout": "horizontal", "padding": 0, "customCss": "" }, "order": 0, "innerWidgets": [ { "id": "2", "x": 19.4, "y": 1.04, "canvas": "0", "width": 7.1, "height": 6.959999999999999, "type": "ATTRIBUTE_DISPLAY", "inputs": { "attribute": { "device": "sys/tg_test/1", "attribute": "short_scalar", "label": "short_scalar" }, "precision": 2, "showDevice": false, "showAttribute": "Label", "scientificNotation": false, "showEnumLabels": false, "textColor": "#000000", "backgroundColor": "#ffffff", "size": 1, "font": "Helvetica" }, "valid": true, "order": 3 }, { "id": "7", "x": 26.5, "y": 1.04, "canvas": "0", "width": 7.1, "height": 6.959999999999999, "type": "BOX", "inputs": { "title": "", "bigWidget": 10, "smallWidget": 1, "textColor": "#000000", "backgroundColor": "#ffffff", "borderColor": "", "borderWidth": 0, "borderStyle": "none", "textSize": 1, "fontFamily": "Helvetica", "layout": "vertical", "padding": 0, "customCss": "" }, "valid": true, "innerWidgets": [ { "id": "8", "x": 26.9, "y": 1.9968, "canvas": "0", "width": 6.3, "height": 6, "type": "ATTRIBUTE_DISPLAY", "inputs": { "attribute": { "device": "sys/tg_test/1", "attribute": "long_scalar", "label": "long_scalar" }, "precision": 2, "showDevice": false, "showAttribute": "Label", "scientificNotation": false, "showEnumLabels": false, "textColor": "#000000", "backgroundColor": "#ffffff", "size": 1, "font": "Helvetica" }, "valid": true, "order": 1 } ], "order": 4 } ], "valid": true, "hover": [] } }'
  ),
  selectedIds: ["1"],
};
const tabularWidget = {
  id: "1",
  type: "TABULAR_VIEW",
  inputs: {
    devices: [
      {
        device: "test/tarantatestdevice/1",
      },
      {
        device: "sys/tg_test/1",
      },
    ],
    showDefaultAttribute: false,
    customAttributes: [
      {
        attribute: {
          device: "",
          attribute: "state",
          label: "State",
        },
      },
      {
        attribute: {
          device: "",
          attribute: "double_scalar",
          label: "double_scalar",
        },
      },
      {
        attribute: {
          device: "",
          attribute: "short_scalar",
          label: "short_scalar",
          hideList: true,
        },
      },
    ],
    widgetCss: "",
    compactTable: true,
    borderedTable: true,
    textColor: "#000000",
    backgroundColor: "#ffffff",
    size: 1,
    font: "Helvetica",
  },
  order: 0,
  valid: 1,
  canvas: "test",
  x: 0,
  y: 0,
  width: 100,
  height: 100,
};

const widgetsToHide: Array<String> = config.WIDGETS_TO_HIDE;
if (Array.isArray(widgetsToHide)) {
  valideDashboardAllWidget = JSON.parse(valideDashboardAllWidget);

  const filteredWidgets = valideDashboardAllWidget["widget"].filter(
    (widget) => -1 === widgetsToHide.indexOf(widget.type)
  );
  valideDashboardAllWidget["widget"] = filteredWidgets;
  valideDashboardAllWidget = JSON.stringify(valideDashboardAllWidget);
}

describe("VALIDATE_JSON", () => {
  it("test if the function validate a well-format JSON", () => {
    expect(validateJson(validDashboard).validationResult).toEqual(true);
  });

  it("test if the function detect a not valid json", () => {
    expect(validateJson(notValidJson).validationResult).toEqual(false);
    expect(validateJson(notValidJson).importError.toString()).toContain(
      "Unexpected token t in JSON at position 1"
    );
  });

  //test if the function not validate wrong field attributes

  it("check name", () => {
    expect(validateJson(NotValidRequiredFields_name).validationResult).toEqual(
      false
    );
    expect(
      validateJson(NotValidRequiredFields_name).importError.toString()
    ).toContain("Dashboard name missing");
  });

  it("check user", () => {
    expect(validateJson(NotValidRequiredField_user).validationResult).toEqual(
      false
    );
    expect(
      validateJson(NotValidRequiredField_user).importError.toString()
    ).toContain("User missing");
  });
  //check updateTime
  it("check updateTime", () => {
    expect(
      validateJson(NotValidRequiredField_updateTime).validationResult
    ).toEqual(false);
    expect(
      validateJson(NotValidRequiredField_updateTime).importError.toString()
    ).toContain("Update time missing");
  });

  it("check widgets", () => {
    expect(validateJson(NotValidRequiredField_widget).validationResult).toEqual(
      false
    );
    expect(
      validateJson(NotValidRequiredField_widget).importError.toString()
    ).toContain("Widgets are missing");
  });

  it("check not allowed elements", () => {
    expect(
      validateJson(NotValidRequiredField_notAllowedElement).validationResult
    ).toEqual(false);
    expect(
      validateJson(
        NotValidRequiredField_notAllowedElement
      ).importError.toString()
    ).toContain("userId not accepted");
  });

  //

  it("test a correct dashboard containing all the widgets available in the version 1.0.1", () => {
    expect(validateJson(valideDashboardAllWidget).validationResult).toEqual(
      true
    );
    expect(validateJson(valideDashboardAllWidget).importError).toEqual([]);
  });

  it("test duplicate widget", () => {
    const newWidgets = Object.assign({}, duplicateWidgetState.widgets);
    const newIds = [];

    duplicateWidgets(duplicateWidgetState, newWidgets, newIds);

    expect(Object.values(newWidgets).length).toEqual(2);
    expect(newWidgets["9"]).not.toEqual(undefined);
  });

  it("test tabular widget", () => {
    // Device field will be updated on all cust attr
    updateDependentInputs([tabularWidget]);
    expect(
      tabularWidget.inputs.customAttributes[0].attribute.device
    ).toEqual("test/tarantatestdevice/1,sys/tg_test/1");
    // Duplicate attr3 will be removed
    expect(tabularWidget.inputs.customAttributes.length).toEqual(2);

    tabularWidget.inputs.showDefaultAttribute = true;
    config.defaultAttribute = ["Status", "ObsState", "AdminMode"];
    // 3 Default attribute will be added
    updateDependentInputs([tabularWidget]);
    expect(tabularWidget.inputs.customAttributes.length).toEqual(5);
  });

  it("test validate function", () => {
    const variables = [
      {id: '11', 'name': 'CSPVar1', class: 'tg_test', device: 'dserver/databaseds/2' },
    ];

     expect(
      validate(attributePlot, [], ['sys/tg_test/11']).valid
    ).toEqual(WIDGET_MISSING_DEVICE);

    expect(
      validate(attributePlot, [], ['sys/tg_test/1']).valid
    ).toEqual(WIDGET_VALID);

    expect(
      validate(attributePlot, variables, ['sys/tg_test/1']).valid
    ).toEqual(WIDGET_VALID);

    attributePlot.inputs.attributes[0].attribute.device = 'var22';
    expect(
      validate(attributePlot, variables).valid
    ).toEqual(WIDGET_WARNING);

  });
});

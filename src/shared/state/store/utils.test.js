import { retriveDeviceFromSub } from './utils'; 

describe('retriveDeviceFromSub', () => {
  test('should return an empty array when sub is undefined', () => {
    const sub = undefined;
    const result = retriveDeviceFromSub(sub);
    expect(result).toEqual([]);
  });

  test('should return the correct array of objects', () => {
    const sub = [
      'sys/tg_test/1/ampli',
      'sys/tg_test/1/State',
      'sys/database/2/status',
      'sys/database/2/State'
    ];
    const result = retriveDeviceFromSub(sub);
    expect(result).toEqual([
      {
        name: 'sys/tg_test/1',
        attributes: [
          { name: 'ampli' },
          { name: 'State' }
        ]
      },
      {
        name: 'sys/database/2',
        attributes: [
          { name: 'status' },
          { name: 'State' }
        ]
      }
    ]);
  });
});
import config from "../../../config.json";

export function socketUrl() {
    const loc = window.location;
    const protocol = loc.protocol.replace("http", "ws");
    const url = loc.href.replace(config.basename, "").split('/');
    return (
      protocol +
      "//" +
      loc.host +
      config.basename +
      "/" +
      url[3] +
      "/socket"
    );
  }

export function retriveDeviceFromSub(sub){

  const result = [];
  if(sub !== undefined)
  {
    sub.forEach(item => {
      const parts = item.split("/");
      const key = parts.slice(0, -1).join("/");
      const value = parts[parts.length - 1];
    
      const existingObj = result.find(obj => obj.name === key);
    
      if (existingObj) {
        existingObj.attributes.push({ name: value });
      } else {
        const newObj = {
          name: key,
          attributes: [{ name: value }]
        };
        result.push(newObj);
      }
    });
  }
  return result
}
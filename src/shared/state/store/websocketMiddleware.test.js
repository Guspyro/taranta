import '../../tests/globalMocks';
import { websocketMiddleware } from './websocketMiddleware'
import { WEBSOCKET, TOGGLE_MODE, DASHBOARD_LOADED } from '../actions/actionTypes'

jest.mock('../../../shared/api/UserActions', () => jest.fn().mockImplementation(() => ({
  executeCommand: jest.fn()
})))

jest.mock('../../../shared/utils/fetchInitialValues', () => ({
  fetchInitialValues: jest.fn()
}))

// Mock a Redux store
const createStore = () => {
  const dispatched = []
  const store = {
    getState: jest.fn(() => ({})),
    dispatch: jest.fn(action => dispatched.push(action)),
    dispatched
  }
  return store
}

describe('websocketMiddleware', () => {
  it('should dispatch WS_SUBSCRIBE action when action.type is WEBSOCKET.WS_SUBSCRIBE', () => {
    // Arrange
    const store = createStore()
    const next = jest.fn()
    const action = {
      type: WEBSOCKET.WS_SUBSCRIBE,
      payload: {
        devices: ['device1/attribute1', 'device2/attribute2']
      }
    }

    websocketMiddleware(store)(next)(action);
    expect(next).toHaveBeenCalledWith(action);
  })

  it('should subscribe to websockets when action.type is DASHBOARD_LOADED and ui.mode is run', async () => {
    // Arrange
    const store = createStore()
    const next = jest.fn()
    const action = {
      type: DASHBOARD_LOADED,
      dashboard: {
        "id": "644a66edc4c44a00123efb04",
        "name": "Untitled dashboard",
        "user": "user1",
        "insertTime": "2023-04-27T12:13:33.670Z",
        "updateTime": "2023-04-27T12:13:43.094Z",
        "group": null,
        "groupWriteAccess": false,
        "lastUpdatedBy": "user1",
        "variables": []
      },
      widgets: [
        {
          "_id": "644a66f7c4c44a00123efb07",
          "id": "1",
          "x": 30,
          "y": 11,
          "canvas": "0",
          "width": 10,
          "height": 2,
          "type": "ATTRIBUTE_DISPLAY",
          "inputs": {
            "attribute": {
              "device": "sys/tg_test/1",
              "attribute": "state",
              "label": "State"
            },
            "precision": 2,
            "showDevice": false,
            "showAttribute": "Label",
            "scientificNotation": false,
            "showEnumLabels": false,
            "showAttrQuality": false,
            "textColor": "#000000",
            "backgroundColor": "#ffffff",
            "size": 1,
            "font": "Helvetica",
            "widgetCss": ""
          },
          "order": 0
        }
      ]
    }

    const mockUiState = {
      mode: 'run',
      // add any other properties you need for your test
    };
    const mockSelectedDashboard = {
      id: 'dashboardId',
    };
    store.getState = jest.fn(() => ({
      ui: mockUiState,
      selectedDashboard: mockSelectedDashboard
    }));

    // Act
    await websocketMiddleware(store)(next)(action);
    expect(store.getState).toHaveBeenCalledTimes(2);
  });


  it('should subscribe to websockets when action.type is TOGGLE_MODE and ui.mode is run', async () => {
    const store = createStore()
    const next = jest.fn()
    const action = {
      type: TOGGLE_MODE
    }

    const mockUiState = {
      mode: 'run',
      // add any other properties you need for your test
    };
    const mockSelectedDashboard = {
      id: 'dashboardId',
      widgets: {
        widgetId: {
          inputs: {
            attribute: {
              device: 'deviceName',
              attribute: 'attributeName'
            }
          }
        }
      }
    };
    store.getState = jest.fn(() => ({
      ui: mockUiState,
      selectedDashboard: mockSelectedDashboard
    }));

    // Act
    await websocketMiddleware(store)(next)(action);
    expect(store.getState).toHaveBeenCalledTimes(1);

  });

  it('should subscribe to websockets when action.type is TOGGLE_MODE and ui.mode is run', async () => {
    const store = createStore()
    const next = jest.fn()
    const action = {
      type: TOGGLE_MODE
    }

    const mockUiState = {
      mode: 'edit',
      // add any other properties you need for your test
    };
    const mockSelectedDashboard = {
      id: 'dashboardId',
      widgets: {
        widgetId: {
          inputs: {
            attribute: {
              device: 'deviceName',
              attribute: 'attributeName'
            }
          }
        }
      }
    };
    store.getState = jest.fn(() => ({
      ui: mockUiState,
      selectedDashboard: mockSelectedDashboard
    }));

    // Act
    await websocketMiddleware(store)(next)(action);
    expect(store.getState).toHaveBeenCalledTimes(4);
  });

});
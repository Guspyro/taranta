export const statePasteExample = {
    "selectedId": null,
    "selectedIds": [
        "1"
    ],
    "widgets": {
        "1": {
            "_id": "6421627682a05800192d35be",
            "id": "1",
            "x": 25,
            "y": 8,
            "canvas": "0",
            "width": 19,
            "height": 4,
            "type": "ATTRIBUTE_DISPLAY",
            "inputs": {
                "attribute": {
                    "device": "sys/tg_test/1",
                    "attribute": "double_scalar",
                    "label": "double_scalar"
                },
                "precision": 2,
                "showDevice": true,
                "showAttribute": "Label",
                "scientificNotation": false,
                "showEnumLabels": false,
                "showAttrQuality": false,
                "textColor": "#000000",
                "backgroundColor": "#ffffff",
                "size": 1,
                "font": "Helvetica",
                "widgetCss": ""
            },
            "order": 0,
            "valid": true
        }
    },
    "id": "6421627682a05800192d35bd",
    "name": "randomAttr",
    "user": "user1",
    "group": null,
    "groupWriteAccess": false,
    "lastUpdatedBy": "user1",
    "insertTime": "2023-03-27T09:31:34.185Z",
    "updateTime": "2023-03-27T12:52:32.835Z",
    "history": {
        "undoActions": [
            {
                "1": {
                    "_id": "6421627682a05800192d35be",
                    "id": "1",
                    "x": 25,
                    "y": 8,
                    "canvas": "0",
                    "width": 19,
                    "height": 4,
                    "type": "ATTRIBUTE_DISPLAY",
                    "inputs": {
                        "attribute": {
                            "device": "sys/tg_test/1",
                            "attribute": "double_scalar",
                            "label": "double_scalar"
                        },
                        "precision": 2,
                        "showDevice": true,
                        "showAttribute": "Label",
                        "scientificNotation": false,
                        "showEnumLabels": false,
                        "showAttrQuality": false,
                        "textColor": "#000000",
                        "backgroundColor": "#ffffff",
                        "size": 1,
                        "font": "Helvetica",
                        "widgetCss": ""
                    },
                    "order": 0,
                    "valid": true
                }
            }
        ],
        "redoActions": [],
        "undoIndex": 1,
        "redoIndex": 0,
        "undoLength": 1,
        "redoLength": 0
    },
    "variables": []
}

export const dashboardPasteExample = [
    {
        "id": "6421627682a05800192d35bd",
        "name": "randomAttr",
        "user": "user1",
        "insertTime": "2023-03-27T09:31:34.185Z",
        "updateTime": "2023-03-27T12:52:32.835Z",
        "group": null,
        "lastUpdatedBy": "user1",
        "tangoDB": "testdb",
        "variables": []
    }
];

export const clipBoardExample = [
    {
        "id": "",
        "x": 25,
        "y": 8,
        "canvas": "0",
        "width": 19,
        "height": 4,
        "type": "ATTRIBUTE_DISPLAY",
        "inputs": {
            "attribute": {
                "device": "sys/tg_test/1",
                "attribute": "double_scalar",
                "label": "double_scalar"
            },
            "precision": 2,
            "showDevice": true,
            "showAttribute": "Label",
            "scientificNotation": false,
            "showEnumLabels": false,
            "showAttrQuality": false,
            "textColor": "#000000",
            "backgroundColor": "#ffffff",
            "size": 1,
            "font": "Helvetica",
            "widgetCss": ""
        },
        "order": 0,
        "valid": true
    }
]

export const addWidgetExample = {
    "x": 32,
    "y": 12,
    "widgetType": "LABEL",
    "canvas": "0",
    "type": "ADD_WIDGET"
}

export const reorderInnerWidgetPayload = {
    "type": "REORDER_INNER_WIDGET",
    "x": 35,
    "y": 10,
    "parentWidget": {
        "id": "2",
        "x": 28,
        "y": 5,
        "canvas": "0",
        "width": 20,
        "height": 10,
        "type": "BOX",
        "inputs": {
            "title": "",
            "bigWidget": 4,
            "smallWidget": 1,
            "textColor": "#000000",
            "backgroundColor": "#ffffff",
            "borderColor": "",
            "borderWidth": 0,
            "borderStyle": "solid",
            "textSize": 1,
            "fontFamily": "Helvetica",
            "layout": "vertical",
            "alignment": "Center",
            "padding": 0,
            "customCss": ""
        },
        "valid": true,
        "order": 1,
        "hover": [],
        "innerWidgets": [
            {
                "id": "1",
                "x": 28.4,
                "y": 6.2,
                "canvas": "0",
                "width": 19.2,
                "height": 4.4,
                "type": "LABEL",
                "inputs": {
                    "text": "",
                    "textColor": "#000000",
                    "backgroundColor": "#ffffff",
                    "size": 1,
                    "borderWidth": 0,
                    "borderColor": "#000000",
                    "font": "Helvetica",
                    "linkTo": "",
                    "customCss": ""
                },
                "valid": true,
                "order": 1
            },
            {
                "id": "3",
                "x": 28.4,
                "y": 10.600000000000001,
                "canvas": "0",
                "width": 19.2,
                "height": 4.4,
                "type": "LABEL",
                "inputs": {
                    "text": "",
                    "textColor": "#000000",
                    "backgroundColor": "#ffffff",
                    "size": 1,
                    "borderWidth": 0,
                    "borderColor": "#000000",
                    "font": "Helvetica",
                    "linkTo": "",
                    "customCss": ""
                },
                "valid": true,
                "order": 2
            }
        ]
    },
    "innerWidget": {
        "id": "3",
        "x": 28.4,
        "y": 10.600000000000001,
        "canvas": "0",
        "width": 19.2,
        "height": 4.4,
        "type": "LABEL",
        "inputs": {
            "text": "",
            "textColor": "#000000",
            "backgroundColor": "#ffffff",
            "size": 1,
            "borderWidth": 0,
            "borderColor": "#000000",
            "font": "Helvetica",
            "linkTo": "",
            "customCss": ""
        },
        "valid": true,
        "order": 2
    }
}

export const addInnerWidget = {
    "type": "ADD_INNER_WIDGET",
    "x": 38,
    "y": 16,
    "widget": {
        "type": "LABEL"
    },
    "parentID": "2",
    "innerWidgetCheck": true
}

export const stateAddInnerWidget = {
    "selectedId": null,
    "selectedIds": [],
    "widgets": {
        "2": {
            "_id": "6421999882a05800192d35e7",
            "id": "2",
            "x": 28,
            "y": 5,
            "canvas": "0",
            "width": 20,
            "height": 10,
            "type": "BOX",
            "inputs": {
                "title": "",
                "bigWidget": 4,
                "smallWidget": 1,
                "textColor": "#000000",
                "backgroundColor": "#ffffff",
                "borderColor": "",
                "borderWidth": 0,
                "borderStyle": "solid",
                "textSize": 1,
                "fontFamily": "Helvetica",
                "layout": "vertical",
                "alignment": "Center",
                "padding": 0,
                "customCss": ""
            },
            "order": 0,
            "innerWidgets": [
                {
                    "id": "3",
                    "x": 28.4,
                    "y": 6.2,
                    "canvas": "0",
                    "width": 19.2,
                    "height": 2.9,
                    "type": "LABEL",
                    "inputs": {
                        "text": "",
                        "textColor": "#000000",
                        "backgroundColor": "#ffffff",
                        "size": 1,
                        "borderWidth": 0,
                        "borderColor": "#000000",
                        "font": "Helvetica",
                        "linkTo": "",
                        "customCss": ""
                    },
                    "valid": true,
                    "order": 2
                },
                {
                    "id": "5",
                    "x": 28.4,
                    "y": 9.1,
                    "canvas": "0",
                    "width": 19.2,
                    "height": 2.9,
                    "type": "ATTRIBUTE_DISPLAY",
                    "inputs": {
                        "attribute": {
                            "device": "sys/tg_test/1",
                            "attribute": "boolean_scalar",
                            "label": "boolean_scalar"
                        },
                        "precision": 2,
                        "showDevice": false,
                        "showAttribute": "Label",
                        "scientificNotation": false,
                        "showEnumLabels": false,
                        "showAttrQuality": false,
                        "textColor": "#000000",
                        "backgroundColor": "#ffffff",
                        "size": 1,
                        "font": "Helvetica",
                        "widgetCss": ""
                    },
                    "valid": true,
                    "order": 3
                },
                {
                    "id": "6",
                    "x": 38,
                    "y": 16,
                    "canvas": "0",
                    "width": 19.2,
                    "height": 2.9,
                    "type": "LABEL",
                    "inputs": {
                        "text": "",
                        "textColor": "#000000",
                        "backgroundColor": "#ffffff",
                        "size": 1,
                        "borderWidth": 0,
                        "borderColor": "#000000",
                        "font": "Helvetica",
                        "linkTo": "",
                        "customCss": ""
                    },
                    "valid": true,
                    "order": 4
                }
            ],
            "valid": true
        }
    },
    "id": "6421627682a05800192d35bd",
    "name": "randomAttr",
    "user": "user1",
    "group": null,
    "groupWriteAccess": false,
    "lastUpdatedBy": "user1",
    "insertTime": "2023-03-27T09:31:34.185Z",
    "updateTime": "2023-03-27T13:26:48.222Z",
    "history": {
        "undoActions": [],
        "redoActions": [],
        "undoIndex": 1,
        "redoIndex": 0,
        "undoLength": 1,
        "redoLength": 0
    },
    "variables": []
}

export const dropInnerWidgetExample = {
    "type": "DROP_INNER_WIDGET",
    "x": 25,
    "y": 22,
    "innerWidget": {
        "id": "3",
        "x": 15.4,
        "y": 17.6,
        "canvas": "0",
        "width": 19.2,
        "height": 8.8,
        "type": "LABEL",
        "inputs": {
            "text": "",
            "textColor": "#000000",
            "backgroundColor": "#ffffff",
            "size": 1,
            "borderWidth": 0,
            "borderColor": "#000000",
            "font": "Helvetica",
            "linkTo": "",
            "customCss": ""
        },
        "valid": true,
        "order": 2
    }
}

export const moveWidgetExample = {
    "type": "MOVE_WIDGET",
    "dx": 1,
    "dy": 0,
    "ids": [
        "1"
    ]
}

export const moveWidgetStateExample = {
    "selectedId": null,
    "selectedIds": [
        "1"
    ],
    "widgets": {
        "1": {
            "_id": "64219a9c82a05800192d35f1",
            "id": "1",
            "x": 32,
            "y": 11,
            "canvas": "0",
            "width": 10,
            "height": 2,
            "type": "LABEL",
            "inputs": {
                "text": "",
                "textColor": "#000000",
                "backgroundColor": "#ffffff",
                "size": 1,
                "borderWidth": 0,
                "borderColor": "#000000",
                "font": "Helvetica",
                "linkTo": "",
                "customCss": ""
            },
            "order": 0,
            "valid": true
        }
    },
    "id": "6421627682a05800192d35bd",
    "name": "randomAttr",
    "user": "user1",
    "group": null,
    "groupWriteAccess": false,
    "lastUpdatedBy": "user1",
    "insertTime": "2023-03-27T09:31:34.185Z",
    "updateTime": "2023-03-27T13:31:08.266Z",
    "history": {
        "undoActions": [
            {
                "1": {
                    "_id": "64219a9c82a05800192d35f1",
                    "id": "1",
                    "x": 32,
                    "y": 11,
                    "canvas": "0",
                    "width": 10,
                    "height": 2,
                    "type": "LABEL",
                    "inputs": {
                        "text": "",
                        "textColor": "#000000",
                        "backgroundColor": "#ffffff",
                        "size": 1,
                        "borderWidth": 0,
                        "borderColor": "#000000",
                        "font": "Helvetica",
                        "linkTo": "",
                        "customCss": ""
                    },
                    "order": 0,
                    "valid": true
                }
            }
        ],
        "redoActions": [],
        "undoIndex": 1,
        "redoIndex": 0,
        "undoLength": 1,
        "redoLength": 0
    },
    "variables": []
}

export const resizeWidgetExample = {
    "type": "RESIZE_WIDGET",
    "mx": 0,
    "my": 0,
    "dx": 1,
    "dy": 1,
    "id": "1"
}

export const deleteWidgetStateExample = {
    "selectedId": null,
    "selectedIds": [
        "2"
    ],
    "widgets": {
        "1": {
            "_id": "64219a9c82a05800192d35f1",
            "id": "1",
            "x": 31,
            "y": 11,
            "canvas": "0",
            "width": 11,
            "height": 3,
            "type": "LABEL",
            "inputs": {
                "text": "",
                "textColor": "#000000",
                "backgroundColor": "#ffffff",
                "size": 1,
                "borderWidth": 0,
                "borderColor": "#000000",
                "font": "Helvetica",
                "linkTo": "",
                "customCss": ""
            },
            "order": 0,
            "valid": true
        },
        "2": {
            "_id": "64219b9e82a05800192d35f5",
            "id": "2",
            "x": 32,
            "y": 12,
            "canvas": "0",
            "width": 11,
            "height": 3,
            "type": "LABEL",
            "inputs": {
                "text": "",
                "textColor": "#000000",
                "backgroundColor": "#ffffff",
                "size": 1,
                "borderWidth": 0,
                "borderColor": "#000000",
                "font": "Helvetica",
                "linkTo": "",
                "customCss": ""
            },
            "order": 1,
            "valid": true
        }
    },
    "id": "6421627682a05800192d35bd",
    "name": "randomAttr",
    "user": "user1",
    "group": null,
    "groupWriteAccess": false,
    "lastUpdatedBy": "user1",
    "insertTime": "2023-03-27T09:31:34.185Z",
    "updateTime": "2023-03-27T13:35:26.836Z",
    "history": {
        "undoActions": [
            {
                "1": {
                    "_id": "64219a9c82a05800192d35f1",
                    "id": "1",
                    "x": 31,
                    "y": 11,
                    "canvas": "0",
                    "width": 11,
                    "height": 3,
                    "type": "LABEL",
                    "inputs": {
                        "text": "",
                        "textColor": "#000000",
                        "backgroundColor": "#ffffff",
                        "size": 1,
                        "borderWidth": 0,
                        "borderColor": "#000000",
                        "font": "Helvetica",
                        "linkTo": "",
                        "customCss": ""
                    },
                    "order": 0,
                    "valid": true
                },
                "2": {
                    "_id": "64219b9e82a05800192d35f5",
                    "id": "2",
                    "x": 32,
                    "y": 12,
                    "canvas": "0",
                    "width": 11,
                    "height": 3,
                    "type": "LABEL",
                    "inputs": {
                        "text": "",
                        "textColor": "#000000",
                        "backgroundColor": "#ffffff",
                        "size": 1,
                        "borderWidth": 0,
                        "borderColor": "#000000",
                        "font": "Helvetica",
                        "linkTo": "",
                        "customCss": ""
                    },
                    "order": 1,
                    "valid": true
                }
            }
        ],
        "redoActions": [],
        "undoIndex": 1,
        "redoIndex": 0,
        "undoLength": 1,
        "redoLength": 0
    },
    "variables": []
}

export const setInputExample = {
    "type": "SET_INPUT",
    "path": [
        "attribute"
    ],
    "value": {
        "device": "sys/tg_test/1",
        "attribute": null,
        "label": null
    },
    "widgetType": "ATTRIBUTE_DISPLAY"
}

export const setInputStatExample = {
    "selectedId": null,
    "selectedIds": [
        "1"
    ],
    "widgets": {
        "1": {
            "_id": "64219ca082a05800192d35f7",
            "id": "1",
            "x": 33,
            "y": 11,
            "canvas": "0",
            "width": 10,
            "height": 2,
            "type": "ATTRIBUTE_DISPLAY",
            "inputs": {
                "attribute": {
                    "device": null,
                    "attribute": null
                },
                "precision": 2,
                "showDevice": false,
                "showAttribute": "Label",
                "scientificNotation": false,
                "showEnumLabels": false,
                "showAttrQuality": false,
                "textColor": "#000000",
                "backgroundColor": "#ffffff",
                "size": 1,
                "font": "Helvetica",
                "widgetCss": ""
            },
            "order": 0,
            "valid": false
        }
    },
    "id": "6421627682a05800192d35bd",
    "name": "randomAttr",
    "user": "user1",
    "group": null,
    "groupWriteAccess": false,
    "lastUpdatedBy": "user1",
    "insertTime": "2023-03-27T09:31:34.185Z",
    "updateTime": "2023-03-27T13:39:44.366Z",
    "history": {
        "undoActions": [
            {
                "1": {
                    "_id": "64219ca082a05800192d35f7",
                    "id": "1",
                    "x": 33,
                    "y": 11,
                    "canvas": "0",
                    "width": 10,
                    "height": 2,
                    "type": "ATTRIBUTE_DISPLAY",
                    "inputs": {
                        "attribute": {
                            "device": null,
                            "attribute": null
                        },
                        "precision": 2,
                        "showDevice": false,
                        "showAttribute": "Label",
                        "scientificNotation": false,
                        "showEnumLabels": false,
                        "showAttrQuality": false,
                        "textColor": "#000000",
                        "backgroundColor": "#ffffff",
                        "size": 1,
                        "font": "Helvetica",
                        "widgetCss": ""
                    },
                    "order": 0,
                    "valid": false
                }
            }
        ],
        "redoActions": [],
        "undoIndex": 1,
        "redoIndex": 0,
        "undoLength": 1,
        "redoLength": 0
    },
    "variables": []
}

export const addInputExample = {
    "type": "ADD_INPUT",
    "path": [
        "attributes"
    ]
}

export const addInputStateExample = {
    "selectedId": null,
    "selectedIds": [
        "1"
    ],
    "widgets": {
        "1": {
            "_id": "64219d2e82a05800192d35fb",
            "id": "1",
            "x": 20,
            "y": 5,
            "canvas": "0",
            "width": 30,
            "height": 20,
            "type": "ATTRIBUTE_PLOT",
            "inputs": {
                "timeWindow": 120,
                "showZeroLine": true,
                "logarithmic": false,
                "attributes": [],
                "textColor": "#000000",
                "backgroundColor": "#ffffff"
            },
            "order": 0,
            "valid": true
        }
    },
    "id": "6421627682a05800192d35bd",
    "name": "randomAttr",
    "user": "user1",
    "group": null,
    "groupWriteAccess": false,
    "lastUpdatedBy": "user1",
    "insertTime": "2023-03-27T09:31:34.185Z",
    "updateTime": "2023-03-27T13:42:06.989Z",
    "history": {
        "undoActions": [
            {
                "1": {
                    "_id": "64219d2e82a05800192d35fb",
                    "id": "1",
                    "x": 20,
                    "y": 5,
                    "canvas": "0",
                    "width": 30,
                    "height": 20,
                    "type": "ATTRIBUTE_PLOT",
                    "inputs": {
                        "timeWindow": 120,
                        "showZeroLine": true,
                        "logarithmic": false,
                        "attributes": [],
                        "textColor": "#000000",
                        "backgroundColor": "#ffffff"
                    },
                    "order": 0,
                    "valid": true
                }
            }
        ],
        "redoActions": [],
        "undoIndex": 1,
        "redoIndex": 0,
        "undoLength": 1,
        "redoLength": 0
    },
    "variables": []
}

export const delInputExample = {
    "type": "DELETE_INPUT",
    "path": [
        "attributes",
        0
    ]
}

export const delInputStateExample = {
    "selectedId": null,
    "selectedIds": [
        "1"
    ],
    "widgets": {
        "1": {
            "_id": "64219d2e82a05800192d35fb",
            "id": "1",
            "x": 20,
            "y": 5,
            "canvas": "0",
            "width": 30,
            "height": 20,
            "type": "ATTRIBUTE_PLOT",
            "inputs": {
                "timeWindow": 120,
                "showZeroLine": true,
                "logarithmic": false,
                "attributes": [
                    {
                        "attribute": {
                            "device": null,
                            "attribute": null
                        },
                        "showAttribute": "Label",
                        "yAxis": "left",
                        "lineColor": "#000000"
                    }
                ],
                "textColor": "#000000",
                "backgroundColor": "#ffffff"
            },
            "order": 0,
            "valid": false
        }
    },
    "id": "6421627682a05800192d35bd",
    "name": "randomAttr",
    "user": "user1",
    "group": null,
    "groupWriteAccess": false,
    "lastUpdatedBy": "user1",
    "insertTime": "2023-03-27T09:31:34.185Z",
    "updateTime": "2023-03-27T13:42:06.989Z",
    "history": {
        "undoActions": [
            {
                "1": {
                    "_id": "64219d2e82a05800192d35fb",
                    "id": "1",
                    "x": 20,
                    "y": 5,
                    "canvas": "0",
                    "width": 30,
                    "height": 20,
                    "type": "ATTRIBUTE_PLOT",
                    "inputs": {
                        "timeWindow": 120,
                        "showZeroLine": true,
                        "logarithmic": false,
                        "attributes": [],
                        "textColor": "#000000",
                        "backgroundColor": "#ffffff"
                    },
                    "order": 0,
                    "valid": true
                }
            },
            {
                "1": {
                    "_id": "64219d2e82a05800192d35fb",
                    "id": "1",
                    "x": 20,
                    "y": 5,
                    "canvas": "0",
                    "width": 30,
                    "height": 20,
                    "type": "ATTRIBUTE_PLOT",
                    "inputs": {
                        "timeWindow": 120,
                        "showZeroLine": true,
                        "logarithmic": false,
                        "attributes": [
                            {
                                "attribute": {
                                    "device": null,
                                    "attribute": null
                                },
                                "showAttribute": "Label",
                                "yAxis": "left",
                                "lineColor": "#000000"
                            }
                        ],
                        "textColor": "#000000",
                        "backgroundColor": "#ffffff"
                    },
                    "order": 0,
                    "valid": false
                }
            }
        ],
        "redoActions": [],
        "undoIndex": 2,
        "redoIndex": 0,
        "undoLength": 2,
        "redoLength": 0
    },
    "variables": []
}

export const reorderWidgetExample = {
    "type": "REORDER_WIDGETS",
    "widgets": [
        {
            "_id": "64219dee82a05800192d35ff",
            "id": "1",
            "x": 38,
            "y": 9,
            "canvas": "0",
            "width": 10,
            "height": 2,
            "type": "LABEL",
            "inputs": {
                "text": "",
                "textColor": "#000000",
                "backgroundColor": "#ffffff",
                "size": 1,
                "borderWidth": 0,
                "borderColor": "#000000",
                "font": "Helvetica",
                "linkTo": "",
                "customCss": ""
            },
            "order": 1,
            "valid": true
        },
        {
            "_id": "64219dee82a05800192d3600",
            "id": "2",
            "x": 41,
            "y": 13,
            "canvas": "0",
            "width": 10,
            "height": 2,
            "type": "LABEL",
            "inputs": {
                "text": "",
                "textColor": "#000000",
                "backgroundColor": "#ffffff",
                "size": 1,
                "borderWidth": 0,
                "borderColor": "#000000",
                "font": "Helvetica",
                "linkTo": "",
                "customCss": ""
            },
            "order": 0,
            "valid": true
        }
    ]
}

export const reorderWidgetStateExample = {
    "selectedId": null,
    "selectedIds": [],
    "widgets": {
        "1": {
            "_id": "64219dee82a05800192d35ff",
            "id": "1",
            "x": 38,
            "y": 9,
            "canvas": "0",
            "width": 10,
            "height": 2,
            "type": "LABEL",
            "inputs": {
                "text": "",
                "textColor": "#000000",
                "backgroundColor": "#ffffff",
                "size": 1,
                "borderWidth": 0,
                "borderColor": "#000000",
                "font": "Helvetica",
                "linkTo": "",
                "customCss": ""
            },
            "order": 1,
            "valid": true
        },
        "2": {
            "_id": "64219dee82a05800192d3600",
            "id": "2",
            "x": 41,
            "y": 13,
            "canvas": "0",
            "width": 10,
            "height": 2,
            "type": "LABEL",
            "inputs": {
                "text": "",
                "textColor": "#000000",
                "backgroundColor": "#ffffff",
                "size": 1,
                "borderWidth": 0,
                "borderColor": "#000000",
                "font": "Helvetica",
                "linkTo": "",
                "customCss": ""
            },
            "order": 0,
            "valid": true
        }
    },
    "id": "6421627682a05800192d35bd",
    "name": "randomAttr",
    "user": "user1",
    "group": null,
    "groupWriteAccess": false,
    "lastUpdatedBy": "user1",
    "insertTime": "2023-03-27T09:31:34.185Z",
    "updateTime": "2023-03-27T13:45:18.538Z",
    "history": {
        "undoActions": [
            {
                "1": {
                    "_id": "64219dee82a05800192d35ff",
                    "id": "1",
                    "x": 38,
                    "y": 9,
                    "canvas": "0",
                    "width": 10,
                    "height": 2,
                    "type": "LABEL",
                    "inputs": {
                        "text": "",
                        "textColor": "#000000",
                        "backgroundColor": "#ffffff",
                        "size": 1,
                        "borderWidth": 0,
                        "borderColor": "#000000",
                        "font": "Helvetica",
                        "linkTo": "",
                        "customCss": ""
                    },
                    "order": 1,
                    "valid": true
                },
                "2": {
                    "_id": "64219dee82a05800192d3600",
                    "id": "2",
                    "x": 41,
                    "y": 13,
                    "canvas": "0",
                    "width": 10,
                    "height": 2,
                    "type": "LABEL",
                    "inputs": {
                        "text": "",
                        "textColor": "#000000",
                        "backgroundColor": "#ffffff",
                        "size": 1,
                        "borderWidth": 0,
                        "borderColor": "#000000",
                        "font": "Helvetica",
                        "linkTo": "",
                        "customCss": ""
                    },
                    "order": 0,
                    "valid": true
                }
            }
        ],
        "redoActions": [],
        "undoIndex": 1,
        "redoIndex": 0,
        "undoLength": 1,
        "redoLength": 0
    },
    "variables": []
}

export const updateWidgetExample = {
    "type": "UPDATE_WIDGET",
    "updatedWidget": {
        "id": "2",
        "x": 43,
        "y": 18,
        "canvas": "0",
        "width": 1.95,
        "height": 4.4,
        "type": "LABEL",
        "inputs": {
            "text": "",
            "textColor": "#000000",
            "backgroundColor": "#ffffff",
            "size": 1,
            "borderWidth": 0,
            "borderColor": "#000000",
            "font": "Helvetica",
            "linkTo": "",
            "customCss": ""
        },
        "valid": true,
        "order": 1,
        "percentage": -2
    }
}

export const dashboards = {
    dashboards: [
        {
            id: "2",

        }
    ]
}

export const updateWidgetStateExample = {
    "selectedId": null,
    "selectedIds": [
        "2"
    ],
    "widgets": {
        "1": {
            "_id": "64219f3d82a05800192d361a",
            "id": "1",
            "x": 35,
            "y": 11,
            "canvas": "0",
            "width": 20,
            "height": 10,
            "type": "BOX",
            "inputs": {
                "title": "",
                "bigWidget": 4,
                "smallWidget": 1,
                "textColor": "#000000",
                "backgroundColor": "#ffffff",
                "borderColor": "",
                "borderWidth": 0,
                "borderStyle": "solid",
                "textSize": 1,
                "fontFamily": "Helvetica",
                "layout": "vertical",
                "alignment": "Center",
                "padding": 0,
                "customCss": ""
            },
            "order": 0,
            "innerWidgets": [
                {
                    "id": "2",
                    "x": 43,
                    "y": 18,
                    "canvas": "0",
                    "width": 19.5,
                    "height": 4.4,
                    "type": "LABEL",
                    "inputs": {
                        "text": "",
                        "textColor": "#000000",
                        "backgroundColor": "#ffffff",
                        "size": 1,
                        "borderWidth": 0,
                        "borderColor": "#000000",
                        "font": "Helvetica",
                        "linkTo": "",
                        "customCss": ""
                    },
                    "valid": true,
                    "order": 1,
                    "percentage": -2
                },
                {
                    "id": "3",
                    "x": 42,
                    "y": 18,
                    "canvas": "0",
                    "width": 19.2,
                    "height": 4.4,
                    "type": "LABEL",
                    "inputs": {
                        "text": "",
                        "textColor": "#000000",
                        "backgroundColor": "#ffffff",
                        "size": 1,
                        "borderWidth": 0,
                        "borderColor": "#000000",
                        "font": "Helvetica",
                        "linkTo": "",
                        "customCss": ""
                    },
                    "valid": true,
                    "order": 2
                }
            ],
            "valid": true
        }
    },
    "id": "6421627682a05800192d35bd",
    "name": "randomAttr",
    "user": "user1",
    "group": null,
    "groupWriteAccess": false,
    "lastUpdatedBy": "user1",
    "insertTime": "2023-03-27T09:31:34.185Z",
    "updateTime": "2023-03-27T13:51:02.140Z",
    "history": {
        "undoActions": [
            {
                "1": {
                    "_id": "64219f3d82a05800192d361a",
                    "id": "1",
                    "x": 35,
                    "y": 11,
                    "canvas": "0",
                    "width": 20,
                    "height": 10,
                    "type": "BOX",
                    "inputs": {
                        "title": "",
                        "bigWidget": 4,
                        "smallWidget": 1,
                        "textColor": "#000000",
                        "backgroundColor": "#ffffff",
                        "borderColor": "",
                        "borderWidth": 0,
                        "borderStyle": "solid",
                        "textSize": 1,
                        "fontFamily": "Helvetica",
                        "layout": "vertical",
                        "alignment": "Center",
                        "padding": 0,
                        "customCss": ""
                    },
                    "order": 0,
                    "innerWidgets": [
                        {
                            "id": "2",
                            "x": 43,
                            "y": 18,
                            "canvas": "0",
                            "width": 19.5,
                            "height": 4.4,
                            "type": "LABEL",
                            "inputs": {
                                "text": "",
                                "textColor": "#000000",
                                "backgroundColor": "#ffffff",
                                "size": 1,
                                "borderWidth": 0,
                                "borderColor": "#000000",
                                "font": "Helvetica",
                                "linkTo": "",
                                "customCss": ""
                            },
                            "valid": true,
                            "order": 1,
                            "percentage": -2
                        },
                        {
                            "id": "3",
                            "x": 42,
                            "y": 18,
                            "canvas": "0",
                            "width": 19.2,
                            "height": 4.4,
                            "type": "LABEL",
                            "inputs": {
                                "text": "",
                                "textColor": "#000000",
                                "backgroundColor": "#ffffff",
                                "size": 1,
                                "borderWidth": 0,
                                "borderColor": "#000000",
                                "font": "Helvetica",
                                "linkTo": "",
                                "customCss": ""
                            },
                            "valid": true,
                            "order": 2
                        }
                    ],
                    "valid": true
                }
            },
            {
                "1": {
                    "_id": "64219f3d82a05800192d361a",
                    "id": "1",
                    "x": 35,
                    "y": 11,
                    "canvas": "0",
                    "width": 20,
                    "height": 10,
                    "type": "BOX",
                    "inputs": {
                        "title": "",
                        "bigWidget": 4,
                        "smallWidget": 1,
                        "textColor": "#000000",
                        "backgroundColor": "#ffffff",
                        "borderColor": "",
                        "borderWidth": 0,
                        "borderStyle": "solid",
                        "textSize": 1,
                        "fontFamily": "Helvetica",
                        "layout": "vertical",
                        "alignment": "Center",
                        "padding": 0,
                        "customCss": ""
                    },
                    "order": 0,
                    "innerWidgets": [
                        {
                            "id": "2",
                            "x": 43,
                            "y": 18,
                            "canvas": "0",
                            "width": 19.5,
                            "height": 4.4,
                            "type": "LABEL",
                            "inputs": {
                                "text": "",
                                "textColor": "#000000",
                                "backgroundColor": "#ffffff",
                                "size": 1,
                                "borderWidth": 0,
                                "borderColor": "#000000",
                                "font": "Helvetica",
                                "linkTo": "",
                                "customCss": ""
                            },
                            "valid": true,
                            "order": 1,
                            "percentage": -2
                        },
                        {
                            "id": "3",
                            "x": 42,
                            "y": 18,
                            "canvas": "0",
                            "width": 19.2,
                            "height": 4.4,
                            "type": "LABEL",
                            "inputs": {
                                "text": "",
                                "textColor": "#000000",
                                "backgroundColor": "#ffffff",
                                "size": 1,
                                "borderWidth": 0,
                                "borderColor": "#000000",
                                "font": "Helvetica",
                                "linkTo": "",
                                "customCss": ""
                            },
                            "valid": true,
                            "order": 2
                        }
                    ],
                    "valid": true
                }
            },
            {
                "1": {
                    "_id": "64219f3d82a05800192d361a",
                    "id": "1",
                    "x": 35,
                    "y": 11,
                    "canvas": "0",
                    "width": 20,
                    "height": 10,
                    "type": "BOX",
                    "inputs": {
                        "title": "",
                        "bigWidget": 4,
                        "smallWidget": 1,
                        "textColor": "#000000",
                        "backgroundColor": "#ffffff",
                        "borderColor": "",
                        "borderWidth": 0,
                        "borderStyle": "solid",
                        "textSize": 1,
                        "fontFamily": "Helvetica",
                        "layout": "vertical",
                        "alignment": "Center",
                        "padding": 0,
                        "customCss": ""
                    },
                    "order": 0,
                    "innerWidgets": [
                        {
                            "id": "2",
                            "x": 43,
                            "y": 18,
                            "canvas": "0",
                            "width": 19.5,
                            "height": 4.4,
                            "type": "LABEL",
                            "inputs": {
                                "text": "",
                                "textColor": "#000000",
                                "backgroundColor": "#ffffff",
                                "size": 1,
                                "borderWidth": 0,
                                "borderColor": "#000000",
                                "font": "Helvetica",
                                "linkTo": "",
                                "customCss": ""
                            },
                            "valid": true,
                            "order": 1,
                            "percentage": -2
                        },
                        {
                            "id": "3",
                            "x": 42,
                            "y": 18,
                            "canvas": "0",
                            "width": 19.2,
                            "height": 4.4,
                            "type": "LABEL",
                            "inputs": {
                                "text": "",
                                "textColor": "#000000",
                                "backgroundColor": "#ffffff",
                                "size": 1,
                                "borderWidth": 0,
                                "borderColor": "#000000",
                                "font": "Helvetica",
                                "linkTo": "",
                                "customCss": ""
                            },
                            "valid": true,
                            "order": 2
                        }
                    ],
                    "valid": true
                }
            }
        ],
        "redoActions": [],
        "undoIndex": 3,
        "redoIndex": 0,
        "undoLength": 3,
        "redoLength": 0
    },
    "variables": []
}

export const attributePlot = {
    "_id": "64219d2e82a05800192d35fb",
    "id": "1",
    "x": 20,
    "y": 5,
    "canvas": "0",
    "width": 30,
    "height": 20,
    "type": "ATTRIBUTE_PLOT",
    "inputs": {
        "timeWindow": 120,
        "showZeroLine": true,
        "logarithmic": false,
        "attributes": [
            {
                "attribute": {
                    "device": 'sys/tg_test/1',
                    "attribute": 'short_scalar'
                },
                "showAttribute": "Label",
                "yAxis": "left",
                "lineColor": "#000000"
            }
        ],
        "textColor": "#000000",
        "backgroundColor": "#ffffff"
    },
    "order": 0,
    "valid": 1
}

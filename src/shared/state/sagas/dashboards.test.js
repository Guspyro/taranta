import { take, put, call, select } from "redux-saga/effects";
import { cloneableGenerator } from '@redux-saga/testing-utils';
import dashboards, {
    loadDashboards,
    renameDashboard,
    deleteDashboard,
    exportDashboardSaga,
    importDashboardSaga,
    shareDashboard,
    cloneDashboard,
    notifyOnExport,
    saveDashboard,
    notifyOnSave,
    notifyOnShare,
    notifyOnClone,
    notifyOnDelete,
} from "./dashboards";
import * as API from "../../../dashboard/dashboardRepo";
import {
    dashboardExported,
    showNotification,
    dashboardSaved,
    dashboardsLoaded,
    dashboardShared,
    dashboardRenamed,
    saveDashboard as saveDashboardAction,
    dashboardDeleted,
    dashboardCloned,
} from "../actions/actionCreators";
import {
    resolveWidgetCompatibility,
} from "../reducers/selectedDashboard/lib";
import {
    EXPORT_DASHBOARD,
    LOAD_DASHBOARDS,
    DASHBOARD_RENAMED,
    DASHBOARD_DELETED,
    DASHBOARD_CLONED,
    DASHBOARD_SAVED,
    SHARE_DASHBOARD,
    RENAME_DASHBOARD,
    DELETE_DASHBOARD,
    CLONE_DASHBOARD,
    IMPORT_DASHBOARD,
    SHOW_NOTIFICATION,
    DASHBOARD_EXPORTED,
    SAVE_DASHBOARD,
    DASHBOARD_CREATED,
    DASHBOARD_SHARED
} from "../actions/actionTypes";
import { LOGIN_SUCCESS } from '../../user/state/actionTypes';
import { NotificationLevel } from "../../notifications/notifications";
import {
    getWidgets,
} from "../selectors";
import { throwError } from 'redux-saga-test-plan/providers';
import { expectSaga } from "redux-saga-test-plan";
import { getDeviceNames } from "../selectors/deviceList";

describe('exportDashboardSaga', () => {
    test('should export dashboard and dispatch dashboardExported action', () => {
        const id = 'testId';
        const action = { type: EXPORT_DASHBOARD, id };
        const gen = cloneableGenerator(exportDashboardSaga)(action);

        expect(gen.next().value).toEqual(take(EXPORT_DASHBOARD));

        const result = {
            widgets: [{
                id: 'testWidgetId',
                type: 'ATTRIBUTE_DISPLAY',
                inputs: {
                    "attribute": {
                        "device": "sys/tg_test/1",
                        "attribute": "double_scalar",
                        "label": "double_scalar"
                    },
                    "precision": 2,
                    "showDevice": true,
                    "showAttribute": "Label",
                    "scientificNotation": false,
                    "showEnumLabels": false,
                    "showAttrQuality": false,
                    "textColor": "#000000",
                    "backgroundColor": "#ffffff",
                    "size": 1,
                    "font": "Helvetica",
                    "widgetCss": ""
                }
            }],
            name: 'testDashboardName',
            user: 'testUser',
            insertTime: new Date(),
            updateTime: new Date(),
            group: 'testGroup',
            lastUpdatedBy: 'testUser',
            variables: {},
        };
        const { widgets, name, user, insertTime, updateTime, group, groupWriteAccess, lastUpdatedBy, variables } = result;

        expect(gen.next(action).value).toEqual(call(API.exportDash, id));

        const { widgets: newWidgets } = resolveWidgetCompatibility(widgets);

        expect(gen.next(result).value).toEqual(put(dashboardExported({ id, name, user, insertTime, updateTime, group, groupWriteAccess, lastUpdatedBy, variables }, newWidgets)));

        const clone = gen.clone();
        const error = new Error('Failed to export dashboard');
        expect(clone.throw(error).value).toEqual(
            put(showNotification(NotificationLevel.ERROR, EXPORT_DASHBOARD, 'Dashboard not exported: ' + error))
        );

        expect(clone.next().value).toEqual(take(EXPORT_DASHBOARD));
    });
});

describe('loadDashboards saga', () => {
    test('should load dashboards and dispatch dashboardsLoaded action', () => {
        const gen = cloneableGenerator(loadDashboards)();
        expect(gen.next().value).toEqual({
            '@@redux-saga/IO': true,
            combinator: false,
            type: 'TAKE',
            payload: {
                pattern: [
                    LOAD_DASHBOARDS,
                    LOGIN_SUCCESS,
                    DASHBOARD_RENAMED,
                    DASHBOARD_DELETED,
                    DASHBOARD_CLONED,
                    DASHBOARD_SAVED
                ],
            }
        }
        );

        const payload = { type: LOAD_DASHBOARDS };
        expect(gen.next(payload).value).toEqual(call(API.loadUserDashboards));

        const result = {
            dashboards: [{ id: 'testDashboardId', name: 'testDashboardName' }],
            groups: [],
        };

        expect(gen.next(result).value).toEqual(put(dashboardsLoaded(result)));

        const clone = gen.clone();
        const error = new Error('Failed to load dashboards');
        expect(clone.throw(error).done).toEqual(false);
    });
});

describe('shareDashboard saga', () => {
    test('should share dashboard and dispatch dashboardShared action', () => {
        const gen = cloneableGenerator(shareDashboard)();

        expect(gen.next().value).toEqual(take(SHARE_DASHBOARD));

        const payload = { id: 'testDashboardId', group: 'testGroup', groupWriteAccess: true };

        expect(gen.next(payload).value).toEqual(call(API.shareDashboard, 'testDashboardId', 'testGroup', true));

        expect(gen.next().value).toEqual(put(dashboardShared('testDashboardId', 'testGroup', true)));

        const clone = gen.clone();
        const error = new Error('Failed to share dashboard');
        try {
            clone.throw(error);
        } catch (e) {
            expect(e).toBe(error);
        }
        expect(clone.next().done).toBe(true);
    });
});

describe('renameDashboard saga', () => {
    test('should rename dashboard and dispatch dashboardRenamed action', () => {
        const gen = cloneableGenerator(renameDashboard)();
        expect(gen.next().value).toEqual(take(RENAME_DASHBOARD));

        const payload = { id: 'testDashboardId', name: 'Test Dashboard Name' };
        expect(gen.next(payload).value).toEqual(call(API.renameDashboard, 'testDashboardId', 'Test Dashboard Name'));
        expect(gen.next().value).toEqual(put(dashboardRenamed('testDashboardId', 'Test Dashboard Name')));

        const clone = gen.clone();
        expect(clone.next().value).toEqual(take(RENAME_DASHBOARD));

        const emptyIdPayload = { id: '', name: 'Test Dashboard Name' };
        expect(clone.next(emptyIdPayload).value).toEqual(select(getWidgets));

        const widgets = {};
        expect(clone.next(widgets).value).toEqual(put(saveDashboardAction('', 'Test Dashboard Name', widgets)));
    });

    test('should catch errors thrown by API.renameDashboard', () => {
        const gen = cloneableGenerator(renameDashboard)();
        expect(gen.next().value).toEqual(take(RENAME_DASHBOARD));

        const payload = { id: 'testDashboardId', name: 'Test Dashboard Name' };
        expect(gen.next(payload).value).toEqual(call(API.renameDashboard, 'testDashboardId', 'Test Dashboard Name'));
    });
});

describe('deleteDashboard saga', () => {
    test('should delete dashboard and dispatch dashboardDeleted action', () => {
        const gen = cloneableGenerator(deleteDashboard)();

        expect(gen.next().value).toEqual(take(DELETE_DASHBOARD));

        const payload = { id: 'testDashboardId' };

        expect(gen.next(payload).value).toEqual(call(API.deleteDashboard, 'testDashboardId'));

        expect(gen.next({ id: 'testDashboardId' }).value).toEqual(put(dashboardDeleted('testDashboardId')));
    });
});

describe('cloneDashboard saga', () => {
    test('should clone dashboard and dispatch dashboardCloned action', () => {
        const gen = cloneableGenerator(cloneDashboard)();

        expect(gen.next().value).toEqual(take(CLONE_DASHBOARD));

        const payload = { id: 'testDashboardId' };

        expect(gen.next(payload).value).toEqual(call(API.cloneDashboard, 'testDashboardId'));

        expect(gen.next({ id: 'newTestDashboardId' }).value).toEqual(put(dashboardCloned('newTestDashboardId')));
    });
});

describe('importDashboardSaga', () => {
    test('should import dashboard and dispatch dashboardSaved action', () => {
        const gen = cloneableGenerator(importDashboardSaga)();

        expect(gen.next().value).toEqual(take(IMPORT_DASHBOARD));

        const deviceList = ["sys/tg_test/1"];
        const payload = { content: 'testDashboardContent' };


        const mockedResult = {
            created: true,
            id: 'newTestDashboardId',
            created: '2022-04-01T12:00:00Z',
            name: 'New Test Dashboard'
        };

        const mockFn = jest.spyOn(API, 'importDash').mockReturnValue(mockedResult);

        expect(gen.next(payload).value).toEqual(select(getDeviceNames));
        expect(gen.next(deviceList).value).toEqual(call(API.importDash, 'testDashboardContent', deviceList));
        expect(gen.next(mockedResult).value.payload.action.type).toEqual(SHOW_NOTIFICATION)

        mockFn.mockRestore();
    });
});

describe("notifyOnExport saga", () => {
    test("should notify when a dashboard is exported", () => {
        const gen = cloneableGenerator(notifyOnExport)();
        const dashboard = { id: "testDashboardId" };
        const action = { type: DASHBOARD_EXPORTED, dashboard };
        const notificationAction = showNotification(
            NotificationLevel.INFO,
            DASHBOARD_EXPORTED,
            "Dashboard exported"
        );

        expect(gen.next(dashboard).value).toEqual(take(DASHBOARD_EXPORTED));
        expect(gen.next(action).value).toEqual(
            put(notificationAction)
        );
    });
});

describe("saveDashboard saga", () => {
    test("should save dashboard and dispatch dashboardSaved action", () => {
        const gen = cloneableGenerator(saveDashboard)();

        expect(gen.next().value).toEqual(take(SAVE_DASHBOARD));

        const payload = {
            id: "testDashboardId",
            widgets: [],
            name: "Test Dashboard",
            variables: {}
        };

        expect(gen.next(payload).value).toEqual(
            call(API.save, "testDashboardId", [], "Test Dashboard", {})
        );

        const newDashboard = {
            id: "newTestDashboardId",
            created: new Date(),
            name: "Test Dashboard",
            variables: {}
        };

        expect(gen.next(newDashboard).value).toEqual(
            put(
                dashboardSaved(
                    "newTestDashboardId",
                    newDashboard.created,
                    "Test Dashboard",
                    {}
                )
            )
        );

        expect(gen.next().value).toEqual(take(SAVE_DASHBOARD));
    });

    test("should show an error notification if the dashboard cannot be saved", () => {
        const gen = cloneableGenerator(saveDashboard)();

        expect(gen.next().value).toEqual(take(SAVE_DASHBOARD));

        const payload = {
            id: "testDashboardId",
            widgets: [],
            name: "Test Dashboard",
            variables: {}
        };

        expect(gen.next(payload).value).toEqual(
            call(API.save, "testDashboardId", [], "Test Dashboard", {})
        );

        const newDashboard = {
            id: "newTestDashboardId",
            created: new Date(),
            name: "Test Dashboard",
            variables: {}
        };

        expect(gen.next(newDashboard).value).toEqual(
            put(
                dashboardSaved(
                    "newTestDashboardId",
                    newDashboard.created,
                    "Test Dashboard",
                    {}
                )
            )
        );

        const clone = gen.clone();
        const error = new Error('Failed to save dashboard');
        try {
            clone.throw(error);
        } catch (e) {
            expect(e).toBe(error);
        }

    });
});

describe("notifyOnSave saga", () => {
    test("should show info notification on dashboard create", () => {
        const gen = cloneableGenerator(notifyOnSave)();

        expect(gen.next().value).toEqual(take(DASHBOARD_SAVED));

        const payload = { created: true };

        expect(gen.next(payload).value).toEqual(
            put(
                showNotification(
                    NotificationLevel.INFO,
                    DASHBOARD_CREATED,
                    "Dashboard created"
                )
            )
        );

        expect(gen.next().value).toEqual(take(DASHBOARD_SAVED));
    });
});


describe("notifyOnShare saga", () => {
    test("should show info notification on dashboard share", () => {
        const gen = cloneableGenerator(notifyOnShare)();

        expect(gen.next().value).toEqual(take(DASHBOARD_SHARED));

        const payload = { group: 'testGroup', groupWriteAccess: false };

        expect(gen.next(payload).value).toEqual(
            put(
                showNotification(
                    NotificationLevel.INFO,
                    DASHBOARD_SHARED,
                    "Dashboard shared with testGroup"
                )
            )
        );

        expect(gen.next().value).toEqual(take(DASHBOARD_SHARED));
    });
});

describe("notifyOnClone saga", () => {
    test("should show info notification on dashboard clone", () => {
        const gen = cloneableGenerator(notifyOnClone)();

        expect(gen.next().value).toEqual(take(DASHBOARD_CLONED));

        expect(gen.next().value).toEqual(
            put(
                showNotification(
                    NotificationLevel.INFO,
                    DASHBOARD_CLONED,
                    "Dashboard cloned"
                )
            )
        );

        expect(gen.next().value).toEqual(take(DASHBOARD_CLONED));
    });
});

describe("notifyOnDelete saga", () => {
    test("should show info notification on dashboard delete", () => {
        const gen = cloneableGenerator(notifyOnDelete)();

        expect(gen.next().value).toEqual(take(DASHBOARD_DELETED));

        expect(gen.next().value).toEqual(
            put(
                showNotification(
                    NotificationLevel.INFO,
                    DASHBOARD_DELETED,
                    "Dashboard deleted"
                )
            )
        );
    });
});

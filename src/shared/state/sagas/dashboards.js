import { take, fork, put, call, select, race, delay } from "redux-saga/effects";
import * as API from "../../../dashboard/dashboardRepo";
import {
    dashboardsLoaded,
    dashboardExported,
    dashboardRenamed,
    dashboardDeleted,
    dashboardCloned,
    dashboardLoaded,
    dashboardSaved,
    showNotification,
    hideNotification,
    dashboardShared,
    saveDashboard as saveDashboardAction,
} from "../actions/actionCreators";
import {
    LOGIN_SUCCESS
} from "../../user/state/actionTypes";
import {
    resolveWidgetCompatibility,
} from "../reducers/selectedDashboard/lib";
import {
    RENAME_DASHBOARD,
    DELETE_DASHBOARD,
    CLONE_DASHBOARD,
    SHARE_DASHBOARD,
    LOAD_DASHBOARD,
    EXPORT_DASHBOARD,
    DASHBOARD_RENAMED,
    DASHBOARD_DELETED,
    DASHBOARD_SHARED,
    DASHBOARD_CLONED,
    SAVE_DASHBOARD,
    DASHBOARD_SAVED,
    DASHBOARD_CREATED,
    LOAD_DASHBOARDS,
    SHOW_NOTIFICATION,
    DASHBOARD_EXPORTED,
    IMPORT_DASHBOARD
} from "../actions/actionTypes";
import {
    getWidgets,
} from "../selectors";
import { editWidget } from "./editWidget";
import { NotificationLevel } from "../../notifications/notifications";
import { getDeviceNames } from "../selectors/deviceList";
import tangoAPIExport from "../../api/tangoAPI";

export default function* dashboards() {
    yield fork(loadDashboards);
    yield fork(renameDashboard);
    yield fork(deleteDashboard);
    yield fork(cloneDashboard);
    yield fork(loadDashboardSaga);
    yield fork(exportDashboardSaga);
    yield fork(importDashboardSaga);
    yield fork(saveDashboard);
    yield fork(notifyOnSave);
    yield fork(notifyOnExport);
    yield fork(notifyOnClone);
    yield fork(notifyOnDelete);
    yield fork(notifyOnShare);
    yield fork(hideNotificationAfterDelay);
    yield fork(shareDashboard);
    yield fork(editWidget);
}


export function* loadDashboards() {
    while (true) {
        const payload = yield take([
            LOAD_DASHBOARDS,
            LOGIN_SUCCESS,
            DASHBOARD_RENAMED,
            DASHBOARD_DELETED,
            DASHBOARD_CLONED,
            DASHBOARD_SAVED
        ]);

        //in the case of DASHBOARD_SAVED, only load the dashboard from the db if it was created.
        //Loading the dashboard on every save becomes very sluggish, e.g. when trying to type text
        //in a widget label
        if (payload.type !== DASHBOARD_SAVED || payload.created) {
            try {
                const result = yield call(API.loadUserDashboards);

                yield put(dashboardsLoaded(result));
            } catch (exception) {
                yield put(
                    showNotification(NotificationLevel.ERROR, LOAD_DASHBOARD, "Dashboard not loaded")
                );
            }
        }
    }
}

export function* shareDashboard() {
    while (true) {
        const { id, group, groupWriteAccess } = yield take(SHARE_DASHBOARD);
        yield call(API.shareDashboard, id, group, groupWriteAccess);
        yield put(dashboardShared(id, group, groupWriteAccess));
    }
}

export function* renameDashboard() {
    while (true) {
        const { id, name } = yield take(RENAME_DASHBOARD);

        if (id === "") {
            const widgets = yield select(getWidgets);
            yield put(saveDashboardAction(id, name, widgets));
        } else {
            try {
                const res = yield call(API.renameDashboard, id, name);
                if (res?.error) {
                    yield put(
                        showNotification(NotificationLevel.WARNING, RENAME_DASHBOARD, res.error)
                    );
                } else
                    yield put(dashboardRenamed(id, name));
            } catch(e) {
                yield put(
                    showNotification(NotificationLevel.WARNING, RENAME_DASHBOARD, "Failed to rename the dashboard: "+e)
                );
            }
        }
    }
}

export function* deleteDashboard() {
    while (true) {
        const { id } = yield take(DELETE_DASHBOARD);
        const result = yield call(API.deleteDashboard, id);
        yield put(dashboardDeleted(result.id));
    }
}

export function* cloneDashboard() {
    while (true) {
        const { id } = yield take(CLONE_DASHBOARD);
        const { id: newId } = yield call(API.cloneDashboard, id);
        yield put(dashboardCloned(newId));
    }
}

export function* loadDashboardSaga() {
    while (true) {
        const payload = yield take([
            LOAD_DASHBOARD,
            DASHBOARD_CLONED,
            DASHBOARD_SAVED
        ]);

        let { id, type, deviceList } = payload;
        //In the case of dashboard_saved, only load dashboard if the dashboard was just created (we need the ID)
        let created = false;
        if (type === DASHBOARD_SAVED) {
            created = payload.created;
        }
        if (!(type === DASHBOARD_SAVED && !created)) {
            // Fetching device list bcz devices still loading hence using below approach
            // TODO: if we can memoize this
            if ((!deviceList) || deviceList.length === 0)
                deviceList = yield call(tangoAPIExport.fetchDeviceNames, API.getTangoDB());

            try {
                const {
                    widgets,
                    name,
                    user,
                    insertTime,
                    updateTime,
                    group,
                    groupWriteAccess,
                    lastUpdatedBy,
                    variables
                } = yield call(API.load, id);

                const { widgets: newWidgets } = resolveWidgetCompatibility(
                    widgets
                );

                yield put(
                    dashboardLoaded(
                        {
                            id,
                            name,
                            user,
                            insertTime,
                            updateTime,
                            group,
                            groupWriteAccess,
                            lastUpdatedBy,
                            variables,
                        },
                        newWidgets,
                        deviceList
                    )
                );
            } catch (exception) {
                yield put(
                    showNotification(NotificationLevel.WARNING, LOAD_DASHBOARD, "Dashboard not found")
                );
            }
        }
    }
}

export function* exportDashboardSaga() {
    while (true) {
        //Generator only executes is type is EXPORT_DASHBOARD
        const payload = yield take(EXPORT_DASHBOARD);
        const { id } = payload;
        try {
            const result = yield call(API.exportDash, id);
            const {
                widgets,
                name,
                user,
                insertTime,
                updateTime,
                group,
                groupWriteAccess,
                lastUpdatedBy,
                variables
            } = result;

            const { widgets: newWidgets } = resolveWidgetCompatibility(
                widgets
            );

            yield put(
                dashboardExported(
                    {
                        id,
                        name,
                        user,
                        insertTime,
                        updateTime,
                        group,
                        groupWriteAccess,
                        lastUpdatedBy,
                        variables
                    },
                    newWidgets
                )
            );

        } catch (exception) {
            yield put(
                showNotification(NotificationLevel.ERROR, EXPORT_DASHBOARD, "Dashboard not exported: " + exception)
            );
        }
    }
}

export function* importDashboardSaga() {
    while (true) {
        //Generator only executes is type is EXPORT_DASHBOARD
        const payload = yield take(IMPORT_DASHBOARD);
        const { content } = payload;
        try {

            const deviceList = yield select(getDeviceNames);
            const result = yield call(API.importDash, content, deviceList);
            if (result.created !== undefined) {
                if (result.warning.length > 0) {
                    yield put(showNotification(NotificationLevel.WARNING, IMPORT_DASHBOARD, result.warning));
                }

                const { id: newId, created, name } = result;
                yield put(dashboardSaved(newId, created, name));

            } else {
                yield put(showNotification(NotificationLevel.ERROR, IMPORT_DASHBOARD, "Dashboard not imported: " + (result.error || result)));
            }
        } catch (exception) {
            yield put(
                showNotification(NotificationLevel.ERROR, IMPORT_DASHBOARD, "Dashboard not imported: " + exception.error)
            );
        }
    }
}

export function* notifyOnExport() {
    while (true) {
        const { dashboard } = yield take(DASHBOARD_EXPORTED);
        if (dashboard.id) {
            yield put(
                showNotification(NotificationLevel.INFO, DASHBOARD_EXPORTED, "Dashboard exported")
            );
        }
    }
}

export function* saveDashboard() {
    while (true) {
        const { id, widgets, name, variables } = yield take(SAVE_DASHBOARD);
        try {
            const { id: newId, created } = yield call(
                API.save,
                id,
                widgets,
                name || "",
                variables
            );

            yield put(dashboardSaved(newId, created, name, variables));
        } catch (exception) {
            yield put(
                showNotification(
                    NotificationLevel.ERROR,
                    SAVE_DASHBOARD,
                    "You cannot edit this dashboard: " + (exception.error || exception)
                )
            );
        }
    }
}

export function* notifyOnSave() {
    while (true) {
        const { created } = yield take(DASHBOARD_SAVED);
        if (created) {
            yield put(
                showNotification(NotificationLevel.INFO, DASHBOARD_CREATED, "Dashboard created")
            );
        }
    }
}

export function* notifyOnShare() {
    while (true) {
        const { group, groupWriteAccess } = yield take(DASHBOARD_SHARED);
        const msg = group
            ? "Dashboard shared with " +
            group +
            (groupWriteAccess ? " (write access)" : "")
            : "Dashboard unshared";
        yield put(showNotification(NotificationLevel.INFO, DASHBOARD_SHARED, msg));
    }
}

export function* notifyOnClone() {
    while (true) {
        yield take(DASHBOARD_CLONED);
        yield put(showNotification(NotificationLevel.INFO, DASHBOARD_CLONED, "Dashboard cloned"));
    }
}

export function* notifyOnDelete() {
    yield take(DASHBOARD_DELETED);
    yield put(showNotification(NotificationLevel.INFO, DASHBOARD_DELETED, "Dashboard deleted"));
}

// Can possibly be simplified using debounce()
export function* hideNotificationAfterDelay() {
    while (true) {
        const { notification } = yield take(SHOW_NOTIFICATION);

        while (true) {
            const { timePassed } = yield race({
                newNotification: take(SHOW_NOTIFICATION),
                timePassed: delay(notification.duration)
            });

            if (timePassed) {
                break;
            }
        }

        yield put(hideNotification());
    }
}

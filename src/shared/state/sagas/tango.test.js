import { expectSaga } from 'redux-saga-test-plan';
import { call, fork, take, put } from 'redux-saga/effects';
import { throwError } from 'redux-saga-test-plan/providers';
import { channel, eventChannel } from 'redux-saga';
import { createMockTask } from '@redux-saga/testing-utils';
import {
    fetchDeviceNames,
    executeCommand,
    setDeviceAttribute,
    setDeviceProperty,
    deleteDeviceProperty,
    fetchDevice,
    fetchDatabaseInfo,
    refetchDeviceStateOnAttributeWrite,
    subscribeDeviceAttrs,
    createChangeEventChannel,
    handleChangeEvents,
} from './tango';
import {
    FETCH_DEVICE_NAMES,
    EXECUTE_COMMAND,
    SET_DEVICE_PROPERTY,
    DELETE_DEVICE_PROPERTY,
    FETCH_DEVICE,
    FETCH_DATABASE_INFO,
    SET_DEVICE_ATTRIBUTE_SUCCESS,
    SUBSCRIBE_DEVICE_ATTRS,
    UNSUBSCRIBE_DEVICE_ATTRS,
    SET_DEVICE_ATTRIBUTE,
    DEVICE_STATE_RECEIVED
} from "../actions/actionTypes";

import TangoAPI from "../../../shared/api/tangoAPI";
import {
    fetchDeviceNamesSuccess,
    fetchDeviceNamesFailed,
    executeCommandSuccess,
    executeCommandFailed,
    setDevicePropertySuccess,
    setDevicePropertyFailed,
    setDeviceAttributeSuccess,
    setDeviceAttributeFailed,
    deleteDevicePropertySuccess,
    deleteDevicePropertyFailed,
    fetchDeviceSuccess,
    fetchDeviceFailed,
    fetchDatabaseInfoSuccess,
    fetchDatabaseInfoFailed,
    attributeFrameReceived
} from "../actions/tango";

import { displayError } from "../actions/error";

describe('fetchDeviceNames', () => {
    it('should handle success case', () => {
        const tangoDB = 'test-db';
        const names = ['device1', 'device2', 'device3'];

        // Mock the TangoAPI.fetchDeviceNames call
        jest.fn().mockResolvedValue(names);

        return expectSaga(fetchDeviceNames)
            .provide([
                [call(TangoAPI.fetchDeviceNames, tangoDB), names],
            ])
            .dispatch({ type: FETCH_DEVICE_NAMES, tangoDB })
            .put(fetchDeviceNamesSuccess(names))
            .silentRun();
    });

    it('should handle failure case', () => {
        const tangoDB = 'test-db';
        const error = new Error('Failed to fetch device names');

        // Mock the TangoAPI.fetchDeviceNames call
        jest.fn().mockRejectedValue(error);

        return expectSaga(fetchDeviceNames)
            .provide([
                [call(TangoAPI.fetchDeviceNames, tangoDB), Promise.reject(error)],
            ])
            .dispatch({ type: FETCH_DEVICE_NAMES, tangoDB })
            .put(fetchDeviceNamesFailed(error.toString()))
            .silentRun();
    });
});

describe('executeCommand', () => {
    it('should handle success case', () => {
        const tangoDB = 'test-db';
        const device = 'test-device';
        const command = 'test-command';
        const argin = 'test-argin';
        const apiResponse = { ok: true, output: 'Command executed successfully' };

        return expectSaga(executeCommand)
            .provide([
                [call(TangoAPI.executeCommand, tangoDB, device, command, argin), apiResponse],
            ])
            .dispatch({ type: EXECUTE_COMMAND, tangoDB, device, command, argin })
            .put(executeCommandSuccess(tangoDB, command, apiResponse.output, device))
            .silentRun();
    });

    it('should handle failure case', () => {
        const tangoDB = 'test-db';
        const device = 'test-device';
        const command = 'test-command';
        const argin = 'test-argin';
        const apiResponse = { ok: false, output: 'Command execution failed' };
      
        return expectSaga(executeCommand)
          .provide([
            [call(TangoAPI.executeCommand, tangoDB, device, command, argin), apiResponse],
          ])
          .dispatch({ type: EXECUTE_COMMAND, tangoDB, device, command, argin })
          .put(executeCommandFailed(tangoDB, device, command, argin))
          .silentRun();
      });

    it('should handle error case', () => {
        const tangoDB = 'test-db';
        const command = 'test-command';
        const argin = 'test-argin';
        const device = 'test-device';
        const error = new Error('Failed to execute command');

        return expectSaga(executeCommand)
            .provide([
                [call(TangoAPI.executeCommand, tangoDB, device, command, argin), throwError(error)],
            ])
            .dispatch({ type: EXECUTE_COMMAND, tangoDB, command, argin, device })
            .put(displayError(error.toString()))
            .silentRun();
    });
});

describe('setDeviceAttribute', () => {
    it('should handle success case', () => {
        const tangoDB = 'test-db';
        const device = 'test-device';
        const name = 'test-name';
        const value = 'test-value';
        const attribute = { name, value };

        const apiResponse = {
            ok: true,
            attribute,
        };

        return expectSaga(setDeviceAttribute)
            .provide([
                [call(TangoAPI.setDeviceAttribute, tangoDB, device, name, value), apiResponse],
            ])
            .dispatch({ type: SET_DEVICE_ATTRIBUTE, tangoDB, device, name, value })
            .put(setDeviceAttributeSuccess(tangoDB, attribute))
            .silentRun();
    });

    it('should handle failure case', () => {
        const tangoDB = 'test-db';
        const device = 'test-device';
        const name = 'test-name';
        const value = 'test-value';

        const apiResponse = {
            ok: false,
        };

        return expectSaga(setDeviceAttribute)
            .provide([
                [call(TangoAPI.setDeviceAttribute, tangoDB, device, name, value), apiResponse],
            ])
            .dispatch({ type: SET_DEVICE_ATTRIBUTE, tangoDB, device, name, value })
            .put(setDeviceAttributeFailed(tangoDB, device, name, value))
            .silentRun();
    });

    it('should handle error case', () => {
        const tangoDB = 'test-db';
        const device = 'test-device';
        const name = 'test-name';
        const value = 'test-value';
        const error = new Error('Failed to set device attribute');

        return expectSaga(setDeviceAttribute)
            .provide([
                [call(TangoAPI.setDeviceAttribute, tangoDB, device, name, value), throwError(error)],
            ])
            .dispatch({ type: SET_DEVICE_ATTRIBUTE, tangoDB, device, name, value })
            .put(displayError(error.toString()))
            .silentRun();
    });
});

describe('setDeviceProperty', () => {
    it('should handle success case', () => {
        const tangoDB = 'test-db';
        const device = 'test-device';
        const name = 'test-name';
        const value = 'test-value';
        const ok = true;

        return expectSaga(setDeviceProperty)
            .provide([
                [call(TangoAPI.setDeviceProperty, tangoDB, device, name, value), ok],
            ])
            .dispatch({ type: SET_DEVICE_PROPERTY, tangoDB, device, name, value })
            .put(setDevicePropertySuccess(tangoDB, device, name, value))
            .silentRun();
    });

    it('should handle failure case', () => {
        const tangoDB = 'test-db';
        const device = 'test-device';
        const name = 'test-name';
        const value = 'test-value';
        const ok = false;

        return expectSaga(setDeviceProperty)
            .provide([
                [call(TangoAPI.setDeviceProperty, tangoDB, device, name, value), ok],
            ])
            .dispatch({ type: SET_DEVICE_PROPERTY, tangoDB, device, name, value })
            .put(setDevicePropertyFailed(tangoDB, device, name, value))
            .silentRun();
    });
});

describe('deleteDeviceProperty', () => {
    it('should handle success case', () => {
        const tangoDB = 'test-db';
        const device = 'test-device';
        const name = 'test-name';
        const ok = true;

        return expectSaga(deleteDeviceProperty)
            .provide([
                [call(TangoAPI.deleteDeviceProperty, tangoDB, device, name), ok],
            ])
            .dispatch({ type: DELETE_DEVICE_PROPERTY, tangoDB, device, name })
            .put(deleteDevicePropertySuccess(tangoDB, device, name))
            .silentRun();
    });

    it('should handle failure case', () => {
        const tangoDB = 'test-db';
        const device = 'test-device';
        const name = 'test-name';
        const ok = false;

        return expectSaga(deleteDeviceProperty)
            .provide([
                [call(TangoAPI.deleteDeviceProperty, tangoDB, device, name), ok],
            ])
            .dispatch({ type: DELETE_DEVICE_PROPERTY, tangoDB, device, name })
            .put(deleteDevicePropertyFailed(tangoDB, device, name))
            .silentRun();
    });
});

describe('fetchDevice', () => {
    it('should handle success case', () => {
        const tangoDB = 'test-db';
        const name = 'test-name';
        const device = { name, attributes: [] };

        return expectSaga(fetchDevice)
            .provide([
                [call(TangoAPI.fetchDevice, tangoDB, name), device],
            ])
            .dispatch({ type: FETCH_DEVICE, tangoDB, name })
            .put(fetchDeviceSuccess(tangoDB, device))
            .silentRun();
    });

    it('should handle failure case', () => {
        const tangoDB = 'test-db';
        const name = 'test-name';
        const device = null;

        return expectSaga(fetchDevice)
            .provide([
                [call(TangoAPI.fetchDevice, tangoDB, name), device],
            ])
            .dispatch({ type: FETCH_DEVICE, tangoDB, name })
            .put(fetchDeviceFailed(tangoDB, name))
            .silentRun();
    });
});

describe('fetchDatabaseInfo', () => {
    it('should handle success case', () => {
        const tangoDB = 'test-db';
        const info = { host: 'localhost', port: 10000 };

        return expectSaga(fetchDatabaseInfo)
            .provide([
                [call(TangoAPI.fetchDatabaseInfo, tangoDB), info],
            ])
            .dispatch({ type: FETCH_DATABASE_INFO, tangoDB })
            .put(fetchDatabaseInfoSuccess(tangoDB, info))
            .silentRun();
    });

    it('should handle failure case', () => {
        const tangoDB = 'test-db';
        const info = null;

        return expectSaga(fetchDatabaseInfo)
            .provide([
                [call(TangoAPI.fetchDatabaseInfo, tangoDB), info],
            ])
            .dispatch({ type: FETCH_DATABASE_INFO, tangoDB })
            .put(fetchDatabaseInfoFailed(tangoDB))
            .silentRun();
    });
});

describe('refetchDeviceStateOnAttributeWrite', () => {
    it('should handle success case', () => {
        const tangoDB = 'test-db';
        const device = 'test-device';
        const attribute = { device, name: 'test-attribute', value: 'test-value' };
        const state = 'ON';

        return expectSaga(refetchDeviceStateOnAttributeWrite)
            .provide([
                [call(TangoAPI.fetchDeviceState, tangoDB, device), state],
            ])
            .dispatch({ type: SET_DEVICE_ATTRIBUTE_SUCCESS, tangoDB, attribute })
            .put({ type: DEVICE_STATE_RECEIVED, device, state })
            .silentRun();
    });
});

describe('subscribeDeviceAttrs', () => {
    it('should subscribe and unsubscribe to device attributes', () => {
        const tangoDB = 'test-db';
        const deviceName = 'test-device';
        const attributes = [
            { name: 'attribute1', dataformat: 'SCALAR' },
            { name: 'attribute2', dataformat: 'SCALAR' },
            { name: 'attribute3', dataformat: 'SPECTRUM' },
        ];

        const scalarChannel = channel();
        const nonScalarChannel = channel();

        const mockScalarHandler = createMockTask();
        const mockNonScalarHandler = createMockTask();

        return expectSaga(subscribeDeviceAttrs)
            .provide([
                [call(createChangeEventChannel, tangoDB, ['test-device/attribute1', 'test-device/attribute2']), scalarChannel],
                [call(createChangeEventChannel, tangoDB, ['test-device/attribute3'], false), nonScalarChannel],
                [fork(handleChangeEvents, scalarChannel), mockScalarHandler],
                [fork(handleChangeEvents, nonScalarChannel), mockNonScalarHandler],
                [take(UNSUBSCRIBE_DEVICE_ATTRS), {}],
            ])
            .dispatch({ type: SUBSCRIBE_DEVICE_ATTRS, tangoDB, deviceName, attributes })
            .fork(handleChangeEvents, scalarChannel)
            .fork(handleChangeEvents, nonScalarChannel)
            .dispatch({ type: UNSUBSCRIBE_DEVICE_ATTRS })
            .silentRun();
    });
});

describe('handleChangeEvents', () => {
    it('should handle incoming frames', () => {
        const frame1 = { device: 'test-device', attribute: 'attr1', value: 123 };
        const frame2 = { device: 'test-device', attribute: 'attr2', value: 'abc' };

        const channel = eventChannel((emitter) => {
            setTimeout(() => {
                emitter(frame1);
            }, 100);

            setTimeout(() => {
                emitter(frame2);
            }, 200);

            return () => { };
        });

        const generator = handleChangeEvents(channel);

        let next = generator.next();
        expect(next.value).toEqual(take(channel));

        next = generator.next(frame1);
        expect(next.value.payload).toEqual(put(attributeFrameReceived(frame1)).payload);

        next = generator.next();
        expect(next.value).toEqual(take(channel));

        next = generator.next(frame2);
        expect(next.value.payload).toEqual(put(attributeFrameReceived(frame2)).payload);

        next = generator.next();
        expect(next.value).toEqual(take(channel));

        next = generator.next();
        expect(next.done).toBe(false);

        generator.return();
        next = generator.next();
        expect(next.done).toBe(true);
    });

    it('should close channel on cancellation', () => {
        const channel = eventChannel(() => () => { });
        const generator = handleChangeEvents(channel);
        generator.next();
        expect(generator.return().value.type).toEqual('CANCELLED');
    });
});
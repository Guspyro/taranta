export interface itemInt {
    userId: number,
    id: number,
    title: string,
    completed: boolean,
    data: string
}

export interface deviceInt {
    device: string,
    attribute: string,
    timestamp: number,
    value: number,
    writeValue: number
}

export interface DataState extends Array<[]> {
    data: Array<number>;
}

export interface RootState {
    data: DataState;
    todo: Array<itemInt>;
    socket: Array<deviceInt>;
}

export interface WebsocketInt {
    type: string;
    data: object;
    id: number;
    field: string;
    value: [itemInt];
    payload: any;
}

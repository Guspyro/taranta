import React from 'react';
import '../tests/globalMocks';
import { configure, mount, shallow } from "enzyme";
import Adapter from "@wojtekmaj/enzyme-adapter-react-17";
import Notifications, {NotificationLevel} from "./notifications";

import { Provider } from "react-redux";

import configureStore from "../../shared/state/store/configureStore";

configure({ adapter: new Adapter() });

describe("Test notifications", () => {

    const store = configureStore();

    it("render with warning notification", () => {

        const elementHtml = React.createElement(Notifications.WrappedComponent, {
            isLoggedIn: true, 
            notifications: {
                notifications: Array({notification: {
                username: "CREAM", 
                level: NotificationLevel.WARNING, 
                message: "Error testBoolean in test/tarantatestdevice/1", 
                notified: false,
                key: 0.123456,
                timestamp: "1608142504203"}})},
            username: "CREAM",
            onClearNotifications: jest.fn(),
            onLoadNotifications: jest.fn(),
            onSetNotifications: jest.fn(),
          });
        
          const pageContent = mount(<Provider store={store}>{elementHtml}</Provider>).html();

    });

    it("render with error notification", () => {

        const elementHtml = React.createElement(Notifications.WrappedComponent, {
            isLoggedIn: true, 
            notifications: {
                notifications: Array({notification: {
                username: "CREAM", 
                level: NotificationLevel.ERROR, 
                message: "Error testBoolean in test/tarantatestdevice/1", 
                notified: false,
                key: 0.123456,
                timestamp: "1608142504203"}})},
            username: "CREAM",
            onClearNotifications: jest.fn(),
            onLoadNotifications: jest.fn(),
            onSetNotifications: jest.fn(),
          });
        
        const pageContent = mount(<Provider store={store}>{elementHtml}</Provider>).html();
          
    });

    it("render with info notification", () => {

        const elementHtml = React.createElement(Notifications.WrappedComponent, {
            isLoggedIn: true, 
            notifications: {
                notifications: Array({notification: {
                username: "CREAM", 
                level: NotificationLevel.INFO, 
                message: "Error testBoolean in test/tarantatestdevice/1", 
                notified: false,
                key: 0.123456,
                timestamp: "1608142504203"}})},
            username: "CREAM",
            onClearNotifications: jest.fn(),
            onLoadNotifications: jest.fn(),
            onSetNotifications: jest.fn(),
          });
        
          const pageContent = mount(<Provider store={store}>{elementHtml}</Provider>).html();
    });

     it("render with no notification", () => {

        const elementHtml = React.createElement(Notifications.WrappedComponent,{
            isLoggedIn: true, 
            username: "CREAM",
            onClearNotifications: jest.fn(),
            onLoadNotifications: jest.fn(),
            onSetNotifications: jest.fn(),
          });
          
        const pageContent = mount(<Provider store={store}>{elementHtml}</Provider>).html();
    });

    it("render with big info notification", () => {

        const elementHtml = React.createElement(Notifications.WrappedComponent, {
            isLoggedIn: true, 
            notifications: {
                notifications: Array({notification: {
                username: "CREAM", 
                level: NotificationLevel.INFO, 
                message: "Error testBoolean in test/tarantatestdevice/1\n \
                Error testBoolean in test/tarantatestdevice/1\n \
                Error testBoolean in test/tarantatestdevice/1", 
                notified: false,
                key: 0.123456,
                timestamp: "1608142504203"}})},
            username: "CREAM",
            onClearNotifications: jest.fn(),
            onLoadNotifications: jest.fn(),
            onSetNotifications: jest.fn(),
        });

        const shallowElement = shallow(elementHtml);
        shallowElement.setState({display: true, username: "CREAM", notPop: false});
        shallowElement.find(".badge").simulate('click');
    });

    it("render with warning notification and setState", () => {

    const elementHtml = React.createElement(Notifications.WrappedComponent, {
        isLoggedIn: true, 
        notifications: {
            notifications: Array({notification: {
            username: "CREAM", 
            level: NotificationLevel.WARNING, 
            message: "Error testBoolean in test/tarantatestdevice/1", 
            notified: true,
            key: 0.123456,
            timestamp: "1608142504203"}})},
        username: "CREAM",
        onClearNotifications: jest.fn(),
        onLoadNotifications: jest.fn(),
        onSetNotifications: jest.fn(),
        });

        const shallowElement = shallow(elementHtml);
        shallowElement.setState({display: true, username: "CREAM", notPop: false});

        shallowElement.find("#clearButton").simulate('click');

    });


    it("render with warning notification and setState", () => {

        const elementHtml = React.createElement(Notifications.WrappedComponent, {
            isLoggedIn: true, 
            notifications: {
                notifications: Array({notification: {
                username: "CREAM", 
                level: NotificationLevel.WARNING, 
                message: "Error testBoolean in test/tarantatestdevice/1", 
                notified: false,
                key: 0.123456,
                timestamp: "1608142504203"}}
                )},
            username: "CREAM",
            onClearNotifications: jest.fn(),
            onLoadNotifications: jest.fn(),
            onSetNotifications: jest.fn(),
            });
    
            const shallowElement = shallow(elementHtml);
            shallowElement.setProps({
                isLoggedIn: true, 
                notifications: {
                    notifications: Array({notification: {
                    username: "CREAM", 
                    level: NotificationLevel.WARNING, 
                    message: "Error testBoolean in test/tarantatestdevice/1", 
                    notified: false,
                    key: 0.123456,
                    timestamp: "1608142504203"}},
                    {notification: {
                        username: "CREAM", 
                        level: NotificationLevel.INFO, 
                        message: "Error testBoolean in test/tarantatestdevice/1",
                        notified: false,
                        key: 0.123457, 
                        timestamp: "1608142504203"}}
                    )},
                username: "CREAM",
                onClearNotifications: jest.fn(),
                onLoadNotifications: jest.fn(),
                onSetNotifications: jest.fn(),
                });

            shallowElement.find(".closepop").simulate('click');
            shallowElement.setState({display: true, username: "CREAM", notPop: false});
    });

    it("render with warning notification and setState", () => {

        const elementHtml = React.createElement(Notifications.WrappedComponent, {
            isLoggedIn: true, 
            notifications: {
                notifications: Array({notification: {
                username: "CREAM", 
                level: NotificationLevel.ERROR, 
                message: "Error testBoolean in test/tarantatestdevice/1", 
                notified: false,
                key: 0.123456,
                timestamp: "1608142504203"}}
                )},
            username: "CREAM",
            onClearNotifications: jest.fn(),
            onLoadNotifications: jest.fn(),
            onSetNotifications: jest.fn(),
            });
    
            const shallowElement = shallow(elementHtml);
            shallowElement.setProps({
                isLoggedIn: true, 
                notifications: {
                    notifications: Array({notification: {
                    username: "CREAM", 
                    level: NotificationLevel.ERROR, 
                    message: "Error testBoolean in test/tarantatestdevice/1",
                    notified: false,
                    key: 0.123456, 
                    timestamp: "1608142504203"}},
                    {notification: {
                        username: "CREAM", 
                        level: NotificationLevel.WARNING, 
                        message: "Error testBoolean in test/tarantatestdevice/1",
                        notified: false,
                        key: 0.123457, 
                        timestamp: "1608142504203"}}
                    )},
                username: "CREAM",
                onClearNotifications: jest.fn(),
                onLoadNotifications: jest.fn(),
                onSetNotifications: jest.fn(),
                });

        shallowElement.setState({display: true, username: "CREAM", notPop: false});

    });

    it("render with warning notification and setState", () => {

        const elementHtml = React.createElement(Notifications.WrappedComponent, {
            isLoggedIn: true, 
            notifications: {
                notifications: Array({notification: {
                username: "CREAM", 
                level: NotificationLevel.WARNING, 
                message: "Error testBoolean in test/tarantatestdevice/1", 
                notified: false,
                key: 0.123456,
                timestamp: "1608142504203"}}
                )},
            username: "CREAM",
            onClearNotifications: jest.fn(),
            onLoadNotifications: jest.fn(),
            onSetNotifications: jest.fn(),
            });
    
            const shallowElement = shallow(elementHtml);
            shallowElement.setProps({
                isLoggedIn: true, 
                notifications: {
                    notifications: Array({notification: {
                    username: "CREAM", 
                    level: NotificationLevel.WARNING, 
                    message: "Error testBoolean in test/tarantatestdevice/1",
                    notified: false,
                    key: 0.123456, 
                    timestamp: "1608142504203"}},
                    {notification: {
                        username: "CREAM", 
                        level: NotificationLevel.ERROR, 
                        message: "Error testBoolean in test/tarantatestdevice/1",
                        notified: false,
                        key: 0.123457, 
                        timestamp: "1608142504203"}}
                    )},
                username: "CREAM",
                onClearNotifications: jest.fn(),
                onLoadNotifications: jest.fn(),
                onSetNotifications: jest.fn(),
                });

        shallowElement.setState({display: true, username: "CREAM", notPop: false});

    });

    it("render with warning notification and setState", async () => {

        jest.useFakeTimers();

        const elementHtml = React.createElement(Notifications.WrappedComponent, {
            isLoggedIn: true, 
            notifications: {
                notifications: Array({notification: {
                username: "CREAM", 
                level: NotificationLevel.WARNING, 
                message: "Error testBoolean in test/tarantatestdevice/1", 
                notified: false,
                key: 0.123456,
                timestamp: "1608142504203"}}
                )},
            username: "CREAM",
            onClearNotifications: jest.fn(),
            onLoadNotifications: jest.fn(),
            onSetNotifications: jest.fn(),
            });
    
            const shallowElement = shallow(elementHtml);
            shallowElement.setProps({
                isLoggedIn: true, 
                notifications: {
                    notifications: Array({notification: {
                    username: "CREAM", 
                    level: NotificationLevel.WARNING, 
                    message: "Error testBoolean in test/tarantatestdevice/1",
                    notified: false,
                    key: 0.123456, 
                    timestamp: "1608142504203"}},
                    {notification: {
                        username: "CREAM", 
                        level: NotificationLevel.INFO, 
                        message: "Dashboard saved",
                        notified: false,
                        key: 0.123457, 
                        timestamp: "1608142504203"}}
                    )},
                username: "CREAM",
                onClearNotifications: jest.fn(),
                onLoadNotifications: jest.fn(),
                onSetNotifications: jest.fn(),
                });
    
        shallowElement.setState({display: true, username: "CREAM", notPop: false});

        shallowElement.find(".closepop").simulate('click');

        jest.runAllTimers();
        
        });

       it("click view more with long message", async () => {

        jest.useFakeTimers();

        const elementHtml = React.createElement(Notifications.WrappedComponent, {
            isLoggedIn: true, 
            notifications: {
                notifications: Array({notification: {
                username: "CREAM", 
                level: NotificationLevel.WARNING, 
                message: "Error testBoolean in test/tarantatestdevice/1", 
                notified: false,
                key: 0.123456,
                timestamp: "1608142504203"}}
                )},
            username: "CREAM",
            onClearNotifications: jest.fn(),
            onLoadNotifications: jest.fn(),
            onSetNotifications: jest.fn(),
            });
    
            const shallowElement = shallow(elementHtml);
            shallowElement.setProps({
                isLoggedIn: true, 
                notifications: {
                    notifications: Array({notification: {
                    username: "CREAM", 
                    level: NotificationLevel.WARNING, 
                    message: "Error testBoolean in test/tarantatestdevice/1",
                    notified: false,
                    key: 0.123456, 
                    timestamp: "1608142504203"}},
                    {notification: {
                        username: "CREAM", 
                        level: NotificationLevel.INFO, 
                        message: "Error testBoolean in test/tarantatestdevice/1\n \
                        Error testBoolean in test/tarantatestdevice/1\n \
                        Error testBoolean in test/tarantatestdevice/1", 
                        notified: false,
                        key: 0.123457, 
                        timestamp: "1608142504203"}}
                    )},
                username: "CREAM",
                onClearNotifications: jest.fn(),
                onLoadNotifications: jest.fn(),
                onSetNotifications: jest.fn(),
                });
    
        shallowElement.setState({display: true, username: "CREAM", notPop: false});

        shallowElement.find(".closepop").simulate('click');

        jest.runAllTimers();

        shallowElement.find("#viewMore").simulate('click');

        shallowElement.find("#btn-close").simulate('click');
            
    });
});
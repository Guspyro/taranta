import React from 'react'
import { configure, mount, shallow } from "enzyme";
import Adapter from "@wojtekmaj/enzyme-adapter-react-17";
import Modal from './Modal';

configure({ adapter: new Adapter() });

describe("Test Modal", () => {

    const Child = () => <div>ChildContent</div>;
    let component;
    
    it('should render all the styled components and the children', () => {
        component = mount(
          <Modal title="My Modal" rightTitle="Right Title">
              <Child />
          </Modal>
        );

      expect(component.html()).toContain('My Modal');
      expect(component.html()).toContain('Right Title');
      expect(component.html()).toContain('ChildContent');
      expect(component.html()).not.toContain('transparent-modal');
      component.unmount();
    });

  it('Test modal with transparent background', async () => {
      component = mount(
          <Modal title="" transparentModal={true}>
            <Child />
          </Modal>
        );

      expect(component.html()).toContain('transform');
      component.unmount();
  });
});
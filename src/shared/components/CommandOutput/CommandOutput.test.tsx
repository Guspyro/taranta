import React from 'react';
import { render } from '@testing-library/react';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';
import CommandOutput from './CommandOutput';

// Create a mock Redux store
const mockStore = configureStore([]);

describe('CommandOutput', () => {
  it('should render command output correctly', () => {
    // Define the mock state
    const mockState = {
      commandOutput: {
        device1: {
          command1: 'Mocked Command Output',
        },
      },
    };

    // Create a mock store with the desired state
    const store = mockStore(mockState);

    // Render the component within the Provider and the mock store
    const {container} = render(
      <Provider store={store}>
        <CommandOutput deviceName="device1" commandName="command1" />
      </Provider>
    );

    // Assert and expect statements
    expect(container.innerHTML).toContain('Mocked Command Output');
  });

  it('should render command output JSON correctly', () => {
    // Define the mock state
    const mockState = {
      commandOutput: {
        device1: {
          command1: '{"key":"value"}',
        },
      },
    };

    // Create a mock store with the desired state
    const store = mockStore(mockState);

    // Render the component within the Provider and the mock store
    const {container} = render(
      <Provider store={store}>
        <CommandOutput deviceName="device1" commandName="command1" />
      </Provider>
    );

    // Assert and expect statements
    expect(container.innerHTML).toContain('ul');
    expect(container.innerHTML).toContain('key');
  });
});

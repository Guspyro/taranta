import React from "react";
import CommandArgsFile from "./CommandArgsFile";
import "@testing-library/jest-dom/extend-expect";
import { configure, mount } from "enzyme";
import Adapter from "@wojtekmaj/enzyme-adapter-react-17";

global.alert = jest.fn();

// Mocks
jest.mock("react-redux", () => ({
  connect: () => (Component) => Component,
}));

jest.mock("../CommandOutput/CommandOutput", () => () => (
  <div data-testid="mocked-command-output" />
));

jest.mock("./DraggableModal", () => () => (
  <div data-testid="mocked-draggable-modal" />
));

jest.mock("../../../dashboard/dashboardRepo", () => ({
  getTangoDB: jest.fn(),
}));

jest.mock("../../user/state/selectors", () => ({
  getIsLoggedIn: jest.fn(),
}));

configure({ adapter: new Adapter() });

describe("test CommandArgsFile core component", () => {
  it("render widget with file content", () => {
    const element = React.createElement(CommandArgsFile, {
      tangoDB: "testdb",
      label: "sys/tg_test/1/DevString",
      uploadBtnCss: {},
      uploadBtnLabel: "Upload Button",
      sendBtnCss: {},
      sendBtnText: "Upload",
      mode: "run",
      requireConfirmation: false,
      alignSendButtonRight: true,
      commands: [],
      command: {
        device: "sys/tg_test/1",
        command: "DevString",
        acceptedType: "DevString",
        intypedesc: "-",
        outtypedesc: "-",
        outtype: "DevString",
        tag: "0",
        output: [null],
        execute: jest.fn(),
      },
      commandFromDevice: undefined,
      commandName: "DevString",
      commandDevice: "sys/tg_test/1",
      outerDivCss: {},
      commandOutputs: {},
      onExecute: jest.fn(),
      loggedIn: true,
      displayOutput: true,
    });

    const shallowElement = mount(element);

    shallowElement.state()["fileName"] = "f1.txt";
    shallowElement.state()["fileContent"] = "Hello World";
    shallowElement.setState({ fileType: "text" });
    shallowElement.state()["updateFileContent"] = "Hello World";

    shallowElement.find(".file-name-wrapper").simulate("click");
    expect(shallowElement.state()["showFileViewModal"]).toBe(true);

    //execute already executing command
    shallowElement.state()["pending"] = true;
    shallowElement.find(".btn-send").simulate("click");

    //execute command & check status
    shallowElement.state()["pending"] = false;
    shallowElement.state()["requireConfirmation"] = false;
    shallowElement.state()["fileContent"] = "Hello World";
    shallowElement.find(".btn-send").simulate("click");
    expect(shallowElement.getDOMNode().innerHTML).toContain("Upload Button");
  });

  it("render widget with show alert, bad type", () => {
    const element = React.createElement(CommandArgsFile, {
      tangoDB: "testdb",
      label: "sys/tg_test/1/DevString",
      uploadBtnCss: {},
      uploadBtnLabel: "Upload Button",
      sendBtnCss: {},
      sendBtnText: "Upload",
      mode: "run",
      requireConfirmation: false,
      alignSendButtonRight: true,
      commands: [],
      command: {
        device: "sys/tg_test/1",
        command: "DevString",
        acceptedType: "DevShort",
        intypedesc: "-",
        outtypedesc: "-",
        outtype: "DevString",
        tag: "0",
        output: [null],
        execute: jest.fn(),
      },
      commandFromDevice: undefined,
      commandName: "DevString",
      commandDevice: "sys/tg_test/1",
      outerDivCss: {},
      commandOutputs: {},
      onExecute: jest.fn(),
      loggedIn: true,
      displayOutput: true,
    });

    const shallowElement = mount(element);

    shallowElement.state()["fileName"] = "f1.txt";
    shallowElement.state()["fileContent"] = "Hello World";
    shallowElement.setState({ fileType: "text" });
    shallowElement.state()["updateFileContent"] = "Hello World";

    shallowElement.find(".file-name-wrapper").simulate("click");
    expect(shallowElement.state()["showFileViewModal"]).toBe(true);

    //execute already executing command
    shallowElement.state()["pending"] = true;
    shallowElement.find(".btn-send").simulate("click");

    //execute command & check status
    shallowElement.state()["pending"] = false;
    shallowElement.state()["requireConfirmation"] = false;
    shallowElement.state()["fileContent"] = "Hello World";
    shallowElement.find(".btn-send").simulate("click");
    expect(shallowElement.getDOMNode().innerHTML).toContain("input >= -32768");
  });
});

describe("CommandArgsFile Component", () => {
  // Mocking FileReader
  class MockFileReader {
    onload: (event: any) => void = () => {};
    onerror: () => null = () => null;

    readAsText = () => {
      this.onload({ target: { result: "fileContent" } });
    };

    static EMPTY = 0;
    static LOADING = 1;
    static DONE = 2;
  }

  (global as any).FileReader = MockFileReader;

  const testProps = {
    tangoDB: "testdb",
    label: "sys/tg_test/1/DevString",
    uploadBtnCss: {},
    uploadBtnLabel: "Upload Button",
    sendBtnCss: {},
    sendBtnText: "Upload",
    mode: "run",
    requireConfirmation: false,
    alignSendButtonRight: true,
    commands: [],
    command: {
      device: "sys/tg_test/1",
      command: "DevString",
      acceptedType: "DevShort",
      intypedesc: "-",
      outtypedesc: "-",
      outtype: "DevString",
      tag: "0",
      output: [null],
      execute: jest.fn(),
    },
    commandFromDevice: undefined,
    commandName: "DevString",
    commandDevice: "sys/tg_test/1",
    outerDivCss: {},
    commandOutputs: {},
    onExecute: jest.fn(),
    loggedIn: true,
    displayOutput: true,
  };

  let wrapper;
  beforeEach(() => {
    wrapper = mount(<CommandArgsFile {...testProps} />);
  });

  it("readFileAsync method works correctly", async () => {
    const file = new File(["dummy content"], "testFile.txt", {
      type: "text/plain",
    });
    const instance = wrapper.instance();
    await instance.readFileAsync(file);
    expect(instance.readFileAsync).toBeDefined();
  });

  it("onFilesChange method works correctly", async () => {
    const file = new File(["dummy content"], "testFile.txt", {
      type: "text/plain",
    });
    const files = [file];

    const instance = wrapper.instance();
    await instance.onFilesChange(files);

    expect(instance.state.fileName).toEqual("testFile.txt");
  });

  it("onFilesError method works correctly", async () => {
    const file = new Blob(["dummy content"], { type: "text/plain" });
    const files = [file];

    const instance = wrapper.instance();
    await instance.onFilesError(files);

    expect(instance.state.fileName).toContain("No file selected");
  });
});

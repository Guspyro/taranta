import React from 'react';
import { render, fireEvent, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import DraggableModal from './DraggableModal';

test('Renders modal title correctly', () => {
    render(<DraggableModal fileName="TestFile" fileType="text" fileContent=""
        updateFileContent="" showCompare={false} restoreValue={false} setState={() => { }} />);
    expect(screen.getByText('View/Edit TestFile')).toBeInTheDocument();
});

test('Calls setState on restore content button click', () => {
    const setState = jest.fn();
    render(<DraggableModal fileName="TestFile" fileType="text" fileContent="Old content"
        updateFileContent="New content" showCompare={false} restoreValue={false} setState={setState} />);

    fireEvent.click(screen.getByText('Restore Content'));
    expect(setState).toHaveBeenCalledWith({ restoreValue: true });
});

test('Closes modal', () => {
    const setState = jest.fn();
    render(<DraggableModal fileName="TestFile" fileType="text" fileContent=""
        updateFileContent="" showCompare={false} restoreValue={false} setState={setState} />);

    fireEvent.click(screen.getByText('Close'));
    expect(setState).toHaveBeenCalledWith({ showFileViewModal: false });
});

test('Compares file content', () => {
    render(<DraggableModal fileName="TestFile" fileType="text" fileContent="Old content"
        updateFileContent="New content" showCompare={true} restoreValue={false} setState={() => { }} />);

    fireEvent.click(screen.getByText('Edit'));
    expect(document.body.innerHTML).toContain('Restore');
});

test('Restore value true click Yes', () => {
    render(<DraggableModal fileName="TestFile" fileType="text" fileContent="Old content"
        updateFileContent="New content" showCompare={true} restoreValue={true} setState={() => { }} />);

    expect(document.body.innerHTML).toContain('Are you sure');
    fireEvent.click(screen.getByText('Yes'));
    expect(document.body.innerHTML).toContain('removed">Old');
});

test('Restore value true click No', () => {
    render(<DraggableModal fileName="TestFile" fileType="text" fileContent="Old content"
        updateFileContent="New content" showCompare={true} restoreValue={true} setState={() => { }} />);

    expect(document.body.innerHTML).toContain('Are you sure');
    fireEvent.click(screen.getByText('No'));
    expect(document.body.innerHTML).toContain('content');
});

test('showCompare false', () => {
    const setState = jest.fn();
    render(<DraggableModal fileName="TestFile" fileType="text" fileContent="" updateFileContent="" showCompare={false} restoreValue={false} setState={setState} />);

    const codeInput = screen.getByRole('textbox');
    fireEvent.change(codeInput, { target: { value: 'New code' } });

    const compareButton = screen.getByTitle('compare');
    fireEvent.click(compareButton);
    expect(document.body.innerHTML).toContain('background-color: #accef7');
});


import { getDashboardVariables, getDeviceOfDashboardVariable, mapVariableNameToDevice, getRunningClasses, getRunningDevices, checkVariableDevice, variablePresent, devicePresent } from './DashboardVariables';
import { DashboardEditHistory } from '../../dashboard/types';
import { attributePlot } from '../state/sagas/stateTestExamples';

let tangoClasses = [
    {
        "name": "DataBase",
        "devices": [
            {
                "name": "sys/database/2",
                "exported": true,
                "connected": true
            }
        ]
    },
    {
        "name": "TangoTest",
        "devices": [
            {
                "name": "sys/tg_test/1",
                "exported": true,
                "connected": false
            }
        ]
    }
]

const tangoDevice = "sys/tg_test/1"
const widgets = [
  {
    _id: '607e8e9c99ba3e0017cf9080',
    id: '1',
    x: 16,
    y: 5,
    canvas: '0',
    width: 14,
    height: 3,
    type: 'ATTRIBUTE_DISPLAY',
    inputs: {
      attribute: {
        device: 'CSPVar1',
        attribute: 'state',
        label: 'State'
      },
      precision: 2,
      showDevice: false,
      showAttribute: 'Label',
      scientificNotation: false,
      showEnumLabels: false,
      textColor: '#000000',
      backgroundColor: '#ffffff',
      size: 1,
      font: 'Helvetica'
    },
    order: 0,
    valid: 1
  },
  {
    _id: '607e946599ba3e0017cf90a0',
    id: '2',
    x: 16,
    y: 10,
    canvas: '0',
    width: 20,
    height: 3,
    type: 'COMMAND',
    inputs: {
      command: {
        device: 'myvar',
        command: 'DevString',
        acceptedType: 'DevString'
      },
      showDevice: true,
      showCommand: true,
      requireConfirmation: true,
      displayOutput: true,
      OutputMaxSize: 20,
      timeDisplayOutput: 3000,
      textColor: '#000000',
      backgroundColor: '#ffffff',
      size: 1,
      font: 'Helvetica'
    },
    order: 1,
    valid: 1
  }
];
let wid = {
  "_id": "609bb1c91233730011f37222",
  "id": "4",
  "x": 17,
  "y": 7,
  "canvas": "0",
  "width": 14,
  "height": 3,
  "type": "DEVICE_STATUS",
  "inputs": {
      "device": "CSPVar1",
      "state": {
          "device": null,
          "attribute": null
      },
      "showDeviceName": true,
      "showStateString": true,
      "showStateLED": true,
      "LEDSize": 1,
      "textSize": 1
  },
  "order": 3,
  "valid": 1
}

describe("test util Dashboard Variable functions", () =>{
  let myHistory: DashboardEditHistory;

  myHistory = {
    undoActions: [{'1': widgets[0]}],
    redoActions: [{'1': widgets[1]}],
    undoIndex: 2,
    redoIndex: 2,
    redoLength: 1,
    undoLength: 1
  }
  const dashboards = [{id:'604b5fac8755940011efeea1',name:'Untitled dashboard',user:'user1',groupWriteAccess:false,selectedIds:[],history:myHistory,insertTime: new Date(),updateTime:new Date(),group:null,lastUpdatedBy:'user1',tangoDB:'testdb',variables:[{id:'0698hln1j359a',name:'myvar','class':'tg_test',device:'sys/tg_test/1'}]},{id:'6073154703c08b0018111066',name:'Untitled dashboard',user:'user1',groupWriteAccess:false,selectedIds:[],history:myHistory,insertTime:new Date(),updateTime: new Date(),group:null,lastUpdatedBy:'user1',tangoDB:'testdb',variables:[{id:'78l4d190kh2g',name:'cspvar1','class':'tg_test',device:'sys/tg_test/1'},{id:'i5am90g1il0e',name:'myvar','class':'tg_test',device:'sys/tg_test/1'}]}];

  const variables = [
    {id: '11', 'name': 'CSPVar1', class: 'tg_test', device: 'dserver/databaseds/2' },
    {id: '12', 'name': 'TMCVar2', class: 'tarantatestdevice', device: 'tarantatestdevice2'},
  ];

  it("test if dashboard variables are replaced by default define in subscribed widgets", () => {
    expect(getDashboardVariables('', dashboards)).toEqual([]);
    expect(JSON.stringify(mapVariableNameToDevice(widgets, variables))).toContain('dserver/databaseds/2');
    expect(JSON.stringify(mapVariableNameToDevice(widgets, variables))).not.toContain('CSPVar1');
    expect(JSON.stringify(mapVariableNameToDevice(widgets, variables))).not.toContain('TMCVar2');
    expect(JSON.stringify(mapVariableNameToDevice(widgets, []))).toEqual(JSON.stringify(widgets));
  });

  it("test if dashboard variables are searched correctly", ()=>{
    expect(getDeviceOfDashboardVariable(variables, 'CSPVar1')).toContain('dserver/databaseds/2');
    expect(getDeviceOfDashboardVariable(variables, 'CSPVa22')).toContain('CSPVa22');
  });

  it("test getRunningClasses", () => {

    //connect the second device
    tangoClasses[1].devices[0].connected = true
    expect(Object.values(getRunningClasses(tangoClasses)).length).toBe(2)

    //disconnect the second device
    tangoClasses[1].devices[0].connected = false
    expect(Object.values(getRunningClasses(tangoClasses)).length).toBe(1)

})

it("test getRunningDevices", () => {

    //connect TangoTestDevice
    let tangoDevices = [
      {name: 'sys/tg_test/1', exported: true, connected: true}
    ];

    expect(Object.values(getRunningDevices(tangoDevices)).length).toBe(1)
    expect(getRunningDevices(tangoDevices)[0].name).toBe(tangoDevice)

    tangoDevices[0].connected = false;
    expect(Object.values(getRunningDevices(tangoClasses)).length).toBe(0)
})

it("test checkVariableDevice", () => {

    let tangoDevices = [
      {name: 'sys/tg_test/1', exported: true, connected: true}
    ];

    let result = checkVariableDevice("sys/tg_test/1",tangoDevices)
    expect(result.connected).toBe(true)
    expect(result.exported).toBe(true)

    //disconnect TangoTestDevice
    tangoDevices[0].connected = false
    result = checkVariableDevice("sys/tg_test/1",tangoDevices)
    expect(result.connected).toBe(false)
    expect(result.exported).toBe(true)

    //test invalid device
    result = checkVariableDevice("not found",tangoDevices)
    expect(result.connected).toBe(false)
    expect(result.exported).toBe(false)
})


it("test if dashboard variables are present", ()=>{
    expect(variablePresent(widgets[0], variables)).toEqual({found: true, device: 'CSPVar1'});
    expect(variablePresent(widgets[1], variables)).toEqual({found: false, device: 'myvar'});

    expect(variablePresent(wid, variables)).toEqual({found: true, device: 'CSPVar1'});
    wid.inputs.device = 'myvar';
    expect(variablePresent(widgets[1], variables)).toEqual({found: false, device: 'myvar'});
    expect(variablePresent(wid, variables)).toEqual({found: false, device: 'myvar'});
  });

  it("test for Table View if dashboard variables are present", ()=>{
    let wid = {
      "_id": "609bb1c91233730011f37222",
      "id": "4",
      "x": 17,
      "y": 7,
      "canvas": "0",
      "width": 14,
      "height": 3,
      "type": "TABULAR_VIEW",
      "inputs": {
        "devices": [
          {
            "device": "CSPVar1"
          },
          {
            "device": "Var2"
          }
        ],
        "compactTable": true,
        "borderedTable": false,
      },
      "order": 3,
      "valid": 1
    }
    expect(variablePresent(wid, variables)).toEqual({found: false, device: 'Var2'});
    wid.inputs.devices[1].device = 'TMCVar2';
    expect(variablePresent(wid, variables)).toEqual({found: true, device: ''});
  });

  it("test for Table View if devices on dashboard are present", ()=>{
    let tableWid = {
      "_id": "609bb1c91233730011f37222",
      "id": "4",
      "x": 17,
      "y": 7,
      "canvas": "0",
      "width": 14,
      "height": 3,
      "type": "TABULAR_VIEW",
      "inputs": {
        "devices": [
          {
            "device": "sys/tg_test/1"
          },
          {
            "device": "sys/tg_test/11"
          }
        ],
        "compactTable": true,
        "borderedTable": false,
      },
      "order": 3,
      "valid": 1
    }
    //attribute test
    // expect(devicePresent(widgets[0], [])).toEqual({found: false, device: 'CSPVar1'});
    expect(devicePresent(widgets[0], ['CSPVar1'])).toEqual({found: true, device: ''});

    //cmd test
    // expect(devicePresent(widgets[1], [])).toEqual({found: false, device: 'myvar'});
    expect(devicePresent(widgets[1], ['myvar'])).toEqual({found: true, device: ''});

    //device test
    // expect(devicePresent(wid, [])).toEqual({found: false, device: 'myvar'});
    expect(devicePresent(wid, ['myvar'])).toEqual({found: true, device: ''});

    //attribute plot test
    expect(devicePresent(attributePlot, [])).toEqual({found: false, device: 'sys/tg_test/1'});
    expect(devicePresent(attributePlot, ['sys/tg_test/1'])).toEqual({found: true, device: ''});

    //tablular view test
    expect(devicePresent(tableWid, ['sys/tg_test/1'])).toEqual({found: false, device: 'sys/tg_test/11'});
    tableWid.inputs.devices[1].device = 'sys/tg_test/1';
    expect(devicePresent(tableWid, ['sys/tg_test/1', 'sys/tg_test/11'])).toEqual({found: true, device: ''});
  });
});
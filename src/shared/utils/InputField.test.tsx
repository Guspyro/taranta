import React from "react";
import { InputField } from "./InputField";
import '@testing-library/jest-dom/extend-expect';

import { configure, shallow, mount } from "enzyme";
import Adapter from "@wojtekmaj/enzyme-adapter-react-17";
import { useSelector, useDispatch } from 'react-redux';

jest.mock('react-redux', () => ({
  useSelector: jest.fn(),
  useDispatch: jest.fn(),
}));

jest.mock('../../dashboard/dashboardRepo', () => ({
  getTangoDB: jest.fn()
}));

configure({ adapter: new Adapter() });

describe("InputField Test Cases", () => {
  const dispatch = jest.fn();
  beforeEach(() => {
    (useDispatch as jest.Mock).mockReturnValue(dispatch);
    (useSelector as jest.Mock).mockReturnValue( "testDB" );
  });

  afterEach(() => {
    jest.resetAllMocks();
  });

  it("renders without crashing", () => {
    const element = React.createElement(InputField, {
      mode: "run",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      isEnabled: true,
      intype: "DevString",
      buttonLabel: "Execute Command"
    });

    const elementHtml = shallow(element)
      .html()
      .replace(/\s/g, "");

    expect(elementHtml).toContain("DevString");
    expect(elementHtml).toContain("ExecuteCommand");
    expect(elementHtml).toContain("input-group");
    expect(elementHtml).toContain("input-group-append");

    let widget = mount(element);
    let input = widget.find("input");
    input.simulate("change", { target: { value: "argument1, argument2" } });
    widget.find("button").simulate("click");
  });

  it("renders for DevULong", () => {
    const element = React.createElement(InputField, {
      mode: "library",
      isEnabled: true,
      intype: "DevULong",
      buttonLabel: "Execute",
      placeholder: "intypedesc",
      intypedesc: "DevULongDesc",
    });

    const elementHtml = shallow(element)
      .html()
      .replace(/\s/g, "");
    expect(elementHtml).toContain("DevULong");
    expect(elementHtml).toContain("75px");
    expect(elementHtml).toContain("DevULongDesc");

    let widget = mount(element);
    let input = widget.find("input");
    input.simulate("change", { target: { value: "22" } });
    widget.find("button").simulate("click");
  });

  it("renders for DevDouble", () => {
    const element = React.createElement(InputField, {
      onExecute: () => null,
      isEnabled: true,
      intype: "DevDouble",
      buttonLabel: "Execute"
    });

    const elementHtml = shallow(element)
      .html()
      .replace(/\s/g, "");
    expect(elementHtml).toContain("DevDouble");

    let widget = mount(element);
    let input = widget.find("input");
    input.simulate("change", { target: { value: "22.1" } });
    widget.find("button").simulate("click");
  });

  it("renders for DevBoolean", () => {
    const element = React.createElement(InputField, {
      onExecute: () => null,
      isEnabled: true,
      intype: "DevBoolean",
      buttonLabel: "Execute"
    });

    const elementHtml = shallow(element)
      .html()
      .replace(/\s/g, "");
    expect(elementHtml).toContain("custom-select");

    let widget = mount(element);
    let input = widget.find("select");
    input.simulate("change", { target: { value: 1 } });
    widget.find("button").simulate("click");
  });

  // Testing array inputs
  it("renders for DevVarLongArray", () => {
    const element = React.createElement(InputField, {
      onExecute: () => null,
      isEnabled: true,
      intype: "DevVarLongArray",
      buttonLabel: "Execute"
    });

    const elementHtml = shallow(element)
      .html()
      .replace(/\s/g, "");
    expect(elementHtml).toContain("DevVarLongArray");

    let widget = mount(element);
    let input = widget.find("input");
    input.simulate("change", { target: { value: "[1,2,4]" } });
    widget.find("button").simulate("click");
    //Testing with single input
    input.simulate("change", { target: { value: "[154]" } });
    widget.find("button").simulate("click");
  });

  it("renders for DevVarStringArray", () => {
    const element = React.createElement(InputField, {
      onExecute: () => null,
      isEnabled: true,
      intype: "DevVarStringArray",
      buttonLabel: "Execute"
    });

    const elementHtml = shallow(element)
      .html()
      .replace(/\s/g, "");
    expect(elementHtml).toContain("DevVarStringArray");

    let widget = mount(element);
    let input = widget.find("input");
    input.simulate("change", { target: { value: "['aa']" } });
    widget.find("button").simulate("click");
  });

  // Testing complex inputs
  it("renders for DevVarLongStringArray", () => {
    const element = React.createElement(InputField, {
      onExecute: () => null,
      isEnabled: true,
      intype: "DevVarLongStringArray",
      buttonLabel: "Execute"
    });

    const elementHtml = shallow(element)
      .html()
      .replace(/\s/g, "");
    expect(elementHtml).toContain("DevVarLongStringArray");

    let widget = mount(element);
    let input = widget.find("input");
    input.simulate("change", { target: { value: "[1,2,5]['aaa','bbbb']" } });
    widget.find("button").simulate("click");
  });

  it("renders for DevVarDoubleStringArray", () => {
    const element = React.createElement(InputField, {
      onExecute: () => null,
      isEnabled: true,
      intype: "DevVarDoubleStringArray",
      buttonLabel: "Execute"
    });

    const elementHtml = shallow(element)
      .html()
      .replace(/\s/g, "");
    expect(elementHtml).toContain("DevVarDoubleStringArray");

    let widget = mount(element);
    let input = widget.find("input");
    input.simulate("change", { target: { value: "[1.2, 2.4, 5]['xyz', 'pqr']" } });
    widget.find("button").simulate("click");
  });

  it("renders for DevDouble", async () => {
    const element = React.createElement(InputField, {
      onExecute: () => null,
      isEnabled: true,
      intype: "DevDouble",
      renderType: "custom",
      commandArgs: [
        {
            "name": "",
            "value": "111",
            "isDefault": false
        },
        {
            "name": "One",
            "value": "String1",
            "isDefault": true
        }
      ],
      buttonLabel: "Execute"
    });

    const elementHtml = shallow(element)
      .html()
      .replace(/\s/g, "");

    expect(elementHtml).toContain("SelectArgument");

    let mountedComponent = mount(element);
    mountedComponent.find('.drp-wrapper').first().find('.drp-bttn').first();
  });
});
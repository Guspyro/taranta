import sanitize_device_name from './SanitizeDeviceName'

describe("test util Collapse Attribute function", () => {

    it("test if it returns the name without special characters", () => {
        expect(sanitize_device_name("test-device_name")).toEqual("test_device_name");
    })

    it("test if it returns the name without special characters", () => {
        expect(sanitize_device_name("test.device/name")).toEqual("test_device_name");
    })

    it("test if it returns the name without special characters", () => {
        expect(sanitize_device_name("test~device,name")).toEqual("test_device_name");
    })
})
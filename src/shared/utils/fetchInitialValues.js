import tangoAPI from "../api/tangoAPI";
import config from "../../config.json";
import { WEBSOCKET } from "../state/actions/actionTypes";

export async function fetchInitialValues(dispatch, element) {
    const url = window.location.href.replace(config.basename, "").split('/');
    const attributes = await tangoAPI.fetchAttributesValues(
        url[3],
        element
    );

    if (attributes[0]) {
        attributes.forEach((attributeData) => {
            const attribute = {
                device: attributeData.device,
                attribute: attributeData.name,
                value: attributeData.value,
                timestamp: attributeData.timestamp,
                quality: attributeData.quality,
                writeValue: attributeData.writevalue
            }
            const data = JSON.stringify({ payload: { data: { attributes: attribute } } });
            dispatch({
                type: WEBSOCKET.WS_MESSAGE,
                value: [{ data }]
            });
        })

    }
}
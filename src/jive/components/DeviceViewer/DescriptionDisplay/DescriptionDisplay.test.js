import React from "react";
import { render, fireEvent, screen } from "@testing-library/react";
import { configure, shallow } from 'enzyme';
import DescriptionDisplay from "./DescriptionDisplay";
import '@testing-library/jest-dom';
import Adapter from "@wojtekmaj/enzyme-adapter-react-17";
configure({ adapter: new Adapter() });

jest.mock('react-tooltip', () => ({
  Tooltip: ({ children }) => <>{children}</>,
}));

describe('DescriptionDisplay', () => {
  it('renders name and description', () => {
    const name = 'Test Name';
    const description = 'Test Description';
    render(<DescriptionDisplay name={name} description={description} />);

    const infoIcon = screen.getByTestId('info');
    expect(infoIcon).toBeInTheDocument();
    fireEvent.click(infoIcon);

    const modalTitle = screen.getByText(name);
    expect(modalTitle).toBeInTheDocument();
    const modalBody = screen.getByText(description);
    expect(modalBody).toBeInTheDocument();
  });
});

describe("DescriptionDisplay component", () => {
  let wrapper;
  beforeEach(() => {
    wrapper = shallow(<DescriptionDisplay name="Test name" description="Test description" />);
  });

  it("should toggle the display of the modal", () => {
    // Initial state of onDisplay
    expect(wrapper.find("Modal").length).toBe(0);

    // Click the info icon to set onDisplay to true
    wrapper.find(".fa-info-circle").simulate("click");
    expect(wrapper.find("Modal").length).toBe(1);

    // Click the Close button to set onDisplay to false
    wrapper.find("Button").simulate("click");
    expect(wrapper.find("Modal").length).toBe(0);
  });
});

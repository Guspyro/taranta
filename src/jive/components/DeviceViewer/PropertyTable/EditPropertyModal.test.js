import React from "react";
import { configure, shallow, mount } from "enzyme";
import EditPropertyModal from "./EditPropertyModal";
import Modal from "../../../../shared/modal/components/Modal/Modal";
import Adapter from "@wojtekmaj/enzyme-adapter-react-17";
import configureStore from "redux-mock-store";

configure({ adapter: new Adapter() });

const mockStore = configureStore([]);

describe("EditPropertyModal component", () => {
  it("renders without crashing", () => {
    const wrapper = mount(
      <EditPropertyModal 
      name="property name" 
      onClose={() => {}} 
      />
    );
    wrapper.find('.btn-outline-secondary').simulate('click');
    expect(wrapper.html()).toContain('Edit Property');
  });

  it("renders a Modal component", () => {
    const wrapper = shallow(
      <EditPropertyModal name="property name" onClose={() => {}} />
    );
    expect(wrapper.find(Modal)).toHaveLength(1);
  });

  it("displays the correct title in the Modal", () => {
    const wrapper = shallow(
      <EditPropertyModal name="property name" onClose={() => {}} />
    );
    expect(wrapper.find(Modal).prop("title")).toEqual("Edit Property");
  });

  it("calls the sunmit with empty value and WARNING", () => {
    const onEdit = jest.fn();
    const wrapper = mount(
      <EditPropertyModal
        name="property name"
        initialValue="initial value"
        onEdit={onEdit}
        onClose={() => {}}
      />
    );
    wrapper.find('.btn-primary').simulate('click');
    expect(wrapper.html()).toContain('WARNING');
  });

  it("updates the value state when the textarea changes", () => {
    const wrapper = mount(
      <EditPropertyModal
        name="property name"
        initialValue="initial value"
        onEdit={() => {}}
        onClose={() => {}}
      />
    );
    wrapper
      .find("textarea")
      .simulate("change", { target: { value: "new value" } });
    expect(wrapper.state().value).toEqual("new value");
  });

});
import React from 'react';
import configureStore from "redux-mock-store";
import { Provider } from "react-redux";
import { configure, mount } from 'enzyme';
import Adapter from "@wojtekmaj/enzyme-adapter-react-17";
import AttributeTable from './AttributeTable';

configure({ adapter: new Adapter() });

const mockStore = configureStore([]);

describe('AttributeTable component', () => {
  let wrapper;
  let props = {
    attributes: [
      {
        name: 'attribute1',
        dataformat: 'SCALAR',
        displevel: '0',
        description: 'Attr SCALAR'
      },
      {
        name: 'attribute2',
        dataformat: 'IMAGE',
        displevel: '0',
        description: 'Attr SCALAR'
      },
      {
        name: 'attribute3',
        dataformat: 'SPECTRUM',
        displevel: '0',
        description: 'Attr SCALAR'
      }
    ],
    selectedFormat: 'SCALAR',
    deviceName: 'device1',
    onSelectDataFormat: jest.fn(),
    onSetDeviceAttribute: jest.fn(),
    isLoggedIn: true,
    tangoDB: 'tangoDB'
  };

  it('renders selectedFormat SCALAR without crashing', () => {

    const store = mockStore({
      user: {
        username: 'user1'
      },
      deviceDetail: {
        activeDataFormat: 'SCALAR',
        disabledDisplevels: ['1'],
      },
      messages: {
        tg_test: {
          attributes: {
            attribute1: {
              values: [null],
              timestamp: [null]
            }
          }
        }
      }
    });

    wrapper = mount(
      <Provider store={store}>
        <AttributeTable {...props} />
      </Provider>);

    expect(wrapper.html()).toContain('class="AttributeTable"');
    expect(wrapper.html()).toContain('SCALAR');
  });

  it('renders selectedFormat SPECTRUM without crashing', () => {
    props.selectedFormat = 'SPECTRUM';

    const store = mockStore({
      user: {
        username: 'user1'
      },
      deviceDetail: {
        activeDataFormat: 'SPECTRUM',
        disabledDisplevels: ['1'],
      },
      messages: {
        tg_test: {
          attributes: {
            attribute1: {
              values: [null],
              timestamp: [null]
            }
          }
        }
      }
    });

    wrapper = mount(
      <Provider store={store}>
        <AttributeTable {...props} />
      </Provider>);

    expect(wrapper.html()).toContain('class="AttributeTable"');
    expect(wrapper.html()).toContain('SPECTRUM');
  });

  it('renders AttributeTable opens edit attribute and submits', () => {
    props.selectedFormat = 'SCALAR';

    const store = mockStore({
      user: {
        username: 'user1'
      },
      deviceDetail: {
        activeDataFormat: 'SCALAR',
        disabledDisplevels: ['1'],
      },
      messages: {
        tg_test: {
          attributes: {
            attribute1: {
              values: [null],
              timestamp: [null]
            }
          }
        }
      }
    });

    wrapper = mount(
      <Provider store={store}>
        <AttributeTable {...props} />
      </Provider>);

    wrapper.find('.fa-pencil').at(0).simulate('click');
    wrapper.find('.btn-primary').at(0).simulate('click');

    expect(wrapper.html()).toContain('class="AttributeTable"');
    expect(wrapper.html()).toContain('SCALAR');
  });


  it('renders AttributeTable opens edit attribute and closes', () => {
    props.selectedFormat = 'SCALAR';

    const store = mockStore({
      user: {
        username: 'user1'
      },
      deviceDetail: {
        activeDataFormat: 'SCALAR',
        disabledDisplevels: ['1'],
      },
      messages: {
        tg_test: {
          attributes: {
            attribute1: {
              values: [null],
              timestamp: [null]
            }
          }
        }
      }
    });

    wrapper = mount(
      <Provider store={store}>
        <AttributeTable {...props} />
      </Provider>);

    wrapper.find('.fa-pencil').at(0).simulate('click');
    wrapper.find('.btn-outline-secondary').at(0).simulate('click');

    expect(wrapper.html()).toContain('class="AttributeTable"');
    expect(wrapper.html()).toContain('SCALAR');
  });

  it('renders AttributeTable clicks on SCALAR TAB', () => {
    props.selectedFormat = 'SCALAR';

    const store = mockStore({
      user: {
        username: 'user1'
      },
      deviceDetail: {
        activeDataFormat: 'SCALAR',
        disabledDisplevels: ['1'],
      },
      messages: {
        tg_test: {
          attributes: {
            attribute1: {
              values: [null],
              timestamp: [null]
            }
          }
        }
      }
    });

    wrapper = mount(
      <Provider store={store}>
        <AttributeTable {...props} />
      </Provider>);

    wrapper.find('a').at(0).simulate('click');
    expect(wrapper.html()).toContain('class="AttributeTable"');
    expect(wrapper.html()).toContain('SCALAR');
  });
});

import React from "react";
import ValueDisplay from "./ValueDisplay";
import { Provider } from "react-redux";
import configureStore from "redux-mock-store";
import { configure, mount } from 'enzyme';
import Adapter from "@wojtekmaj/enzyme-adapter-react-17";

configure({ adapter: new Adapter() });

const mockStore = configureStore([]);

describe("ValueDisplay", () => {

  it("should render 'No value' if value is null", () => {
    const store = mockStore({
      messages: {
        tg_test: {
          attributes: {
            attribute1: {
              values: [null],
              timestamp: [null]
            }
          }
        }
      }
    });

    const wrapper = mount(
      <Provider store={store}>
        <ValueDisplay
          deviceName={'tg_test'}
          name={'attribute1'}
          dataformat={'SCALAR'} />
      </Provider>
    );

    expect(wrapper.html()).toContain('No value');
  });

  it("should return empty string if value is undefined", () => {
    const store = mockStore({
      messages: {
        tg_test: {
          attributes: {
            attribute1: {
              values: [undefined],
              timestamp: [undefined]
            }
          }
        }
      }
    });

    const wrapper = mount(
      <Provider store={store}>
        <ValueDisplay />
      </Provider>
    );

    expect(wrapper.html()).toEqual("");
  });
  
  it("should render ValueDisplay scalar DevString if dataformat is 'SCALAR' and datatype is 'DevString'", () => {
    const store = mockStore({
      messages: {
        tg_test: {
          attributes: {
            attribute1: {
              values: ["test"],
              timestamp: [1675251195.549226]
            }
          }
        }
      }
    });

    const wrapper = mount(
      <Provider store={store}>
        <ValueDisplay
          deviceName={'tg_test'}
          name={'attribute1'}
          dataformat={'SCALAR'}
          datatype={'DevString'}
        />
      </Provider>
    );
    
    expect(wrapper.html()).toContain('ValueDisplay scalar DevString');
  });
  
  it("should render ValueDisplay spectrum DevString if dataformat is 'SCALAR' and datatype is 'DevString'", () => {
    const store = mockStore({
      messages: {
        tg_test: {
          attributes: {
            attribute1: {
              values: ["test"],
              timestamp: [1675251195.549226]
            }
          }
        }
      }
    });

    const wrapper = mount(
      <Provider store={store}>
        <ValueDisplay
          deviceName={'tg_test'}
          name={'attribute1'}
          dataformat={'SPECTRUM'}
          datatype={'DevString'}
        />
      </Provider>
    );

    expect(wrapper.html()).toContain('ValueDisplay spectrum DevString');
  });
  
  it("should render ValueDisplay spectrum DevEnum if dataformat is 'SCALAR' and datatype is 'DevString'", () => {
    const store = mockStore({
      messages: {
        tg_test: {
          attributes: {
            attribute1: {
              values: ["test"],
              timestamp: [1675251195.549226]
            }
          }
        }
      }
    });

    const wrapper = mount(
      <Provider store={store}>
        <ValueDisplay
          deviceName={'tg_test'}
          name={'attribute1'}
          dataformat={'SPECTRUM'}
          datatype={'DevEnum'}
        />
      </Provider>
    );

    expect(wrapper.html()).toContain('ValueDisplay spectrum DevEnum');
  });

  it("should return ValueDisplay scalar DevShort if dataformat is 'SCALAR' and datatype is 'DevShort'", () => {
    const store = mockStore({
      messages: {
        tg_test: {
          attributes: {
            attribute1: {
              values: ["test"],
              timestamp: [1675251195.549226]
            }
          }
        }
      }
    });

    const wrapper = mount(
      <Provider store={store}>
        <ValueDisplay
          deviceName={'tg_test'}
          name={'attribute1'}
          dataformat={'SCALAR'}
          datatype={'DevShort'}
        />
      </Provider>
    );

    expect(wrapper.html()).toContain('ValueDisplay scalar DevShort');
  });

  it("should return Unsupported encoding '${type} if dataformat is 'SCALAR' and datatype is 'DevEncoded'", () => {
    const store = mockStore({
      messages: {
        tg_test: {
          attributes: {
            attribute1: {
              values: ["test"],
              timestamp: [1675251195.549226]
            }
          }
        }
      }
    });

    const wrapper = mount(
      <Provider store={store}>
        <ValueDisplay
          deviceName={'tg_test'}
          name={'attribute1'}
          dataformat={'SCALAR'}
          datatype={'DevEncoded'}
        />
      </Provider>
    );

    expect(wrapper.html()).toContain('ValueDisplay scalar DevEncoded');
  });
  
  it("should return Invalid JSON if dataformat is 'SCALAR' and datatype is 'DevEncoded'", () => {
    const store = mockStore({
      messages: {
        tg_test: {
          attributes: {
            attribute1: {
              values: [
                ["json", { message: "operation completed successfully" }]
              ],
              timestamp: [1675251195.549226]
            }
          }
        }
      }
    });

    const wrapper = mount(
      <Provider store={store}>
        <ValueDisplay
          deviceName={'tg_test'}
          name={'attribute1'}
          dataformat={'SCALAR'}
          datatype={'DevEncoded'}
        />
      </Provider>
    );

    expect(wrapper.html()).toContain('Invalid JSON');
  });
 
});

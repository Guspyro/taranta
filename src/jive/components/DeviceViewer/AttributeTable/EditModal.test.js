import React from "react";
import { render, cleanup, fireEvent } from "@testing-library/react";
import EditModal from "./EditModal";
import { configure, mount } from 'enzyme';
import Adapter from "@wojtekmaj/enzyme-adapter-react-17";
import '@testing-library/jest-dom';

configure({ adapter: new Adapter() });

afterEach(cleanup);

describe("EditModal component", () => {
  const mockAttribute = {
    name: "attribute",
    datatype: "DevDouble",
    dataformat: "SCALAR",
    writeValue: 0,
    minvalue: 0,
    maxvalue: 100,
  };

  const mockOnClose = jest.fn();
  const mockOnWrite = jest.fn();

  it("calls onClose when close button is clicked", () => {
    const wrapper = mount(
      <EditModal
        attribute={mockAttribute}
        onClose={mockOnClose}
        onWrite={mockOnWrite}
      />
    );
    wrapper.find('.btn-outline-secondary').at(0).simulate('click');
  });

  it("calls onWrite when write button is clicked", () => {
    const wrapper = mount(
      <EditModal
        attribute={mockAttribute}
        onClose={mockOnClose}
        onWrite={mockOnWrite}
      />
    );

    wrapper.find('.btn-primary').at(0).simulate('click');

  });

  it("renders with proper values", () => {
    const attribute = {
      name: "attribute1",
      datatype: "DevDouble",
      writeValue: 10,
      minvalue: 0,
      maxvalue: 100,
      dataformat: "SCALAR",
    };
    const onClose = jest.fn();
    const onWrite = jest.fn();

    const { getByText, getByDisplayValue } = render(
      <EditModal attribute={attribute} onClose={onClose} onWrite={onWrite} />
    );

    expect(getByText("attribute1")).toBeInTheDocument();
    expect(getByDisplayValue("10")).toBeInTheDocument();

    fireEvent.change(getByDisplayValue("10"), {
      target: { value: "20" },
    });
    fireEvent.submit(getByDisplayValue("20").closest("form"));

    expect(onWrite).toHaveBeenCalledWith(20);
  });

  it("renders with proper values", () => {
    const attribute = {
      name: "attribute1",
      datatype: "DevString",
      writeValue: "",
      dataformat: "SCALAR",
    };
    const onClose = jest.fn();
    const onWrite = jest.fn();

    const { getByDisplayValue } = render(
      <EditModal attribute={attribute} onClose={onClose} onWrite={onWrite} />
    );

    fireEvent.change(getByDisplayValue(""), {
      target: { value: "hello" },
    });

    const input = getByDisplayValue("hello");
    expect(input.value).toEqual("hello");
  });

  it("renders with proper values", () => {
    const attribute = {
      name: "attribute1",
      datatype: "DevBoolean",
      writeValue: "",
      dataformat: "SCALAR",
      value: "false"
    };
    const onClose = jest.fn();
    const onWrite = jest.fn();

    const wrapper = mount(
      <EditModal attribute={attribute} onClose={onClose} onWrite={onWrite} />
    );
    
    expect(wrapper.html()).toContain('attribute1');
  });

});

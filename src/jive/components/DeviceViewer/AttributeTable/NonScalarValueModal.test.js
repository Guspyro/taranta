import React from 'react';
import configureStore from "redux-mock-store";
import { Provider } from "react-redux";
import { configure, mount } from 'enzyme';
import Adapter from "@wojtekmaj/enzyme-adapter-react-17";
import { NonScalarValueModal } from './NonScalarValueModal';

configure({ adapter: new Adapter() });

const mockStore = configureStore([]);

describe('NonScalarValueModal component', () => {
  let wrapper;
  let props = {
    attributes: [
      {
        name: 'attribute1',
        dataformat: 'SCALAR',
        displevel: 0
      },
      {
        name: 'attribute2',
        dataformat: 'IMAGE',
        displevel: 0
      },
      {
        name: 'attribute3',
        dataformat: 'SPECTRUM',
        displevel: 0
      }
    ],
    deviceName: 'device1',
    onClose: jest.fn(),
    isLoggedIn: true,
    tangoDB: 'tangoDB',
    datatype: 'DevDouble',
    dataformat: 'SPECTRUM'
  };

  it('renders datatype DevDouble and dataformat SPECTRUM without crashing', () => {

    props.device = 'device1';
    props.attribute = 'spectrum_attr';

    const store = mockStore({
      user: {
        username: 'user1'
      },
      deviceDetail: {
        activeDataFormat: false,
        disabledDisplevels: ['1'],
      },
      messages: {
        device1: {
          attributes: {
            spectrum_attr: {
              values: [[1, 2, 3], [4, 5, 6]]
            }
          }
        }
      }
    });

    wrapper = mount(
      <Provider store={store}>
        <NonScalarValueModal {...props} />
      </Provider>);

    expect(wrapper.html()).toContain('modal-content');
    expect(wrapper.html()).toContain('device1/spectrum_attr');
  });

  it('renders datatype DevDouble and dataformat SPECTRUM without value', () => {

    props.device = 'device1';
    props.attribute = 'spectrum_attr';

    const store = mockStore({
      user: {
        username: 'user1'
      },
      deviceDetail: {
        activeDataFormat: false,
        disabledDisplevels: ['1'],
      },
      messages: {
        device1: {
          attributes: {
            spectrum_attr: {
              values: []
            }
          }
        }
      }
    });

    wrapper = mount(
      <Provider store={store}>
        <NonScalarValueModal {...props} />
      </Provider>);

    expect(wrapper.html()).toContain('modal-content');
    expect(wrapper.html()).toContain('device1/spectrum_attr');
  });

  it('renders datatype DevString and dataformat SPECTRUM without crashing', () => {

    props.dataformat = 'SPECTRUM';
    props.datatype = 'DevString';
    props.device = 'device1';
    props.attribute = 'spectrum_attr';

    const store = mockStore({
      user: {
        username: 'user1'
      },
      deviceDetail: {
        activeDataFormat: false,
        disabledDisplevels: ['1'],
      },
      messages: {
        device1: {
          attributes: {
            spectrum_attr: {
              values: [[1234567]]
            }
          }
        }
      }
    });
    wrapper = mount(
      <Provider store={store}>
        <NonScalarValueModal {...props} />
      </Provider>);

    expect(wrapper.html()).toContain('1234567');
    expect(wrapper.html()).toContain('device1/spectrum_attr');
  });

  it('renders datatype DevString and dataformat IMAGE and closes it', () => {

    props.dataformat = "IMAGE";
    props.datatype = 'DevDouble';
    props.device = 'device1';
    props.attribute = 'image';

    const store = mockStore({
      user: {
        username: 'user1'
      },
      deviceDetail: {
        activeDataFormat: false,
        disabledDisplevels: ['1'],
      },
      messages: {
        device1: {
          attributes: {
            image: {
              values: [[[1, 2, 3], [1, 2, 3], [1, 2, 3]]]
            }
          }
        }
      }
    });

    const useRef = jest.fn(() => ({ current: document.createElement('canvas') }));
    jest.spyOn(React, 'useRef').mockImplementation(useRef);

    wrapper = mount(
      <Provider store={store}>
        <NonScalarValueModal {...props} />
      </Provider>);

    wrapper.find('.btn-primary').at(0).simulate('click');

    expect(wrapper.html()).toContain('device1/image');
  });

});

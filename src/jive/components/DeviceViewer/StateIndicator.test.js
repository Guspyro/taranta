import React from "react";
import { configure, mount } from "enzyme";
import Adapter from "@wojtekmaj/enzyme-adapter-react-17";
import { createStore } from 'redux';
import { MemoStateIndicator } from "./StateIndicator";
import { Provider } from "react-redux";
import attributeReducer from '../../../shared/state/reducers/attributes'; 

configure({ adapter: new Adapter() });

describe("StateIndicator test", () => {

    let mockStoreData = {
        messages: {
            'sys/tg_test/1': {
                attributes: {
                    state: {
                        values: ['RUNNING']
                    }
                }
            }
        }
    }
    
    it("render with RUNNING run", () => {
        const element = React.createElement(MemoStateIndicator, {
            mode: "edit",
            t0: 1,
            deviceName: 'sys/tg_test/1',
        });

        const getWrapper = (mockStore = createStore(attributeReducer, mockStoreData)) => mount(
            <Provider store={mockStore}>
              {element}
            </Provider>
        );
        
        let mockStore = createStore(attributeReducer, mockStoreData)
        mockStore.dispatch = jest.fn();

        const wrapper = getWrapper(mockStore);
        expect(wrapper.html()).toContain('StateIndicator running');    
    });

    it("render with invalid status", () => {
        const element = React.createElement(MemoStateIndicator, {
            mode: "edit",
            t0: 1,
            deviceName: 'sys/tg_test/invalid',
        });

        const getWrapper = (mockStore = createStore(attributeReducer, mockStoreData)) => mount(
            <Provider store={mockStore}>
              {element}
            </Provider>
        );
        
        let mockStore = createStore(attributeReducer, mockStoreData)
        mockStore.dispatch = jest.fn();

        const wrapper = getWrapper(mockStore);
        expect(wrapper.html()).toContain('StateIndicator invalid');    
    });

})
import React from "react";
import { configure, mount } from "enzyme";
import Adapter from "@wojtekmaj/enzyme-adapter-react-17";
import { createStore } from 'redux';
import { Provider } from "react-redux";
import { MemoryRouter } from "react-router";

import rootReducer from '../../../shared/state/reducers/rootReducer';
import DeviceViewer from "./DeviceViewer";

configure({ adapter: new Adapter() });

describe("StateIndicator test", () => {

    let mockStoreData = {
        deviceDetail: {
            disabledDisplevels: []
        },
        deviceList: {
            nameList: [
              'dserver/databaseds/2',
              'sys/database/2',
              'sys/tg_test/1',
            ],
            filter: ''
        },
        database: {
            info: 'TANGO Database sys/database/2\n \nRunning since 2023-01-22 15:09:27\n \nDevices defined  = 8\nDevices exported  = 8\nDevice servers defined  = 4\nDevice servers exported  = 4\n \nDevice properties defined  = 14 [History lgth = 14]\nClass properties defined  = 84 [History lgth = 180]\nDevice attribute properties defined  = 0 [History lgth = 0]\nClass attribute properties defined  = 0 [History lgth = 0]\nObject properties defined  = 0 [History lgth = 0]\n'
        },
        loadingStatus: {
            loadingNames: false,
            loadingDevice: false,
            loadingOutput: {}
        },
        devices: {
            'sys/tg_test/1': {
                connected: true,
                state: 'RUNNING',
                server: {
                    id: 'TangoTest/test',
                    host: 'd654f51b0ac5'
                },
                errors: ['This is test error'],
                name: 'sys/tg_test/1'
            }
        },
        commandOutput: {},
        loggedActions: {},
        error: '',
        user: {
          stateUrls: {
            deviceUrl: '',
            dashboardUrl: ''
          },
          awaitingResponse: false,
          loginFailed: false,
          awaitingNotification: false,
          notificationFailed: false,
          loginDialogVisible: false,
          notifications: []
        },
        attributes: {
            'sys/tg_test/1': {
                state: {
                    name: 'state',
                    label: 'State',
                    dataformat: 'SCALAR',
                    datatype: 'DevState',
                    writable: 'READ',
                    description: 'No description',
                    displevel: 'OPERATOR',
                    minvalue: null,
                    maxvalue: null,
                }
            }
        },
        commands: {
            'sys/tg_test/1': {
                CrashFromDevelopperThread: {
                    name: 'CrashFromDevelopperThread',
                    tag: 0,
                    displevel: 'EXPERT',
                    intype: 'DevVoid',
                    intypedesc: 'Uninitialised',
                    outtype: 'DevVoid',
                    outtypedesc: 'Uninitialised'
                }
            }
        },
        properties: {
            'sys/tg_test/1': {}
        },
        messages: {
            'sys/tg_test/1': {
                attributes: {
                    state: {
                        values: ['RUNNING'],
                        timestamp: [1675270982.900539],
                        quality: ['ATTR_VALID']
                    }
                }
            }
        }
    }
    let deviceName = 'sys/tg_test/1'
    const onRequireDevice = jest.fn();

    
    it("render with server tab", () => {

        const element = React.createElement(DeviceViewer.WrappedComponent, {
            tangoDB:"testdb",
            // selectedTab: "serverinfo",
            deviceName,
            loading: mockStoreData.loadingStatus.loadingDevice,
            device: mockStoreData.devices[deviceName],
            attributes: mockStoreData.attributes[deviceName],
            properties: mockStoreData.properties[deviceName],
            commands: mockStoreData.commands[deviceName],
            errors: mockStoreData.error,
            disabledDisplevels: mockStoreData.disabledDisplevels,
            onRequireDevice: onRequireDevice
        });

        const getWrapper = (mockStore = createStore(rootReducer, mockStoreData)) => mount(
            <MemoryRouter initialEntries={['/testdb/devices/sys/tg_test/1/properties']}>
                <Provider store={mockStore}>
                    {element}
                </Provider>
            </MemoryRouter>
        );
        
        const wrapper = getWrapper();        
        expect(wrapper.html()).toContain("d654f51b0ac5");
        expect(wrapper.html()).toContain("TangoTest/test");
    });

    it("render with properties tab", () => {

        const element = React.createElement(DeviceViewer.WrappedComponent, {
            tangoDB:"testdb",
            selectedTab: "properties",
            deviceName,
            loading: mockStoreData.loadingStatus.loadingDevice,
            device: mockStoreData.devices[deviceName],
            attributes: mockStoreData.attributes[deviceName],
            properties: mockStoreData.properties[deviceName],
            commands: mockStoreData.commands[deviceName],
            errors: mockStoreData.error,
            disabledDisplevels: mockStoreData.disabledDisplevels,
            onRequireDevice: onRequireDevice
        });

        const getWrapper = (mockStore = createStore(rootReducer, mockStoreData)) => mount(
            <MemoryRouter initialEntries={['/testdb/devices/sys/tg_test/1/properties']}>
                <Provider store={mockStore}>
                    {element}
                </Provider>
            </MemoryRouter>
        );
        
        const wrapper = getWrapper();
        expect(wrapper.html()).toContain("There are no properties defined for this device");      
    });

    it("render with commands tab", () => {

        const element = React.createElement(DeviceViewer.WrappedComponent, {
            tangoDB:"testdb",
            selectedTab: "commands",
            deviceName,
            loading: mockStoreData.loadingStatus.loadingDevice,
            device: mockStoreData.devices[deviceName],
            attributes: mockStoreData.attributes[deviceName],
            properties: mockStoreData.properties[deviceName],
            commands: mockStoreData.commands[deviceName],
            errors: mockStoreData.error,
            disabledDisplevels: mockStoreData.disabledDisplevels,
            onRequireDevice: onRequireDevice
        });

        const getWrapper = (mockStore = createStore(rootReducer, mockStoreData)) => mount(
            <MemoryRouter initialEntries={['/testdb/devices/']}>
                <Provider store={mockStore}>
                    {element}
                </Provider>
            </MemoryRouter>
        );
        
        const wrapper = getWrapper();
        expect(wrapper.html()).toContain("CrashFromDevelopperThread");
    });

    it("render with attributes tab", () => {

        const element = React.createElement(DeviceViewer.WrappedComponent, {
            tangoDB:"testdb",
            selectedTab: "attributes",
            deviceName,
            loading: mockStoreData.loadingStatus.loadingDevice,
            device: mockStoreData.devices[deviceName],
            attributes: mockStoreData.attributes[deviceName],
            properties: mockStoreData.properties[deviceName],
            commands: mockStoreData.commands[deviceName],
            errors: mockStoreData.error,
            disabledDisplevels: mockStoreData.disabledDisplevels,
            onRequireDevice: onRequireDevice
        });

        const getWrapper = (mockStore = createStore(rootReducer, mockStoreData)) => mount(
            <MemoryRouter initialEntries={['/testdb/devices/']}>
                <Provider store={mockStore}>
                    {element}
                </Provider>
            </MemoryRouter>
        );
        
        expect(getWrapper().html()).toContain("DevState");
    });

    it("render with errors tab", () => {

        const element = React.createElement(DeviceViewer.WrappedComponent, {
            tangoDB:"testdb",
            selectedTab: "errors",
            deviceName,
            loading: mockStoreData.loadingStatus.loadingDevice,
            device: mockStoreData.devices[deviceName],
            attributes: mockStoreData.attributes[deviceName],
            properties: mockStoreData.properties[deviceName],
            commands: mockStoreData.commands[deviceName],
            errors: mockStoreData.error,
            disabledDisplevels: mockStoreData.disabledDisplevels,
            onRequireDevice: onRequireDevice
        });

        const getWrapper = (mockStore = createStore(rootReducer, mockStoreData)) => mount(
            <MemoryRouter initialEntries={['/testdb/devices/']}>
                <Provider store={mockStore}>
                    {element}
                </Provider>
            </MemoryRouter>
        );
        
        expect(getWrapper().html()).toContain("This is test error");
    });

    it("render with logs tab", () => {

        const element = React.createElement(DeviceViewer.WrappedComponent, {
            tangoDB:"testdb",
            selectedTab: "logs",
            deviceName,
            loading: mockStoreData.loadingStatus.loadingDevice,
            device: mockStoreData.devices[deviceName],
            attributes: mockStoreData.attributes[deviceName],
            properties: mockStoreData.properties[deviceName],
            commands: mockStoreData.commands[deviceName],
            errors: mockStoreData.error,
            disabledDisplevels: mockStoreData.disabledDisplevels,
            onRequireDevice: onRequireDevice
        });

        const getWrapper = (mockStore = createStore(rootReducer, mockStoreData)) => mount(
            <MemoryRouter initialEntries={['/testdb/devices/']}>
                <Provider store={mockStore}>
                    {element}
                </Provider>
            </MemoryRouter>
        );
        
        expect(getWrapper().html()).toContain("Recent user actions on");
    });

    it("render with loading spinner", () => {

        const element = React.createElement(DeviceViewer.WrappedComponent, {
            tangoDB:"testdb",
            selectedTab: "logs",
            deviceName,
            loading: true,
            device: mockStoreData.devices[deviceName],
            attributes: mockStoreData.attributes[deviceName],
            properties: mockStoreData.properties[deviceName],
            commands: mockStoreData.commands[deviceName],
            errors: mockStoreData.error,
            disabledDisplevels: mockStoreData.disabledDisplevels,
            onRequireDevice: onRequireDevice
        });

        const getWrapper = (mockStore = createStore(rootReducer, mockStoreData)) => mount(
            <MemoryRouter initialEntries={['/testdb/devices/']}>
                <Provider store={mockStore}>
                    {element}
                </Provider>
            </MemoryRouter>
        );
        
        expect(getWrapper().html()).toContain("Spinner");
    });

    it("render with loading spinner", () => {

        const element = React.createElement(DeviceViewer.WrappedComponent, {
            tangoDB:"testdb",
            selectedTab: "logs",
            deviceName,
            loading: false,
            device: {},
            attributes: mockStoreData.attributes[deviceName],
            properties: mockStoreData.properties[deviceName],
            commands: mockStoreData.commands[deviceName],
            errors: mockStoreData.error,
            disabledDisplevels: mockStoreData.disabledDisplevels,
            onRequireDevice: onRequireDevice
        });

        const getWrapper = (mockStore = createStore(rootReducer, mockStoreData)) => mount(
            <MemoryRouter initialEntries={['/testdb/devices/']}>
                <Provider store={mockStore}>
                    {element}
                </Provider>
            </MemoryRouter>
        );
        
        expect(getWrapper().html()).toContain("Couldn't load");
    });

})
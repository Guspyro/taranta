import React from "react";
import { configure, mount } from "enzyme";
import Adapter from "@wojtekmaj/enzyme-adapter-react-17";
import { createStore } from 'redux';
import CommandTable from "./CommandTable";
import { Provider } from "react-redux";
import commandReducer from '../../../../shared/state/reducers/commands'; 

configure({ adapter: new Adapter() });

describe("CommandTable test", () => {

    const mockStoreData = {
        deviceDetail: {
            disabledDisplevels: []
        },
        commandOutput: {},
        loadingStatus: {
            loadingNames: false,
            loadingDevice: false,
            loadingOutput: {}
        },
        user: {
            username: 'user1',
        },
        commands: {
            'sys/tg_test/1': {
                DevShort: {
                    name: 'DevShort',
                    tag: 0,
                    displevel: 'OPERATOR',
                    intype: 'DevShort',
                    intypedesc: 'Any DevShort value',
                    outtype: 'DevShort',
                    outtypedesc: 'Echo of the argin value'
                },
                DevString: {
                    name: 'DevString',
                    tag: 0,
                    displevel: 'OPERATOR',
                    intype: 'DevString',
                    intypedesc: '-',
                    outtype: 'DevString',
                    outtypedesc: '-'
                },
                    SwitchStates: {
                    name: 'SwitchStates',
                    tag: 0,
                    displevel: 'OPERATOR',
                    intype: 'DevVoid',
                    intypedesc: 'Uninitialised',
                    outtype: 'DevVoid',
                    outtypedesc: 'Uninitialised'
                }
            }
        }
    }

    it("render with RUNNING run", () => {
        const element = React.createElement(CommandTable, {
            type: '',
            tangoDB:"testdb",
            deviceName: 'sys/tg_test/1',
            commands: Object.values(mockStoreData.commands['sys/tg_test/1'])
        });

        const getWrapper = (mockStore = createStore(commandReducer, mockStoreData)) => mount(
            <Provider store={mockStore}>
              {element}
            </Provider>
        );

        let mockStore = createStore(commandReducer, mockStoreData);
        mockStore.dispatch = jest.fn();
        mockStore.onExecute = jest.fn();

        const wrapper = getWrapper(mockStore);
        expect(wrapper.html()).toContain('DevString');
        expect(wrapper.html()).toContain('SwitchStates');

        expect(wrapper.find('input[placeholder="DevString"]').length).toBe(1);

        wrapper.find('input[placeholder="DevString"]').simulate('change', { target: { value: 'TestInput' } });
        expect(wrapper.html()).toContain('TestInput');
        // console.log(wrapper.find('input[placeholder="DevString"]').parent().find('.btn-sm').length);
        // wrapper.find('input[placeholder="DevString"]').closest('div').find('.btn-sm').simulate('click');
    });
})
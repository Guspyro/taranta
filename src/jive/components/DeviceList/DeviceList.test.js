import React from 'react';
import { render } from '@testing-library/react';
import { Provider } from 'react-redux';
import { createStore } from 'redux';

import DeviceList from './DeviceList';
import { setDeviceFilter } from '../../../shared/state/actions/deviceList';
// import { fetchDeviceNames } from '../../../shared/state/actions/tango';

import { BrowserRouter } from "react-router-dom";
import { getDeviceNames } from '../../../shared/state/selectors/deviceList';

jest.mock('../../../shared/state/actions/deviceList', () => ({
  setDeviceFilter: jest.fn(),
}));
jest.mock('../../../shared/state/actions/tango', () => ({
  getDeviceNames: jest.fn(),
}));
jest.mock('query-string', () => ({
  parse: jest.fn(),
}));

const testDeviceNames = [
  'sys/tg_test/1',
  'sys/tg_test/2',
  'sys/tg_test/3',
  'sys/tg_test/4',
];

const testFilter = 'test';

let loading = false;
let initalState = {
  deviceList: {
    nameList: [
      "dserver/databaseds/2",
      "dserver/starter/8507b18b52c9",
      "dserver/tangoaccesscontrol/1",
      "dserver/tangotest/test",
      "dserver/tarantatestdevice/test",
      "sys/access_control/1",
      "sys/database/2",
      "sys/tg_test/1",
      "tango/admin/8507b18b52c9",
      "test/tarantatestdevice/1"
  ], 
    filter: ''
  },
  loadingStatus: {
      loadingNames: false,
      loadingDevice: false,
      loadingOutput: {}
  }
};

describe('DeviceList', () => {
  it('renders correctly', () => {
    const props = {};
    const { container } = render(
      <Provider store={createStore(() => (initalState))}>
        <BrowserRouter>
        <DeviceList
          tangoDB="test"
          deviceNames={testDeviceNames}
          currentDeviceName={testDeviceNames[0]}
          filter={testFilter}
          loading={loading}
          onRequireDeviceNames={getDeviceNames}
          onSetFilter={setDeviceFilter}
          location={{ search: '' }}
          {...props}
        />
        </BrowserRouter>
      </Provider>
    );

    expect(container.innerHTML).not.toContain('loading');
  });

  it('renders correctly with loading true', () => {

    const { container } = render(
      <Provider store={createStore(() => ({
        deviceList: {
          nameList: [
            "dserver/databaseds/2",
            "dserver/starter/8507b18b52c9",
            "dserver/tangoaccesscontrol/1",
            "dserver/tangotest/test",
            "dserver/tarantatestdevice/test",
            "sys/access_control/1",
            "sys/database/2",
            "sys/tg_test/1",
            "tango/admin/8507b18b52c9",
            "test/tarantatestdevice/1"
        ], 
          filter: ''
        },
        loadingStatus: {
            loadingNames: true,
            loadingDevice: true,
            loadingOutput: {}
        }
      }))}>
        <BrowserRouter>
        <DeviceList
          tangoDB="test"
          deviceNames={testDeviceNames}
          currentDeviceName={testDeviceNames[0]}
          filter={testFilter}
          loading={true}
          onRequireDeviceNames={getDeviceNames}
          onSetFilter={setDeviceFilter}
          location={{ search: '' }}
        />
        </BrowserRouter>
      </Provider>
    );

    expect(container.innerHTML).toContain('loading');
  });

  it('renders correctly with deviceName null', () => {

    const { container } = render(
      <Provider store={createStore(() => ({
        deviceList: {
          nameList: [
            "dserver/databaseds/2",
            "dserver/starter/8507b18b52c9",
            "dserver/tangoaccesscontrol/1",
            "dserver/tangotest/test",
            "dserver/tarantatestdevice/test",
            "sys/access_control/1",
            "sys/database/2",
            "sys/tg_test/1",
            "tango/admin/8507b18b52c9",
            "test/tarantatestdevice/1"
        ], 
          filter: ''
        },
        loadingStatus: {
            loadingNames: true,
            loadingDevice: true,
            loadingOutput: {}
        }
      }))}>
        <BrowserRouter>
        <DeviceList
          tangoDB="test"
          deviceNames={testDeviceNames}
          currentDeviceName={null}
          filter={testFilter}
          loading={true}
          onRequireDeviceNames={getDeviceNames}
          onSetFilter={setDeviceFilter}
          location={{ search: '' }}
        />
        </BrowserRouter>
      </Provider>
    );

    expect(container.innerHTML).toContain('loading');
  });

});
import React from 'react';
import { render, cleanup, fireEvent } from '@testing-library/react';
import { configure, mount } from 'enzyme';
import Adapter from "@wojtekmaj/enzyme-adapter-react-17";
import TreeView from './TreeView';
import '@testing-library/jest-dom';

configure({ adapter: new Adapter() });

afterEach(cleanup);

const data = {
  sys: {
    "access_control": {
      "1": {}
    },
    "database": {
      "2": {}
    },
    "tg_test": {
      "1": {}
    }
  }
};

const renderLeaf = (path) => <span>{path.join('/')}</span>;

describe('TreeView', () => {
  it('renders correctly', () => {
    const { container } = render(
      <TreeView data={data} expansion={{}} onChangeExpansion={jest.fn()} renderLeaf={renderLeaf} />
    );
    expect(container.innerHTML).toContain('sys');
  });


  it('expands and collapses items correctly', () => {

    const expansion = {
      sys: [
        true,
        {
          tg_test: [
            true,
            {
              1: [
                true,
                {}
              ]
            }
          ]
        }
      ]
    }
    const onChangeExpansion = jest.fn();

    const { getByText, queryByText } = render(
      <TreeView data={data} expansion={expansion} onChangeExpansion={onChangeExpansion} renderLeaf={renderLeaf} />
    );

    fireEvent.click(getByText('sys'));

    expect(onChangeExpansion).toHaveBeenCalled();
    expect(getByText('sys/tg_test/1')).toBeTruthy();

    fireEvent.click(getByText('sys'));
    expect(queryByText('sys/tg_test')).toBeNull();
  });

  test("clicking an item should toggle its expansion state", () => {
    const data = { a: {}, b: { c: {}, d: {} } };
    const expansion = { a: [false, {}], b: [false, { c: [false, {}], d: [false, {}] }] };
    const onChangeExpansion = jest.fn();

    const wrapper = mount(
      <TreeView data={data} expansion={expansion} renderLeaf={path => <div>{path.join("/")}</div>} onChangeExpansion={onChangeExpansion} />
    );

    wrapper.find("li").at(1).simulate("click");
    expect(onChangeExpansion.mock.calls.length).toBe(1);
    expect(onChangeExpansion.mock.calls[0][0]).toEqual({ a: [false, {}], b: [true, { c: [false, {}], d: [false, {}] }] });
  });

  test("expandAll prop should expand all items", () => {
    const data = { a: { b: {} }, c: { d: {} } };
    const expansion = { a: [false, { b: [false, {}] }], c: [false, { d: [false, {}] }] };

    const wrapper = mount(
      <TreeView data={data} expansion={expansion} renderLeaf={path => <div>{path.join("/")}</div>} onChangeExpansion={() => { }} expandAll />
    );

    expect(wrapper.find("li").at(0).find("ul").length).toBe(1);
  });

});

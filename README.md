# Description

With this device explorer built on TangoGQL, you can:

1. View a list of all Tango devices
2. View and modify device properties
3. View and modify device attributes
4. View and execute device commands
5. Create web interfaces for interacting with Tango devices (on /&lt;tangoDB&gt;/dashboard)

# Usage

1. Clone the repository.
2. Run `npm install`.
3. Type `npm start`.

Minimum node version: 7.6 (introduced async/wait)

Verified working node version: 9.11.2 (currently used by the dockerfile)

# Online demo

https://taranta-demo.maxiv.lu.se/ (log in with demo/demo)

# For developers

* [How to create a widget](Widgets.md)
* building docs: `make build-docs`
* search for a string on the code: `make search-string STR='hello world'`

# Authors

taranta was written by the KITS Group at MAX IV Laboratory .
